<?php 
 $background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
 $background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
 $background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
 $background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
 $background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
 $background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
 ?>
<!--=================================
 banner -->
<section id="home-slider" class="fullscreen">
	<?php 
  //echo "<pre>";print_r($sliders);die;
	$img;$text;
	if(!empty($sliders[2]['id'])){
		$img=$sliders[2]['image'];
		$text=$sliders[2]['title'];
		$t=$sliders[1]['title'].",".$sliders[3]['title'];
	}
	?>
	<div class="banner bg-1 d-flex align-items-center" style="background-image: url(<?php echo base_url('Newassets/images/'.$img);?>);" data-gradient-red="4">
		<div class="container">
			<div class="row text-center">
				<div class="col-sm-12 text-white">
					<h1 class="animated3 text-white mb-2"><?php echo $text;?></h1>
				</div>
			</div>
		</div>
	</div>
</section>
<!--=================================
 banner -->
 <!--=================================
 Page Section -->
<section class="page-section-ptb">
    <div class="container">
        <div class="row justify-content-center">
			<?php
			foreach($our_history as $history)
			{
				if(!empty($history['id'])){
					?>
					<div class="col-md-8 text-center mb-5 xs-mb-3">
						<h2 class="title divider mb-3"><?php echo $history['title']; ?></h2>
						<p><?php echo $history['contain'];  ?></p>
					</div>
					<?php
				}
			}
			?>
		</div>
        <?php 
		foreach($our_history_inner as $history_inner){
			if(!empty($history_inner['id'])){
				echo $history_inner['contain'];
			}
		}	
		?>
    </div>
</section>
<!--=================================
Counter -->
<section class="page-section-pb">
	<div class="container">
		<div class="row">
			<div class="col-sm-3  d-flex justify-content-center">
				<div class="counter left_pos">
					<i class="glyph-icon flaticon-people-2"></i>
					<span id="total_users" class="timer" data-to="<?php echo $total_users; ?>" data-speed="10000">1600</span><label><?php echo lang('lbl_home_total_members'); ?></label>
				</div>
			</div>    
			<div class="col-sm-3  d-flex justify-content-center">
				<div class="counter left_pos">
					<i class="glyph-icon flaticon-favorite"></i>
					<span id="online_users" class="timer" data-to="<?php echo $online_users; ?>" data-speed="10000">750</span><label><?php echo lang('lbl_home_online_members'); ?></label>
				</div>
			</div>
			<div class="col-sm-3  d-flex justify-content-center">
				<div class="counter left_pos">
					<i class="glyph-icon flaticon-charity"></i>
					<span id="male_users" class="timer" data-to="<?php echo $male_users; ?>" data-speed="10000">380</span><label><?php echo lang('lbl_home_men_online'); ?></label>
				</div>
			</div>
			<div class="col-sm-3  d-flex justify-content-center">
				<div class="counter left_pos">
					<i class="glyph-icon flaticon-candelabra"></i>
					<span id="female_users" class="timer" data-to="<?php echo $female_users; ?>" data-speed="10000">370</span><label><?php echo lang('lbl_home_women_online'); ?></label>
				</div>
			</div>
		</div>
	</div>
</section>
<!--=================================
video -->
<section class="page-section-ptb text-white bg-overlay-black-70 bg text-center" style="background: url(<?php echo $background3['background_url']; ?>) no-repeat 0 0; background-size: cover;">
	<div class="container">
		<?php if(!empty($blocks[4]['id'])){?>
			<div class="row justify-content-center mb-3">
				<div class="col-md-10">
					<h2 class="title divider mb-3"><?php echo $blocks[4]['title']; ?></h2>
					<p class="lead"><?php echo $blocks[4]['contain']; ?></p>
				</div>
			</div>
		<?php } ?>
		<div class="row">
			<div class="col-sm-12">
				<h5 class="title mb-3"><?php echo lang('lbl_home_see_video'); ?></h5>
				<div class="popup-gallery">
					<a href="<?php echo $header[12]['status']; ?>" class="play-btn popup-youtube"> <span><i class="glyph-icon flaticon-play-button"></i></span></a>
				</div>
			</div>
		</div>
	</div>
</section>
<!--=================================
video -->
<!--=================================
 timeline-section -->
<section class="page-section-ptb text-center">
    <div class="container">
		<?php if(!empty($blocks[0]['id'])){?>
			<div class="row justify-content-center mb-5 xs-mb-3">
				<div class="col-md-8">
					<h2 class="title divider-2 mb-3"><?php echo $blocks[0]['title']; ?></h2>
					<p class="lead xs-mt-3"><?php echo $blocks[0]['contain']; ?></p>
				</div>
			</div>
		<?php } ?>
		 <div class="row">
			 <?php if(!empty($blocks[1]['id'])){?>
				<div class="col-sm-4 xs-mb-3">
					<div class="timeline-badge mb-2"><img class="img-center" src="<?php echo base_url('images/timeline/01.png'); ?>" alt=""/></div>
					<h4 class="title divider-3 mb-3"><?php echo $blocks[1]['title']; ?></h4>
					<p><?php echo $blocks[1]['contain']; ?></p>
				</div>
			 <?php } ?>
			  <?php if(!empty($blocks[2]['id'])){?>
					<div class="col-sm-4 xs-mb-3">
						<div class="timeline-badge mb-2"><img class="img-center" src="<?php echo base_url('images/timeline/02.png'); ?>" alt=""/></div>
						<h4 class="title divider-3 mb-3"><?php echo $blocks[2]['title']; ?></h4>
						<p><?php echo $blocks[2]['contain']; ?></p>
					</div>
			  <?php } ?>
			   <?php if(!empty($blocks[3]['id'])){?>
					<div class="col-sm-4">
						<div class="timeline-badge mb-2"><img class="img-center" src="<?php echo base_url('images/timeline/03.png'); ?>" alt=""/></div>
						<h4 class="title divider-3 mb-3"><?php echo $blocks[3]['title']; ?></h4>
						<p><?php echo $blocks[3]['contain']; ?></p>
					</div>
			 <?php } ?>
		</div>
    </div>
</section>
<!--=================================
 timeline-section -->
<!--=================================
 our-app  -->
<section class="page-section-ptb pb-0 text-white text-center our-app our-app-2 pos-r overflow-h" style="background: url(<?php echo $background4['background_url']; ?>) no-repeat 0 0; background-size: cover;">
	<div class="timeline-inner" style="background: url('<?php echo base_url(); ?>images/app-img.png');">
		<div class="container-fluid">
			<div class="row justify-content-center mb-5 xs-mb-3">
				<div class="col-md-10">
					<h2 class="title divider-2"><?php echo lang('lbl_home_download_our_app'); ?></h2>
				</div>
			</div>
			<div class="row mt-5 xs-mt-3">
				<div class="col-sm-12">
					<h2 class="title mb-3"><?php echo lang('lbl_home_want_at_find_match'); ?> <br><?php echo lang('lbl_home_get_our_app_now'); ?></h2>
					<a class="button btn-dark btn-md  full-rounded"><i class="fa fa-apple" aria-hidden="true"></i><span><span><?php echo lang('lbl_home_available_on_the'); ?></span> <?php echo lang('lbl_home_app_store'); ?></span></a>
					<a class="button btn-lg full-rounded white-bg text-black"><i class="glyph-icon flaticon-play-button"></i><span><span><?php echo lang('lbl_home_get_it_on'); ?></span><?php echo lang('lbl_home_google_play'); ?></span></a>
					<div class="counter mt-3">
						<span class="timer text-orange" data-to="1600" data-speed="10000">1600</span><label><?php echo lang('lbl_home_download');?></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--=================================
 our-app  -->
    <!--=================================
Blogs -->
<section class="page-section-ptb grey-bg">
	<div class="container">
	<?php if(!empty($blocks[5]['id'])){?>
		<div class="row justify-content-center mb-5 xs-mb-3">
			<div class="col-md-10 text-center">
				<h2 class="title divider-2 mb-3"><?php echo $blocks[5]['title']; ?></h2>
				<p class="lead"><?php echo $blocks[5]['contain']; ?></p>
			</div>
		</div>
		<?php } ?>
		<div class="row post-article" id="ajax_table">  
			<?php
			$cnt=1;$b_id;
			foreach($blogs as $blog)
			{	 
				$b_id=$blog['id'];
				if(!empty($blog['id']))
				{
					if($cnt<=3)
					{ 
						?>
						<div class="col-lg-4 col-md-4 d-flex">
							<div class="post post-artical top-pos">
								<div class="post-image clearfix">
									<?php
									if(!empty($blog['image']))
									{
										$str=base_url("Newassets/images/thumbnail/".$blog['image']);
										if(file_exists(DIRECTORY_PATH."Newassets/images/thumbnail/".$blog['image']))
										{      
											?>
											<img class="img-fluid" src="<?php echo $str; ?>" alt="">
											<?php
										}
										else
										{
											?>
											<img class="img-fluid" src="<?php echo base_url('images/blog/01.jpg');?>" alt="">
											<?php
										}       
									}
									else
									{
										?><img class="img-fluid" src="<?php echo base_url('images/blog/01.jpg');?>" alt="">
										<?php
									}
									?>
								</div>
								<div class="post-details">
									<div class="post-date"><?php echo date_format(date_create($blog['created_date']),"d"); ?><span class="text-black"><?php echo date_format(date_create($blog['created_date']),"M"); ?></span></div>
									<div class="post-meta">
										<a href="#"><i class="fa fa-user"></i> <?php echo $blog['author']; ?></a>
										<a href="#"><i aria-hidden="true" class="fa fa-heart-o"></i>98 Like</a>
										<a href="#"><i class="fa fa-comments-o last-child"></i>Comments</a>
									</div>
									<div class="post-title"><h5 class="title text-uppercase"><a href="<?php if($this->session->userdata('logged_in'))  echo base_url('blog/blog_details/'.$blog['id']); else "#";?>"><?php echo $blog['title']; ?></a></h5></div>
									<div class="post-content"><p><?php echo substr($blog['description'], 0, 100); ?></p></div>
									<a class="button" href="<?php if($this->session->userdata('logged_in'))  echo base_url('blog/blog_details/'.$blog['id']); else "#";?>"><?php echo lang('lbl_home_read_more');?></a>
								</div>                
							</div>  
						</div>
					   <?php
					}
					$cnt++;
				}
			}
			?>
		</div>
		<div class="row mt-5 xs-mt-3">
			<div class="col-sm-12 text-center">
				<button class="button  btn-lg btn-theme full-rounded animated right-icn" id="load_more" data-val="0" data-type="<?php echo $b_id;?>"><span><?php echo lang('lbl_home_show_more');?><i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span>
					<img style="display: none" id="loader" src="<?php echo str_replace('index.php','',base_url('Newassets/images/loader.GIF')); ?>">
				</button>
			</div>
		</div>
	 </div>
</section>
<!--=================================
Blogs -->        
<!--=================================
Testimonials -->
<section class="page-section-ptb pb-130 xs-pb-60 dark-bg bg fixed bg-overlay-black-80 text-white" style="background: url(<?php echo $background6['background_url']; ?>) no-repeat 0 0; background-size: cover;">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10 text-center">
				<h2 class="title divider"><?php echo lang('lbl_home_testimonials'); ?></h2>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-md-10">
				<div class="owl-carousel" data-nav-dots="true" data-items="1" data-md-items="1" data-sm-items="1">
				 <?php
					foreach($testimonials as $testomonial)
					{
						if(!empty($testomonial['id'])){
						?>
						<div class="item">
							<div class="testimonial clean">
								<div class="testimonial-avatar">
									<?php
									if(!empty($testomonial['author_image']))
									{
										$str=base_url("Newassets/images/thumbnail/".$testomonial['author_image']);
										if(file_exists(DIRECTORY_PATH."Newassets/images/thumbnail/".$testomonial['author_image']))
										{						
											?>
											<img src="<?php echo $str; ?>" alt=""/>
											<?php
										}
										else
										{
											?>
											<img src="<?php echo base_url('Newassets/images/thumbnail/thum-0.png'); ?>" alt="">
											<?php
										}							
									}
									else
									{
										?><img src="<?php echo base_url('Newassets/images/thumbnail/thum-0.png'); ?>" alt="">
										<?php
									}
									?>
								</div>
								<div class="testimonial-info"><p><?php echo $testomonial['description']; ?></p></div>
								<div class="author-info"> <strong><?php echo $testomonial['author']; ?></strong> </div>
							</div>
						</div>
						<?php
						}
					}?>
				</div>
			</div>
		</div>
	</div>
</section>
<!--=================================
story-slider-->
<section class="page-section-ptb grey-bg story-slider"> 
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10 text-center">
				<h2 class="title divider"><?php echo lang('lbl_home_they_found_true_love'); ?></h2>
			</div>
		</div>
	</div>
	<div class="owl-carousel" data-nav-dots="true" data-items="5" data-md-items="4" data-sm-items="2" data-xs-items="1" data-space="30">
		<?php
		foreach($stories as $story)
		{
			if(!empty($story['id'])){
			?>
				<div class="item">
					<div class="story-item">
						<div class="story-image clearfix">
							<?php
							if(!empty($story['image']))
							{
								$str=base_url("Newassets/images/thumbnail/".$story['image']);
								if(file_exists(DIRECTORY_PATH."Newassets/images/thumbnail/".$story['image']))
								{						
									?>
									<img class="img-fluid" src="<?php echo $str; ?>" alt="" ><!-- style="height:155px;width:300px;" -->
									<?php
								}
								else
								{
									?>
									<img class="img-fluid" src="<?php echo base_url('images/story/01.jpg'); ?>" alt="">
									<?php
								}							
							}
							else
							{
								?><img class="img-fluid" src="<?php echo base_url('images/story/01.jpg'); ?>" alt="">
								<?php
							}
							?>
							<div class="story-link">
								<a href="<?php echo base_url('stories/stories_details/'.$story['id']);?>">
									<i class="glyph-icon flaticon-add"></i>
								</a>
							</div>				
						</div>
						<div class="story-details text-center">
							<div class="about-des"><?php echo $story['excerpt']; ?>
							</div>
						</div>
						<div class="yellow-bg story-text ptb-20 text-white text-center">
							<h5 class="title text-uppercase"><?php echo $story['title']; ?></h5>
						</div> 
					</div>
				</div>
			<?php
			}
		}
		?>
	</div>
</section>
<!--=================================
story-slider-->
<!--=================================
 header -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#load_more").click(function(e){
            e.preventDefault();
			var page = $(this).data('val');
			var blog_id = $(this).data('type');
			getblog(page,blog_id);
        });
    });
    var getblog = function(page,blog_id){
        $("#loader").show();
        $.ajax({
            url:"<?php echo base_url() ?>user/getblog",
            type:'GET',
            data: {page:page,blog_id:blog_id}
        }).done(function(response){
            $("#ajax_table").append(response);
            $("#loader").hide();
            $('#load_more').data('val', ($('#load_more').data('val')+1));
            scroll();
			$("#load_more").hide();
        });
    };
    var scroll  = function(){
        $('html, body').animate({
            scrollTop: $('#load_more').offset().top
        }, 1000);
    };
</script>