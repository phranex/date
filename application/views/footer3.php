		<!--footer3 section-->
		<?php 
		$lange= $this->admin_model->get_default_language();
		$language=$lange['status'];
		$languages = $this->session->userdata('site_lang');
		$lg=$languages;
		?>
		<!--Subscribe section-->
		<?php
		foreach($homepage3_subscribe as $homepage3_sub){
			if(!empty($homepage3_sub['id'])){
				echo $homepage3_sub['contain'];
			}
		}
		?>
		<?php
		foreach($footer_contact_us as $contact_us){
			if(!empty($contact_us['id'])){
				echo $contact_us['contain'];
			}
		}
		?>
		<!--======footer -->
		<footer class="text-white text-center">
			<div class="footer-widget">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-8">
							<div class="footer-logo mb-2">
							<?php
									if(!empty($header[2]['status']))
									{
										$str=base_url("Newassets/images/".$header[2]['status']);
										if(file_exists(DIRECTORY_PATH."Newassets/images/".$header[2]['status']))
										{						
											?>
											<img class="img-center" src="<?php echo $str; ?>" alt="">
											<?php
										}
										else
										{
											?>
											<img class="img-center"  src="images/thumbnail/01.jpg" alt="">
											<?php
										}							
									}
									else
									{
										?><img class="img-center"  src="images/thumbnail/01.jpg" alt="">
										<?php
									}
									?>
							</div>
							<div class="social-icons color-hover mb-1">
								<ul>
									<li class="social-facebook"><a href="<?php echo $header[6]['status']; ?>"><i class="fa fa-facebook"></i></a></li>
									<li class="social-twitter"><a href="<?php echo $header[7]['status']; ?>"><i class="fa fa-twitter"></i></a></li>
									<li class="social-dribbble"><a href="<?php echo $header[8]['status']; ?>"><i class="fa fa-dribbble"></i></a></li>
									<li class="social-gplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
									<li class="social-youtube"><a href="#"><i class="fa fa-youtube"></i></a></li>
								</ul>
							</div>
							<p class="text-white"><?php echo $header[10]['status']; ?> </p>
						</div>
					</div>
				</div>
			  </div>
		</footer> 
		<?php $this->load->view('user/_crop'); ?>	
		<!--=================================
		footer -->
		<div id="back-to-top"><a class="top arrow" href="#top"><i class="fa fa-level-up"></i></a></div>
			<?php $logged_in_user = $this->session->userdata('logged_in'); ?>
			<?php
			$this->XMPP_ENABLE = $this->Common_model->get_key_configuration(array('key'=>'XMPP_ENABLE'));
			$this->XMPP_HOST = $this->Common_model->get_key_configuration(array('key'=>'XMPP_HOST'));
			$this->XMPP_SERVER = $this->Common_model->get_key_configuration(array('key'=>'XMPP_SERVER'));
			$this->XMPP_HTTP_BIND = 'http://'.$this->XMPP_SERVER.':5280/http-bind';
			$this->XMPP_DEFAULT_PASSWORD = $this->Common_model->get_key_configuration(array('key'=>'XMPP_DEFAULT_PASSWORD'));
			$this->FACEBOOK_KEY = $this->Common_model->get_key_configuration(array('key'=>'FACEBOOK_KEY'));
			if($this->XMPP_ENABLE == 'true')
			{
				if($logged_in_user) 
				{ ?>
				<link rel="stylesheet" type="text/css" media="screen" href="https://cdn.conversejs.org/css/converse.min.css">
				<script src="https://cdn.conversejs.org/dist/converse.min.js"></script>
			<?php } 
			} ?>
			<!--   Core JS Files   -->
			<script>
			$(document).on('click','.imgupload1',function(){

				var position=$(this).attr('id');
				$('.avatar-position').val('');
				$('.avatar-position').val(position);
				$(".avatar-view").click();					 
			});

			$(document).on('click','.imgupload2',function(){
				var position=$(this).attr('id');
				$('.avatar-position').val('');
				$('.avatar-position').val(position);
				$(".avatar-view2").click();						 
			});

			$(document).on('click','.imgupload3',function(){
				var position=$(this).attr('id');
				$('.avatar-position').val('');
				$('.avatar-position').val(position);
				$(".avatar-view3").click();						 
			});

			$(document).on('click','.imgupload4',function(){
				var position=$(this).attr('id');
				$('.avatar-position').val('');
				$('.avatar-position').val(position);
				$(".avatar-view4").click();						 
			});

			$(document).on('click','.imgupload5',function(){
				var position=$(this).attr('id');
				$('.avatar-position').val('');
				$('.avatar-position').val(position);
				$(".avatar-view5").click();						 
			});

			$(document).on('click','.imgupload6',function(){
				var position=$(this).attr('id');
				$('.avatar-position').val('');
				$('.avatar-position').val(position);
				$(".avatar-view6").click();						 
			});

			$(document).on('click','.remove-bg',function(){
				var position = $(this).attr('data-position');
				$(this).remove();
				if(position == '2'){
					var imgpath = "<?php echo base_url('images/step/02.png');?>";
					$('.imgupload'+position).attr('src',+imgpath);
				}
				if(position == '3'){
					var imgpath = "<?php echo base_url('images/step/03.png');?>";
					$('.imgupload'+position).attr('src',+imgpath);
				}
				if(position == '4'){
					var imgpath = "<?php echo base_url('images/step/04.png');?>";
					$('.imgupload'+position).attr('src',+imgpath);
				}
				if(position == '5'){
					var imgpath = "<?php echo base_url('images/step/05.png');?>";
					$('.imgupload'+position).attr('src',+imgpath);
				}
				if(position == '6'){
					var imgpath = "<?php echo base_url('images/step/06.png');?>";
					$('.imgupload'+position).attr('src',+imgpath);
				}
				$.ajax({
					url:'<?php echo base_url(); ?>user/ajax_remove_gallery_image',
					type:'POST',
					data:{ 
						'position' : position,
						},
					success: function(response){
						var data = $.parseJSON(response);
						$('.imgupload'+position).attr('src',data['url']);
						
					},
				});					 
			});
			</script>
			<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
			<script src="<?php  echo base_url("Newassets/js/bootstrap-notify.js"); ?>"  type="text/javascript"></script>
			<script src="<?php  echo base_url("Newassets/js/demo.js"); ?>"  type="text/javascript"></script>
			<script src="<?php  echo base_url("Newassets/js/bootstrap-select.js"); ?>"  type="text/javascript"></script>
			<script src="<?php  echo base_url("Newassets/js/bootstrap-select.min.js"); ?>"  type="text/javascript"></script>
			<script src="<?php  echo base_url("Newassets/js/magnific-popup/jquery.magnific-popup.min.js"); ?>"  type="text/javascript"></script>
			<script src="<?php  echo base_url("Newassets/js/chartist.min.js"); ?>"></script>
			<script src="<?php  echo base_url("Newassets/js/counter/jquery.countTo.js"); ?>"  type="text/javascript"></script>
			<script src='https://www.google.com/recaptcha/api.js'></script>
			<script src="<?php  echo base_url("Newassets/js/light-bootstrap-dashboard.js"); ?>"  type="text/javascript"></script>
			<script src="<?php  echo base_url("Newassets/js/owl.carousel.min.js"); ?>"  type="text/javascript"></script>
			<script src="<?php  echo base_url("Newassets/js/custom.js"); ?>"></script>
			<script src="<?php  echo base_url("Newassets/js/jquery.appear.js"); ?>" type="text/javascript"></script>
			<script src="<?php  echo base_url("Newassets/js/mega-menu/mega_menu.js"); ?>" type="text/javascript"></script>
			<script  type="text/javascript">
					$(document).ready(function(){
						$(".imgs").change(function(){
							this.form.toString();
							$(location).attr("href","<?php echo base_url("user/edit_gallery_image"); ?>");
						});
					});
			</script>
			<script type="text/javascript">
	$( ".sortable" ).sortable({
		axis: 'x',
		containment: "parent" 
	});
	$( ".sortable" ).sortable({
			update: function( event, ui ) {
				var strItems = "";
				$("#sortable").children().each(function (i) {
					var li = $(this);
					strItems += li.attr("id")+",";
				});
				var strItems = strItems.substring(0, strItems.length-1);
				$("#datepref").val(strItems);
			}
		});
	</script>
		<script>
		  window.fbAsyncInit = function() {
			FB.init({
			  appId: '<?php echo $this->FACEBOOK_KEY;  ?>',
			  cookie: true, // This is important, it's not enabled by default
			  version: 'v2.2'
			});
		  };
		  (function(d, s, id){
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		  }(document, 'script', 'facebook-jssdk'));
		   function fb_login() {
				console.log("fb_login");
				FB.login(
				 function (response) {
					 if (response.authResponse) {
						 console.log('Welcome!  Fetching your information.... ');
						 FB.api('/me', { fields: 'name, email,gender,picture' } , function (response) {
							 $.post("<?php echo base_url("user/loginfb"); ?>",{"name": response.name,
																	"fbid":response.id,
																	"email" : response.email,
																	"gender" : response.gender,
																	},function(data){
															if(data.indexOf("") != -1/*data==''*/)
															{
																$(location).attr("href","<?php echo base_url("user/registerfb"); ?>");
																 location.reload();
															}else if(data.indexOf("Login") != -1/*data=='Login'*/){
															  $(location).attr("href","<?php echo base_url("user/my_profile"); ?>");
															   location.reload();
															}
															else
															{
																$('#error').text(data);
															}
							 });
						 });
					 } else{
						 console.log('User cancelled login or did not fully authorize.');
					 }
				 });
			}
		$.ajax({
			url: "<?php echo site_url("user/get_all_session_data")?>", 
			success: function(response){
       			var obj = jQuery.parseJSON( response );
       			$("#total_users").attr("data-to",obj.total_users);
       			$("#online_users").attr("data-to",obj.total_user_online);
       			$("#male_users").attr("data-to",obj.men_onlie);
       			$("#female_users").attr("data-to",obj.female_online);
       			$("#total_users").html(obj.total_users);
       			$("#online_users").html(obj.total_user_online);
       			$("#male_users").html(obj.men_onlie);
       			$("#female_users").html(obj.female_online);
    		}
    	});
   $(document).ready(function(){
		$(".alert").delay(6000).hide(0, function() {
		});
   });
		</script> 
			<!--add if user is login -->
		<?php $logged_in_user = $this->session->userdata('logged_in'); ?>
		<?php
			if($this->XMPP_ENABLE == 'true')
			{
				if($logged_in_user) 
				{ ?>
					<script>
						converse.initialize({
						bosh_service_url: '<?php echo $this->XMPP_HTTP_BIND; ?>', // Please use this connection manager only for testing purposes
						show_controlbox_by_default: true,
						auto_login:true,
						jid:'<?php echo $this->session->userdata['user_id']; ?>_<?php echo $this->session->userdata['fname']; ?>@<?php echo $this->XMPP_SERVER; ?>',
						password:'<?php echo $this->XMPP_DEFAULT_PASSWORD; ?>',
						allow_registration:false,
						allow_contact_requests:false,
						allow_contact_removal:false,
						allow_logout: false,
						allow_muc:false,
						auto_subscribe: true,
						allow_chat_pending_contacts:true,
						hide_muc_server:true,
					});	
					</script>
					<?php 
				} 
			}
		 ?>
		<!--end -->
	</body>
</html>