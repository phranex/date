<!DOCTYPE html>
<html lang="en" <?php if($this->Common_model->is_rtl()){ echo "dir='rtl'";}?>>
	<head>
	    <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Cupid Love - Dating HTML5 Template" />
		<meta name="author" content="potenzaglobalsolutions.com" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<title>Cupid Love Web</title>
		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo base_url('images/favicon.ico');?>"/>		 
		<style>
		#preloader {background: #343434!important;bottom: 0;height: 100%;left: 0;overflow: hidden!important;position: fixed;right: 0;top: 0;width: 100%;z-index: 99999;text-align: center;}
		.loading-effect {width: 350px;height: 350px;}
		.clear-loading {text-align: center;position: absolute;top: 50%;left: 50%;margin-left: -152px;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;-ms-box-sizing: border-box;-o-box-sizing: border-box;display: inline-block;text-align: center;transform: translateY(-50%);-webkit-transform: translateY(-50%);-o-transform: translateY(-50%);-moz-transform: translateY(-50%);-ms-transform: translateY(-50%);}
		.loading-effect img {width: 300px;}
		</style>
			
	</head>
	<body id="main" <?php if($this->Common_model->is_rtl()){ echo "dir='rtl'";}?>>
		<div id="preloader">
			<div class="clear-loading loading-effect"><img src="<?php echo base_url('images/loading.gif');?>" alt=""></div>
		</div>
		<header id="header" class="dark">
			<div class="topbar">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<div class="topbar-left">
								<ul class="list-inline">
									<li><a href="mailto:support@website.com"><i class="fa fa-envelope-o"> </i> support@website.com </a></li>
									<li><a href="tel:(007)1234567890"><i class="fa fa-phone"></i> (007) 123 456 7890 </a></li>  
								</ul>
								<ul class="list-inline social-icons color-hover visible-xs d-none">
									<li class="social-facebook"><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>   
									<li class="social-twitter"><a href="https://twitter.com/login"><i class="fa fa-twitter"></i></a></li>   
									<li class="social-instagram"><a href="https://www.instagram.com/?hl=en"><i class="fa fa-instagram"></i></a></li>   
								</ul>
							</div>
						</div>
						<div class="col-lg-6 col-md-6">
							<div class="topbar-right text-right">
								<ul class="list-inline social-icons color-hover hidden-xs">
									<li class="social-facebook"><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>   
									<li class="social-twitter"><a href="https://twitter.com/login"><i class="fa fa-twitter"></i></a></li>   
									<li class="social-instagram"><a href="https://www.instagram.com/?hl=en"><i class="fa fa-instagram"></i></a></li>   
								</ul>
								<ul class="list-inline text-uppercase top-menu">
									<?php
									if(!$this->session->userdata('logged_in'))
									{
										?>
										<li><a href="<?php echo base_url('register');?>"><?php echo lang('lbl_menu_name_register'); ?></a></li>
										<li><a href="<?php echo base_url('login');?>"><?php echo lang('lbl_menu_name_login'); ?></a></li>  
										<?php			 
									}
									?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--=================================
			mega menu -->
			<div class="menu">  
			<!-- menu start -->
				<nav id="menu" class="mega-menu">
				<!-- menu list items container -->
					<section class="menu-list-items Newclass">
						<div class="container"> 
							<div class="row"> 
								<div class="col-lg-12 col-md-12"> 
									<!-- menu logo -->
									<ul class="menu-logo">
										<li>
										<a href="<?php echo base_url();?>"><img src="<?php echo base_url('images/logo.png');?>" alt="logo"> </a>
										</li>
									</ul>
									<!-- menu links -->
									<ul class="menu-links">
										<!-- active class -->
										<li><a href="<?php echo base_url(); ?>"><?php echo lang('lbl_menu_name_home'); ?></a></li>
										<li><a href="javascript:void(0)"><?php echo lang('lbl_menu_name_profiles'); ?> <i class="fa fa-angle-down fa-indicator"></i></a>
										<!-- drop down multilevel  -->
										<ul class="drop-down-multilevel left-menu">
											<li><a href="<?php echo base_url('user/dashboard'); ?>"><?php echo lang('lbl_sidebar_dashboard'); ?></a></li>
											<li><a href="<?php echo base_url('user/my_profile'); ?>"><?php echo lang('lbl_menu_name_my_profile'); ?></a></li>
											<li><a href="<?php echo base_url('user/my_preferance'); ?>"><?php echo lang('lbl_menu_name_my_preferances'); ?></a></li>
											<li><a href="<?php echo base_url('user/location'); ?>"><?php echo lang('lbl_location'); ?></a></li>
											<li><a href="<?php echo base_url('friends'); ?>"><?php echo lang('lbl_sidebar_friends'); ?></a></li>
											<li><a href="<?php echo base_url('chat'); ?>"><?php echo lang('lbl_chat'); ?></a></li>											
											<li><a href="<?php echo base_url('user/gallery'); ?>"><?php echo lang('lbl_sidebar_gallery'); ?></a></li>
										</ul>
										</li>
										<li><a href="<?php echo base_url('stories'); ?>"><?php echo lang('lbl_menu_name_stories'); ?></a></li>
										<li><a href="<?php echo base_url('about'); ?>"><?php echo lang('lbl_menu_name_about'); ?></a></li>
										<li><a href="<?php echo base_url('team'); ?>"><?php echo lang('lbl_menu_name_team'); ?></a></li>
										<li><a href="javascript:void(0)"><?php echo lang('lbl_menu_name_pages'); ?><i class="fa fa-angle-down fa-indicator"></i></a>
										<!-- drop down multilevel  -->
										<ul class="drop-down-multilevel left-menu">
											<li><a href="<?php echo base_url('error404'); ?>"><?php echo lang('lbl_menu_name_404_error'); ?></a></li>
											<li><a href="<?php echo base_url('comingsoon'); ?>"><?php echo lang('lbl_menu_name_coming_soon'); ?></a></li>
											<li><a href="<?php echo base_url('privacy'); ?>"><?php echo lang('lbl_menu_name_privacy_policy'); ?></a></li>
											<li><a href="<?php echo base_url('Terms_conditions'); ?>"><?php echo lang('lbl_menu_name_terms_conditions'); ?></a></li>
											<li><a href="<?php echo base_url('faq'); ?>"><?php echo lang('lbl_menu_name_faq'); ?></a></li>
										</ul>
										</li>
										<li><a href="<?php echo base_url('blog');?>"> <?php echo lang('lbl_menu_name_blog'); ?></a></li>
										<li><a href="<?php echo base_url('contact');?>"> <?php echo lang('lbl_menu_name_contact'); ?></a></li>
										<?php if($this->session->userdata('logged_in')) {?>
										<li><a href="<?php echo base_url('search'); ?>"><?php echo lang('lbl_sidebar_search'); ?> <span class="search hidden-sm hidden-xs"></span></a></li>	<?php } ?>	
										<?php if($this->session->userdata('logged_in')) {?>
										<li><a href="<?php echo base_url('notification');?>"> <?php echo lang('lbl_menu_name_notification'); ?></a></li>			
										<li><a href="<?php echo base_url().'user/logout'; ?>"><?php echo lang('lbl_sidebar_log_out'); ?></a></li><?php } ?>
									</ul>
								</div>
							</div>
						</div>
					</section>
				</nav>
			<!-- menu end -->
			</div>
		</header>