<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1><?php echo lang('lbl_log_in_success_messages');?></h1>
			</div>
			<p><?php echo lang('lbl_log_in_success_long_messages');?></p>
		</div>
	</div><!-- .row -->
</div><!-- .container -->