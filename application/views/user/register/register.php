<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php session_start(); ?>
<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<!--=================================
 inner-intro-->
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url']; ?>);">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="pos-r divider"><?php echo lang('lbl_menu_name_register'); ?><span class="sub-title"><?php echo lang('lbl_menu_name_register'); ?></span></h1></div>
			</div>
			<div class="col-sm-12 mt-5 xs-mt-3">
				<ul class="page-breadcrumb mt-5">
					<li>
						<a href="<?php echo base_url();?>">
							<i class="fa fa-home"></i>
							<?php echo lang('lbl_menu_name_home'); ?>
						</a> 
						<i class="fa fa-angle-double-right"></i>
					</li>
					<li>
						<span><?php echo lang('lbl_menu_name_register'); ?></span> 
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="login-form register-img dark-bg page-section-ptb" style="background: url(<?php echo $background7['background_url']; ?>) no-repeat 0 0; background-size: cover;">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-6 col-md-10">
				<div class="login-1-form register-1-form clearfix text-center">
					<h4 class="title divider-3 text-white mb-3"><?php echo lang('lbl_register_register'); ?></h4>
				 <?php 	if (isset($error)) : ?>
						<div class="row">
							<div class="col-md-12">
								<div class="alert myerrormsg" role="alert">
								<?php echo $error; ?>
								</div>
							</div>
						</div>
				<?php 	endif; ?>
				<?php 	if (validation_errors()) : ?>
							<div class="row">
								<div class="col-md-12">
								   <div class="alert myerrormsg" role="alert">
										<?php echo validation_errors(); ?>
								   </div>
								</div>
							</div>
					<?php endif; ?>
					<?php echo form_open_multipart('User/register', array('id' => 'user_reg_form')); ?>							
						<div class="section-field mb-3">
							<div class="field-widget">
								<i class="glyph-icon flaticon-user"></i>
								<input tabindex="1" id="Firstname" name="fname" type="text" placeholder="<?php echo lang('lbl_register_firstname'); ?>*" value="<?php echo set_value('fname'); ?>">
							</div> 
						</div>
						<input id="" name="fb_id" type="hidden" value="<?php echo $_SESSION['fb_id'] ?>">
						<div class="section-field mb-3">
							<div class="field-widget">
								<i class="glyph-icon flaticon-user"></i>
								<input tabindex="2" id="Lastname" name="lname" type="text" placeholder="<?php echo lang('lbl_register_lastname'); ?>*" value="<?php echo set_value('lname'); ?>">
							</div> 
						</div>
						<div class="section-field mb-3">
							<div class="field-widget">
								<i class="fa fa-envelope-o" aria-hidden="true"></i>
								<input tabindex="4" id="email" name="email" class="email" type="email" autocomplete="off" placeholder="<?php echo lang('lbl_register_email'); ?>*" value="<?php echo set_value('email'); ?>">
							</div> 
						</div>
						<div class="section-field mb-3">
							<div class="field-widget">
								<i class="glyph-icon flaticon-padlock"></i>
								<input tabindex="5" id="password" name="password" autocomplete="off" class="password" type="password" placeholder="<?php echo lang('lbl_register_password'); ?>*" name="password">
							</div> 
						</div>
						<div class="section-field mb-3">
							<div class="field-widget">
								<i class="glyph-icon flaticon-padlock"></i>
								<input tabindex="6" id="password_confirm" name="password_confirm" autocomplete="off" class="Password" type="password" placeholder="<?php echo lang('lbl_register_confirm_password'); ?>*" name="password_confirm">
							</div> 
						</div>
						<div class="section-field mb-3">
							<div class="field-widget">
								<i class="fa fa-graduation-cap" aria-hidden="true"></i>
								<input tabindex="7" id="education" name="education" type="text" placeholder="<?php echo lang('lbl_register_education'); ?>*" value="<?php echo set_value('education'); ?>">
							</div> 
						</div>
						<div class="section-field mb-3">
							<div class="field-widget">
								<i class="fa fa-briefcase" aria-hidden="true"></i>
								<input tabindex="8" id="profession" name="profession" type="text" placeholder="<?php echo lang('lbl_register_profession'); ?>*" value="<?php echo set_value('profession'); ?>">
							</div> 
						</div>
						<div class="form-group mt-2">
							<div class="checkbox">
								<input tabindex="9" name="terms" id="terms" type="checkbox">
								<label for="terms">
									<a href="<?php echo base_url('terms_conditions'); ?>" target="_blank">
										<?php echo lang('lbl_register_i_agree_to_the'); ?>
										<span style="color:#2cc0d9"></span>
										<u>
										<?php echo strtolower(lang('lbl_menu_name_terms_conditions')); ?>
										</u>
									</a>
									<?php echo lang('lbl_and'); ?>
									<a href="<?php echo base_url('privacy'); ?>" target="_blank">
										<u>
										<?php echo strtolower(lang('lbl_menu_name_privacy_policy')); ?>
										<span style="color:#2cc0d9"></span>
										</u>
									</a>
								</label>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="section-field text-uppercase text-center mt-2">
							<a tabindex="10" id="user-reg-check" class="button  btn-lg btn-theme full-rounded animated right-icn" href="javascript:void(0);" ><span ><?php echo lang('lbl_menu_name_next'); ?><i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></a>
							<a tabindex="11" style="display: none;" id="user-reg-btn" class="button  btn-lg btn-theme full-rounded animated right-icn" href="javascript:void(0);" ><span ><?php echo lang('lbl_menu_name_next'); ?><i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></a>
						</div>
					</form>
					<div class="clearfix"></div>
					<div class="section-field mt-2 text-center text-white">
						<p class="lead mb-0"><?php echo lang('lbl_log_in_have_a_account');?>
							<a class="text-white" href="<?php echo base_url('login');?>">
								<u><?php echo lang('lbl_log_in_btn_sign_in');?>!</u> 
							</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--modal pop up -->
<div class="modal fade" id="user_verification_code_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel" style="color:white"><?php echo lang("lbl_verification")?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="text-align:center">
				<p><?php echo lang("lbl_verification_msg")?></p>
				<p id="msgajax"></p>
				<form name="verify_code" id="verify_code" method="post">
						<div class="section-field mb-3">
							<div class="field-widget">
								<input tabindex="1" id="verification_code" name="verification_code" type="text" placeholder="" value="" length="4" style="width:100%;text-align:center">
							</div> 

						</div>
						<div class="section-field text-uppercase text-center mt-2">
							<input type="submit" tabindex="11" id="user-verify-btn" class="button  btn-lg btn-theme full-rounded animated right-icn" href="javascript:void(0);" value="Verify"/>
						</div>
					</form>
			</div>
			</div>
		</div>
	</div>
<!--modal popup ends-->
<style>
.error{color:red; text-align: left; width:100%;}
</style>