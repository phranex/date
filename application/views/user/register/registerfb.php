<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script>
<?php
session_start();
/*
	From here new register with user name,first name,last name etc 
*/
?>
<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<!--=================================
 inner-intro-->
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url']; ?>);">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="pos-r divider"><?php echo lang('lbl_menu_name_register'); ?><span class="sub-title"><?php echo lang('lbl_menu_name_register'); ?></span></h1></div>
			</div>
			<div class="col-sm-12 mt-5 xs-mt-3">
				<ul class="page-breadcrumb mt-5">
					<li><a href="<?php echo base_url('register');?>"><i class="fa fa-home"></i><?php echo lang('lbl_menu_name_home'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><span><?php echo lang('lbl_menu_name_register'); ?> </span> </li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="login-form register-img dark-bg page-section-ptb" style="background: url(<?php echo $background7['background_url']; ?>) no-repeat 0 0; background-size: cover;">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-6 col-md-10">
				<div class="login-1-form register-1-form clearfix text-center">
					<h4 class="title divider-3 text-white mb-3"><?php echo lang('lbl_register_register'); ?></h4>
			 <?php 	if (isset($error)) : ?>
					<div class="row">
						<div class="col-md-12">
							<div class="alert myerrormsg" role="alert">
							<?php echo $error; ?>
							</div>
						</div>
					</div>
			<?php 	endif; ?>
			<?php if (validation_errors()) : ?>
					<div class="row">
						<div class="col-md-12">
						   <div class="alert myerrormsg" role="alert">
								<?php echo validation_errors(); ?>
						   </div>
						</div>
					</div>
			<?php endif; ?>
			<?php echo form_open_multipart('User/register', array('id' => 'user_reg_form')); ?>							
						<div class="section-field mb-3">
							<div class="field-widget">
								<i class="glyph-icon flaticon-user"></i>
								<input id="Firstname" name="fname" type="text" placeholder="<?php echo lang('lbl_register_firstname'); ?>" value="<?php echo $_SESSION['fb_fname'] ?>">
							</div> 
						</div>
						<input id="" name="fb_id" type="hidden" value="<?php echo $_SESSION['fb_id'] ?>">
						<div class="section-field mb-3">
							<div class="field-widget">
								<i class="glyph-icon flaticon-user"></i>
								<input id="Lastname" name="lname" type="text" placeholder="<?php echo lang('lbl_register_lastname'); ?>" value="<?php echo $_SESSION['fb_lname'] ?>">
							</div> 
						</div>
						<!--div class="section-field mb-3">
							<div class="field-widget">
								<i class="glyph-icon flaticon-user"></i>
								<input id="username" name="username" class="username" type="username" placeholder="<?php echo lang('lbl_register_username'); ?>" value="<?php echo set_value('username'); ?>">
							</div> 
						</div-->
						<div class="section-field mb-3">
							<div class="field-widget">
								<i class="fa fa-envelope-o" aria-hidden="true"></i>
								<input id="email" name="email" class="email" type="email" placeholder="<?php echo lang('lbl_register_email'); ?>" value="<?php echo $_SESSION['fb_email'] ?>" name="email">
							</div> 
						</div>
						<div class="section-field mb-3">
							<div class="field-widget">
								<i class="glyph-icon flaticon-padlock"></i>
								<input id="password" name="password" class="password" type="password" placeholder="<?php echo lang('lbl_register_password'); ?>" name="password">
							</div> 
						</div>
						<div class="section-field mb-3">
							<div class="field-widget">
								<i class="glyph-icon flaticon-padlock"></i>
								<input id="password_confirm" name="password_confirm" class="Password" type="password" placeholder="<?php echo lang('lbl_register_confirm_password'); ?>" name="password_confirm">
							</div> 
						</div>
						<div class="section-field mb-3">
							<div class="field-widget">
								<i class="fa fa-graduation-cap" aria-hidden="true"></i>
								<input id="education" name="education" type="text" placeholder="Education" value="<?php echo set_value('education'); ?>">
							</div> 
						</div>
						<div class="section-field mb-3">
							<div class="field-widget">
								<i class="fa fa-briefcase" aria-hidden="true"></i>
								<input id="profession" name="profession" type="text" placeholder="Proffesion" value="<?php echo set_value('profession'); ?>">
							</div> 
						</div>
						<div class="clearfix"></div>
						<div class="section-field text-uppercase text-center mt-2">
							<a id="user-reg-btn" class="button  btn-lg btn-theme full-rounded animated right-icn" href="javascript:void(0);" ><span><?php echo lang('lbl_menu_name_next'); ?><i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></a>
						</div>
					</form>
					<div class="clearfix"></div>
					<div class="section-field mt-2 text-center text-white">
						<p class="lead mb-0"><?php echo lang('lbl_log_in_have_a_account');?><a class="text-white" href="<?php echo base_url('login');?>"><u><?php echo lang('lbl_log_in_btn_sign_in');?>!</u> </a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<style>
.error{color:red; text-align: left; width:100%;}
</style>
<script>
$(document).ready(function () {
  $("form").validate({
    rules: {
      fname: {
        required: true
      },
      lname: {
        required: true
      },
      username: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      password: {
        required: true
      },
      password_confirm: {
        required: true,
        equalTo : "#password"
      },
      education: {
        required: true
      },
      profession: {
        required: true
      }
    },
    messages: {
      fname: {
        required: "Please Add First Name"
      },
      lanme: {
        required: "Please Add Last Name"
      },
      username: {
        required: "Please Add Username"
      },
      email: {
        required: "Please Add Email Address"
      },
      password: {
        required: "Please Add Password"
      },
      password_confirm: {
        required: "Please Add Re Enter Password"
      },
      education: {
        required: "Please Add Education"
      },
      profession: {
        required: "Please Add Profession"
      }
		},
		errorPlacement: function(error, element) {
			error.insertAfter( element.parent() );
		},
		errorClass: "error-light-pink",
  });
});
</script>