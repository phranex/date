<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url']; ?>);">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="pos-r divider"><?php if($this->session->userdata('logged_in')) {echo "Gallery";} else { echo "register";}?><span class="sub-title"><?php if($this->session->userdata('logged_in')) {echo "Gallery";} else { echo "register";}?></span></h1></div>
			</div>
			<div class="col-sm-12 mt-7">
				<ul class="page-breadcrumb">
					<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i><?php echo lang('lbl_menu_name_home'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><span><?php if($this->session->userdata('logged_in')) {echo "Gallery";} else { echo "register";}?> </span> </li>
				</ul>
			</div>
		</div>
	</div>
</section>

	<div class="ajaxloader">
		<img src="<?php echo base_url("Newassets/images/ajaxloader.gif")?>" id="ajaxloader" style="display: none;">
	</div>

<section class="page-section-ptb text-white" style="background: url(<?php echo $background7['background_url']; ?>) no-repeat 0 0; background-size: cover;">
	<div class="container">
		<div class="row justify-content-center">			
			<div class="col-md-8">  
				<div class="step-form">
					<div class="stepwizard">
						<div class="stepwizard-row setup-panel">
							<div class="stepwizard-step">
								<a href="#step-1" class="btn btn-circle">1 <span><i class="fa fa-check" aria-hidden="true"></i></span></a>
								<p><?php echo lang('lbl_gallery_photo_name'); ?></p>
							</div>
							<div class="stepwizard-step">
								<a href="#step-2" class="btn btn-circle" disabled="disabled">2 <span><i class="fa fa-check" aria-hidden="true"></i></span></a>
								<p><?php echo lang('lbl_register_about_you'); ?></p>
							</div>
							<div class="stepwizard-step">
								<a href="#step-3" class="btn btn-circle" disabled="disabled">3 <span><i class="fa fa-check" aria-hidden="true"></i></span></a>
								<p><?php echo lang('lbl_menu_name_preferences'); ?></p>
							</div>
						</div>
					</div>
					<form action="<?php echo base_url('user/reg_contact'); ?>" method="post" class="text-center mt-3">
						<div class="row setup-content" id="step-1">
							<div class="col-md-12">
								<h4 class="title divider-3 mb-5"><?php echo lang('lbl_gallery_photo_name'); ?></h4>
																
								<div class="alert text-center" align="center">		
								<?php 
									if(!empty($_SESSION['register_success']))
									{?>
								
										<div class="row" align="center">
											<div class="col-md-12 ">
											   <div class="alert mysuccessmsg" role="success">
													<?php echo $_SESSION['register_success']; ?>
											   </div>
											</div>
										</div>
										<!--h3 style="color: green;margin-bottom:10px;"><?php echo $_SESSION['register_success'];?></h3-->
									 <?php             
									}
									?>								
								<?php 	
					if(!empty($_SESSION['success']))
						{
							?>
							<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert mysuccessmsg" role="success">
										<?php echo $_SESSION['success']; ?>
								   </div>
								</div>
							</div>
							<?php
						}
						else if(!empty($_SESSION['fail']))
						{
							?>
							<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert myerrormsg" role="alert">
										<?php echo $_SESSION['fail']; ?>
								   </div>
								</div>
							</div>
							<?php
						}
									?>
								</div>
								<div class="row justify-content-center">
									<div class="col-lg-6 col-md-10">
										<div class="form-group">									
											<div class="row">
												<div class="col-sm-8 mb-4">
													<!--input style="display:none" type="file" id="imgupload1" name="imgupload1" /-->                            
														<label for="img1" class="red-text avatar-view" >
														<?php
															if(!empty($userdetail->profile_image))
															{															
																$str=base_url("uploads/".$userdetail->profile_image);
																if(file_exists(DIRECTORY_PATH."uploads/".$userdetail->profile_image))
																{																			
																	?>
																	<img class="img-center w-100 imgupload1" id="1" src="<?php echo $str; ?>" alt="" >
																	<?php
																}
																else
																{
																	?>
																	<img class="img-center w-100 imgupload1" id="1" src="<?php echo base_url('images/step/01.png');?>" alt="">
																	<?php
																}							
															}
															else
															{															
																?><img class="img-center w-100 imgupload1" id="1" src="<?php echo base_url('images/step/01.png');?>" alt="">
																<?php
															}
															?>
														</label>																			
												</div>											
												<!--input class="edit-images img1" type="file" id="img1" class="" name="1"  /-->			
												<div class="col-sm-4">
													<div class="row">
														<?php												
														$imgurl="";
														$img="";
														$imgid="";
														for($i=0;$i<=4;$i++)
														{	
															$n=count($content);
															$imgurl=base_url("images/step/0".($i+2).".png");
															for($j=0;$j<$n;$j++)
															{
																$ik="img".($i+2);
																if($content[$j]["img_key"]==$ik)
																{
																	if($content[$j]["img_url"]!="")
																	$imgurl=base_url("uploads/".$content[$j]["img_url"]);
																	$img=$content[$j]["img_url"];
																	$imgid=$content[$j]["id"];
																}
															}
																if($i<=1)
																	echo "<div class='col-sm-12 mb-4' id='imgupload".($i+2)."'>";
																else
																	echo "<div class='col-sm-4' id='imgupload".($i+2)."'>";
															?>																									
																		<label for="<?php echo "img".($i+2); ?>" class="red-text  avatar-view<?php echo ($i+2); ?>" >
																	<?php	
																		if($img!="")
																		{																							
																			if(file_exists(DIRECTORY_PATH."uploads/$img"))
																			{	
																				?>
																				<img class="img-center w-100 imgupload<?php echo ($i+2); ?>" id="<?php echo ($i+2); ?>" src="<?php echo $imgurl; ?>" alt="" >
																				<?php
																			}
																			else
																			{
																				?>
																				<img class="img-center w-100 imgupload<?php echo ($i+2); ?>" id="<?php echo ($i+2); ?>" src="<?php echo base_url('images/step/0'.($i+2).'.png');?>" alt="">
																				<?php
																			}							
																		}
																		else
																		{															
																			?><img class="img-center w-100 imgupload<?php echo ($i+2); ?>" id="<?php echo ($i+2); ?>" src="<?php echo base_url('images/step/0'.($i+2).'.png');?>" alt="">
																			<?php
																		}																
																		?>
																		</label>													
																		<?php
																		if((strpos($imgurl, 'step') === false)){
																			?>
																			<a class="remove-bg" data-position="<?php echo ($i+2);?>">
																				<i class="fa fa-close "></i>
																			</a>
																			<?php
																		}?>			
																</div>														
																<!--input class="edit-images " type="file" id="<?php echo "img".($i+2); ?>" name="<?php echo ($i+2); ?>"  /-->
															<?php
															if($i==1)
																echo "</div></div></div><div class='row'>";
														}?>												
													</div>
												</div>																			
											</div>								
										</div>
											<div class="clearfix"></div>
											<p>
												Recommanded Resolution For Images<br/>
												Image #1 	:	280px	*	250px<br/>
												Image #2-#6	:	1498px	*	844px<br/>
											</p>	
										<div class="form-group mb-0">
										<button type="submit" class="button btn-theme full-rounded btn nextxBtn btn-lg mt-2 animated right-icn" ><?php echo lang('lbl_menu_name_next'); ?></button>	
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</form>
				</div>
			</div> 
		</div>
	</div>
</section>