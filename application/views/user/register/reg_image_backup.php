<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="page-section-ptb text-white" style="background: url(<?php echo $background7['background_url']; ?>) no-repeat 0 0; background-size: cover;">
	<div class="container">
		<div class="row">
			<div class="col-md-offset-2 col-md-8">  
				<div class="step-form">
					<div class="stepwizard">
						<div class="stepwizard-row setup-panel">
							<div class="stepwizard-step">
								<a href="#step-1" class="btn btn-circle">1 <span><i class="fa fa-check" aria-hidden="true"></i></span></a>
								<p>Photo</p>
							</div>
							<div class="stepwizard-step">
								<a href="#step-2" class="btn btn-circle" disabled="disabled">2 <span><i class="fa fa-check" aria-hidden="true"></i></span></a>
								<p>about you</p>
							</div>
							<div class="stepwizard-step">
								<a href="#step-3" class="btn btn-circle" disabled="disabled">3 <span><i class="fa fa-check" aria-hidden="true"></i></span></a>
								<p>preferences</p>
							</div>
						</div>
					</div>
					<div class="row" align="center">				
					<?php 	if(!empty($this->session->flashdata('success')))
						{
							?>
							<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert mysuccessmsg" role="success">
										<?php echo $_SESSION['success']; ?>
								   </div>
								</div>
							</div>
							<?php
						}
						else if(!empty($this->session->flashdata('fail')))
						{
							?>
							<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert myerrormsg" role="alert">
										<?php echo $_SESSION['fail']; ?>
								   </div>
								</div>
							</div>
							<?php
						}
						?>
					</div>
					<form action="<?php echo base_url('user/reg_contact'); ?>" method="post">
						<div class="row setup-content" id="step-1">
							<div class="col-md-12">
								<h4 class="title divider-3 mb-50">Photo</h4>
								<div class="row row-eq-height">
									<div class="col-md-offset-3 col-md-6">
										<div class="form-group">									
											<div class="row mb-10 row-eq-height">
												<div class="col-sm-8 pr-15">
													<input style="display:none" type="file" id="imgupload1" name="imgupload1" />                            
														<label for="img1" class="red-text" >
														<?php
															if(!empty($userdetail->profile_image))
															{															
																$str=base_url("uploads/thumbnail/".$userdetail->profile_image);
																if(file_exists(DIRECTORY_PATH."uploads/thumbnail/".$userdetail->profile_image))
																{																			
																	?>
																	<img class="img-center full-width"  src="<?php echo $str; ?>" alt="" >
																	<?php
																}
																else
																{
																	?>
																	<img class="img-center full-width" src="<?php echo base_url('images/step/01.png');?>" alt="">
																	<?php
																}							
															}
															else
															{															
																?><img class="img-center full-width" src="<?php echo base_url('images/step/01.png');?>" alt="">
																<?php
															}
															?>
														</label>																			
												</div>											
												<input class="edit-images img1" type="file" id="img1" class="" name="1"  />			
												<div class="col-sm-4">
													<div class="row">
														<?php
														for($i=0;$i<=1;$i++)
														{	
															?>													
															<div class="col-sm-12 mb-30">													
																<label for="<?php echo "img".($i+2); ?>" class="red-text" >
																	<?php	
																	if(!empty($content[$i]["img_url"]))
																	{															
																		$str=base_url("uploads/thumbnail/".$content[$i]["img_url"]);
																		if(file_exists(DIRECTORY_PATH."uploads/thumbnail/".$content[$i]["img_url"]))
																		{	
																			?>
																			<img class="img-center"  src="<?php echo $str; ?>" alt="" >
																			<?php
																		}
																		else
																		{
																			?>
																			<img class="img-center" src="<?php echo base_url('images/step/0'.($i+2).'.png');?>" alt="">
																			<?php
																		}							
																	}
																	else
																	{															
																		?><img class="img-center" src="<?php echo base_url('images/step/0'.($i+2).'.png');?>" alt="">
																		<?php
																	}																
																	?>
																</label>																	
															</div>														
															<input class="edit-images " type="file" id="<?php echo "img".($i+2); ?>" name="<?php echo ($i+2); ?>"  />
															<?php
														}?>												
													</div>
												</div>
											</div>
											<div class="row mt-30">
												<?php
												for($i=2;$i<=4;$i++)
												{
													?>
													<div class="col-sm-4">
														<label for="<?php echo "img".($i+2); ?>" class="red-text" >
														<?php	
															if(!empty($content[$i]["img_url"]))
															{																
																$str=base_url("uploads/thumbnail/".$content[$i]["img_url"]);
																if(file_exists(DIRECTORY_PATH."uploads/thumbnail/".$content[$i]["img_url"]))
																{	
																	?>
																	<img class="img-center"  src="<?php echo $str; ?>" alt="" >
																	<?php
																}
																else
																{
																	?>
																	<img class="img-center" src="<?php echo base_url('images/step/0'.($i+2).'.png');?>" alt="">
																	<?php
																}							
															}
															else
															{															
																?><img class="img-center" src="<?php echo base_url('images/step/0'.($i+2).'.png');?>" alt="">
																<?php
															}																
															?>
															</label>		
													</div>
													<input class="edit-images " type="file" id="<?php echo "img".($i+2); ?>"  name="<?php echo ($i+2); ?>"  />
													<?php
												}
												?>										
											</div>
										</div>
										<div class="form-group mb-0">
											<button type="submit" class="button btn-theme full-rounded btn nextxBtn btn-lg mt-20 animated right-icn" >Next</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div> 
		</div>
	</div>
</section>