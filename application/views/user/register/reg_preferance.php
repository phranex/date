<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url']; ?>);">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="pos-r divider"><?php echo lang('lbl_menu_name_register'); ?><span class="sub-title"><?php echo lang('lbl_menu_name_register'); ?></span></h1></div>
			</div>
			<div class="col-sm-12 mt-7">
				<ul class="page-breadcrumb">
					<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i><?php echo lang('lbl_menu_name_home'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><span><?php echo lang('lbl_menu_name_register'); ?> </span> </li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="page-section-ptb text-white" style="background: url(<?php echo $background7['background_url']; ?>) no-repeat 0 0; background-size: cover;">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">  
				<div class="step-form">
					<div class="stepwizard">
						<div class="stepwizard-row setup-panel">
							<div class="stepwizard-step">
								<a href="#" class="btn btn-circle">1 <span><i class="fa fa-check" aria-hidden="true"></i></span></a>
								<p><?php echo lang('lbl_gallery_photo_name'); ?></p>
							</div>
							<div class="stepwizard-step">
								<a href="" class="btn btn-circle">2 <span><i class="fa fa-check" aria-hidden="true"></i></span></a>
								<p><?php echo lang('lbl_register_about_you'); ?></p>
							</div>
							<div class="stepwizard-step">
								<a href="#step-3" class="btn btn-circle">3 <span><i class="fa fa-check" aria-hidden="true"></i></span></a>
								<p><?php echo lang('lbl_menu_name_preferences'); ?></p>
							</div>
						</div>
					</div>	
					<?php 	
					if(!empty($this->session->flashdata('success')))
						{
							?>
							<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert mysuccessmsg" role="success">
										<?php echo $this->session->flashdata('success'); ?>
								   </div>
								</div>
							</div>
							<?php
						}
						else if(!empty($this->session->flashdata('fail')))
						{
							?>
							<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert myerrormsg" role="alert">
										<?php echo $this->session->flashdata('fail'); ?>
								   </div>
								</div>
							</div>
							<?php
						}
					  if (validation_errors()) : ?>
							<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert myerrormsg" role="alert">
										<?php echo validation_errors(); ?>
								   </div>
								</div>
							</div>
					<?php endif; ?>
					<form action="<?php echo base_url('user/reg_step3'); ?>" method="post" class="text-center mt-3">
						<div class="row setup-content" id="step-3">
							<div class="col-md-12">
								<div class="row justify-content-center">
									<div class="col-lg-8 col-md-10 text-left text-capitalize text-white">
										<div class="form-group">
											<label class="title divider-3 mb-3"><?php echo lang('lbl_register_interested_in'); ?>*</label>
											<div class="row">
												<div class="col-sm-4 xs-mb-2">
													<div class="radio">
														<input type="radio" name="gender_pref" id="radio3" value="female" checked="checked">
														<label for="radio3"><?php echo lang('lbl_register_female'); ?></label>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="radio">
														<input type="radio"  name="gender_pref" id="radio4" value="male">
														<label for="radio4"><?php echo lang('lbl_register_male'); ?></label>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="title divider-3 mb-3"><?php echo lang('lbl_register_date_preferances'); ?>*</label>
											<ul id="sortable" class="sortable" style="list-style:none">
												<li class=" datepref" id="1" value="1" style="border:0px;list-style:none;margin-right: 25px;float: left;">
													<div class="Preference-icon">
														<img class="img-center" alt="#" src="<?php echo base_url('Newassets/images/dateimg1.png')?>">
													</div>
													<div class="text"><?php echo lang('lbl_register_coffee'); ?></div>
												</li>
												<li class=" datepref " id="2" value="2"  style="border:0px;list-style:none;margin-right: 25px;float: left;">
													<div class="Preference-icon">
														<img class="img-center" alt="#" src="<?php echo base_url('Newassets/images/dateimg2.png')?>">
													</div>
													<div class="text"><?php echo lang('lbl_register_drink'); ?></div>
												</li>
												<li class=" datepref " id="3" value="3"  style="border:0px;list-style:none;margin-right: 25px;float: left;" >
													<div class="Preference-icon">
														<img class="img-center" alt="#" src="<?php echo base_url('Newassets/images/dateimg3.png')?>">
													</div>
													<div class="text"><?php echo lang('lbl_register_food'); ?>	</div>
												</li>
												<li class=" datepref " id="4" value="4"  style="border:0px;list-style:none;margin-right: 25px;float: left;">
													<div class="Preference-icon">
														<img class="img-center" alt="#" src="<?php echo base_url('Newassets/images/dateimg4.png')?>">
													</div>
													<div class="text"><?php echo lang('lbl_register_fun'); ?></div>
												</li>
											</ul>
											<input type="hidden" id="datepref" name="datepref" value="1,2,3,4">
										</div>
										<div class="form-group">
											<label class="title divider-3 mb-5"><?php echo lang('lbl_register_age_preferances'); ?>*</label>
											<div>
												<input id="ex2" type="text" class="span2" value="" data-slider-min="10" data-slider-max="100" data-slider-step="1" data-slider-value="[20,25]"/>
												<div id="slider-range"></div>
												<input id="slider1"  type="hidden" value="20" name="age-min" id="age-min" />
												<input id="slider2"  type="hidden" value="25"  name="age-max" id="age-max"/>
											</div>
										</div>
										<div class="form-group mt-2">
											<label class="title divider-3 mb-5"><?php echo lang('lbl_register_distance_preferances'); ?>*</label>
											<div>
												<input id="ex3" type="text" class="span2" value="" data-slider-min="0" data-slider-max="200" data-slider-step="1" data-slider-value="[20,160]"/>
												<input id="slider3" type="hidden" value="20" name="dist-min" id="dist-min"/>
												<input id="slider4" type="hidden" value="160" name="dist-max" id="dist-max"/>
											</div>
										</div>
										<!--div class="form-group">
											<label for="ethnicity" class="title divider-3 mb-3"><?php echo lang('lbl_register_ehtnticity'); ?>*</label>
											<select class="selectpicker" id="ethnicity" name="ethnicity[]" multiple>
												<?php 	
												foreach($ethnicity as $eathni)
												{ 
												?>										
													<option value="<?php echo $eathni['id']; ?>">
													<?php 
															echo $eathni['name'];
														?>
													</option>
												<?php 	
												}
												?>
											</select>
										</div-->
										<div class="form-group">
											<label for="question" class="title divider-3 mb-3"><?php echo lang('lbl_register_question'); ?>*</label>
											<select class="selectpicker" id="question" name="question">
												<?php 	foreach($questions as $question)
												{								
												?>
													<option value="<?php echo $question['id']; ?>">
													<?php 
														echo $question['name'];
														?>
													</option>
												<?php 	
												}
												?>
											</select>
										</div>
										<div class="form-group">
											<label class="title divider-3 mb-3"><?php echo lang('lbl_register_question_answer'); ?>*</label>
											<input type="text" maxlength="100" class="form-control" id="question-ans" name="question-ans" value="<?php echo set_value('question-ans'); ?>">
										</div>
										<div class="form-group">
											<label class="title divider-3 mb-3"><?php echo lang('lbl_register_question_location'); ?>*</label>
											<div class="row">
												<div class="col-sm-4 xs-mb-2">
													<div class="radio">
														<input name="access_loc" checked="checked" id="radio5" type="radio" value="1">
														<label for="radio5"><?php echo lang('lbl_profile_yes'); ?></label>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="radio">
														<input name="access_loc" id="radio6" type="radio" value="0">
														<label for="radio6"><?php echo lang('lbl_profile_no'); ?></label>
													</div>
												</div>
											</div>
										</div>
										<!-- <div class="form-group">
											<div class="checkbox">
												<input name="terms" id="terms" type="checkbox">
												<label for="terms">
													<a href="<?php echo base_url(); ?>terms.html" target="_blank"><?php echo lang('lbl_register_i_agree_to_the'); ?>
														<span style="color:#2cc0d9"></span> <?php echo lang('lbl_register_tearms_agrement'); ?>
													</a>
												</label>
											</div>
										</div> -->
										<div class="form-group mb-0 text-center">
											<button type="submit" class="button btn-theme full-rounded btn nextxBtn btn-lg mt-20 animated right-icn" id="btnstep3"><?php echo lang('lbl_register_submit'); ?><i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div> 
		</div>
	</div>
</section>