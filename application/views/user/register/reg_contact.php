<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url']; ?>
);">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="pos-r divider"><?php echo lang('lbl_menu_name_register'); ?><span class="sub-title"><?php echo lang('lbl_menu_name_register'); ?></span></h1></div>
			</div>
			<div class="col-sm-12 mt-7">
				<ul class="page-breadcrumb">
					<li><a href="<?php echo base_url('register');?>"><i class="fa fa-home"></i><?php echo lang('lbl_menu_name_home'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><span><?php echo lang('lbl_menu_name_register'); ?></span> </li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="page-section-ptb text-white" style="background: url(<?php echo $background7['background_url']; ?>) no-repeat 0 0; background-size: cover;">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">  
				<div class="step-form">
					<div class="stepwizard">
						<div class="stepwizard-row setup-panel">
							<div class="stepwizard-step">
								<a href="<?php echo base_url('user/reg_image'); ?>" class="btn btn-circle">1 <span><i class="fa fa-check" aria-hidden="true"></i></span></a>
								<p><?php echo lang('lbl_gallery_photo_name'); ?></p>
							</div>
							<div class="stepwizard-step">
								<a href="#step-2" class="btn btn-circle" disabled="disabled">2 <span><i class="fa fa-check" aria-hidden="true"></i></span></a>
								<p><?php echo lang('lbl_register_about_you'); ?></p>
							</div>
							<div class="stepwizard-step">
								<a href="#step-3" class="btn btn-circle" disabled="disabled">3 <span><i class="fa fa-check" aria-hidden="true"></i></span></a>
								<p><?php echo lang('lbl_menu_name_preferences'); ?></p>
							</div>
						</div>
					</div>
					<?php 	
					if(!empty($this->session->flashdata('success')))
						{
							?>
							<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert mysuccessmsg" role="success">
										<?php echo $this->session->flashdata('success'); ?>
								   </div>
								</div>
							</div>
							<?php
						}
						else if(!empty($this->session->flashdata('fail')))
						{
							?>
							<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert myerrormsg" role="alert">
										<?php echo $this->session->flashdata('fail'); ?>
								   </div>
								</div>
							</div>
							<?php
						}
					  if (validation_errors()) : ?>
							<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert myerrormsg" role="alert">
										<?php echo validation_errors(); ?>
								   </div>
								</div>
							</div>
					<?php endif; ?>
					<form action="<?php echo base_url('user/reg_step2'); ?>" method="post" class="text-center mt-3">
						<div class="row setup-content<?php echo "1"; ?>" id="step-2">
							<div class="col-md-12">
								<div class="row justify-content-center">
									<div class="col-md-8 text-left text-capitalize text-white">
										<div class="form-group">
											<label class="title divider-3 mb-3"><?php echo lang('lbl_register_gender'); ?>*</label>
											<div class="row">
												<div class="col-sm-4 xs-mb-2">
													<div class="radio">
														<input name="gender" <?php echo ($userdetail->gender=="female")? "checked":""; ?> id="radio1" type="radio" value="female">
														<label for="radio1"><?php echo lang('lbl_register_female'); ?></label>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="radio">
														<input name="gender" <?php echo ($userdetail->gender=="male")? "checked":""; ?> id="radio2" type="radio" value="male">
														<label for="radio2"><?php echo lang('lbl_register_male'); ?></label>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="title divider-3 mb-3"><?php echo lang('lbl_register_birthdate'); ?>*</label>
											<div class="row">
												<div class="col">
													<input type="number" min="1" max="12" onkeyup="checkMonth(this.value)" class="form-control" name="month" id="exampleInputMonth1" placeholder="<?php echo lang('lbl_register_birthdate_month'); ?>" required oninvalid="this.setCustomValidity('Month is Required')" oninput="setCustomValidity('')" value="<?php echo date_format(date_create($userdetail->dob),"m"); ?>">
												</div>
												<div class="col">
													<input type="number" min="1" max="31" onkeyup="checkDay(this.value)"  class="form-control" id="exampleInputDay" placeholder="<?php echo lang('lbl_register_birthdate_day'); ?>" name="day" required oninvalid="this.setCustomValidity('Day is Required')" oninput="setCustomValidity('')" value="<?php echo date_format(date_create($userdetail->dob),"d"); ?>">
												</div>
												<div class="col">
													<input type="number" min="<?php echo (date("Y")-65)  ?>" max="<?php echo date("Y") ?>"  class="form-control" onkeyup="checkYear(this.value)"  onfocusout="checkYearF(this.value)" id="exampleInputyear" placeholder="<?php echo lang('lbl_register_birthdate_year'); ?>" name="year" required  oninvalid="this.setCustomValidity('Year is Required')" oninput="setCustomValidity('')" value="<?php echo date_format(date_create($userdetail->dob),"Y"); ?>">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="title divider-3 mb-3"><?php echo lang('lbl_register_about'); ?>*</label>
											<textarea class="form-control" id="about" maxlength="500" name="about" placeholder="<?php echo lang('lbl_register_about'); ?>*" rows="3"><?php if(isset($userdetail->about)) echo $userdetail->about; else echo set_value('about');?></textarea>
										</div>
										<div class="form-group">
											<label for="ethnicity" class="title divider-3 mb-3"><?php echo lang('lbl_register_ehtnticity'); ?>*</label>
											<select class="selectpicker" id="ethnicity" name="ethnicity">
												<?php 	
												foreach($ethnicity as $eathni)
												{ 
												?>										
													<option <?php echo (in_array($eathni['id'],explode(",",$userdetail->ethnicity)))?'selected':''?> value="<?php echo $eathni['id']; ?>">
													<?php 
															echo $eathni['name'];
														?>
													</option>
												<?php 	
												}
												?>
											</select>
										</div>
										<div class="form-group">
											<label class="title divider-3 mb-3"><?php echo lang('lbl_register_religion'); ?>*</label>
											<select class="selectpicker" id="religion" name="religion">
												
												<?php
												foreach($religion as $relig)
												{
												?>
													<option  value="<?php echo $relig['id']; ?>" <?php echo (in_array($relig['id'],explode(",",$userdetail->religion)))?'selected':''?>>
														<?php 
															echo $relig['name'];
														?>
													</option>
												<?php 	
												}
												?>
											</select>
										</div>
										<div class="form-group">
											<?php
											$hight="3'0 (92 cm), 3'1 (94 cm),3'2 (97 cm),3'3 (99 cm),3'4 (102 cm),3'5 (104 cm),3'6 (107 cm),3'7 (109 cm),3'8 (112 cm),3'9 (114 cm),3'10 (117 cm),3'11 (119 cm),4'0 (122 cm),4'1 (125 cm),4'2 (127 cm),4'3 (130 cm),4'4 (132 cm),4'5 (135 cm),4'6 (137 cm),4'7 (140 cm),4'8 (142 cm),4'9 (145 cm),4'10 (147 cm),4'11 (150 cm),5'0 (152 cm), 5'1 (155 cm),5'2 (158 cm),5'3 (160 cm),5'4 (163 cm),5'5 (165 cm),5'6 (168 cm),5'7 (170 cm),5'8 (173 cm),5'9 (175 cm),5'10 (178 cm),5'11 (180 cm),6'0 (183 cm),6'1 (185 cm),6'2 (188 cm),6'3 (191 cm),6'4 (193 cm),6'5 (196 cm),6'6 (198 cm),6'7 (201 cm),6'8 (203 cm),6'9 (206 cm),6'10 (208 cm),6'11 (211 cm),7'0 (213 cm)";
											$height=explode(",",$hight);					
											?>
												<label class="title divider-3 mb-3"><?php echo lang('lbl_register_height'); ?>*</label>
												<select class="selectpicker" id="height" name="height" value="<?php echo set_value('height'); ?>">
											<?php 	for($i=0;$i<count($height);$i++)
													{  ?>
														<option value="<?php echo $height[$i];  ?>" <?php echo ($userdetail->height==$height[$i])?"selected":"";?>><?php echo $height[$i];  ?></option>
											<?php  	} ?>
												</select>						
										</div>
										<div class="form-group">
											<label class="title divider-3 mb-3"><?php echo lang('lbl_register_kids'); ?>*</label>
											<select class="selectpicker" id="kids" name="kids">
												<option value="1" <?php echo ($userdetail->kids==1)?"selected":"";?>><?php echo lang('lbl_register_kids'); ?> - 1</option>
												<option value="2" <?php echo ($userdetail->kids==2)?"selected":"";?>><?php echo lang('lbl_register_kids'); ?> - 2</option>
												<option value="3" <?php echo ($userdetail->kids==3)?"selected":"";?>><?php echo lang('lbl_register_kids'); ?> - 3</option>
												<option value="4" <?php echo ($userdetail->kids==4)?"selected":"";?>><?php echo lang('lbl_register_kids'); ?> - 4</option>
												<option value="5" <?php echo ($userdetail->kids==5)?"selected":"";?>><?php echo lang('lbl_register_kids'); ?> - 5</option>
												<option value="6" <?php echo ($userdetail->kids==6)?"selected":"";?>><?php echo lang('lbl_register_kids'); ?> - 6</option>
												<option value="7" <?php echo ($userdetail->kids==7)?"selected":"";?>><?php echo lang('lbl_register_kids'); ?> - 7</option>
												<option value="None" <?php echo ($userdetail->kids=="None")?"selected":"";?>><?php echo lang('lbl_register_none'); ?></option>
												<option value="One Day" <?php echo ($userdetail->kids=="One Day")?"selected":"";?>><?php echo lang('lbl_register_one_day'); ?></option>
												<option value="I Don''t Want Kids" <?php echo ($userdetail->kids=="I Don''t Want Kids")?"selected":"";?>><?php echo lang('lbl_register_i_dont_want_kids'); ?></option>
											</select>
										</div>
										<div class="form-group">
											<label class="title divider-3 mb-3"><?php echo lang('lbl_register_address'); ?>*</label>
											<input type="text" class="form-control" id="address" name="address"  placeholder="<?php echo lang('lbl_register_address'); ?>*" value="<?php if(isset($userdetail->address)) echo $userdetail->address; else echo set_value('address');?>">
											<?php 
												if(empty($googleapiskey['value'])){
													?>
														<br>Enter Google place API key in configuration.
													<?php
												}
											?>
										</div>
										<!--div class="form-group text-center">
											<div class="profile-info">
												<p class="mb-0"><i class="fa fa-info-circle" aria-hidden="true"></i> by clicking submit you are agreeing to our terms and conditions of use.</p>
											</div>
										</div-->
										<div class="form-group text-center">
											<button type="submit" class="button btn-theme full-rounded btn nextxBtn btn-lg mt-2 animated right-icn" id="btnstep3"><?php echo lang('lbl_menu_name_next'); ?><i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div> 
		</div>
	</div>
</section>
<?php ?>
<script
  src="https://maps.googleapis.com/maps/api/js?key=<?php echo $googleapiskey['value'];?>&amp;libraries=geometry,places"  type="text/javascript"></script>

  <script>
  function initialize() {
                       var input = document.getElementById('address');
                       var autocomplete = new google.maps.places.Autocomplete(input);
               }
               google.maps.event.addDomListener(window, 'load', initialize);
    function checkMonth(v){
   		if(v<=12 && v>=1){
   			return true;
   		}
   		else{
   			$("#exampleInputMonth1").val("1");
   		}
   }
  	function checkDay(v){
   		if(v<=31 && v>=1){
   			return true;
   		}
   		else{
   			$("#exampleInputDay").val("1");
   		}
   }
  	function checkYear(v){
   		var d = new Date();
   		//debugger;
   		//alert($("#exampleInputyear").val());
   		if(v<=(d.getFullYear()-15) && v>=(d.getFullYear()-65)){
   			return true;
   		}
   		else if(v.toString().length<=4 && !isNaN(v)){
   			return true;
   		}
   		else{
   			$("#exampleInputyear").val(d.getFullYear()-25);
   		}
   }
   function checkYearF(v){
   		//debugger;
   		var d = new Date();
   		if(v<=(d.getFullYear()-15) && v>=(d.getFullYear()-65)){
   			return true;
   		}
   		else{
   			$("#exampleInputyear").val(d.getFullYear()-15);
   		}
   }
  </script>			