<?php
/*
	When user click on forgot password link then that redirect to this on this page user enter its mail id if it if correct then send reset password link to its email account
*/
?>
<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="login-form login-img dark-bg page-section-ptb100 pb-70" style="background: url(<?php echo $background7['background_url']; ?>) no-repeat 0 0; background-size: cover;">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-md-offset-3"></div>
			<div class="col-md-6 col-md-offset-3">
				<div class="login-1-form clearfix text-center">  
					<div class="row">
						<?php 
						if(!empty($_SESSION['success']))
						{?>
							<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert mysuccessmsg" role="error">
										<?php  echo $_SESSION["success"];  ?>
								   </div>
								</div>
							</div>
							<!--h3 style="color: green;"><?php echo $_SESSION['success'];?></h3-->
						 <?php             
						}
						else if(!empty($_SESSION['fail']))
						{?>
					
			            	<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert myerrormsg" role="error">
										<?php  echo $_SESSION["fail"];  ?>
								   </div>
								</div>
							</div>
							<!--h3 style="color: red;"><?php echo $_SESSION['fail'];?></h3-->
						 <?php
						}
							unset($_SESSION['success']);
							unset($_SESSION['fail']);
						?>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-md-offset-3"></div>
		</div>
	</div>
</section>