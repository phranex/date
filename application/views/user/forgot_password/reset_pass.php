<?php
/*
	At this page link is open from the mail and we can be update the user password from here
*/
?>
<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="login-form login-img dark-bg page-section-ptb100 pb-70" style="background: url(<?php echo $background7['background_url']; ?>) no-repeat 0 0; background-size: cover;">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-md-offset-3"></div>
			<div class="col-md-6 col-md-offset-3">
				<div class="login-1-form clearfix text-center">  
						<?php
						if(!empty($_SESSION['success']))
						{?>
			            	<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert mysuccessmsg" role="error">
										<?php  echo $_SESSION["success"];  ?>
								   </div>
								</div>
							</div>
							<?php
							//echo "<h6 class='successful'>$_SESSION[success]</h6>";
						}
						else if(!empty($_SESSION['fail']))
						{?>
			            	<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert myerrormsg" role="error">
										<?php  echo $_SESSION["fail"];  ?>
								   </div>
								</div>
							</div>
							<?php
							//echo "<h6 class='alert'>$_SESSION[fail]</h6>";
						}
						unset($_SESSION['success']);
						unset($_SESSION['fail']);
						?>
				<?php 	if (validation_errors()) : ?>
			            	<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert myerrormsg" role="error">
										<?php echo validation_errors();?>
								   </div>
								</div>
							</div>
							<!-- <div class="col-md-12">
								<div class="alert" role="alert">
									<?php echo validation_errors();?>
								</div>
							</div> -->
				<?php 	endif; ?>
				<?php 	if (isset($error)) : ?>
			            	<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert myerrormsg" role="error">
										<?php echo $error;?>
								   </div>
								</div>
							</div>
							<!-- <div class="col-md-12">
								<div class="alert" role="alert">
									<?php echo $error;?>
								</div>
							</div> -->
				<?php 	endif; ?>
					<?= form_open(base_url()."user/change_pass") ?>
						<div class="form-group">
							<h4 class="title divider-3 text-white mb-5 xs-mb-3"><?php echo lang('lbl_reset_password_reset_password'); ?></h4><br/>
							<div style="color: white; margin-bottom: 15px;"><h5>Password reset for <?php echo $email;?></h5></div>
							<input type="hidden" name="id" value="<?php echo $id;?>" /> 
							<input type="hidden" name="token" value="<?php echo $token;?>" /> 
							<label for="reset_password"><?php echo lang('lbl_reset_password_new_password'); ?>*</label>
							<input type="password" class="form-control" id="reset_password" value="" name="reset_password" placeholder="<?php echo lang('lbl_reset_password_new_password'); ?>*">
						</div> 
						<div class="form-group">
							<label for="confirm_password"><?php echo lang('lbl_reset_password_confirm_password'); ?>*</label>
							<input type="password" class="form-control" id="confirm_password" value="" name="confirm_password" placeholder="<?php echo lang('lbl_reset_password_confirm_password'); ?>*">
						</div>  
						<div class="section-field text-uppercase text-center mt-20">
							<input type="submit" class="button  btn-lg btn-theme full-rounded animated right-icn" name="reset_pass" value="<?php echo lang('lbl_reset_password_reset_password'); ?>">
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-3 col-md-offset-3"></div>
		</div>
	</div>
</section>