<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1><?php echo lang('lbl_heading_log_in_success');?></h1>
			</div>
			<p><?php echo lang('lbl_heading_you_are_now_log_in_success');?></p>
		</div>
	</div><!-- .row -->
</div><!-- .container -->