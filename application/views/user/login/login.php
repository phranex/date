<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<!--- Login Page Start -->
<!--inner-intro-->
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url']; ?>)">
    <div class="container">
		<div class="row intro-title text-center">
            <div class="col-sm-12">
				<div class="section-title"><h1 class="position-relative divider"><?php echo lang('lbl_menu_name_login'); ?> <span class="sub-title"><?php echo lang('lbl_menu_name_login'); ?></span></h1></div>
			</div>
            <div class="col-sm-12 mt-5 xs-mt-3">
				<ul class="page-breadcrumb mt-5">
					<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i> <?php echo lang('lbl_menu_name_home'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><span><?php echo lang('lbl_menu_name_login'); ?></span> </li>
				</ul>
			</div>
        </div>
    </div>
</section>
<section class="login-form login-img dark-bg page-section-ptb100 pb-70" style="background: url(<?php echo base_url('images/pattern/04.png');?>) no-repeat 0 0; background-size: cover;">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-6 col-md-10">
				<div class="login-1-form clearfix text-center">  
					<h4 class="title divider-3 text-white mb-5 xs-mb-3"><?php echo lang('lbl_log_in_btn_sign_in'); ?></h4>
					<div class="login-1-social  mb-5 xs-mb-3 text-center clearfix">
						<ul class="list-inline text-capitalize">
							<div>
								<?php
								echo "<h5 style='color:white' id='error'></h5>";
								?>
							</div>
							<li><a class="fb" href="#" onClick="fb_login()" scope="public_profile,email"><i class="fa fa-facebook"></i> <?php echo lang('lbl_log_in_sign_up_facebook'); ?></a></li>
						</ul>
					</div>
					<div class="row">
				<?php 	if (validation_errors()) : ?>
							<div class="col-md-12">
								<div class="myerrormsg alert" role="alert">
									<?php echo validation_errors();?>
								</div>
							</div>
				<?php 	endif; ?>
				<?php 	if (isset($error)) : ?>
							<div class="col-md-12">
								<div class="myerrormsg alert" role="alert">
									<?php echo $error;?>
								</div>
							</div>
				<?php 	endif; ?>
					</div>
					<?php 
					$attributes = array('id' => '');
					echo form_open(); ?>
						<div class="section-field mb-2">
							<div class="field-widget">
								<i class="glyph-icon flaticon-user"></i>
								<input tabindex="1" id="username" class="web" autocomplete="off" type="text" placeholder="<?php echo lang('lbl_log_in_email'); ?>" name="username" >
							</div> 
						</div>
						<div class="section-field mb-1">
							<div class="field-widget">
							   <i class="glyph-icon flaticon-padlock"></i>
							   <input id="password" tabindex="2" class="Password" autocomplete="off" type="password" placeholder="<?php echo lang('lbl_log_in_password'); ?>" name="password" >
							</div> 
						</div>
						<div class="section-field text-uppercase">
							 <a href="<?php echo base_url("/user/forgot_pass");?>" class="pull-right text-white"><?php echo lang('lbl_log_in_forgot_password');?></a>
						</div>
						<div class="clearfix"></div>
						<div class="section-field text-uppercase text-center mt-2">
							<input tabindex="3" type="submit" class="button  btn-lg btn-theme full-rounded animated right-icn" name="<?php echo lang('lbl_log_in_btn_sign_in'); ?>" value="<?php echo lang('lbl_log_in_btn_sign_in'); ?>">
						</div>
						<div class="clearfix"></div>
						<div class="section-field mt-2 text-center text-white">
							<p class="lead mb-0"><?php echo lang('lbl_log_in_not_account');?> <a class="text-white" href="<?php echo base_url('register'); ?>"><u><?php echo lang('lbl_log_in_register_now'); ?></u> </a></p>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<style>
.error{color:red; text-align: left; width:100%;}
</style>