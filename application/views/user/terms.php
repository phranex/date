<div class="content">
	<section>
		<div class="container-fluid text-center">
			<div class="row">
				<div class="col-sm-12 text-uppercase">
					<h3><?php echo lang('lbl_home_terms_condition'); ?></h3>
					<div><img class="img-center" src="<?php echo base_url('images/title-bdr.png'); ?>" alt=""></div>
				</div>
			</div>
			<div class="about-text">
				<p>
				Illegal or Abusive Usage is Strictly Prohibited: You must not abuse, harass, threaten, impersonate or intimidate other users of our Website. You may not use the Association’s Website for any illegal or unauthorized purpose. International users agree to comply with all local laws regarding online conduct and acceptable content. Should you be found to have engaged in illegal or abusive usage of our Website, Association will suspend your account or usage as applicable.
				</p>
				<p>
					Electronic Communication: When you visit our Website or send us emails, you are communicating with us electronically. You consent to receive communications from us electronically. We will communicate with you by email or by posting notices on the Website. You agree that all agreements, notices, disclosures and other communications that we provide to you electronically satisfy any legal requirement that such communications be in writing.
				</p>
				<p>
					Copyright: All content included on this site, such as text, graphics, logos, button icons, images, audio clips, digital downloads, data compilations, and software, is the property of Association protected by United States and international copyright laws. The compilation of all content on this site is the exclusive property of the Association and protected by U.S. and international copyright laws. All software used on this site is the property of Association or its software suppliers and protected by United States and international copyright laws.
				</p>
				<p>
					Trademarks: Gypsum Association or www.gypsum.org and other Association graphics, logos, page headers, button icons, scripts, and service names are trademarks, registered trademarks or trade dress of Association in the U.S. and/or other countries. Association’s trademarks and trade dress may not be used in connection with any product or service that is not Association’s, in any manner that is likely to cause confusion among customers or in any manner that disparages or discredits Association. All other trademarks not owned by Association that appear on this site are the property of their respective owners, who may or may not be affiliated with, connected to, or sponsored by Association.
				</p>
				<p>
					License and Site Access: Association grants you a limited license to access and make personal use of this site and not to download (other than page caching) or modify it, or any portion of it, except with express written consent. This license does not include any resale or commercial use of this site or its contents; any collection and use of any product listings, descriptions, or prices; any derivative use of this site or its contents; or any use of data mining, robots, or similar data gathering and extraction tools. This site or any portion of this site may not be reproduced, duplicated, copied, sold, resold, visited, or otherwise exploited for any commercial purpose without express written consent of Association. You may not frame or utilize framing techniques to enclose any trademark, logo, or other proprietary information including but not limited to images, text, page layout, or form of Association without express written consent. You may not use any meta tags or any other “hidden text” utilizing Company’s name or trademarks without the express written consent. Any unauthorized use terminates the permission or license granted. You are granted a limited, revocable, and nonexclusive right to create a hyperlink to the home page of Association so long as the link does not portray Association, or its products or services in a false, misleading, derogatory, or otherwise offensive matter. You may not use any Association logo or other proprietary graphic or trademark as part of the link without express written permission.
				</p>
				<p>
					Your Account: This site uses cookies. If you use this site, you are responsible for maintaining the confidentiality of your account and password and for restricting access to your computer, and you agree to accept responsibility for all activities that occur under your account or password. Association reserves the right to refuse service, terminate accounts, remove or edit content, or cancel orders in their sole discretion.
				</p>
				<p>
					Returns and Refunds: Every effort is made to ensure the prompt and accurate filling of orders. Damaged materials or incorrect shipments must be reported to eh Gypsum Association within two weeks of receipt to be eligible for a full refund , credit or exchange. Refunds will be made on returns totaling $25 or greater. Returned material not previously reported a damaged must arrive in resalable condition to be eligible for refund, credit or exchange. All returns of materials that are not returned due to damage or incorrect fulfillment are subject to a 25% restocking charge. No refund, credits, or exchanges will be authorized or issued on returned items after 60 days of an order date of shipment.
				</p>
				<p>
					For overpayments or returned orders under $25, credit will be applied to future orders; all credits become void if not used within one year. Recorded materials, such as CDs, will not be refunded but may be exchanged for the same title or edition only.
					The Gypsum Association shall not be responsible for returned merchandise that goes astray in the mail while being returned. To ensure delivery use a method such as register mail or UPS that provides and return receipt and traceability. All return shipping charges are to be paid by the customer.
					Risk of Loss: All items purchased from Association are made pursuant to a shipment contract. This means that the risk of loss and title for such items pass to you upon our delivery to the carrier.
				</p>				
				<p>
					TO THE FULL EXTENT PERMISSIBLE BY APPLICABLE LAW, ASSOCIATION DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. COMPANY DOES NOT WARRANT THAT THIS SITE; INFORMATION, CONTENT, MATERIALS, PRODUCTS (INCLUDING SOFTWARE) OR SERVICES INCLUDED ON OR OTHERWISE MADE AVAILABLE TO YOU THROUGH THIS SITE; THEIR SERVERS; OR E-MAIL SENT FROM ASSOCIATION ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. ASSOCIATION WILL NOT BE LIABLE FOR ANY DAMAGES OF ANY KIND ARISING FROM THE USE OF THIS SITE OR FROM ANY INFORMATION, CONTENT, MATERIALS, PRODUCTS (INCLUDING SOFTWARE) OR SERVICES INCLUDED ON OR OTHERWISE MADE AVAILABLE TO YOU THROUGH THIS SITE, INCLUDING, BUT NOT LIMITED TO DIRECT, INDIRECT, INCIDENTAL, PUNITIVE, AND CONSEQUENTIAL DAMAGES, UNLESS OTHERWISE SPECIFIED IN WRITING.
				</p>
				<p>
					CERTAIN STATE LAWS DO NOT ALLOW LIMITATIONS ON IMPLIED WARRANTIES OR THE EXCLUSION OR LIMITATION OF CERTAIN DAMAGES. IF THESE LAWS APPLY TO YOU, SOME OR ALL OF THE ABOVE DISCLAIMERS, EXCLUSIONS, OR LIMITATIONS MAY NOT APPLY TO YOU, AND YOU MIGHT HAVE ADDITIONAL RIGHTS.
				</p>
				<p>
					Applicable Law: By visiting our website, you agree that the laws of the State of Maryland, without regard to principles of conflict of laws, will govern these Terms of Service and any dispute of any sort that might arise between you and Association.
				</p>
			</div>	
		</div>
	</section>
</div> 