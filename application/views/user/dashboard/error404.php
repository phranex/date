<?php 
$lange= $this->admin_model->get_default_language();
$language=$lange['status'];
$languages = $this->session->userdata('site_lang');
$lg=$languages;
?>
<?php
foreach($error404 as $error){
	if(!empty($error['id'])){
		$errr_img = base_url('Newassets/images/'.$error['error_image']); ?>
		<section class="page-section-ptb text-center clearfix error-page position-relative">
			<div class="container">
				<div class="row justify-content-strat">
					<div class="col-lg-7 col-md-12">
						<div class="big-title"><?php echo $error['title']; ?></div>
						<h3 class="title mb-4 xs-mb-3"><b><?php echo $error['sub_title']; ?></b></h3>
						<p class="lead"><?php echo $error['description']; ?></p>
						<a href="<?php echo site_url(); ?>" class="button btn-lg btn-theme full-rounded animated right-icn mt-3">
						<span><?php echo lang('lbl_home_go_to_home_page'); ?><i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></a>
					</div>
				</div>
			</div>
		</section>
		<?php
	}
}
?>