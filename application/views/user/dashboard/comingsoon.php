<?php
foreach($comingsoon as $coming)
{
	if(!empty($coming['id'])){
		?>
		<?php $background_image = base_url('Newassets/images/'.$coming['comingsoon_image']); ?>
		<section class="coming-soon page-section-ptb bg-1 fixed text-white" style="background:url(<?php echo $background_image;?>) no-repeat 0 0; background-size: cover; height: 100vh;" data-gradient-red="4">
			<div class="coming-soon-text">
				<div class="container">
					<div class="row justify-content-end">
						<div class="col-md-12 mt-3">
							<img src="<?php echo base_url('images/logo.png');?>" alt="">
							<h1 class="title text-uppercase mb-1"><?php echo $coming['title']; ?></h1>
							<?php
							$date = new DateTime($coming['date']);
							?>
							<ul class="countdown1 list-unstyled">
								<li><span class="days">302</span><p class="days_ref">Days</p></li>
								<li><span class="hours">10</span><p class="hours_ref">Hours</p></li>
								<li><span class="minutes">42</span><p class="minutes_ref">Minutes</p></li>
								<li><span class="seconds">36</span><p class="seconds_ref">Seconds</p></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php 
	}
} 
?>