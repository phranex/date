<?php
/*
Fetch user Gallery and from that fetch first profile image of friend
*/
	$url = array_column($gallery, 'img_url');
	$flg=0;
	foreach($gallery as $gallery_data)
	{
		if(!empty($gallery_data["img_url"]))
		{
			$flg=1;
		}
	}
	/*
	Another Gallery images are display in slider here of Friend
	*/
	?>
	<section class=" details-page bg bg-fixed  pos-r" style="background-image:url()">
		<div class="container-fluid">
			<div class="row px-4">
				<div class="col-sm-3">
					<div class="row">
						<div id="myCarousel" class="carousel slide mt-4" data-ride="carousel">
							<!-- Indicators -->
							<ol class="carousel-indicators">
								<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
								<?php $i=1; foreach($gallery as $row) { ?>
								<li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>"></li>
								<?php $i++; } ?>
							</ol>

							<!-- Wrapper for slides -->
							<div class="carousel-inner">
								<div class="carousel-item active">
									<?php
									if(!empty($content->profile_image))
									{
										$str=base_url("uploads/thumbnail/".$content->profile_image);
										if(file_exists(DIRECTORY_PATH."uploads/thumbnail/".$content->profile_image))
										{      
											?>
											<img src="<?php echo $str; ?>" alt="" style="height:350px;">
											<?php
										}
										else
										{
											?>
											<img src="<?php echo base_url('images/profile/profile-img.png');?>" alt="" style="height:350px;">
											<?php
										}       
									}
									else
									{
										?>
										<img src="<?php echo base_url('images/profile/profile-img.png');?>" alt="" style="height:350px;">
										<?php
									}
									?> 
								</div>

								<?php 
								foreach($gallery as $row)
								{
									if(!empty($row['img_url'])){
										?>
											<div class="carousel-item">
												<img src="<?php echo base_url("uploads/thumbnail/".$row['img_url']); ?>" style="height:350px;">
											</div>
										<?php
									}
								}
								?>
							</div>
							<!-- Left and right controls -->
							<a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
								<span class="carousel-control-prev-icon"></span>
							</a>
							<a class="carousel-control-next" href="#myCarousel" data-slide="next">
								<span class="carousel-control-next-icon"></span>
							</a>
						</div>
					</div>
					<div class="row mt-4">
						<div class="col-md-12 text-center">
							<div class="profile-text"> 
								<h2><?php echo $content->fname." ".$content->lname; ?></h2> 
								<h5>
									<?php $today=date('Y-m-d');
									$dob=$content->dob;				
									$diff = date_diff(date_create($dob),date_create($today));
									echo $age = $diff->format('%Y');
									echo lang('lbl_friend_year_old'); ?>
								</h5>
							</div>		
						</div>
					</div>
				</div>
				<div class="col-sm-9">
					<div class="row mt-4">
						<div class="col-sm-12 ">
							<h4 class="title divider-3 mb-5"><?php echo lang('lbl_friend_basic_detail'); ?></h4>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="table-responsive">
								<table class="table table-bordered text-center">
									<tbody>
										<tr>
											<td><?php echo lang('lbl_user_gender'); ?></td>
											<td><?php echo ucfirst($content->gender); ?></td>
											<td><?php echo lang('lbl_user_education'); ?></td>
											<td><?php echo ucfirst($content->education); ?></td>
										</tr>
										<tr class="tr-bg">
											<td><?php echo lang('lbl_user_age'); ?></td> 
											<td><?php $today=date('Y-m-d');
												$dob=$content->dob;				
												$diff = date_diff(date_create($dob),date_create($today));
												echo $age = $diff->format('%Y');	?> <?php echo lang('lbl_user_year_old'); ?></td>
											<td><?php echo lang('lbl_user_height'); ?></td>
											<td><?php echo $content->height; ?></td>
										</tr>
										<tr class="tr-bg">
											<td><?php echo lang('lbl_user_birthdate'); ?></td>
											<td><?php echo date_format(new DateTime($content->dob),"F d , Y");; ?></td>
											<td><?php echo lang('lbl_user_kids'); ?></td>
											<td><?php echo $content->kids; ?></td>
										</tr>
										<tr class="tr-bg">
											<td><?php echo lang('lbl_user_looking'); ?></td>
											<td><?php echo ucfirst($content->gender_pref); ?></td>
											<td><?php echo lang('lbl_user_work_as'); ?></td>
											<td><?php echo ucfirst($content->profession); ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="page-section-ptb pt-100 text-left">
		<div class="container">		
			<div class="row mt-5">
				<div class="col-sm-12 text-left">
					<h4 class="title divider-3 mb-5"><?php echo lang('lbl_friend_about'); ?></h4>
					<p><?php echo $content->about; ?></p>
				</div>
			</div>
		</div>
	</section>