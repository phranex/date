<?php 
$lange= $this->admin_model->get_default_language();
$language=$lange['status'];
$lg = $this->session->userdata('site_lang');
?>
<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url']; ?>)">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="position-relative divider"><?php echo lang('lbl_menu_name_stories'); ?><span class="sub-title"><?php echo lang('lbl_menu_name_stories'); ?></span></h1></div>
			</div>
			<div class="col-sm-12 mt-5 xs-mt-3">
				<ul class="page-breadcrumb mt-5">
					<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i> <?php echo lang('lbl_menu_name_home'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><span><?php echo lang('lbl_menu_name_stories'); ?></span> </li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!--=================================
 banner -->
<!--=================================
 Page Section -->
<section class="page-section-ptb pb-3 xs-pb-5 story-slider">
    <div class="container">
		<?php
		if(isset($stories))
		{
			?>
			<div class="row mb-5 xs-mb-3">
				<?php
				$i=0;
				foreach($stories as $story)
				{
					if($i%3==0)
					echo '</div><div class="row mb-5 xs-mb-3">';
					if(!empty($story['id'])){
					?>
					<div class="col-lg-4 col-md-4 col-sm-6 xs-mb-3 d-flex">
						<div class="story-item">
							<div class="story-image clearfix">
							<?php
							if(!empty($story['image']))
							{
								$str=base_url("Newassets/images/thumbnail/".$story['image']);
								 if(file_exists(DIRECTORY_PATH."Newassets/images/thumbnail/".$story['image']))
								 {      
								  ?>
								  <img class="img-fluid" src="<?php echo $str; ?>" alt="" >
								  <?php
								 }
								 else
								 {
								  ?>
								  <img class="img-fluid" src="<?php echo base_url('images/story/01.jpg');?>" alt="">
								  <?php
								 }       
							}
							else
							{
							 ?>
							   <img class="img-fluid" src="<?php echo base_url('images/story/01.jpg');?>" alt="">
							 <?php
							}
							?>  
								<div class="story-link">
									<a href="<?php echo base_url('stories/stories_details/'.$story['id']);?>">
										<i class="glyph-icon flaticon-add"></i>
									</a>
								</div>
							</div>
							<div class="story-details text-center">
							  <h5 class="title divider-3"><?php echo $story['title'];?></h5>
							 <div class="about-des mt-3"><?php echo substr($story['description'], 0, 100); ?></div>              
									<a class="button" href="<?php echo base_url('stories/stories_details/'.$story['id']); ?>"><?php echo lang('lbl_home_read_more'); ?></a>	             
							</div>
						</div>
					</div>
					<?php
					}
					$i++;
				}
				?>
			</div>
			<?php
		}
		?>
    </div>
</section>