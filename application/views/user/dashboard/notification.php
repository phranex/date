<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url']; ?>)">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="position-relative divider"><?php echo lang('lbl_menu_name_notification'); ?> <span class="sub-title"><?php echo lang('lbl_menu_name_notification'); ?></span></h1></div>
			</div>
			<div class="col-sm-12 mt-5 xs-mt-3">
				<ul class="page-breadcrumb mt-5">
					<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i><?php echo lang('lbl_menu_name_home'); ?></a><i class="fa fa-angle-double-right"></i></li>
					<li><span><?php echo lang('lbl_menu_name_notification'); ?></span> </li>
				</ul>
			</div>
		</div>
	</div>
</section>
	<div class="ajaxloader">
		<img src="<?php echo base_url("Newassets/images/ajaxloader.gif")?>" id="ajaxloader" style="display: none;">
	</div>
<section class="page-section-ptb profile-slider"> 
    <div class="container">
        <?php if(!empty($notification))
		{
			?>
			<div class="row mb-5 xs-mb-3">
				<div class="col-sm-12 text-center">
					<h2 class="title divider"><?php echo lang('lbl_notification'); ?></h2>
	                <div class="row" align="center">
						<div class="col-md-12 ">
						   	<div class="success_msg alert mysuccessmsg" role="success" style="display: none;">
						   	</div>
						</div>
					</div>
					<?php 
					if(!empty($_SESSION['friend_approved']))
					{?>
			            <div class="row" align="center">
							<div class="col-md-12 ">
							   <div class="alert mysuccessmsg" role="success">
									<?php echo $_SESSION['friend_approved'];?>
							   </div>
							</div>
						</div>
					 <?php             
					}
					?>
				</div>
			</div>
			<?php 
        }
        else
        {
			?>
			<div class="row mb-5 xs-mb-3">
				<div class="col-sm-12 text-center">
					<h2>No notification</h2>
				</div>
			</div>
			<?php 
        }
        if(isset($_SESSION["error"]))
		{
			?>
            	<div class="row" align="center">
					<div class="col-md-12 ">
					   <div class="alert myerrormsg" role="error">
							<?php  echo $_SESSION["error"];  ?>
					   </div>
					</div>
				</div>
			<?php	
        } 
        if(isset($notification) && !empty($notification))
		{
			?>
			<div class="row mb-5 xs-mb-3">
				<?php
				$i=0;
				foreach($notification as $friend) 
				{ 
					if($i%4==0)
					echo '</div><div class="row mb-5 xs-mb-3">';
					 ?>
						<div class="col-sm-3 xs-mb-3 userblock">
							<div class="profile-image fix-size clearfix">
								<?php
									if(!empty($friend['profile_image']))
									{
										if(file_exists(DIRECTORY_PATH."/uploads/thumbnail/".$friend['profile_image']))
										{ ?>
											<img src="<?php echo base_url("uploads/thumbnail/".$friend['profile_image']); ?>" class="img-fluid" alt="<?php echo $friend["fname"]." ".$friend["lname"]; ?>"/>
								<?php 	}
									}
									else
									{?>
										<img src="<?php echo base_url("/Newassets/images/profile/01.jpg"); ?>" class="img-fluid"  alt="No Image" />
										<?php 
									}
								?>
							</div>
							<div class="profile-details profile-text">
								<h5 class="title"><a href="<?php echo base_url('friends/friend_profile/'.$friend['id']); ?>"><?php echo $friend['fname'].' '.$friend['lname']; ?></a></h5>
								<span class="text-black">
									<?php 
									$today=date('Y-m-d');
									$dob=$friend['dob'];				
									$diff = date_diff(date_create($dob),date_create($today));
									echo $age = $diff->format('%Y');?>
									<?php echo lang('lbl_notification_years_old'); ?>
								</span>
								<ul class="list-inline mt-1 info-ld">
									<li>
										<a class="yellow-text like" title="<?php echo lang('lbl_like'); ?>" href="<?php echo base_url('friends/friend_approved/'.$friend['send_user_id']); ?>" >
											<i class="fa fa-thumbs-up"></i>
										</a>
									</li>
									<li>
										<a class="red-text dis-like" href="<?php echo base_url('friends/friend_decline/'.$friend['send_user_id']); ?>" title="<?php echo lang('lbl_dislike'); ?>">
											<i class="fa fa-thumbs-down"></i>
										</a>
									</li>
									<!--li>
										<a  href="<?php echo base_url('friends/friend_approved/'.$friend['send_user_id']); ?>"  title="<?php echo lang('lbl_like'); ?>">
											<i class="fa fa-thumbs-o-up text-green " class="like" aria-hidden="true"></i>
										</a>
									</li>
									<li>
										<a  href="<?php echo base_url('friends/friend_decline/'.$friend['send_user_id']); ?>" title="<?php echo lang('lbl_dislike'); ?>">
											<i class="fa fa-thumbs-o-down text-red " class="dis-like" aria-hidden="true"></i>
										</a>
									</li-->
								 </ul>
							</div>
						</div>
				   <?php
				   $i++; 
				}
				?>
		   </div>
		   <?php
		}    
		?>
    </div>
</section>
<style type="text/css">
	/*.ajaxloader{
		position:absolute;
		top:0px;
		right:0px;
		width:100%;
		height:100%;
	}*/
</style>