<?php
//echo "<pre>";print_r($userdetail);die;
/*
Fetch user Gallery and from that fetch first profile image of login user
*/
$url = array_column($gallery, 'img_url');
$flg=0;
foreach($gallery as $gallery_data)
{
	if(!empty($gallery_data["img_url"]))
	{
		$flg=1;
	}
}
?>
<!--=================================
 banner -->
<section class="details-page bg bg-fixed  position-relative" style="background-image:url()">
<div class="container-fluid">
        <div class="row px-4">
            <div class="col-sm-3">
				<div class="row">
					<div id="myCarousel" class="carousel slide mt-4" data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<?php $i=1; foreach($gallery as $row) { ?>
							<li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>"></li>
							<?php $i++; } ?>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner">
							<div class="carousel-item active">
								<img src="<?php if(!empty($_SESSION['profile_image'])){ echo base_url("uploads/thumbnail/".$_SESSION['profile_image']);} else{echo base_url("images/profile/profile-img.png");}?>" style="height:350px;">
							</div>

							<?php 
							foreach($gallery as $row)
							{
								if(!empty($row['img_url'])){
									?>
										<div class="carousel-item">
											<img src="<?php echo base_url("uploads/thumbnail/".$row['img_url']); ?>" style="height:350px;">
										</div>
									<?php
								}
							}
							?>
						</div>
						<!-- Left and right controls -->
						<a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
							<span class="carousel-control-prev-icon"></span>
						</a>
						<a class="carousel-control-next" href="#myCarousel" data-slide="next">
							<span class="carousel-control-next-icon"></span>
						</a>
					</div>
				</div>
				<div class="row mt-4">
					<div class="col-md-12 text-center">
						<div class="profile-text"> 
							<h2><?php echo $_SESSION['fname']." ".$_SESSION['lname']; ?></h2> 
							<h5>
								<?php $today=date('Y-m-d');
								$dob=$userdetail->dob;              
								$diff = date_diff(date_create($dob),date_create($today));
								echo $age = $diff->format('%Y');
								?>           
								<?php echo lang('lbl_dashboard_years_old'); ?>
							</h5>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-9">
			
			<div class="row mt-4">
            <div class="col-sm-12 text-left">
                <h4 class="title divider-3 mb-5 xs-mb-3"><?php echo lang('lbl_dashboard_basic_detail'); ?></h4>
                <div class="table-responsive">
                    <table class="table table-bordered text-center">
                        <tbody>
                            <tr>
                                <td><?php echo lang('lbl_dashboard_gender'); ?></td>
                                <td><?php echo ucfirst($userdetail->gender); ?></td>
                                <td><?php echo lang('lbl_dashboard_education'); ?></td>
                                <td><?php echo ucfirst($userdetail->education); ?></td>
                            </tr>
                            <tr class="tr-bg">
                                <td><?php echo lang('lbl_dashboard_age'); ?></td> 
                                <td><?php 
                                $today=date('Y-m-d');
								$dob=$userdetail->dob;				
								$diff = date_diff(date_create($dob),date_create($today));
								echo $age = $diff->format('%Y');
								?> 
                                <?php echo lang('lbl_dashboard_years_old'); ?></td>
                                <td><?php echo lang('lbl_dashboard_height'); ?></td>
                                <td><?php echo $userdetail->height; ?></td>
                            </tr>
                            <tr class="tr-bg">
                                <td><?php echo lang('lbl_dashboard_birth_date'); ?></td>
                                <td><?php echo date_format(new DateTime($userdetail->dob),"F d , Y");; ?></td>
                                <td><?php echo lang('lbl_dashboard_kids'); ?></td>
                                <td><?php echo $userdetail->kids; ?></td>
                            </tr>
                            <tr>
                                <td><?php echo lang('lbl_dashboard_question'); ?></td>
                                <td colspan="3"><?php echo $question->name; ?></td>
                            </tr>
                            <tr class="tr-bg">
                                <td><?php echo lang('lbl_dashboard_answer'); ?></td>
                                <td colspan="3"><?php echo ucfirst($userdetail->que_ans); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo lang('lbl_dashboard_looking_for'); ?></td>
                                <td><?php echo ucfirst($userdetail->gender_pref); ?></td>
                                <td><?php echo lang('lbl_dashboard_work_as'); ?></td>
                                <td><?php echo ucfirst($userdetail->profession); ?></td>
                            </tr>
                            <tr class="tr-bg">
                                <td><?php echo "Religion"; ?></td>
								<?php 
								$relid=explode(',',$userdetail->religion);
								if(!empty($relid))
								{
									$religion=$this->Common_model->fetch_data('religions',array('id'=>$relid[0])); 
								}
								//echo '<pre>';print_r($religion);die;
								?>
                                <td><?php echo ucfirst($religion[0]['name']); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
				


			</div>
		</div>
	</div>
</section>
<section class="profile-cntn">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="text-white">
                    <!--ul>
                        <li>
                            <img class="profile-pic" src="<?php if(!empty($_SESSION['profile_image'])){ echo base_url("uploads/thumbnail/".$_SESSION['profile_image']);} else{echo base_url("images/profile/profile-img.png");}?>">
                        </li>
                    </ul-->
                </div>
            </div>
        </div>
    </div>
</section>

<!--=================================
 banner -->
<section class="page-section-ptb">
<?php  if($this->session->flashdata("error"))
            {?>
            	<div class="row" align="center">
					<div class="col-md-12 ">
					   <div class="alert myerrormsg" role="error">
							<?php echo $this->session->flashdata('error'); ?>
					   </div>
					</div>
				</div>
    <?php   }
            if($this->session->flashdata("success"))
            { ?>
            <div class="row" align="center">
				<div class="col-md-12 ">
				   <div class="alert mysuccessmsg" role="success">
						<?php echo $this->session->flashdata('success'); ?>
				   </div>
				</div>
			</div>
    <?php   } ?>
			<?php 
			if(!empty($_SESSION['report_user']))
			{?>
				<div class="row" align="center">
					<div class="col-md-12 ">
					   <div class="alert mysuccessmsg" role="success">
							<?php echo $_SESSION['report_user'];?>
					   </div>
					</div>
				</div>
			 <?php             
			}
			if(!empty($_SESSION['friend_decline']))
			{?>

				<div class="row" align="center">
					<div class="col-md-12 ">
					   <div class="alert mysuccessmsg" role="success">
							<?php echo $_SESSION['friend_decline'];?>
					   </div>
					</div>
				</div>
			 <?php             
			}
			if(!empty($_SESSION['blocked_user']))
			{?>

			<div class="row" align="center">
				<div class="col-md-12 ">
				   <div class="alert mysuccessmsg" role="success">
						<?php echo $_SESSION['blocked_user'];?>
				   </div>
				</div>
			</div>
			 <?php             
			}
			?>
    <div class="container">
        
        
        <div class="row mt-5 xs-mt-3">
            <div class="col-sm-12 text-left">
                <h4 class="title divider-3 mb-5 xs-mb-3"><?php echo lang('lbl_dashboard_about_me'); ?></h4>
                <p class="wordpad-text"><?php echo $userdetail->about; ?></p>
            </div>
        </div>
    </div>
</section>

<?php $logged_in_user = $this->session->userdata('logged_in'); ?>
<?php
	/*if(XMPP_ENABLE)
	{
		if($logged_in_user) 
		{ ?>
        <link rel="stylesheet" type="text/css" media="screen" href="https://cdn.conversejs.org/css/converse.min.css">
		<script src="https://cdn.conversejs.org/dist/converse.min.js"></script>
		<script>
		(function (root, factory) {
				if (typeof define === 'function' && define.amd) {
					// AMD. Register as a module called "myplugin"
					//define(["converse"], factory);
					define("myplugin", ['converse'], factory);
				} else {
					// Browser globals. If you're not using a module loader such as require.js,
					// then this line below executes. Make sure that your plugin's <script> tag
					// appears after the one from converse.js.
					factory(converse);
				}
			}(this, function (converse) {

				
				///console.log(converse);
				converse.plugins.add('myplugin', {

					initialize: function () {
						var _converse = this._converse;
						//check all messages 
						console.log($(".chat-message"));
						_converse.on('afterMessagesFetched', function (chatboxview) {
							$(".chat-message").each(function(){
								var id=$(this).attr('data-msgid');
								var message=$(this).find('.chat-msg-content').html();
								message=message.trim();
								message = message.replace(/(\r\n\t|\n|\r\t)/gm,"");
								var datedata=message.match("{(.*)}");
								if(datedata!=null){
									var dateobj=JSON.parse(datedata[0]);
									if(dateobj!=null){
										//process it
										var title=dateobj.Title;
										var dateandtime=dateobj.TimeOfDate;
										var string_message="Would you like to Come on Date ?<br/>"+" On <b>"+dateandtime+" <br/> @ </b><b>"+ title +"</b>";

										$(".chat-message[data-msgid='"+id+"']").html(string_message);
									}
									
								}
							});
						});
						_converse.on('messageAdded', function (data) {
							//check for the date request 
							var id=data.message.attributes.msgid;
							var message=String(data.message.attributes.message);
							message=message.trim();
							message = message.replace(/(\r\n\t|\n|\r\t)/gm,"");
							var datedata=message.match("{(.*)}");
							if(datedata!=null){
								var dateobj=JSON.parse(datedata[0]);
								if(dateobj!=null){
									//process it
									var title=dateobj.Title;
									var dateandtime=dateobj.TimeOfDate;
									var string_message="Would you like to Come on Date ?<br/>"+" On <b>"+dateandtime+" <br/> @ </b><b>"+ title +"</b>";
									$(".chat-message[data-msgid='"+id+"']").html(string_message);
								}
								
							}
							
						});
					}
				});	
				converse.initialize({
					bosh_service_url: '<?php echo XMPP_HTTP_BIND; ?>', // Please use this connection manager only for testing purposes
					show_controlbox_by_default: true,
					auto_login:true,
					jid:'<?php echo $ejuser; ?>@<?php echo XMPP_SERVER; ?>',
					password:'<?php echo XMPP_DEFAULT_PASSWORD; ?>',
					allow_registration:false,
					allow_contact_requests:false,
					allow_contact_removal:false,
					allow_logout: false,
					allow_muc:false,
					auto_subscribe: true,
					allow_chat_pending_contacts:false,
					hide_muc_server:false,
					hide_offline_users:false,
					use_vcards:false,
					locked_domain:'<?php echo XMPP_SERVER; ?>',
					whitelisted_plugins: ['myplugin']
				});	
			})
		);

		</script>
		<?php 
		} 
	}*/
 ?>
 <script type="text/javascript">
     $(document).keydown( function(eventObject) {
        if(eventObject.which==37) {//left arrow
            $('.owl-prev').click();//emulates click on prev button
        } else if(eventObject.which==39) {//right arrow
            $('.owl-next').click();//emulates click on next button
        }
    } );
 </script>