<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
 $background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
 $background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
 $background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
 $background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
 $background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
 $background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
 $background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
 ?>
<?php
/*
	From here new register with user name,first name,last name etc 
*/
?>
<!--=================================
 inner-intro-->
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url'];?>);">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="position-relative divider"><?php echo lang('lbl_menu_name_my_profile'); ?><span class="sub-title"><?php echo lang('lbl_menu_name_my_profile'); ?></span></h1></div>
			</div>
			<div class="col-sm-12 mt-5 xs-mt-3">
				<ul class="page-breadcrumb mt-5">
					<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i> <?php echo lang('lbl_menu_name_home'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><span><?php echo lang('lbl_menu_name_my_profile'); ?></span> </li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="login-form register-img page-section-ptb" style="background: url(<?php echo $background7['background_url'];?>) no-repeat 0 0; background-size: cover;">
	<div class="container">
		<div class="row justify-content-center">
		   <div class="col-lg-6 col-md-8">
			   <div class="login-1-form register-1-form clearfix text-center">
					<h4 class="title divider-3 text-white mb-3"><?php echo lang('lbl_menu_name_my_profile'); ?></h4>
					<?php 
						if(!empty($this->session->flashdata('success')))
						{
							?>
							<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert mysuccessmsg" role="success">
										<?php echo $this->session->flashdata('success'); ?>
								   </div>
								</div>
							</div>
							<?php
						}
						else if(!empty($this->session->flashdata('fail')))
						{
							?>
							<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert myerrormsg" role="alert">
										<?php echo $this->session->flashdata('fail'); ?>
								   </div>
								</div>
							</div>
							<?php
						}
						if (isset($error)) : ?>
							<div class="row">
								<div class="col-md-12">
									<div class="alert myerrormsg" role="alert">
									<?php echo $error; ?>
									</div>
								</div>
							</div>
					<?php 	endif; ?>
					<?php if (validation_errors()) : ?>
							<div class="row">
								<div class="col-md-12">
								   <div class="alert myerrormsg" role="alert">
										<?php echo validation_errors(); ?>
								   </div>
								</div>
							</div>
					<?php endif; ?>
					<?php echo form_open_multipart('User/my_profile', array('id' => 'user_reg_form')); ?>							
						<div class="section-field mb-3">
							<div class="field-widget">
								<input type="hidden" id="id" name="id" value="<?php if(isset($userdetail->id)) echo $userdetail->id;?>">
								<i class="glyph-icon flaticon-user"></i>
								<input  tabindex="1" id="Firstname" name="fname" type="text" placeholder="<?php echo lang('lbl_register_firstname'); ?>*"  value="<?php if(isset($userdetail->fname)) echo $userdetail->fname;?>">
							</div> 
						</div>
						<div class="section-field mb-3">
							<div class="field-widget">
								<i class="glyph-icon flaticon-user"></i>
								<input tabindex="2" id="Lastname" name="lname" type="text" placeholder="<?php echo lang('lbl_register_lastname'); ?>*"  value="<?php if(isset($userdetail->lname)) echo $userdetail->lname;?>">
							</div> 
						</div>
						<!--div class="section-field mb-3" style="display:none">
							<div class="field-widget">
								<i class="glyph-icon flaticon-user"></i>
								<input tabindex="3" id="username" name="username" class="username" type="text" placeholder="<?php echo lang('lbl_register_username'); ?>*" disabled value="<?php if(isset($userdetail->username)) echo $userdetail->username;?>">
							</div> 
						</div-->
						<div class="section-field mb-3">
							<div class="field-widget">
								<i class="fa fa-envelope-o" aria-hidden="true"></i>
								<input tabindex="4" id="email" name="email" class="email" type="email" placeholder="<?php echo lang('lbl_register_email'); ?>*"  name="email" value="<?php if(isset($userdetail->email)) echo $userdetail->email;?>">
							</div> 
						</div>
						<div class="form-group">
							<label class="title divider-3 mb-3"><?php echo lang('lbl_register_gender'); ?>*</label>
							<div class="row">
								<div class="col-sm-4 xs-mb-2">
									<div class="radio">
										<input tabindex="5" name="gender" <?php echo ($userdetail->gender=="female")? "checked":""; ?> id="radio1" type="radio" value="female">
										<label for="radio1"><?php echo lang('lbl_register_female'); ?></label>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="radio">
										<input tabindex="6" name="gender" <?php echo ($userdetail->gender=="male")? "checked":""; ?> id="radio2" type="radio" value="male">
										<label for="radio2"><?php echo lang('lbl_register_male'); ?></label>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="title divider-3 mb-3"><?php echo lang('lbl_register_birthdate'); ?>*</label>
							<div class="row">
								<div class="col">
									<input tabindex="7"  type="number"  min="1" max="12" onkeyup="checkMonth(this.value)" class="form-control" name="month" id="exampleInputMonth1" placeholder="<?php echo lang('lbl_register_birthdate_month'); ?>*" required oninvalid="this.setCustomValidity('Month is Required')" onchange="setCustomValidity('')" value="<?php echo date_format(date_create($userdetail->dob),"m"); ?>">
								</div>
								<div class="col">
									<input  tabindex="8" type="number" min='1' max="31" onkeyup="checkDay(this.value)" class="form-control" id="exampleInputDay" placeholder="<?php echo lang('lbl_register_birthdate_day'); ?>*" name="day" required oninvalid="this.setCustomValidity('Day is Required')" oninput="setCustomValidity('')" value="<?php echo date_format(date_create($userdetail->dob),"d"); ?>">
								</div>
								<div class="col">
									<input tabindex="8" type="number" min="<?php echo (date("Y")-65)  ?>" max="<?php echo (date("Y")-15) ?>" onkeyup="checkYear(this.value)" onfocusout="checkYearF(this.value)" class="form-control" id="exampleInputyear" placeholder="<?php echo lang('lbl_register_birthdate_year'); ?>*" name="year" required  oninvalid="this.setCustomValidity('Year is Required')" oninput="setCustomValidity('')" value="<?php echo date_format(date_create($userdetail->dob),"Y"); ?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="title divider-3 mb-3"><?php echo lang('lbl_register_about'); ?>*</label>
							<textarea tabindex="9" class="form-control" maxlength="500" id="about" name="about" placeholder="<?php echo lang('lbl_register_about'); ?>*" rows="3"><?php if(isset($userdetail->about)) echo $userdetail->about;?></textarea>
						</div>
						<div class="form-group">
							<?php
							$hight="3'0 (92 cm), 3'1 (94 cm),3'2 (97 cm),3'3 (99 cm),3'4 (102 cm),3'5 (104 cm),3'6 (107 cm),3'7 (109 cm),3'8 (112 cm),3'9 (114 cm),3'10 (117 cm),3'11 (119 cm),4'0 (122 cm),4'1 (125 cm),4'2 (127 cm),4'3 (130 cm),4'4 (132 cm),4'5 (135 cm),4'6 (137 cm),4'7 (140 cm),4'8 (142 cm),4'9 (145 cm),4'10 (147 cm),4'11 (150 cm),5'0 (152 cm), 5'1 (155 cm),5'2 (158 cm),5'3 (160 cm),5'4 (163 cm),5'5 (165 cm),5'6 (168 cm),5'7 (170 cm),5'8 (173 cm),5'9 (175 cm),5'10 (178 cm),5'11 (180 cm),6'0 (183 cm),6'1 (185 cm),6'2 (188 cm),6'3 (191 cm),6'4 (193 cm),6'5 (196 cm),6'6 (198 cm),6'7 (201 cm),6'8 (203 cm),6'9 (206 cm),6'10 (208 cm),6'11 (211 cm),7'0 (213 cm)";
							$height=explode(",",$hight);					
							?>
								<label class="title divider-3 mb-3"><?php echo lang('lbl_register_height'); ?>*</label>
								<select class="selectpicker" id="height" name="height" value="<?php echo set_value('height'); ?>">
							<?php 	for($i=0;$i<count($height);$i++)
									{  ?>
										<option value="<?php echo $height[$i];  ?>" <?php echo ($userdetail->height==$height[$i])?"selected":"";?>><?php echo $height[$i];  ?></option>
							<?php  	} ?>
								</select>						
						</div>
						<div class="form-group mt-2">
							<label for="ethnicity" class="title divider-3 mb-3"><?php echo lang('lbl_register_ehtnticity'); ?>*</label>
							<select class="selectpicker" id="ethnicity" name="ethnicity">
								<?php 	
								foreach($ethnicity as $eathni)
								{ 
								?>										
									<option <?php echo (in_array($eathni['id'],explode(",",$userdetail->ethnicity)))?'selected':''?> value="<?php echo $eathni['id']; ?>">
									<?php 
											echo $eathni['name'];
										?>
									</option>
								<?php 	
								}
								?>
							</select>
						</div>
						<div class="form-group mt-2">
							<label for="disabledSelect" class="title divider-3 mb-3"><?php echo lang('lbl_profile_religion'); ?>*</label>
							<select id="religion" name="religion" class="selectpicker">
								<?php 
								foreach($religion as $relig){?>
									<option <?php echo (in_array($relig['id'],explode(",",$userdetail->religion)))?'selected':''?> value="<?php echo $relig['id']; ?>">
									<?php 
										echo $relig['name'];
									?>
									</option>
								<?php }
								?>
							</select>
						</div>
						<div class="form-group">
							<label for="kids" class="title divider-3 mb-3"><?php echo lang('lbl_register_kids'); ?></label>
							<select id="kids" name="kids" class="selectpicker">
								<option value="1" <?php echo ($userdetail->kids==1) ? 'selected' : '';?>><?php echo lang('lbl_register_kids'); ?> - 1</option>
								<option value="2" <?php echo ($userdetail->kids==2) ? 'selected' : '';?>><?php echo lang('lbl_register_kids'); ?> - 2</option>
								<option value="3" <?php echo ($userdetail->kids==3) ? 'selected' : '';?>><?php echo lang('lbl_register_kids'); ?> - 3</option>
								<option value="4" <?php echo ($userdetail->kids==4) ? 'selected' : '';?>><?php echo lang('lbl_register_kids'); ?> - 4</option>
								<option value="5" <?php echo ($userdetail->kids==5) ? 'selected' : '';?>><?php echo lang('lbl_register_kids'); ?> - 5</option>
								<option value="6" <?php echo ($userdetail->kids==6) ? 'selected' : '';?>><?php echo lang('lbl_register_kids'); ?> - 6</option>
								<option value="7" <?php echo ($userdetail->kids==7) ? 'selected' : '';?>><?php echo lang('lbl_register_kids'); ?> - 7</option>
								<option value="None" <?php echo ($userdetail->kids=='None') ? 'selected' : '';?>><?php echo lang('lbl_register_none'); ?></option>
								<option value="One Day" <?php echo ($userdetail->kids=='One Day') ? 'selected' : '';?>><?php echo lang('lbl_register_one_day'); ?></option>
								<option value="I Dont Want Kids" <?php echo ($userdetail->kids=='I Dont Want Kids') ? 'selected' : '';?>><?php echo lang('lbl_register_i_dont_want_kids'); ?></option>
							</select>
						</div>
						<div class="form-group">
							<label class="title divider-3 mb-3"><?php echo lang('lbl_register_address'); ?>*</label>
							<input type="text" class="form-control" id="address" name="address"  placeholder="<?php echo lang('lbl_register_address'); ?>*" value="<?php if(isset($userdetail->address)) echo $userdetail->address;?>">
						</div>
						<div class="section-field mb-3">
							<div class="field-widget">
								<i class="fa fa-graduation-cap" aria-hidden="true"></i>
								<input id="education" name="education" type="text" placeholder="<?php echo lang('lbl_register_education'); ?>*" value="<?php if(isset($userdetail->education)) echo $userdetail->education;?>">
							</div> 
						</div>
						<div class="section-field mb-3">
							<div class="field-widget">
								<i class="fa fa-briefcase" aria-hidden="true"></i>
								<input id="profession" name="profession" type="text" placeholder="<?php echo lang('lbl_register_profession'); ?>*" value="<?php if(isset($userdetail->profession)) echo $userdetail->profession;?>">
							</div> 
						</div>
						<!-- <div class="form-group mt-2">
							<div class="checkbox">
								<input name="terms" id="terms" type="checkbox">
								<label for="terms">
									<a href="<?php echo base_url(); ?>terms.html" target="_blank"><?php echo lang('lbl_register_i_agree_to_the'); ?>
										<span style="color:#2cc0d9"></span> <?php echo lang('lbl_register_tearms_agrement'); ?>
									</a>
								</label>
							</div>
						</div> -->
						<div class="clearfix"></div>
						<div class="section-field text-uppercase text-center mt-20">
							<a id="user-reg-btn" class="button  btn-lg btn-theme full-rounded animated right-icn" href="javascript:void(0);" ><span><?php echo lang('lbl_profile_update'); ?><i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY;?>&amp;libraries=geometry,places"  type="text/javascript"></script>
  <script>
  function initialize() {
                       var input = document.getElementById('address');
                       var autocomplete = new google.maps.places.Autocomplete(input);
               }
               google.maps.event.addDomListener(window, 'load', initialize);

   function checkMonth(v){
   		if(v<=12 && v>=1){
   			return true;
   		}
   		else{
   			$("#exampleInputMonth1").val("");
   			$("#exampleInputMonth1").focus();
   		}
   }
  	function checkDay(v){
   		if(v<=31 && v>=1){
   			return true;
   		}
   		else{
   			$("#exampleInputDay").val("");
   			$("#exampleInputDay").focus();
   		}
   }
  	function checkYear(v){
   		var d = new Date();
   		//debugger;
   		//alert($("#exampleInputyear").val());
   		if(v<=(d.getFullYear()-15) && v>=(d.getFullYear()-65)){
   			return true;
   		}
   		else if(v.toString().length<=4 && !isNaN(v)){
   			return true;
   		}
   		else{
   			$("#exampleInputyear").val(d.getFullYear()-25);
   		}
   }
   function checkYearF(v){
   		//debugger;
   		var d = new Date();
   		if(v<=(d.getFullYear()-15) && v>=(d.getFullYear()-65)){
   			return true;
   		}
   		else{
   			$("#exampleInputyear").val(d.getFullYear()-15);
   		}
   }
  </script>