<!--=================================
 banner -->
<?php 
$lange= $this->admin_model->get_default_language();
$language=$lange['status'];
$languages = $this->session->userdata('site_lang');
$lg=$languages;
//echo "<pre>";print_r($contact);die;
?>
<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url']; ?>)">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="position-relative divider"><?php echo lang('lbl_menu_name_contact'); ?><span class="sub-title"><?php echo lang('lbl_menu_name_contact'); ?></span></h1></div>
			</div>
			<div class="col-sm-12 mt-5 xs-mt-3">
				<ul class="page-breadcrumb mt-5">
					<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i> <?php echo lang('lbl_menu_name_home'); ?></a><i class="fa fa-angle-double-right"></i></li>
					<li><span><?php echo lang('lbl_menu_name_contact'); ?> </span> </li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!--=================================
 banner -->
<!--=================================
 Page Section -->
<section class="contact-sec page-section-pt gray-bg">
	<div class="container">
		<div class="row mb-5 xs-mb-3">
 			<div class="col-md-6">
				<?php if(!empty($blocks[9]['id'])){?>
					<div class="row mb-3">   
						<div class="col-sm-12 text-center">
							<h4 class="title divider-3 text-uppercase text-center mb-5 xs-mb-3"><?php echo $blocks[9]['title']; ?></h4>
							<p class="text-left"><?php echo $blocks[9]['contain'];?></p>
						</div>
					</div>
					<?php 
				}?>
				<div class="row">
					<div class="col-sm-6 mb-3">
						<div class="address-block fill">
							<h3 class="title text-uppercase"><?php echo lang('lbl_admin_user_detail_address'); ?> :</h3>
							<address><?php echo $contact[13]['status']; ?></address>
						</div>
					</div> 
					<div class="col-sm-6 mb-3">     
						<div class="address-block fill">
							<h3 class="title text-uppercase"><?php echo lang('lbl_admin_sign_in_username'); ?> :</h3>
							<a href="#"><?php echo $contact[14]['status']; ?></a>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">      
						<div class="address-block fill">
							<h3 class="title text-uppercase"><?php echo lang('lbl_contact_phone'); ?> :</h3>
							<span><?php echo lang('lbl_contact_landline'); ?> : <a href="tel:+911234567890"><?php echo $contact[16]['status']; ?></a></span>
							<span><?php echo lang('lbl_contact_mobile'); ?> : <a href="tel:+912345678900"><?php echo $contact[17]['status']; ?></a></span>
						</div>
					</div>
					<div class="col-sm-6">   
						<div class="address-block fill">
							<h3 class="title text-uppercase"><?php echo lang('lbl_contact_social_media'); ?> :</h3>
								<div class="social-icons color-hover">
									<ul>
										<li class="social-facebook"><a href="<?php echo $contact[6]['status']; ?>"><i class="fa fa-facebook"></i></a></li>
										<li class="social-twitter"><a href="<?php echo $contact[7]['status']; ?>"><i class="fa fa-twitter"></i></a></li>
										<li class="social-linkedin"><a href="<?php echo $contact[8]['status']; ?>"><i class="fa fa-instagram"></i></a></li>
									</ul>
								</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row mb-3">   
					<div class="col-sm-12 text-center">
						<h4 class="title divider-3 text-uppercase text-center mb-5 xs-mb-3"><?php echo lang('lbl_contact_contact_from'); ?></h4>
					</div>
				</div>
				<div class="defoult-form">
				<?php if($this->session->flashdata('success')) { ?>
			            <div class="row" align="center">
							<div class="col-md-12 ">
							   <div class="alert mysuccessmsg" role="success">
									<?php echo $this->session->flashdata('success'); ?>
							   </div>
							</div>
						</div>
						<?php 
					} ?>
					<?php if($this->session->flashdata('fail')) { ?>
			            <div class="row" align="center">
							<div class="col-md-12 ">
							   <div class="alert myerrormsg" role="success">
									<?php echo $this->session->flashdata('fail'); ?>
							   </div>
							</div>
						</div>
						<?php
					} ?>
					<?php 	if (validation_errors()) : ?>
						<div class="row" align="center">
							<div class="col-md-12 ">
							   <div class="alert myerrormsg" role="alert">
									<?php echo validation_errors(); ?>
							   </div>
							</div>
						</div>
					<?php endif; ?>
					<form id="contact-form" method="post" action="<?php echo base_url('contact/contact_insert');?>">
						<div class="form-group">
							<input type="text" name="name" class="form-control" value="<?php echo set_value('name'); ?>" id="InputName" placeholder="<?php echo lang('lbl_contact_name'); ?>*">
						</div>
							<div class="form-group">
							<input type="email" class="form-control" name="email" id="InputEmail1" placeholder="<?php echo lang('lbl_register_email'); ?>*"  value="<?php echo set_value('email'); ?>">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="subject" id="InputSubject" placeholder="<?php echo lang('lbl_contact_subject'); ?>*" value="<?php echo set_value('subject'); ?>">
						</div>
						<div class="form-group">
							<div class="input-group">
								<textarea class="form-control input-message" placeholder="<?php echo lang('lbl_contact_comment'); ?>*" rows="5" name="message" ><?php echo set_value('message'); ?></textarea>
							</div>
						</div>
						<div class="form-group">
						 <button type="submit" class="button btn-lg btn-theme full-rounded animated right-icn">
						 <span><?php echo lang('lbl_contact_submit_now'); ?><i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></button>
						</div>
					</form>
					<div id="ajaxloader" style="display:none"><img class="center-block" src="<?php echo base_url('images/loading.gif');?>" alt=""></div> 
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row no-gutter">
			<div class="col-sm-12 ">
				<iframe  src="<?php echo $contact[15]['status'];?>" height="400" width="100%" allowfullscreen>
				</iframe>
			</div>
		</div>
	</div>
</section>
<!--=================================
 page-section -->