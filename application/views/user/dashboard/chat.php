<div id="conversejs" style="position: relative;width: 100%;z-index: 2;/*margin-top: 160px;width: 100%;height: 60%;top: 0;left: 0;right: 0;bottom: 0;z-index: 2;cursor: pointer;*/">
</div>
<?php $logged_in_user = $this->session->userdata('logged_in'); ?>
<?php
$this->XMPP_ENABLE = $this->Common_model->get_key_configuration(array('key'=>'XMPP_ENABLE'));
$this->XMPP_SERVER = $this->Common_model->get_key_configuration(array('key'=>'XMPP_SERVER'));
$this->XMPP_DEFAULT_PASSWORD = $this->Common_model->get_key_configuration(array('key'=>'XMPP_DEFAULT_PASSWORD'));
$this->XMPP_HTTP_BIND = 'http://'.$this->XMPP_SERVER.':5280/http-bind';
?>
<script>
var XMPP_HTTP_BIND='<?php echo $this->XMPP_HTTP_BIND; ?>';
var EJUSER='<?php echo $ejuser; ?>';
var XMPP_SERVER='<?php echo $this->XMPP_SERVER; ?>';
var XMPP_DEFAULT_PASSWORD='<?php echo $this->XMPP_DEFAULT_PASSWORD; ?>';
var cafe=<?php echo $cafe; ?>;
var bar=<?php echo $bar; ?>;
var restaurant=<?php echo $restaurant; ?>;
var beach=<?php echo $beach; ?>;
</script>

<!--date request modal-->
<div class="modal fade" id="daterequestmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?php echo lang('lbl_date_request'); ?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="daterequesticons" style="display:none">
				<ul id="sortable" class="sortable" style="list-style:none;"> <li class="datepref ui-sortable-handle" id="1" value="1" style="border:0px"> <div class="Preference-icon"><img class="img-center" alt="#" src="<?php echo base_url();?>Newassets/images/dateimg1.png" data-type="cafe"></div></li><li class="datepref ui-sortable-handle" id="2" value="2" style="border:0px"><div class="Preference-icon"><img class="img-center"  alt="#" src="<?php echo base_url();?>Newassets/images/dateimg2.png" data-type="bar"></div></li><li class="datepref ui-sortable-handle" id="3" value="3" style="border:0px"><div class="Preference-icon"><img class="img-center"  alt="#" src="<?php echo base_url();?>Newassets/images/dateimg3.png" data-type="restaurant"></div></li><li class="datepref ui-sortable-handle" id="4" value="4" style="border:0px"><div class="Preference-icon"><img class="img-center" alt="#" src="<?php echo base_url();?>Newassets/images/dateimg4.png" data-type="beach"></div></li></ul>
			</div>
			<div class="places" style="display:none"></div>
			<div class="daterequestdatetime" style="display:none"><input type="text" id="datetimepicker_format" class="TimeOfDate input input-wide" /><input type="button" class="btnsendrequest" data-receiverid="'+receiverid+'" value="Send Date Request"/></div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo lang('lbl_close'); ?></button>
		</div>
		</div>
	</div>
</div>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>Newassets/css/inverse.css">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>Newassets/css/daterequest.css">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>Newassets/css/jquery.datetimepicker.css">
<style>
.message.chat-info.chat-date{display:none !important}
.message-from-them {
	background: rgba(242, 255, 0, 0.46);
	text-align: right;
	padding: 10px 15px !important;
	border-radius: 10px;
	border: 1px solid #ccc;
	float: right;
	clear: both;
	margin: 1px !important;
}
.message-from-me {
	background: rgba(7, 255, 0, 0.39);
	text-align: left;
	padding: 10px 15px !important;
	border-radius: 10px;
	border: 1px solid #ccc;
	float: left;
	clear: both;
	margin: 1px !important;
}
.flyout.box-flyout{
	border-top-width: 1px !important;
	border-bottom-width: 1px !important;
	border-left-width: 1px !important;
	border-bottom-color: black !important;
	border-left-color: black !important;
	border-right-color: black !important;
	border-top-color:black !important;
}

.chat-head.controlbox-head{
	margin:0px !important;
}
img.avatar{
	margin-top:8px !important;
	margin-left:5px !important;
}
.chat-title{
	padding-top:12px !important;
	
}
.modal-body{
	color: #000000 !important;
}
.chatbox.logged-out{display:none !important}
</style>
<script>
	var dtrequestreceiverid='';
	var default_place_image='<?php echo base_url();?>images/default-place-placeholder.png';
	var chatboxid="";
</script>