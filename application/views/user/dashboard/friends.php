<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url']; ?>);">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="position-relative divider"><?php echo lang('lbl_sidebar_friends'); ?> <span class="sub-title"><?php echo lang('lbl_sidebar_friends'); ?></span></h1></div>
			</div>
			<div class="col-sm-12 mt-5 xs-mt-3">
				<ul class="page-breadcrumb mt-5">
					<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i> <?php echo lang('lbl_menu_name_home'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><a href="<?php echo base_url('user/dashboard');?>"><?php echo lang('lbl_menu_name_profiles'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><span><?php echo lang('lbl_sidebar_friends'); ?></span> </li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!--=================================
 banner -->
<!--=================================
 your friens -->
<section class="page-section-ptb profile-slider"> 
    <div class="container">
        <?php if(!empty($content))
			{?>
			<div class="row mb-5 xs-mb-3">
				<div class="col-sm-12 text-center">
					<h2 class="title divider"><?php echo lang('lbl_friends_your_friend'); ?></h2>
				</div>
			</div>
			<?php 
        }
        else{
			?>
			<div class="row mb-5 xs-mb-3">
				<div class="col-sm-12 text-center">
					<h2><?php echo lang('lbl_friends_no_friends'); ?></h2>
				</div>
			</div>
			<?php
        }
		if(isset($content) && !empty($content))
		{
			?>
			<div class="row mb-50 xs-mb-30">
				<?php
				$i=0;
				foreach($content as $friend) 
				{ 
					if($i%4==0)
					echo '</div><div class="row mb-5 xs-mb-3">';
					?>
					<div class="col-sm-3 xs-mb-3">
						<a href="<?php echo base_url().'friends/friend_profile/'.$friend['send_user_id']; ?>" class="profile-item">
							<div class="profile-image  clearfix">
								<?php 
									if(file_exists(DIRECTORY_PATH."uploads/thumbnail/".$friend['profile_image']) and $friend['profile_image']!="")
									{ ?>
										<img src="<?php echo base_url("uploads/thumbnail/".$friend['profile_image']); ?>" class="img-fluid"/>
								<?php 	}
									else
									{ 
								?>
										<img src="<?php echo base_url("/Newassets/images/profile/01.jpg"); ?>" class="img-fluid" />
								<?php 	} 
									 ?>
							</div>
							<div class="profile-details profile-text">
								<h5 class="title"><?php echo $friend['fname'].' '.$friend['lname']; ?></h5>
								<span class="text-black">
									<?php $today=date('Y-m-d');
										$dob=$friend['dob'];				
										$diff = date_diff(date_create($dob),date_create($today));
										echo $age = $diff->format('%Y');
									  ?> 
									<?php echo lang('lbl_friends_years_old'); ?>
								</span>
							</div>
						 </a>
					</div>
				   <?php
				   $i++; 
				}?>
			</div>
			<?php    
		}
	   ?>
    </div>
</section>
<!--=================================
 last add friends -->
<section class="page-section-ptb profile-slider pb-3 xs-pb-5 grey-bg"> 
    <div class="container">
        <?php 
        if(!empty($content))
		{
			?>
			<div class="row justify-content-center mb-3 xs-mb-0">
				<div class="col-md-10 text-center">
					<h2 class="title divider"><?php echo lang('lbl_friends_last_added'); ?></h2>
				</div>
			</div>
			<?php 
        }
        ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="owl-carousel" data-nav-arrow="true" data-items="4" data-md-items="4" data-sm-items="3" data-xs-items="2" data-xx-items="1" data-space="30">
					<?php 	
					foreach($content as $friend) 
					{ 
						?>
    					<div class="item">
    						<a href="<?php echo base_url().'friends/friend_profile/'.$friend['send_user_id']; ?>" class="profile-item">
                                <div class="profile-image fix-size clearfix">
									<?php 
									if(file_exists(DIRECTORY_PATH."uploads/thumbnail/".$friend['profile_image']) and $friend['profile_image']!=""){ ?>
										<img src="<?php echo base_url("uploads/thumbnail/".$friend['profile_image']); ?>" class="img-fluid" />
										<?php 
									}else{ 
										?>
										<img src="<?php echo base_url("/Newassets/images/profile/01.jpg"); ?>" class="img-fluid" />
										<?php 
									} ?>
                                </div>
    							<div class="profile-details text-center">
    								<h5 class="title"><?php echo $friend['fname'].' '.$friend['lname']; ?></h5>
    								<span><?php $today=date('Y-m-d');
    									$dob=$friend['dob'];				
    									$diff = date_diff(date_create($dob),date_create($today));
    									echo $age = $diff->format('%Y');?> <?php echo lang('lbl_friends_years_old'); ?>
    								</span>
    							</div>
    						</a>
    					</div>
						<?php 	
					}
					?>   
                </div>
            </div>
        </div>
    </div>
</section>