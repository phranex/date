<div class="content">
	<section>
		<div class="container-fluid text-center">
			<div class="row mt-50">
				<?php 
				if(!empty($cms))
				{
					?>
					<div class="post-details">
						<div class="post-title mt-20">
							<h5 class="title text-uppercase mt-20"><a href="#"><?php echo $cms['title'];?></a></h5>
							<div><img class="img-center" src="<?php echo base_url('images/title-bdr.png'); ?>" alt=""></div>
						</div>                           
						<div class="post-icon">
							<div class="post-content">
								 <?php echo $cms['content'];?>
							</div>             
						</div>
					</div>	   
					<?php 	
				}
				else
				{
					?>
					<div>
						<h3><?php echo "No Match Found"; ?></h3>
					</div>
					<?php
				}
				?>      
			</div>
		</div>
	</section>
</div>