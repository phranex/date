<?php 
$lange= $this->admin_model->get_default_language();
$language=$lange['status'];
$languages = $this->session->userdata('site_lang');
$lg=$languages;
?>
<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url']; ?>)">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="pos-r divider"><?php echo lang('lbl_blog_our_blogs'); ?> <span class="sub-title"><?php echo lang('lbl_blog_micro_blog'); ?></span></h1></div>
			</div>
			<div class="col-sm-12 mt-5 xs-mt-3">
				<ul class="page-breadcrumb mt-5">
					<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i> <?php echo lang('lbl_menu_name_home'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><span><?php echo lang('lbl_blog_name'); ?></span></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!--=================================
 banner -->
<!--=================================
 Page Section -->
<section class="page-section-ptb" ><div class="container">
	<div class="row">
    	<div class="col-sm-12 mb-5 xs-mb-3">
            <div class="text-center">
			     <h2 class="title divider"><?php echo lang('lbl_blog_from_the_blog'); ?></h2>
			</div>
        </div>
    </div>
    <div class="row post-article">                
        <div class="col-sm-12">
			<?php 
			if(isset($db_blog))
			{
				foreach($db_blog as $blog)
				{
					if(!empty($blog['id']))
					{
					?>
						<div class="post post-artical mb-5">
							<div class="post-image clearfix">
							<?php
								if(!empty($blog['image']))
								{
									$str=base_url("Newassets/images/thumbnail/".$blog['image']);
									 if(file_exists(DIRECTORY_PATH."Newassets/images/thumbnail/".$blog['image']))
									 {      
										  ?>
										  <img class="img-fluid" src="<?php echo $str; ?>" alt="">
										  <?php
									 }
									 else
									 {
										  ?>
										  <img class="img-fluid" src="<?php echo $str;?>" alt="">
										  <?php
									 }       
								}
								else
								{
									 ?>
									   <img class="img-fluid" src="<?php echo base_url('images/blog/01.jpg');?>" alt="">
									 <?php
								}
								?> 
							</div>
							<div class="post-details">
								<div class="post-title mt-2">
									<h5 class="title text-uppercase mt-2">
										<a href="<?php echo base_url('blog/blog_details/'.$blog['id']);?>"><?php echo $blog['title'];?></a>
									</h5>
								</div>
								<p><?php echo date_format(date_create($blog['created_date']),"F , Y")?> <?php echo lang('lbl_home_by'); ?><a href="<?php echo base_url("blog/index/".str_replace(" ", "_", $blog['author']))?>"><?php echo $blog['author'];?></a></p>              
								<div class="post-icon">
									<div class="post-content">
										<p><?php echo $blog['description'];?></p>
									</div>
									<a class="button" href="<?php echo base_url('blog/blog_details/'.$blog['id']);?>"><?php echo lang('lbl_home_read_more'); ?>..</a>
								</div>
							</div>                
						</div> 
					<?php 
					}
				}
			}
			?> 
      </div>
  </div>
<div class="col-sm-12 text-center">
    <nav class="pagination-nav mt-4">
      <?php
         echo $this->pagination->create_links();
      ?>
    </nav>    
</div>
   </div>
</section>