<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url']; ?>);">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="position-relative divider"> <?php echo lang('lbl_preference_my_name'); ?><span class="sub-title"> <?php echo lang('lbl_preference_my_name'); ?></span></h1></div>
			</div>
			<div class="col-sm-12 mt-5 xs-mt-3">
				 <ul class="page-breadcrumb mt-5">
					<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i>  <?php echo lang('lbl_menu_name_home'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><span> <?php echo lang('lbl_preference_my_name'); ?></span> </li>
				 </ul>
			</div>
		</div>
	</div>
</section>
<section class="page-section-ptb text-white" style="background: url(<?php echo $background7['background_url']; ?>) no-repeat 0 0; background-size: cover;">
	<div class="container" style="max-width: 100% !important;">
		<div class="row justify-content-center">
			<div class="col-md-8">  
				<div class="step-form">
					<?php 
						if(!empty($this->session->flashdata('success')))
						{
							?>
							<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert mysuccessmsg" role="success">
										<?php echo $this->session->flashdata('success'); ?>
								   </div>
								</div>
							</div>
							<?php
						}
						else if(!empty($this->session->flashdata('fail')))
						{
							?>
							<div class="row" align="center">
								<div class="col-md-12 ">
								   <div class="alert myerrormsg" role="alert">
										<?php echo $this->session->flashdata('fail'); ?>
								   </div>
								</div>
							</div>
							<?php
						}
						if (isset($error)) : ?>
							<div class="row">
								<div class="col-md-12">
									<div class="alert myerrormsg" role="alert">
									<?php echo $error; ?>
									</div>
								</div>
							</div>
					<?php 	endif; ?>
					<?php if (validation_errors()) : ?>
							<div class="row">
								<div class="col-md-12">
								   <div class="alert myerrormsg" role="alert">
										<?php echo validation_errors(); ?>
								   </div>
								</div>
							</div>
					<?php endif; ?>
					<form action="<?php echo base_url('user/my_preferance'); ?>" method="post" >
						<div class="row setup-content" id="step-3">
							<div class="col-lg-12">
								<div class="row justify-content-center">
									<div class="col-lg-8 text-capitalize text-white text-center">
										<h4 class="title divider-3 text-white mb-3"><?php echo lang('lbl_preference_my_name'); ?></h4>
										<div class="form-group mt-2">
											<input type="hidden" id="id" name="id" value="<?php if(isset($content->id)) echo $content->id;?>">
											<label class="title divider-3 mb-3"><?php echo lang('lbl_register_interested_in'); ?>*</label>
											<div class="row">
												<div class="col-sm-4 xs-mb-2">
													<div class="radio">
														<input type="radio" name="gender_pref" id="radio3" value="female" <?php echo ($content->gender_pref=="female")? "checked":""; ?>>
														<label for="radio3"><?php echo lang('lbl_profile_female'); ?></label>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="radio">
														<input type="radio"  name="gender_pref" id="radio4" value="male" <?php echo ($content->gender_pref=="male")? "checked":""; ?>>
														<label for="radio4"><?php echo lang('lbl_profile_male'); ?></label>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group mt-2">
											<label class="title divider-3 mb-3" for="exampleInputEmail1"><?php echo lang('lbl_profile_date_preferances'); ?>*</label>
												<ul id="sortable" class="sortable" style="list-style:none;">
													 <?php 
													 $dateprefvalue=array("Cafe","Bar","Hotel","Joy");
													 foreach($datepref as $key=>$date){ ?>
														<li class=" datepref" id="<?php echo $key; ?>" value="<?php echo $key; ?>" style="border:0px">
														   <div class="Preference-icon"><img class="img-center" alt="#" src="<?php echo base_url("Newassets/images/dateimg$key.png"); ?>"></div><div class="text"><?php echo $dateprefvalue[$key-1] ?></div>
														</li>
													 <?php } ?>
												 </ul>
												<input type="hidden" id="datepref" name="datepref" value="<?php echo $content->date_pref;  ?>">
										 </div>	
										<div class="form-group mt-2">
											<label class="title divider-3 mb-5"><?php echo lang('lbl_register_age_preferances'); ?>*</label>
											<div>
												<input id="ex2" type="text" class="span2" value="" data-slider-min="18" data-slider-max="100" data-slider-step="1" data-slider-value="[<?php echo $content->min_age_pref; ?>,<?php echo $content->max_age_pref; ?>]"/>
												<div id="slider-range"></div>
												<input id="slider1"  type="hidden" name="age-min" id="age-min"  value="<?php echo $content->min_age_pref; ?>"  />
												<input id="slider2"  type="hidden"  name="age-max" id="age-max" value="<?php echo $content->max_age_pref; ?>"  />
											</div>
										</div>
										<div class="form-group mt-2">
											<label class="title divider-3 mb-5"><?php echo lang('lbl_register_distance_preferances'); ?>*</label>
											<div>
												<input id="ex3" type="text" class="span2" value="" data-slider-min="0" data-slider-max="200" data-slider-step="1" data-slider-value="[<?php echo $content->min_dist_pref; ?>,<?php echo $content->max_dist_pref; ?>]"/>
												<input id="slider3" type="hidden" name="dist-min" id="dist-min" value="<?php echo $content->min_dist_pref; ?>" />
												<input id="slider4" type="hidden" name="dist-max" id="dist-max" value="<?php echo $content->max_dist_pref; ?>" />
											</div>
										</div>				
										<div class="form-group mt-2">
											<label for="ethnicity" class="title divider-3 mb-3"><?php echo lang('lbl_register_ehtnticity'); ?></label>
											<select class="selectpicker" id="ethnicity" name="ethnicity[]" multiple>
												<?php 	
												foreach($ethnicity as $eathni)
												{ 
												?>										
													<option <?php echo (in_array($eathni['id'],explode(",",$content->ethnicity_pref)))?'selected':''?> value="<?php echo $eathni['id']; ?>">
													<?php 
															echo $eathni['name'];
														?>
													</option>
												<?php 	
												}
												?>
											</select>
										</div>
										<div class="form-group mt-2">
											<label for="disabledSelect" class="title divider-3 mb-3"><?php echo lang('lbl_profile_religion'); ?></label>
											<select id="religion" name="religion[]" class="selectpicker" multiple>
												<?php 
												foreach($religion as $relig){?>
													<option <?php echo (in_array($relig['id'],explode(",",$content->religion_pref)))?'selected':''?> value="<?php echo $relig['id']; ?>">
														<?php 									
															echo $relig['name'];
														?>
													</option>
												<?php }
												?>
											</select>
										</div>
										<div class="form-group mt-2">
											<label for="disabledSelect"  class="title divider-3 mb-3"><?php echo lang('lbl_register_question'); ?>*</label>
											<select class="selectpicker" id="question" name="question">
												<?php 											
												foreach($questions as $question)
												{								
												?>
													<option <?php echo ($question['id']==$content->que_id)?'selected':'';?> value="<?php echo $question['id']; ?>" >
													<?php 
														echo $question['name'];
														?>
													</option>
												<?php 	
												}
												?>
											</select>
										</div>
										<div class="form-group mt-2">
											<label class="title divider-3 mb-3"><?php echo lang('lbl_register_question_answer'); ?>*</label>
											<input type="text" maxlength="100" class="form-control" id="question_ans" name="question_ans" value="<?php echo $content->que_ans; ?>">
										</div>
										<input name="access_loc" checked="checked" id="radio5" type="hidden" value="1">
										<div class="form-group mb-0 text-center">
											<button type="submit" class="button btn-theme full-rounded btn nextxBtn btn-lg mt-20 animated right-icn" id="btnstep3"><?php echo lang('lbl_profile_update'); ?><i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></button>
										</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div> 
		</div>
	</div>
</section>