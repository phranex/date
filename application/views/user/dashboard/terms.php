<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url']; ?>)">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="position-relative divider"><?php echo lang('lbl_menu_name_terms_conditions'); ?><span class="sub-title"><?php echo lang('lbl_menu_name_terms_conditions'); ?></span></h1></div>
			</div>
			<div class="col-sm-12 mt-5 xs-mt-3">
				<ul class="page-breadcrumb mt-5">
					<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i> <?php echo lang('lbl_menu_name_home'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><span><?php echo lang('lbl_menu_name_terms_conditions'); ?></span></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<?php
foreach($terms as $termscondition){
	if(!empty($termscondition['id'])){ ?>
		<section class="terms-and-conditions page-section-ptb">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 text-center mb-5 xs-mb-3"><h2 class="title divider"><?php echo $termscondition['title']; ?></h2></div>
					<div class="col-sm-12">
						<?php echo $termscondition['contain']; ?>
					</div>
				</div>
			</div>
		</section>
		<?php
	}
}
?>