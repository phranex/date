<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
	<div class="page_wrapper">
    	<div class="row padd-bottom">
			<div class="col-sm-12">
				<h1 class="page-header"><?php echo lang('lbl_menu_name_preferences'); ?></h1>
			</div>
		</div>
		<div class="row">
	  <?php if (validation_errors()) : ?>
            	<div class="row" align="center">
					<div class="col-md-12 ">
					   <div class="alert myerrormsg" role="error">
							<?= validation_errors() ?>
					   </div>
					</div>
				</div>
	<?php 	endif; ?>
	  <?php if (isset($error)) : ?>
			<div class="row" align="center">
				<div class="col-md-12">
					<div class="alert myerrormsg" role="alert">
						<?= $error ?>
					</div>
				</div>
			</div>
	<?php 	endif; ?>
	<?php echo form_open_multipart('', array('role'=>'form')); ?>
				<input type="hidden" name="user_id" value="<?php echo $content->id; ?>" />
				<input type="hidden" name="username" value="<?php echo $content->username; ?>" />
				<div class="form-group">
					<label for="address"><?php echo lang('lbl_register_address'); ?></label>
					<input id="address" class="form-control"  value="<?php echo $content->address; ?>" name="address" type="text" size="50" placeholder="Enter a location" autocomplete="on">
				</div>
				<div class="form-group">
					<label for="religion"><?php echo lang('lbl_register_religion'); ?></label>
					<select class="form-control" id="religion" name="religion" >
				<?php 	foreach($religion as $relig)
						{?>
							<option <?php echo ($relig['id']==$content->religion)?'selected':''?> value="<?php echo $relig['id']; ?>"><?php echo $relig['name']; ?></option>
				  <?php } ?>
					</select>
				</div>
				<div class="form-group">
					<label for="kids"><?php echo lang('lbl_register_kids'); ?></label>
					<select class="form-control" id="kids" name="kids">
						<option <?php echo ($content->kids==1)?'selected':''; ?> value="1"><?php echo lang('lbl_register_kids'); ?> - 1</option>
						<option <?php echo ($content->kids==2)?'selected':''; ?> value="2"><?php echo lang('lbl_register_kids'); ?> - 2</option>
						<option <?php echo ($content->kids==3)?'selected':''; ?> value="3"><?php echo lang('lbl_register_kids'); ?> - 3</option>
						<option <?php echo ($content->kids==4)?'selected':''; ?> value="4"><?php echo lang('lbl_register_kids'); ?> - 4</option>
						<option <?php echo ($content->kids==5)?'selected':''; ?> value="5"><?php echo lang('lbl_register_kids'); ?> - 5</option>
						<option <?php echo ($content->kids==6)?'selected':''; ?> value="6"><?php echo lang('lbl_register_kids'); ?> - 6</option>
						<option <?php echo ($content->kids==7)?'selected':''; ?> value="7"><?php echo lang('lbl_register_kids'); ?> - 7</option>
						<option <?php echo ($content->kids==8)?'selected':''; ?> value="8"><?php echo lang('lbl_register_none'); ?></option>
						<option <?php echo ($content->kids==9)?'selected':''; ?> value="9"><?php echo lang('lbl_register_one_day'); ?></option>
						<option <?php echo ($content->kids==10)?'selected':''; ?> value="10"><?php echo lang('lbl_register_i_dont_want_kids'); ?></option>
					</select>
				</div>
				<div class="form-group">
					<label for="date_pref"><?php echo lang('lbl_register_date_preferances'); ?></label>
					<ul id="sortable" class="sortable" style="list-style:none">
				<?php 	foreach($datepreference as $key=>$value)
						{ ?>
							<li  class="form-group ui-state-default datepref" id="<?php echo $key; ?>" ><?php echo $value; ?></li>
				<?php  	} ?>
					</ul>
					<input type="hidden" id="datepref" name="datepref" value="1,2,3,4">
				</div><br /><br />
				<div class="form-group">
					<label><?php echo lang('lbl_register_gender'); ?></label>
					<input type="radio" name="gender_pref" value="male" <?php echo ($content->gender=='male')?'checked':'' ?>><?php echo lang('lbl_register_male'); ?>
					<input type="radio" name="gender_pref" value="female" <?php echo ($content->gender=='female')?'checked':'' ?>><?php echo lang('lbl_register_female'); ?>
				</div>
				<div class="form-group">
					<label><?php echo lang('lbl_register_age_preferances'); ?></label>
					<br/>
					<label for="age-min" ><?php echo lang('lbl_register_min_age'); ?></label>
					<input type="text" class="form-control" name="age-min" id="age-min" value="<?php echo $content->min_age_pref; ?>">
					<label for="age-max" ><?php echo lang('lbl_register_max_age'); ?></label>
					<input type="text" class="form-control" name="age-max" id="age-max" value="<?php echo $content->max_age_pref; ?>">
				</div>
				<div class="form-group">
					<label><?php echo lang('lbl_register_distance_preferances'); ?></label>
					<br/>
					<label for="dist-min"><?php echo lang('lbl_register_min_distance'); ?></label>
					<input type="text" class="form-control" name="dist-min" id="dist-min" value="<?php echo $content->min_dist_pref; ?>">
					<label for="dist-max"><?php echo lang('lbl_register_max_distance'); ?></label>
					<input type="text" class="form-control" name="dist-max" id="dist-max" value="<?php echo $content->max_dist_pref; ?>">
				</div>
				<div class="form-group">
					<label for="ethnicity"><?php echo lang('lbl_register_ehtnticity'); ?></label>
					<select class="form-control" id="ethnicity" name="ethnicity">
				<?php 	foreach($ethnicity as $eathni)
						{?>
							<option <?php echo ($eathni['id']==$content->ethnicity)?'selected':''?> value="<?php echo $eathni['id']; ?>"><?php echo $eathni['name']; ?></option>
				  <?php } ?>
					</select>
				</div>
				<div class="form-group">
					<label><?php echo lang('lbl_register_question_location'); ?></label>
					<input type="radio" name="access_loc" <?php echo ($content->access_location=='1')?'checked':'' ?> value="1"><?php echo lang('lbl_register_question_location_yes'); ?> 
					<input type="radio" name="access_loc" <?php echo ($content->access_location=='0')?'checked':'' ?> value="0"><?php echo lang('lbl_register_question_location_no'); ?> 
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-default" value="<?php echo lang('lbl_profile_update'); ?>">
				</div>
			<?php echo form_close();  ?>
		</div>
	</div>
	<script>
	$(document).ready(function(){
		$( ".sortable" ).sortable();
		$(".sortable").sortable({
		  update: function( event, ui ) {
				var strItems = "";
				$("#sortable").children().each(function (i) {
					var li = $(this);
					strItems += li.attr("id")+",";
				});
				var strItems = strItems.substring(0, strItems.length-1);
				$("#datepref").val(strItems);
		  }
		});
	});
	</script>