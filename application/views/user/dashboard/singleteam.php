<?php 
$lange= $this->admin_model->get_default_language();
$language=$lange['status'];
$languages = $this->session->userdata('site_lang');
$lg=$languages;
?>
<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url']; ?>)">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="pos-r divider"><?php echo lang('lbl_signal_team'); ?><span class="sub-title"><?php echo lang('lbl_signal_team'); ?></span></h1></div>
			</div>
			<div class="col-sm-12 mt-70">
				<ul class="page-breadcrumb">
					<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i> <?php echo lang('lbl_menu_name_home'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><span><?php echo lang('lbl_signal_team'); ?></span></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="page-section-ptb pb-50">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12 mb-30"><img class="img-fluid full-width" src="<?php echo base_url('Newassets/images/'.$member->member_image);?>" alt="">
			</div>
			<div class="col-md-6 col-sm-12">
				<div class="clearfix">
					<div class="section-title pull-left"><h2 class="title text-blue"><?php echo $member->member_name; ?></h2> <span class="pos"><?php echo $member->designation; ?></span></div>
					<div class="team-social-icon pull-right social-icons border color-hover">
						<ul>
							<?php if($member->facebook_url!=''){ ?><li class="social-facebook"><a href="<?php echo $member->facebook_url; ?>"><i class="fa fa-facebook"></i></a></li><?php } ?>
							<?php if($member->twitter_url!=''){ ?><li class="social-twitter"><a href="<?php echo $member->twitter_url; ?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
							<?php if($member->google_url!=''){ ?><li class="social-gplus"><a href="<?php echo $member->google_url; ?>"><i class="fa fa-google-plus"></i></a></li><?php } ?>
							<?php if($member->dribble_url!=''){ ?><li class="social-dribbble"><a href="<?php echo $member->dribble_url; ?>"><i class="fa fa-dribbble"></i></a></li><?php } ?>
						</ul>
					</div>
				</div>
				<p class="lead"><?php echo $member->member_detail; ?></p> 
			 </div>
		</div>
	</div>
</section>
<?php
foreach($our_activity_section as $our_activity){
	if(!empty($our_activity[$lg]['id'])){
		echo $our_activity[$lg]['contain'];
	}else{
		echo $our_activity[$language]['contain'];
	}
}
?>
<section class="page-section-ptb pb-0 grey-bg">
	<div class="container">
		<div class="row valign">
			<div class="col-md-7 col-sm-12 pb-50">
				<div class="section-title text-left text-black">
					<h2 class="title"><?php echo lang('lbl_do_you_want_to_contact_me'); ?></h2>
					<p><?php echo lang('lbl_do_you_want_to_contact_me_contain'); ?></p>
				</div>
				<?php if($this->session->flashdata('success')) { ?>
			            <div class="row" align="center">
							<div class="col-md-12 ">
							   <div class="alert mysuccessmsg" role="success">
									 <?php echo $this->session->flashdata('success'); ?>
							   </div>
							</div>
						</div>	
						<!-- <div class="alert alert-success">
						  <strong>Success !</strong> <?php echo $this->session->flashdata('success'); ?>
						</div> -->
						<?php 
					} ?>
				<?php if($this->session->flashdata('fail')) { ?>
			            <div class="row" align="center">
							<div class="col-md-12 ">
							   <div class="alert myerrormsg" role="success">
									 <?php echo $this->session->flashdata('fail'); ?>
							   </div>
							</div>
						</div>						
						<?php 
					} ?>
					<form id="contact-form" class="form input3-col" method="post" action="<?php echo base_url('team/team_contact_insert');?>">
						<div class="row" align="center">
							<div class="col-md-12 ">
							   <div class="alert" role="alert">
									<?php echo validation_errors(); ?>
							   </div>
							</div>
						</div>
						<div class="form-group half-group">
							<input type="text" name="name" class="form-control" id="InputName" placeholder="<?php echo lang('lbl_contact_name'); ?>*">
							<input type="hidden" name="team_id" value="<?php echo $member->id; ?>" />
						</div>
						<div class="form-group half-group">
							<input type="email" class="form-control" name="email" id="InputEmail1" placeholder="<?php echo lang('lbl_register_email'); ?>*">
						</div>
						<div class="form-group half-group">
							<input type="text" class="form-control" name="subject" id="InputSubject" placeholder="<?php echo lang('lbl_contact_subject'); ?>*">
						</div>
						<div class="form-group">
							<div class="input-group">
								<textarea class="form-control input-message" placeholder="<?php echo lang('lbl_contact_comment'); ?>*" rows="5" name="message"></textarea>
							</div>
						</div>
						<div class="form-group">
							 <button type="submit" class="button btn-lg btn-theme full-rounded animated right-icn">
							 <span><?php echo lang('lbl_contact_submit_now'); ?><i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></button>
						</div>
					</form>
				<div id="ajaxloader" style="display:none"><img class="center-block" src="<?php echo base_url('images/loading.gif');?>" alt=""></div>
			</div>
			<div class="col-md-5 hidden-sm" data-valign-overlay="bottom">
				<img src="http://themes.potenzaglobalsolutions.com/cupidlovedesign/webapp/Newassets/images/action-box-img.png" alt="">
			</div>
		</div>
	</div>
</section>