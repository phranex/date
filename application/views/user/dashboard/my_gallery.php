<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url']; ?>);">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="pos-r divider"><?php if($this->session->userdata('logged_in')) {echo lang('lbl_sidebar_gallery');} else { echo lang('lbl_menu_name_register');}?><span class="sub-title"><?php if($this->session->userdata('logged_in')) {echo lang('lbl_sidebar_gallery');} else { echo lang('lbl_menu_name_register');}?></span></h1></div>
			</div>
			<div class="col-sm-12 mt-5 xs-mt-3">
				<ul class="page-breadcrumb mt-5">
					<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i> <?php echo lang('lbl_menu_name_home'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><span><?php if($this->session->userdata('logged_in')) {echo lang('lbl_sidebar_gallery');} else { echo lang('lbl_menu_name_register');}?> </span> </li>
				</ul>
			</div>
		</div>
	</div>
</section>
	<div class="ajaxloader">
		<img src="<?php echo base_url("Newassets/images/ajaxloader.gif")?>" id="ajaxloader" style="display: none;">
	</div>

<section class="page-section-ptb text-white" style="background: url(<?php echo $background7['background_url']; ?>) no-repeat 0 0; background-size: cover;">	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">  
				<div class="step-form">
					<form action="<?php echo base_url('user/reg_contact'); ?>" method="post" class="text-center mt-30">
						<div class="row justify-content-center setup-content" id="step-1">
							<div class="col-lg-8 col-sm-12">
								<h4 class="title divider-3 mb-5"><?php echo lang('lbl_gallery_photo_name'); ?></h4>								
								<div class="alert" align="center">	
					    <?php  if($this->session->flashdata("error"))
					            {?>
					                <br/>
					                <div class="row">
					                    <div class="col-sm-12 text-uppercase  text-danger">
					                    	<div class='alert myerrormsg'>
						                        <?php 
						                        echo $this->session->flashdata("error");  ?>
					                        </div>
					                    </div>
					                </div>
					    <?php   }
					            if($this->session->flashdata("success"))
					            { ?>
					                <br/>
					                <div class="row">
					                   <div class="col-sm-12 text-uppercase  text-success">
					                   	<div class='alert mysuccessmsg'>
					                      <?php 
					                      echo $this->session->flashdata("success");  ?>
					                  </div>
					                   </div>
					                </div>
					    <?php   } ?>			
									<?php 	
									/*if(!empty($_SESSION['success']))
									{
										echo "<h3 class='successful'>$_SESSION[success]</h3>";
									}
									else if(!empty($_SESSION['fail']))
									{
										echo "<h4 class='alert'>$_SESSION[fail]</h4>";
									}*/
									?>
								</div>
							<div class="row justify-content-center">
								<div class="col-lg-10 col-md-9">
									<div class="form-group">									
										<div class="row">
											<div class="col-sm-8 mb-4">
												<!--input style="display:none" type="file" id="imgupload1" name="imgupload1" /-->                       
												<label for="img1" class="red-text avatar-view" >
													<?php
													if(!empty($userdetail->profile_image))
													{															
														$str=base_url("uploads/thumbnail/".$userdetail->profile_image);
														if(file_exists(DIRECTORY_PATH."uploads/thumbnail/".$userdetail->profile_image))
														{																			
															?>
															<img class="w-100 imgupload1" id="1" src="<?php echo $str; ?>" alt="" >
															<?php
														}
														else
														{
															?>
															<img class="w-100 imgupload1" id="1" src="<?php echo base_url('images/step/01.png');?>" alt="">
															<?php
														}							
													}
													else
													{															
														?><img class="w-100 imgupload1" id="1" src="<?php echo base_url('images/step/01.png');?>" alt="">
														<?php
													}
													?>
												</label>																			
											</div>												


											<div class="col-sm-4">
												<div class="row">
													<?php												
													$imgurl="";
													$img="";
													$imgid="";
													for($i=0;$i<=4;$i++)
													{	
														$n=count($content);
														$imgurl=base_url("images/step/0".($i+2).".png");
														for($j=0;$j<$n;$j++)
														{
															$ik="img".($i+2);
															if($content[$j]["img_key"]==$ik)
															{
																if($content[$j]["img_url"]!="")
																$imgurl=base_url("uploads/thumbnail/".$content[$j]["img_url"]);
																$img=$content[$j]["img_url"];
																$imgid=$content[$j]["id"];
															}
														}
														if($i<=1)
															echo "<div class='col-sm-12 mb-3' id='imgupload".($i+2)."'>";
														else
															echo "<div class='col-sm-4 col-xs-4' id='imgupload".($i+2)."'>";
														?>																	
															<label for="<?php echo "img".($i+2); ?>" class="red-text avatar-view<?php echo ($i+2); ?>" >
																<?php	
																if($img!="")
																{																							
																	if(file_exists(DIRECTORY_PATH."uploads/thumbnail/$img"))
																	{	
																		?>
																		<img class="w-100 imgupload<?php echo ($i+2); ?>" id="<?php echo ($i+2); ?>" src="<?php echo $imgurl; ?>" alt="" >
																		<?php
																	}
																	else
																	{
																		?>
																		<img class="w-100 imgupload<?php echo ($i+2); ?>"  id="<?php echo ($i+2); ?>" src="<?php echo base_url('images/step/0'.($i+2).'.png');?>" alt="">
																		<?php
																	}							
																}
																else
																{															
																	?><img class="w-100 imgupload<?php echo ($i+2); ?>"  id="<?php echo ($i+2); ?>" src="<?php echo base_url('images/step/0'.($i+2).'.png');?>" alt="">
																	<?php
																}																
																?>
															</label>	
															<?php
															if((strpos($imgurl, 'step') === false)){
																?>
																<a class="remove-bg" href="javascript:void(0)" data-position="<?php echo ($i+2);?>">
																	<i class="fa fa-close "></i>
																</a>
																<?php
															}?>																
															</div>														
															<!--input class="edit-images " type="file" id="<?php echo "img".($i+2); ?>" name="<?php echo ($i+2); ?>" /-->
														<?php
														if($i==1)
															echo "</div></div></div><div class='row'>";
													}
													?>												
													</div>
												</div>																			
											</div>				
										</div>
										<div class="clearfix"></div>
										<p>
											Recommanded Resolution For Images<br/>
											Image #1 	:	280px	*	250px<br/>
											Image #2-#6	:	1498px	*	844px<br/>
										</p>											
									</div>
								</div>
							</div>
						</div>
					</form>
					</div>
				</div>
			</div> 
		</div>
	</div>
</section>