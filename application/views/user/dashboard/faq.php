<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url']; ?>)">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="position-relative divider"><?php echo lang('lbl_menu_name_faq'); ?><span class="sub-title"><?php echo lang('lbl_menu_name_faq'); ?></span></h1></div>
			</div>
			<div class="col-sm-12 mt-5 xs-mt-3">
				<ul class="page-breadcrumb mt-5">
					<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i><?php echo lang('lbl_menu_name_home'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><span><?php echo lang('lbl_menu_name_faq'); ?></span></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="faq-page page-section-ptb">
    <div class="container">
	<?php
	foreach($faq as $faq1){
		if(!empty($faq1['id'])){
			?>
			<div class="row mb-3">
				<div class="col-sm-12 text-center">
					<h2 class="title divider mb-3"><?php echo $faq1['title']; ?></h2>
					<p class="lead"><?php echo $faq1['contain']; ?></p>
				</div>
			</div>
		<?php
		}else{
			?>
			<div class="row mb-3">
				<div class="col-sm-12 text-center">
					<h2 class="title divider mb-3"><?php echo $faq1[$language]['title'];  ?></h2>
					<p class="lead"><?php echo $faq1[$language]['contain'];  ?></p>
				</div>
			</div>
		<?php
		}
	}
	?>
	<?php
	foreach($faq_questions as $faq2){
		if(!empty($faq2['id'])){
			?>
			<div class="row">
				<div class="col-sm-12">
					<div class="accordion arrow light-rounded">
						<?php echo $faq2['contain']; ?>
					</div>
				</div>
			</div>
		<?php
		}else{
			?>
			<div class="row">
				<div class="col-sm-12">
					<div class="accordion arrow light-rounded">
						<?php echo $faq2[$language]['contain'];?>
					</div>
				</div>
			</div>
		<?php
		}
	}
	?>
    </div>
</section>