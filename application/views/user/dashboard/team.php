<?php 
$lange= $this->admin_model->get_default_language();
$language=$lange['status'];
$languages = $this->session->userdata('site_lang');
$lg=$languages;
?>
<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url']; ?>)">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="position-relative divider"><?php echo lang('lbl_menu_name_team'); ?><span class="sub-title"><?php echo lang('lbl_menu_name_team'); ?></span></h1></div>
			</div>
			<div class="col-sm-12 mt-5 xs-mt-3">
				<ul class="page-breadcrumb mt-5">
					<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i> <?php echo lang('lbl_menu_name_home'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><span><?php echo lang('lbl_menu_name_team'); ?></span></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="page-section-ptb">
    <div class="container">
       <?php
		foreach($our_team_member as $team_member){
			if(!empty($team_member['id'])){
				?>
				<div class="row justify-content-center mb-5 xs-mb-3">
					<div class="col-md-10 text-center">
						<h2 class="title divider mb-3"><?php echo $team_member['title']; ?></h2>
						<p class="lead"><?php  echo $team_member['contain']; ?></p>
					</div>
				</div>
			<?php
			}
		}
		?>
        <div class="row">
            <?php 
			foreach($members as $membr)
			{ 
				if(!empty($membr['id'])){
					?>
					<div class="col-lg-3 col-md-6 col-sm-6 col-xx-12">
						<div class="team team-1">
							<div class="team-images"> 
								<img src="<?php echo base_url('Newassets/images/'.$membr['member_image']);?>" alt=""/> 
							</div>
							<div class="team-description">
								<div class="team-tilte">
									<h5 class="title"><a href="<?php echo base_url('team/singleteam/'.$membr['id']); ?>"><?php echo $membr['member_name'] ?></a></h5>
									<span><?php echo $membr['designation']; ?></span>
								</div>
								<p><?php echo substr($membr['member_detail'], 0, 100).'...' ?></p>  
								<div class="team-social-icon social-icons color-hover">
									<ul>
										<?php if($membr['facebook_url']!=''){ ?><li class="social-facebook"><a href="<?php echo $membr['facebook_url']; ?>"><i class="fa fa-facebook"></i></a></li><?php } ?>
										<?php if($membr['twitter_url']!=''){ ?><li class="social-twitter"><a href="<?php echo $membr['twitter_url']; ?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
										<?php if($membr['google_url']!=''){ ?><li class="social-gplus"><a href="<?php echo $membr['google_url']; ?>"><i class="fa fa-google-plus"></i></a></li><?php } ?>
										<?php if($membr['dribble_url']!=''){ ?><li class="social-dribbble"><a href="<?php echo $membr['dribble_url']; ?>"><i class="fa fa-dribbble"></i></a></li><?php } ?>
									</ul>
								</div>
							</div>
						</div>
					</div> 
					<?php 
				}
			} 
			?>
        </div>
    </div>
</section>