<?php 
$lange= $this->admin_model->get_default_language();
$language=$lange['status'];
$lg = $this->session->userdata('site_lang');

$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url( <?php echo $background2['background_url']; ?>)">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="position-relative divider"><?php echo lang('lbl_menu_name_about'); ?><span class="sub-title"><?php echo lang('lbl_menu_name_about'); ?></span></h1></div>
			</div>
			<div class="col-sm-12 mt-5 xs-mt-3">
				<ul class="page-breadcrumb mt-5">
					<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i><?php echo lang('lbl_menu_name_home'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><span><?php echo lang('lbl_menu_name_about'); ?></span></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!--=================================
 banner -->
<section class="page-section-ptb">
    <div class="container">
        <div class="row justify-content-center">
			<?php
			foreach($our_history as $history)
			{
				if(!empty($history['id'])){
				?>
				<div class="col-md-8 text-center mb-5 xs-mb-3">
					<h2 class="title divider mb-3"><?php echo $history['title']; ?></h2>
					<p><?php echo $history['contain'];  ?></p>
				</div>
				<?php
				}
			}
			?>
		</div>
        <?php 
		foreach($our_history_inner as $history_inner){
			if(!empty($history_inner['id'])){
				echo $history_inner['contain'];
			}
		}	
		?>
    </div>
</section>
 <!--=====================our history======-->
<!--=================================
counter  -->
<section class="page-section-ptb text-white" style="background: url(<?php echo $background5['background_url']; ?>) no-repeat 0 0; background-size: cover;">
	<div class="container">
		<div class="row justify-content-center mb-5 xs-mb-3">
			<div class="col-md-10 text-center">
				<h2 class="title divider"><?php echo lang('lbl_home_animated_fun_facts'); ?></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-sm-3 col-xs-6 col-xx-12 text-center">
				<div class="counter">
					<img src="<?php echo base_url('images/counter/01.png'); ?>" alt="">
					<span class="timer" data-to="<?php echo $total_users; ?>" data-speed="10000"><?php echo $total_users; ?></span><label><?php echo lang('lbl_home_total_members'); ?></label>
				</div>
			</div>
			<div class="col-lg-3 col-sm-3 col-xs-6 col-xx-12 text-center">
				<div class="counter">
					<img src="<?php echo base_url('images/counter/02.png'); ?>" alt="">
					<span class="timer" data-to="<?php echo $online_users; ?>" data-speed="10000"><?php echo $online_users; ?></span><label><?php echo lang('lbl_home_online_members'); ?></label>
				</div>
			</div>
			<div class="col-lg-3 col-sm-3 col-xs-6 col-xx-12 text-center">
				<div class="counter">
					<img src="<?php echo base_url('images/counter/03.png'); ?>" alt="">
					<span class="timer" data-to="<?php echo $male_users; ?>" data-speed="10000"><?php echo $male_users; ?></span><label><?php echo lang('lbl_home_men_online'); ?></label>
				</div>
			</div>
			<div class="col-lg-3 col-sm-3 col-xs-6 col-xx-12 text-center">
				<div class="counter">
					<img src="<?php echo base_url('images/counter/04.png'); ?>" alt="">
					<span class="timer" data-to="<?php echo $female_users; ?>" data-speed="10000" ><?php echo $female_users; ?></span><label><?php echo lang('lbl_home_women_online'); ?></label>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="page-section-ptb grey-bg">
    <div class="container">
		<?php
		foreach($why_choose_us as $why_choose){
			if(!empty($why_choose['id'])){
			?>
			<div class="row justify-content-center mb-5 xs-mb-3">
				<div class="col-md-10 text-center">
					<h2 class="title divider mb-3"><?php echo $why_choose['title']; ?></h2>
					<p class="lead"><?php echo $why_choose['contain']; ?></p>
				</div>
			</div>
			<?php 
			}
		}
		?>
        <div class="row justify-content-center mb-5 xs-mb-3">
            <div class="col-md-10 text-center">
                <h4 class="title divider-3 text-uppercase"><?php echo lang('lbl_home_we_are_the_one'); ?></h4>
            </div>
        </div>
        <div class="row mb-5 xs-mb-3">
		<?php 
			foreach($members as $membr){
				if(!empty($membr['id'])){?>
					<div class="col-md-3 col-sm-6 col-xs-6 col-xx-12">
						<div class="team team-1">
							<div class="team-images"> 
								<img class="img-fluid" src="<?php echo base_url('Newassets/images/'.$membr['member_image']);?>" alt=""/> 
							</div>
							<div class="team-description">
								<div class="team-tilte">
									<h5 class="title"><a href="<?php echo base_url('team/singleteam/'.$membr['id']); ?>"><?php echo $membr['member_name'] ?></a></h5>
									<span><?php echo $membr['designation']; ?></span>
								</div>
								<p><?php echo substr($membr['member_detail'], 0, 100).'...' ?></p>  
								<div class="team-social-icon social-icons color-hover">
									<ul>
										<?php if($membr['facebook_url']!=''){ ?><li class="social-facebook"><a href="<?php echo $membr['facebook_url']; ?>"><i class="fa fa-facebook"></i></a></li><?php } ?>
										<?php if($membr['twitter_url']!=''){ ?><li class="social-twitter"><a href="<?php echo $membr['twitter_url']; ?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
										<?php if($membr['google_url']!=''){ ?><li class="social-gplus"><a href="<?php echo $membr['google_url']; ?>"><i class="fa fa-google-plus"></i></a></li><?php } ?>
										<?php if($membr['dribble_url']!=''){ ?><li class="social-dribbble"><a href="<?php echo $membr['dribble_url']; ?>"><i class="fa fa-dribbble"></i></a></li><?php } ?>
									</ul>
								</div>
							</div>
						</div>
					</div> 
				<?php 
				}
			} ?>
        </div>
        <div class="row text-center">
            <?php
			foreach($team_inner_box1 as $inner_box1){
				if(!empty($inner_box1['id'])){
					?>
					<div class="col-sm-4">
						<div class="about-cntn">
							<h5 class="title divider-3 text-uppercase mb-4 xs-mb-2"><?php echo $inner_box1['title']; ?></h5>
							<p><?php echo $inner_box1['contain']; ?></p>
						</div>
					</div>
					<?php
				}
			}
			?>
			<?php
			foreach($team_inner_box2 as $inner_box2){
				if(!empty($inner_box2['id'])){
					?>
					<div class="col-sm-4">
						<div class="about-cntn">
							<h5 class="title divider-3 text-uppercase mb-4 xs-mb-2"><?php echo $inner_box2['title']; ?></h5>
							<p><?php echo $inner_box2['contain']; ?></p>
						</div>
					</div>
					<?php
				}
			}
			foreach($team_inner_box3 as $inner_box3){
				if(!empty($inner_box3['id'])){
					?>
					<div class="col-sm-4">
						<div class="about-cntn">
							<h5 class="title divider-3 text-uppercase mb-4 xs-mb-2"><?php echo $inner_box3['title']; ?></h5>
							<p><?php echo $inner_box3['contain']; ?></p>
						</div>
					</div>
					<?php
				}
			}
			?>
        </div>
    </div>
</section>
<!--=================================
Testimonials -->
<section class="page-section-pt dark-bg text-white" style="background: url(<?php echo $background6['background_url']; ?>) no-repeat 0 0; background-size: cover;">
	<div class="container">
		<div class="row justify-content-center mb-5 xs-mb-0">
			<div class="col-md-10 text-center">
				<h2 class="title divider"><?php echo lang('lbl_home_testimonials'); ?></h2>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-md-10"><div class="owl-carousel" data-nav-arrow="true" data-items="1" data-md-items="1" data-sm-items="1">
				<?php
				foreach($testimonials as $testomonial)
				{
					if(!empty($testomonial['id'])){?>
						<div class="item"><div class="testimonial bottom_pos">
							<div class="testimonial-avatar">
								<?php
								if(!empty($testomonial['author_image']))
								{
									$str=base_url("Newassets/images/thumbnail/".$testomonial['author_image']);
									if(file_exists(DIRECTORY_PATH."Newassets/images/thumbnail/".$testomonial['author_image']))
									{						
										?>
										<img src="<?php echo $str; ?>" alt=""></div>
										<?php
									}
									else
									{
										?>
										<img src="<?php echo base_url('Newassets/images/thumbnail/thum-0.png'); ?>" alt=""></div>
										<?php
									}							
								}
								else
								{
									?><img src="<?php echo base_url('Newassets/images/thumbnail/thum-0.png'); ?>" alt=""></div>
									<?php
								}
								?>
								<div class="testimonial-info"><p><?php echo $testomonial['description']; ?></p>
									<div class="author-info"> <strong><?php echo $testomonial['author']; ?></strong> </div>
								</div>
							</div>
						</div>
					<?php
					}
				}?>
				</div>
			</div>
		</div>
	</div>
</section>
<!--=================================
story-slider-->
<section class="page-section-ptb grey-bg story-slider"> 
	<div class="container">
		<div class="row justify-content-center mb-2 xs-mb-0">
			<div class="col-md-10 text-center">
				<h2 class="title divider"><?php echo lang('lbl_home_they_found_true_love'); ?></h2>
			</div>
		</div>
	</div>
	<div class="owl-carousel" data-nav-dots="true" data-items="5" data-md-items="4" data-sm-items="3" data-xs-items="2" data-xx-items="1" data-space="30">
		<?php
		foreach($stories as $story)
		{
			if(!empty($story['id'])){
			?>
				<div class="item">
					<div class="story-item">
						<div class="story-image clearfix">
							<?php
							if(!empty($story['image']))
							{
								$str=base_url("Newassets/images/thumbnail/".$story['image']);
								if(file_exists(DIRECTORY_PATH."Newassets/images/thumbnail/".$story['image']))
								{						
									?>
									<img class="img-fluid" src="<?php echo $str; ?>" alt="">
									<?php
								}
								else
								{
									?>
									<img class="img-fluid" src="<?php echo base_url('images/story/01.jpg'); ?>" alt="">
									<?php
								}							
							}
							else
							{
								?><img class="img-fluid" src="<?php echo base_url('images/story/01.jpg'); ?>" alt="">
								<?php
							}
							?>
							<div class="story-link">
								<a href="<?php echo base_url('stories/stories_details/'.$story['id']);?>">
									<i class="glyph-icon flaticon-add"></i>
								</a>
							</div>				
						</div>
						<div class="story-details text-center">
							<h5 class="title divider-3"><?php echo $story['title']; ?></h5>                  
							<div class="about-des mt-3"><?php echo $story['excerpt']; ?></div>   
						</div>
					</div>
				</div>
			<?php
			}
		}
		?>
	</div>
</section>
<!--====story-slider-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#load_more").click(function(e){
            e.preventDefault();
            var page = $(this).data('val');
            getblog(page);
        });
    });
    var getblog = function(page){
        $("#loader").show();
        $.ajax({
            url:"<?php echo base_url() ?>user/getblog",
            type:'GET',
            data: {page:page}
        }).done(function(response){
            $("#ajax_table").append(response);
            $("#loader").hide();
            $('#load_more').data('val', ($('#load_more').data('val')+1));
            scroll();
			$("#load_more").hide();
        });
    };
    var scroll  = function(){
        $('html, body').animate({
            scrollTop: $('#load_more').offset().top
        }, 1000);
    };
</script>