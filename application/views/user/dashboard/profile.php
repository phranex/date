<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
<style>
.ui-slider .ui-slider-handle {
    width:3em;
    left:-.6em;
	hight:14em; 
	background:#FABD0D;
    text-decoration:none;
    text-align:center;
}
.ui-slider-range{
	background:#FABD0D;
}
.slider-tip {
    opacity:1;
    bottom:120%;
    margin-left: -1.36em;
}
.tooltip-inner{
	background:#FABD0D;
	width:3em;
	hight:10em; 
	font-size:1em;
}
.tooltip-arrow{
	color:#FABD0D;
}
.ui-state-default{
	background:none;
}
</style>
<?php
/*
from here we can update login user profile information except first name of the users
*/
?>
<?php $datepref=explode(",",$content->date_pref); ?>
<div class="page_wrapper">
	<div class="row padd-bottom">
		<div class="col-sm-12"><h1 class="page-header"><?php echo lang('lbl_profile_Profile'); ?></h1>
		</div>
	</div>
	<div class="row">
<?php if (validation_errors()) : ?>
    	<div class="row" align="center">
			<div class="col-md-12 ">
			   <div class="alert myerrormsg" role="error">
					<?= validation_errors() ?>
			   </div>
			</div>
		</div>
		<!-- <div class="col-md-5"></div>
		<div class="col-md-6">
			<div class="alert" role="alert">
				<?= validation_errors() ?>
			</div>
		</div> -->
<?php endif; ?>
		<?php if (isset($error) ) : ?>
            	<div class="row" align="center">
					<div class="col-md-12 ">
					   <div class="alert myerrormsg" role="error">
							<?= $error ?>
					   </div>
					</div>
				</div>
		<!-- <div class="col-md-5"></div>
		<div class="col-md-6">
			<div class="alert" role="alert">
				<?= $error ?>
			</div>
		</div> -->
		<?php endif; ?>
		<?php if(isset($_SESSION["error"])) : ?>
            	<div class="row" align="center">
					<div class="col-md-12 ">
					   <div class="alert myerrormsg" role="error">
							<?php echo $_SESSION["error"]; ?>
					   </div>
					</div>
				</div>
		<!-- <div class="col-md-5"></div>
		<div class="col-md-6">
			<div class="alert" role="alert">
				<?php echo $_SESSION["error"]; ?>
			</div>
		</div> -->
		<?php endif; ?>
		<?php if(isset($_SESSION["Sucess"])) { ?>
            <div class="row" align="center">
				<div class="col-md-12 ">
				   <div class="alert mysuccessmsg" role="success">
						<?php echo $_SESSION["Sucess"]; ?>
				   </div>
				</div>
			</div>
		<!-- <div class="col-md-5"></div>
		<div class="col-md-6">
			<div class="alert" role="alert">
				<?php echo $_SESSION["Sucess"]; ?>
			</div>
		</div> -->
		<?php } ?>
	<?php echo form_open_multipart('', array('role'=>'form')); 
			if($content->profile_image!='')
			{
				$imgurl=$content->profile_image;
				if(file_exists(DIRECTORY_PATH."uploads/thumbnail/$imgurl"))
					$profile_img = base_url("uploads/thumbnail/$imgurl");
				else
					$profile_img = base_url('Newassets/images/default.png');
			}
			else 
			{
				$profile_img = base_url().'Newassets/images/default.png';
			}?>
			<div class="col-lg-4">
				<div class="form-group">
					<div class="controls">
						<img id="profile_image_display" src="<?php echo $profile_img; ?>" height="250" width="250" alt="<?php echo $content->fname; ?>" />
					</div>
				</div>
				<div class="form-group">
						<label for="profile_img"><?php echo lang('lbl_profile_profile_image'); ?></label>
						<input type="file" class="" id="profile_img" name="profile_img">
				</div>
			</div>
			<input type="hidden" name="user_id" value="<?php echo $content->id; ?>" />
			<input type="hidden" name="username" value="<?php echo $content->username; ?>" />
			<div class="col-lg-8">
				<div class="form">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo lang('lbl_profile_firstname'); ?></label>
								<input type="User name" class="form-control" id="exampleInputEmail1" id="fname" name="fname" value="<?php echo $content->fname; ?>" placeholder="<?php echo lang('lbl_profile_firstname'); ?>">
							</div>		
							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo lang('lbl_profile_lastname'); ?></label>
								<input type="Last name" class="form-control" id="lname" name="lname" value="<?php echo $content->lname; ?>" placeholder="<?php echo lang('lbl_register_lastname'); ?>">
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo lang('lbl_profile_birthdate'); ?></label>
								<div class="row">
									<div class="col-sm-4">
										<input type="number" min="1" max="12" value="<?php echo date_format(date_create($content->dob),"m"); ?>" class="form-control" name="month" id="exampleInputMonth1" placeholder="Month" required oninvalid="this.setCustomValidity('Month is Required')" oninput="setCustomValidity('')">
									</div>
									<div class="col-sm-4">
										<input type="number" min="1" max="31"  value="<?php echo date_format(date_create($content->dob),"d"); ?>" class="form-control" id="exampleInputDay" placeholder="Day" name="day" required oninvalid="this.setCustomValidity('Day is Required')" oninput="setCustomValidity('')">
									</div>
									<div class="col-sm-4">
										<input type="number" min="<?php echo (date("Y")-65)  ?>" max="<?php echo date("Y") ?>"  value="<?php echo date_format(date_create($content->dob),"Y"); ?>" class="form-control" id="exampleInputyear" placeholder="year" name="year" required  oninvalid="this.setCustomValidity('Year is Required')" oninput="setCustomValidity('')">
									</div>
								</div>
								<div class="form-group">
									<label for="exampleInputMassage"><?php echo lang('lbl_profile_birthdate'); ?></label>
									<textarea class="form-control" name="about" placeholder="Massage" rows="3"><?PHP echo $content->about; ?></textarea>
								</div>
								<div class="form-group">
									<label for="exampleInputMassage"><?php echo lang('lbl_profile_gender'); ?></label>
									<div class="row">
										<div class="col-sm-6">
											<div class="radio <?php echo ($content->gender=="male")? "checked":""; ?>" >  
												<label>
													<?php if($content->gender=="male") { 
															?><span class="icons">
															<span class="first-icon fa fa-circle-o"></span>
															<span class="second-icon fa fa-dot-circle-o"></span>
															</span>	
															<?php 
														} ?>
													<input type="radio" name="gender" id="blankRadio1" <?php echo ($content->gender=="male")? "checked":""; ?> value="male" aria-label="..."><?php echo lang('lbl_profile_male'); ?>
												</label>
											</div>		  
										</div>
										<div class="col-sm-6">
											<div class="col-sm-6">
											   <div class="radio <?php echo ($content->gender=="female")? "checked":""; ?>" >  
													<label>
												  <?php if($content->gender=="male") 
														{ ?>
															<span class="icons">
																<span class="first-icon fa fa-circle-o"></span>
																<span class="second-icon fa fa-dot-circle-o"></span>
															</span>	
												<?php 	} ?>
															<input type="radio" name="gender" id="blankRadio1" <?php echo ($content->gender=="female")? "checked":""; ?> value="female" aria-label="..."><?php echo lang('lbl_profile_female'); ?>
													</label>
												</div>		  
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1"><?php echo lang('lbl_profile_education'); ?></label>
									<input type="Education" class="form-control" value="<?php echo $content->education; ?>" id="education" name="education">
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1"><?php echo lang('lbl_profile_profession'); ?></label>
									<input type="Profession" class="form-control" value="<?php echo $content->profession; ?>" id="profession" name="profession">
								</div>
								<div class="form-group">
									<label for="disabledSelect"><?php echo lang('lbl_profile_religion'); ?></label>
									<select id="religion" name="religion[]" class="form-control" multiple>
										<?php 
										foreach($religion as $relig){?>
											<option <?php echo (in_array($relig['id'],explode(",",$content->religion)))?'selected':''?> value="<?php echo $relig['id']; ?>">
												<?php 
												if(strcmp($_SESSION['site_lang'],"french")==0)
													echo $relig['french'];
												elseif(strcmp($_SESSION['site_lang'],"russian")==0)
													echo $relig['russian'];
												else
													echo $relig['english'];
												?>
											</option>
											<?php 
										}?>
									</select>
								</div>
								<?php
								$hight="3'0 (92 cm), 3'1 (94 cm),3'2 (97 cm),3'3 (99 cm),3'4 (102 cm),3'5 (104 cm),3'6 (107 cm),3'7 (109 cm),3'8 (112 cm),3'9 (114 cm),3'10 (117 cm),3'11 (119 cm),4'0 (122 cm),4'1 (125 cm),4'2 (127 cm),4'3 (130 cm),4'4 (132 cm),4'5 (135 cm),4'6 (137 cm),4'7 (140 cm),4'8 (142 cm),4'9 (145 cm),4'10 (147 cm),4'11 (150 cm),5'0 (152 cm), 5'1 (155 cm),5'2 (158 cm),5'3 (160 cm),5'4 (163 cm),5'5 (165 cm),5'6 (168 cm),5'7 (170 cm),5'8 (173 cm),5'9 (175 cm),5'10 (178 cm),5'11 (180 cm),6'0 (183 cm),6'1 (185 cm),6'2 (188 cm),6'3 (191 cm),6'4 (193 cm),6'5 (196 cm),6'6 (198 cm),6'7 (201 cm),6'8 (203 cm),6'9 (206 cm),6'10 (208 cm),6'11 (211 cm),7'0 (213 cm)";
								$height=explode(",",$hight);					
								?>
								<div class="form-group">
									<label for="disabledSelect"><?php echo lang('lbl_profile_height'); ?></label>
									<select  class="form-control" id="height" name="height">
										<option selected value=""><?php echo lang('lbl_profile_select_height'); ?></option>
										<?php for($i=0;$i<count($height);$i++){  ?>
										<option <?php echo ($height[$i]==$content->height)?'selected':''?> value="<?php echo $height[$i];  ?>">
										<?php echo $height[$i];  ?>
										</option>
										<?php  } ?>
									</select>
								</div>
								<div class="form-group">
									<label for="disabledSelect"><?php echo lang('lbl_profile_kids'); ?></label>
									<select id="kids" name="kids" class="form-control">
										<option selected value=""><?php echo lang('lbl_profile_select_kids'); ?></option>
										<option <?php echo ($content->kids==1)?'selected':''; ?> value="1"><?php echo lang('lbl_profile_select_kids'); ?> - 1</option>
										<option <?php echo ($content->kids==2)?'selected':''; ?> value="2"><?php echo lang('lbl_profile_select_kids'); ?> - 2</option>
										<option <?php echo ($content->kids==3)?'selected':''; ?> value="3"><?php echo lang('lbl_profile_select_kids'); ?> - 3</option>
										<option <?php echo ($content->kids==4)?'selected':''; ?> value="4"><?php echo lang('lbl_profile_select_kids'); ?> - 4</option>
										<option <?php echo ($content->kids==5)?'selected':''; ?> value="5"><?php echo lang('lbl_profile_select_kids'); ?> - 5</option>
										<option <?php echo ($content->kids==6)?'selected':''; ?> value="6"><?php echo lang('lbl_profile_select_kids'); ?> - 6</option>
										<option <?php echo ($content->kids==7)?'selected':''; ?> value="7"><?php echo lang('lbl_profile_select_kids'); ?> - 7</option>
										<option <?php echo ($content->kids=="None")?'selected':''; ?> value="None"><?php echo lang('lbl_profile_none'); ?></option>
										<option <?php echo ($content->kids=="One Day")?'selected':''; ?> value="One Day"><?php echo lang('lbl_profile_one_day'); ?></option>
										<option <?php echo ($content->kids=="I Don''t Want Kids")?'selected':''; ?> value="I Don''t Want Kids"><?php echo lang('lbl_profile_i_dont_want_kids'); ?></option>
									</select>
								</div>
								<div class="form-group">
									<label for="exampleInputMassage"><?php echo lang('lbl_profile_address'); ?></label>
									<input type="text" class="form-control" id="address" name="address"  placeholder="Address" value="<?php echo $content->address; ?>">						   
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="exampleInputEmail1"><?php echo lang('lbl_profile_date_preferances'); ?></label>
						<ul id="sortable" class="sortable" style="list-style:none">
							<?php 
							$dateprefvalue=array("Coffee","Drink","Food","Fun");
							foreach($datepref as $date){ ?>
								<li class="ui-state-default datepref" id="<?php echo $date; ?>" value="<?php echo $date; ?>" style="border:0px">
								<div class="Preference-icon"><img class="img-center" alt="#" src="<?php echo base_url("Newassets/images/dateimg$date.png"); ?>"></div><div class="text"><?php echo $dateprefvalue[$date-1] ?></div>
								</li>
								<?php 
							} ?>
						</ul>
						<input type="hidden" id="datepref" name="datepref" value="<?php echo $content->date_pref;  ?>">
					</div>
					<div class="form-group">
						<label for="exampleInputEmail1"><?php echo lang('lbl_profile_interested_in'); ?></label>
						<div class="row">
							<div class="col-sm-4">
								<div class="radio <?php echo ($content->gender_pref=="female")? "checked":""; ?>">
									<label  class="Intrested">
										<input type="radio"  name="gender_pref" id="gender_male" <?php echo ($content->gender_pref=="female")? "checked":""; ?> value="female"/>
										<?php if($content->gender_pref=="female") { ?><span class="icons">
											<span class="first-icon fa fa-circle-o"></span>
											<span class="second-icon fa fa-dot-circle-o"></span>
										</span>	<?php } ?>
										<img class="img-fluid fmale" alt="#" src="<?php echo base_url('images/fmail.png'); ?>">
										<img class="img-fluid fmale2" alt="#" src="<?php echo base_url('images/fmail1.png'); ?>">
									</label>
								</div>
							</div>
							<div class="col-sm-3">  
								<div class="radio <?php echo ($content->gender_pref=="male")? "checked":""; ?>">
									<label  class="Intrested"> 
										<input type="radio"  name="gender_pref" id="gender_fmale" value="male" <?php echo ($content->gender_pref=="male")? "checked":""; ?> />
										<?php if($content->gender_pref=="male") { ?><span class="icons">
											<span class="first-icon fa fa-circle-o"></span>
											<span class="second-icon fa fa-dot-circle-o"></span>
										</span>	<?php } ?>
										<img class="img-fluid male2" alt="#" src="<?php echo base_url('images/mail1.png'); ?>">
										<img class="img-fluid male1" alt="#" src="<?php echo base_url('images/mail.png'); ?>">
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="disabledSelect"><?php echo lang('lbl_profile_age_preferances'); ?></label>
						<br>
						<br>
						<input type="hidden" class="form-control" name="age-min" id="age-min" value="<?php echo set_value('age-min'); ?>">
						<input type="hidden" class="form-control" name="age-max" id="age-max" value="<?php echo set_value('age-max'); ?>">
						<p>
						</p>
						<div id="age-Preference"></div>
					</div>
					<div class="form-group">
						<label for="disabledSelect"><?php echo lang('lbl_profile_distance_preferances'); ?></label>
						<br/><br/>
						<input type="hidden" class="form-control" name="dist-min" id="dist-min"  value="<?php echo set_value('dist-min'); ?>">
						<input type="hidden" class="form-control" name="dist-max" id="dist-max"  value="<?php echo set_value('dist-max'); ?>">
						<p></p>
						<div id="Distance-Preference"></div>
					</div>
					<div class="form-group">
						<label for="disabledSelect"><?php echo lang('lbl_profile_ehtnticity'); ?></label>
						<select  class="form-control" id="ethnicity" name="ethnicity[]" multiple>
					<?php	foreach($ethnicity as $eathni)
							{?>
								<option <?php echo (in_array($eathni['id'],explode(",",$content->ethnicity)))?'selected':''?> value="<?php echo $eathni['id']; ?>">
								<?php 
									if(strcmp($_SESSION['site_lang'],"french")==0)
										echo $eathni['french'];
									elseif(strcmp($_SESSION['site_lang'],"russian")==0)
										echo $eathni['russian'];
									else
										echo $eathni['english'];
								?>
								</option>
					<?php 	}	?>
						</select>
					</div>
					<div class="form-group">
						<label for="disabledSelect"><?php echo lang('lbl_profile_question'); ?></label>
						<select id="question" name="question" class="form-control">
							<option><?php echo lang('lbl_profile_select_question'); ?></option>
						<?php 
						foreach($questions as $question)
						{?>
							<option <?php echo ($question['id']==$content->que_id)?'selected':''?> value="<?php echo $question['id']; ?>">
							<?php 
									if(strcmp($_SESSION['site_lang'],"french")==0)
										echo $question['french'];
									elseif(strcmp($_SESSION['site_lang'],"russian")==0)
										echo $question['russian'];
									else
										echo $question['english'];
							?>
							</option>
				<?php 	}	?>
						</select>
					</div>
					<div class="form-group">
						<label for="disabledSelect"><?php echo lang('lbl_profile_question_answer'); ?></label>
						<input type="text" class="form-control" id="question_ans" value="<?php echo $content->que_ans; ?>" name="question_ans">
					</div>
					<div class="form-group">
						<label><?php echo lang('lbl_profile_question_location'); ?></label>
						<input type="radio" name="access_loc" <?php echo ($content->access_location=='1')?'checked':'' ?> value="1"><?php echo lang('lbl_profile_yes'); ?>
						<input type="radio" name="access_loc" <?php echo ($content->access_location=='0')?'checked':'' ?> value="0"><?php echo lang('lbl_profile_no'); ?>
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-default" value="<?php echo lang('lbl_profile_update'); ?>">
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCykRLEzpOdEoBqafqgeaJwgjxd3cT1bM8&amp;libraries=geometry,places"  type="text/javascript"></script>
<script>
  $( function() {
    $( "#age-Preference" ).slider({
      range: true,
      min: 15,
      max: 60,
      values: [<?php echo $content->min_age_pref." , ". $content->max_age_pref;  ?>],
      slide: function( event, ui ) {
		  if(ui.values[1] - ui.values[0] < 3){
			  return false;
		  }
		  else
		  {
			   var target = ui.handle || $('.ui-slider-handle'); 
			   var curValue = ui.value || initialValue;
			  $(target).html('<div class="tooltip top slider-tip"><div class="tooltip-arrow"></div><div class="tooltip-inner">' + curValue+ '</div></div>');
				$( "#agepref" ).html( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
				$( "#age-min" ).val(  ui.values[ 0 ] );
				$( "#age-max" ).val(  ui.values[ 1 ] );
		  }
      }
    });
	$( "#agepref" ).html( $( "#age-Preference" ).slider( "values", 0 ) +
      " -  " + $( "#age-Preference" ).slider( "values", 1 ) );
	//set initial valu to field
	$( "#age-min" ).val(  $( "#age-Preference" ).slider( "values", 0 ));
	$( "#age-max" ).val( $( "#age-Preference" ).slider( "values", 1 ) );
    $( "#Distance-Preference" ).slider({
      range: true,
      min: 0,
      max: 200,
      values: [ <?php echo $content->min_dist_pref." , ". $content->max_dist_pref;  ?> ],
      slide: function( event, ui ) {
		  if(ui.values[1] - ui.values[0] < 10){
			  return false;
		  }
		  else
		  {
			   var target = ui.handle || $('.ui-slider-handle'); 
			   var curValue = ui.value || 0;
			  $(target).html('<div class="tooltip top slider-tip"><div class="tooltip-arrow"></div><div class="tooltip-inner">' + curValue+ '</div></div>');
				$( "#distpref" ).html( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
				$( "#dist-min" ).val(  ui.values[ 0 ] );
				$( "#dist-max" ).val(  ui.values[ 1 ] );
		  }
      }
    });
  $(document).ready( function(){
		var sliderValues = [
			$( "#age-Preference" ).slider( "values", 0 ),
			$( "#age-Preference" ).slider( "values", 1 ),
			$( "#Distance-Preference" ).slider( "values", 0 ),
			$( "#Distance-Preference" ).slider( "values", 1 )
		];
		console.log(sliderValues);
		var tmp=0;
		$( '.ui-slider-handle' ).each(function( index ) {
			var target = $(this); 
			var curValue = sliderValues[tmp++];
			$(target).html('<div class="tooltip top slider-tip"><div class="tooltip-arrow"></div><div class="tooltip-inner">' + curValue+ '</div></div>');
		});
	});
	$( "#distpref" ).html( $( "#Distance-Preference" ).slider( "values", 0 ) +
      " - " + $( "#Distance-Preference" ).slider( "values", 1 ) );
	//set initial valu to field
	$( "#dist-min" ).val(  $( "#Distance-Preference" ).slider( "values", 0 )  );
	$( "#dist-max" ).val(  $( "#Distance-Preference" ).slider( "values", 1 )  );
  } );
</script>
<script>
$( ".sortable" ).sortable();
$( ".sortable" ).sortable({
  update: function( event, ui ) {
                var strItems = "";
                $("#sortable").children().each(function (i) {
                    var li = $(this);
                    strItems += li.attr("id")+",";
                });
				var strItems = strItems.substring(0, strItems.length-1);
				$("#datepref").val(strItems);
			}
});
function initialize() {
	   var input = document.getElementById('address');
	   var autocomplete = new google.maps.places.Autocomplete(input);
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>