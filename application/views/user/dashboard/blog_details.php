<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url']; ?>);">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="pos-r divider"><?php echo lang('lbl_blog_details'); ?> <span class="sub-title"><?php echo lang('lbl_blog_micro_blog'); ?></span></h1></div>
			</div>
			<div class="col-sm-12 mt-5 xs-mt-5">
				 <ul class="page-breadcrumb mt-5">
					<li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i> <?php echo lang('lbl_menu_name_home'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><a href="<?php echo base_url('blog');?>"><?php echo lang('lbl_blog_name'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><span><?php echo lang('lbl_blog_details'); ?></span> </li>
				 </ul>
			</div>
		</div>
	</div>
</section>
<?php
	$blog=$blog_details;
	if(!empty($blog['id'])){
		?>
		<section class="page-section-ptb">
			<div class="container">
			   <div class="row">                    
					<div class="col-sm-12">
						<div class="post">
							<div class="post-image clearfix">
								<?php
								if(!empty($blog['image']))
								{
									$str=base_url("Newassets/images/blog/".$blog['image']);
									 if(file_exists(DIRECTORY_PATH."Newassets/images/blog/".$blog['image']))
									 {      
									  ?>
									  <img class="img-fluid" src="<?php echo $str; ?>" alt="">
									  <?php
									 }
									 else
									 {
									  ?>
									  <img class="img-fluid" src="<?php echo $str;?>" alt="">
									  <?php
									 }       
								}
								else
								{
								 ?>
								   <img class="img-fluid " src="<?php echo base_url('images/blog/blog-1.jpg');?>" alt="">
								 <?php
								}
								?>  
							</div>
							<div class="post-details">
								<div class="post-title mt-20">
									<h5 class="title text-uppercase mt-20"><a href="#"><?php echo $blog['title'];?></a></h5>
								</div>
								<p><?php echo date_format(date_create($blog['created_date']),"d F , Y")?><a href="<?php echo base_url("blog/index/".$blog['author'])?>"><?php echo $blog['author'];?></a></p>              
								<div class="post-icon">
									<div class="post-content">
										 <p><?php echo $blog['description'];?></p>
									</div>             
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php	
	}
?>