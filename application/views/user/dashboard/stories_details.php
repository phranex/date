<?php 
$background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
$background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
$background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
$background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
$background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
$background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
$background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
?>
<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(<?php echo $background2['background_url']; ?>);">
	<div class="container">
		<div class="row intro-title text-center">
			<div class="col-sm-12">
				<div class="section-title"><h1 class="position-relative divider">						
						<?php
						$storydata=$story;
							if(!empty($storydata['id'])){ 
								
								echo $storydata['title'];
							}
						?>
						<span class="sub-title"><?php echo lang('lbl_love_story');?></span></h1>
				</div>
			</div>
		   <div class="col-sm-12 mt-5 xs-mt-3">
				<ul class="page-breadcrumb mt-5">
					<li><a href="<?php echo base_url()?>"><i class="fa fa-home"></i> <?php echo lang('lbl_menu_name_home'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><a href="<?php echo base_url("user/stories")?>"><?php echo lang('lbl_menu_name_stories'); ?></a> <i class="fa fa-angle-double-right"></i></li>
					<li><span><?php echo lang('lbl_stories_details');?></span> </li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!--=================================
 banner -->
 <?php
	if(!empty($storydata['id'])){ ?> 
		<section class="page-section-ptb">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h3 class="title"><?php echo lang('lbl_post_by');?>-<span class="text-yellow text-uppercase"><?php echo $storydata['story_author'];?></span></h3>
						<h6 class="clearfix mb-2">
						<?php
						echo date_format(date_create($storydata['created_date']),"d F , Y")
						?>
						</h6>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<?php
							if(!empty($storydata['image']))
							{
								$str=base_url("Newassets/images/thumbnail/".$storydata['image']);
								if(file_exists(DIRECTORY_PATH."Newassets/images/thumbnail/".$storydata['image']))
								{      
									?>
									<img class="img-fluid xs-mb-3 w-100" src="<?php echo $str; ?>" alt="" >
									<?php
								}
								else
								{
									?>
									<img class="img-fluid xs-mb-3" src="<?php echo base_url('images/story/stories-1.jpg');?>" alt="">
									<?php
								}       
							}
							else
							{
								?>
								<img class="img-fluid xs-mb-3" src="<?php echo base_url('images/story/stories-1.jpg');?>" alt="">
								<?php
							}
							?>      
						<p class="mt-2"><?php echo $storydata['description'];?></p> 
					</div>
				</div>
			</div>
		</section>
	<?php 
	}
?>