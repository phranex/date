<?php
/*
	Here display the gallery images of login user except his/her profile image and from here we can also update and delete user profile and gallery images
*/
?>
<div class="content">
    <section>
        <div class="container-fluid text-center">
			<div class="row">
			   <div class="col-sm-12 text-uppercase">
				   <h3 align="center"><?php echo lang('lbl_gallery_photo'); ?></h3>
				   <div><img class="img-center" src="<?php echo base_url('images/title-bdr.png'); ?>" alt=""></div>
			   </div>
			</div>
	<?php  if(isset($_SESSION["error"])) 
			{?>
				<br/>
            	<div class="row" align="center">
					<div class="col-md-12 ">
					   <div class="alert myerrormsg" role="error">
							<?php echo $_SESSION["error"]; ?>
					   </div>
					</div>
				</div>
	<?php 	}
			if(isset($_SESSION["success"])) 
			{ ?>
				<br/>
	            <div class="row" align="center">
					<div class="col-md-12 ">
					   <div class="alert mysuccessmsg" role="success">
							<?php echo $_SESSION["success"]; ?>
					   </div>
					</div>
				</div>
	<?php 	} ?>
			<div class="row mt-5">
				<div class="col-sm-4">
					<div class="frnd-box gallery">
					   <?php
						$imgurl=base_url("/Newassets/images/default.png");
						if(!empty($userdetail->profile_image))
						{
							 $imgp=$userdetail->profile_image;
							if(file_exists(DIRECTORY_PATH."uploads/thumbnail/$imgp"))
							{ ?>
								<img class="img-center full-width"  src="<?php echo base_url("uploads/thumbnail/$imgp"); ?>"  alt="<?php echo $userdetail->fname; ?>"  />
					<?php 	}
							else
							{ ?>
								<img class="img-center full-width" src="<?php echo $imgurl; ?>"   />
								<?php
							}
						}
						else
						{ ?>
							<img class="img-center full-width" src="<?php echo $imgurl; ?>" />
							<?php 
						 }
						?>
						<div class="frnd-cntn">
							<form  enctype="multipart/form-data" action="<?php echo base_url("user/edit_gallery_image"); ?>" method="post">
								<ul class="list-inline mt-1">
									<li>
										<label for="img1" class="red-text" >
											<i class="fa fa-pencil fa-2x" aria-hidden="true"></i>
										</label>
									</li>
							<?php 	if($userdetail->profile_image!="")
									{ ?>
										<li><a class="yellow-text fa-2x" href="<?php echo base_url().'user/remove_profile_image/'.$userdetail->id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
							  <?php } ?>
								</ul>
								<input class="imgs edit-images" type="file" id="img1" name="img1"/>
							</form>
						</div>
					</div>
				</div>
				<?php  
				$img="";
				for($i=1;$i<6;$i++ ) 
				{
					$n=count($content);
					$imgurl=base_url("/Newassets/images/default.png");
					for($j=0;$j<$n;$j++)
					{
						$ik="img".($i+1);
						if($content[$j]["img_key"]==$ik)
						{
							 if($content[$j]["img_url"]!="")
							$imgurl=base_url("uploads/thumbnail/".$content[$j]["img_url"]);
							$img=$content[$j]["img_url"];
							$imgid=$content[$j]["id"];
						}
					}
					if($i%3==0)
					{
					}
					?>
					<div class="col-sm-4">
						<div class="frnd-box gallery">
							<?php 
							if($img!="")
							{
								if(file_exists(DIRECTORY_PATH."uploads/thumbnail/$img"))
								{ ?>
									<img src="<?php echo $imgurl; ?>" class="img-center full-width" alt="No Image"  />
						<?php 	}
								else
								{ ?>
									<img alt="No Image" class="img-center full-width"  src="<?php echo base_url("/Newassets/images/default.png"); ?>"  />
						<?php	}
							}
							else
							{?>
								<img alt="No Image" class="img-center full-width" src="<?php echo $imgurl; ?>"  />
					<?php	}	?>
							<div class="frnd-cntn">
								<form enctype="multipart/form-data" action="<?php echo base_url("user/edit_gallery_image"); ?>" method="post">
									<ul class="list-inline mt-1">
										<li><label for="img<?php echo $i+1; ?>" class="red-text" > <i class="fa fa-pencil" aria-hidden="true"></i></label></li>
								  <?php if($img!="")
										{ ?>
											<li><a class="yellow-text" href="<?php echo base_url().'user/remove_gallery_image/'.$imgid; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
											<?php 
											$img="";
										} ?>
									</ul>
									<input type="hidden" name="position" value="<?php echo $i+1; ?>" />
									<input class="imgs edit-images" type="file" id="img<?php echo $i+1; ?>" name="img<?php echo $i+1; ?>" />
								</form>
							</div>
						</div>
					</div>
		<?php 	} ?>
			</div>
    </section>
</div>