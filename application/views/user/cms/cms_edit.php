<div class="page_wrapper">
	<div class="row padd-bottom"><div class="col-sm-12"><h1 class="page-header"><?php echo lang('lbl_cms_edit_cms'); ?></h1></div></div>
	<?php if (validation_errors()) : ?>
    	<div class="row" align="center">
			<div class="col-md-12 ">
			   <div class="alert myerrormsg" role="error">
					<?= validation_errors() ?>
			   </div>
			</div>
		</div>
	<?php endif; ?>
	<?php if (isset($error)) : ?>
    	<div class="row" align="center">
			<div class="col-md-12 ">
			   <div class="alert myerrormsg" role="error">
					<?= $error ?>
			   </div>
			</div>
		</div>
	<?php endif; ?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><?php echo lang('lbl_cms_edit_cms'); ?></div>
				<div class="panel-body">
					<!-- /.Form start -->
					<form name="add_edit_user" id="add_edit_user" action="<?php echo base_url('admin/Cms/update');?>" enctype="multipart/form-data" method="post">
					    <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $edit_cms['id']; ?>" />
						<div class="form-group">
							<label class="control-label"><?php echo lang('lbl_cms_title'); ?></label>
							<div class="controls">    
								<input class="form-control" type="text" name="cmstitle" id="cmstitle" value="<?php echo $edit_cms['title'];?>"/>	
							</div>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo lang('lbl_cms_slug'); ?></label>
							<div class="controls">    
								<input class="form-control" type="text" name="cmsslug" id="cmsslug" value="<?php echo $edit_cms['slug'];?>"/>	
							</div>
						</div>					
						<div class="form-group">
							<label class="control-label"><?php echo lang('lbl_cms_content'); ?></label>
							<div class="controls">  
								<textarea class="form-control" type="text" name="cmscontent" id="cmscontent"><?php echo $edit_cms['content'];?></textarea>	
							</div>
						</div>							
						<div class="form-actions">
							<input type="submit" class="btn btn-primary" name="btn_submit" id="btn_submit" value="<?php echo lang('lbl_admin_language_edit_update'); ?>"/>
						</div>
					</form>
					<!-- /.form over -->
				</div>
			</div>
		</div>
			<!-- /.col-lg-3 col-md-6 -->
	</div>
</div>
<script>
$("#cmstitle").keyup(function(){
    var Text = $('#cmstitle').val();
	Text = Text.replace(/[^a-z0-9-]/gi,'-');
    Text = Text.toLowerCase();
    Text = Text.replace('/\s/g','-');
    $("#cmsslug").val(Text);
	$('#cmsslug').attr("readonly", "readonly");
});
</script>
<script>tinymce.init({ selector:'textarea' });</script>