<div class="page_wrapper">
	<section>
		<div class="container-fluid text-center">
			<div class="row">
				<div class="col-sm-12 text-uppercase page-title">
					<h3><?php echo lang('lbl_cms_list_cms'); ?></h3>
					<div><img class="img-center" src="" alt=""></div>
				</div>
			</div>  
	<?php 	if(!empty($_SESSION['success']))
			{
				?>
			    	<div class="row" align="center">
						<div class="col-md-12 ">
						   <div class="alert mysuccessmsg" role="error">
								<?php echo $_SESSION['success']?>
						   </div>
						</div>
					</div>
				<?php
				//echo "<h3 class='successful'>$_SESSION[success]</h3>";
			}
			else if(!empty($_SESSION['fail']))
			{
				?>
			    	<div class="row" align="center">
						<div class="col-md-12 ">
						   <div class="alert myerrormsg" role="error">
								<?php echo $_SESSION['fail']?>
						   </div>
						</div>
					</div>
				<?php
				//echo "<h4 class='fail'>$_SESSION[fail]</h4>";
			} ?>    	
	<?php 	if(count($cms)!=0)
			{ 
			/*
			on this page we can display blockeduser information which is requested by another user and from here we can be permenant block that users
			*/
			?>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading"></div>
						<div class="panel-body">
							<div class="button-group">
								<a class="btn btn-primary" href="<?php echo base_url().'admin/Cms/add'  ?>"><?php echo lang('lbl_admin_language_add'); ?></a>
							</div>
							<div class="table-responsive">  
							<!-- /.Table start --> 
								<table  class="table table-striped table-bordered table-hover" id="adminuser-table">
									<thead>
										<tr>
											<th><?php echo lang('lbl_cms_list_id'); ?></th>
											<th><?php echo lang('lbl_admin_notification_title'); ?></th>				
											<th><?php echo lang('lbl_cms_list_slug'); ?></th>
											<th><?php echo lang('lbl_cms_list_content'); ?></th>		
											<th><?php echo lang('lbl_admin_notification_created_date'); ?></th>											
											<th><?php echo lang('lbl_admin_block_request_action'); ?></th>
										</tr>
									</thead>
									<tbody>
										<?php	
										foreach($cms as $row)
										{
										?>
											<tr id="item_1" style="cursor:move">
												<td><?php echo $row['id']; ?></td>
												<td><?php echo $row['title']; ?></td>
												<td><?php echo $row['slug']; ?></td>
												<td><?php echo $row['content']; ?></td>
												<td><?php echo $row['created_date']; ?></td>										
												<td><a class="btn btn-primary" href="<?php echo base_url().'admin/Cms/edit/'.$row['id'];  ?>"><?php echo lang('lbl_admin_language_list_edit'); ?></a> 
												<a class="btn btn-primary" href="<?php echo base_url().'admin/Cms/delete/'.$row['id'];  ?>"><?php echo lang('lbl_admin_language_list_delete'); ?></a> 											
												</td>
											</tr>
										<?php									
										} 
										?> 
									</tbody>
								</table>
							<!-- /.Table over -->
							</div>
						</div>
					</div>
				</div>	
				<!-- /.col-lg-3 col-md-6 -->
			</div>
<?php 	} ?>
		</div>
	</section>
</div>