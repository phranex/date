<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
.ui-slider .ui-slider-handle {
    width:3em;
    left:-.6em;
	hight:14em; 
	background:#FABD0D;
    text-decoration:none;
    text-align:center;
}
.ui-slider-range{
	background:#FABD0D;
}
.slider-tip {
    opacity:1;
    bottom:120%;
    margin-left: -1.36em;
}
.tooltip-inner{
	background:#FABD0D;
	width:3em;
	hight:10em; 
	font-size:1em;
}
.tooltip-arrow{
	color:#FABD0D;
}
.ui-state-default{
	background:none;
}
</style>
<div class="container">
	<div class="register-form">
		<div class="row">
			<div class="col-sm-12">
				<div class="heading"><h3><?php echo lang('lbl_menu_name_register'); ?></h3><p><?php echo lang('lbl_register_register_msg'); ?></p></div>
			</div>
			<div class="col-sm-12">
				<div class="register-icon"><img class="icon" alt="#" src="<?php echo base_url("images/img.png")?>"></div>
			</div>
		</div>
		<div class="row">
			<?php if (isset($error)) : ?>
				<div class="row">
					<div class="col-md-12">
						<div class="alert myerrormsg" role="alert">
						<?php echo $error; ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<div class="col-md-12">
				<div class="container-blog">
					<div class="row">
						<?php if (validation_errors()) : ?>
							<div class="col-md-12">
							   <div class="alert myerrormsg" role="alert">
									<?php echo validation_errors(); ?>
							   </div>
							</div>
						<?php endif; ?>
						<div class="form">
							<div class="row">
								<div class="col-sm-12">
									<?= form_open_multipart() ?>
										<div class="form-group">
											<label for="username"><?php echo lang('lbl_register_username'); ?></label>
											<input type="text" class="form-control" id="username" name="username" placeholder="Enter a username" value="<?php echo set_value('username'); ?>">
										</div>
										<div class="form-group">
											<label for="fname"<?php echo lang('lbl_register_firstname'); ?></label>
											<input type="text" class="form-control" id="fname" name="fname" placeholder="Enter a First Name" value="<?php echo set_value('fname'); ?>">
										</div>
										<div class="form-group">
											<label for="lname"><?php echo lang('lbl_register_lastname'); ?></label>
											<input type="text" class="form-control" id="lname" name="lname" placeholder="Enter a Last Name" value="<?php echo set_value('lname'); ?>">
										</div>
										<div class="form-group">
											<label for="email"><?php echo lang('lbl_register_email'); ?></label>
											<input type="email" class="form-control" id="email" name="email" placeholder="Enter your email" autocomplete="off" value="<?php echo set_value('email'); ?>">
										</div>
										<div class="form-group">
											<label for="password"><?php echo lang('lbl_register_password'); ?></label>
											<input type="password" autocomplete="off" class="form-control" id="password" name="password" placeholder="Enter a password">
										</div>
										<div class="form-group">
											<label for="password_confirm"><?php echo lang('lbl_register_confirm_password'); ?></label>
											<input type="password" autocomplete="off" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm your password">
										</div>
										<div class="form-group">
											<label for="profile_img"><?php echo lang('lbl_register_profile_image'); ?></label>
											<input type="file" class="" id="profile_img" name="profile_img">
										</div>
										<div class="form-group">
											<label for="exampleInputEmail1"><?php echo lang('lbl_register_birthdate'); ?>:</label>
											<div class="row">
												<div class="col-sm-4">
													<input type="number" min="1" max="12" class="form-control" name="Month" id="exampleInputMonth1" placeholder="<?php echo lang('lbl_register_birthdate_month'); ?>">
												</div>
												<div class="col-sm-4">
													<input type="number" min="1" max="31"  class="form-control" id="exampleInputDay" placeholder="<?php echo lang('lbl_register_birthdate_day'); ?>">
												</div>
												<div class="col-sm-4">
													<input type="number" min="<?php echo (date("Y")-65)  ?>" max="<?php echo date("Y") ?>"  class="form-control" id="exampleInputyear" placeholder="<?php echo lang('lbl_register_birthdate_year'); ?>">
												</div>
											</div>
											<div class="form-group">
												<label for="about"><?php echo lang('lbl_register_about'); ?></label>
												<textarea class="form-control" id="about" name="about" placeholder="Massage" rows="3"><?php echo set_value('about'); ?></textarea>
											</div>
											<div class="form-group">
												<label for="gender"><?php echo lang('lbl_register_gender'); ?></label>
												<div class="row">
													<div class="col-sm-6">
														<div class="radio">
															<label><input type="radio"  id="gender" name="gender" value="male"><?php echo lang('lbl_register_male'); ?></label>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="radio">
															<label><input type="radio"  id="gender" name="gender" value="female"><?php echo lang('lbl_register_female'); ?>	</label>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label for="education"><?php echo lang('lbl_register_education'); ?></label>
												<input type="text" class="form-control" id="education" name="education" value="<?php echo set_value('education'); ?>">
											</div>
											<div class="form-group">
												<label for="profession"><?php echo lang('lbl_register_profession'); ?></label>
												<input type="text" class="form-control" id="profession" name="profession" value="<?php echo set_value('profession'); ?>">
											</div>
											<div class="form-group">
												<label for="religion"><?php echo lang('lbl_register_religion'); ?></label>
												<select class="form-control" id="religion" name="religion">
													<?php 
													foreach($religion as $relig){?>
														<option  value="<?php echo $relig['id']; ?>"><?php echo $relig['name']; ?></option>
													<?php }
													?>
												</select>
											</div>
											<div class="form-group">
												<?php
												$hight="3'0 (92 cm), 3'1 (94 cm),3'2 (97 cm),3'3 (99 cm),3'4 (102 cm),3'5 (104 cm),3'6 (107 cm),3'7 (109 cm),3'8 (112 cm),3'9 (114 cm),3'10 (117 cm),3'11 (119 cm),4'0 (122 cm),4'1 (125 cm),4'2 (127 cm),4'3 (130 cm),4'4 (132 cm),4'5 (135 cm),4'6 (137 cm),4'7 (140 cm),4'8 (142 cm),4'9 (145 cm),4'10 (147 cm),4'11 (150 cm),5'0 (152 cm), 5'1 (155 cm),5'2 (158 cm),5'3 (160 cm),5'4 (163 cm),5'5 (165 cm),5'6 (168 cm),5'7 (170 cm),5'8 (173 cm),5'9 (175 cm),5'10 (178 cm),5'11 (180 cm),6'0 (183 cm),6'1 (185 cm),6'2 (188 cm),6'3 (191 cm),6'4 (193 cm),6'5 (196 cm),6'6 (198 cm),6'7 (201 cm),6'8 (203 cm),6'9 (206 cm),6'10 (208 cm),6'11 (211 cm),7'0 (213 cm)";
												$height=explode(",",$hight);					
												?>
												<label for="height"><?php echo lang('lbl_register_height'); ?></label>
												<select class="form-control" id="height" name="height" value="<?php echo set_value('height'); ?>">
													<?php for($i=0;$i<count($height);$i++){  ?>
																<option value="<?php echo $height[$i];  ?>"><?php echo $height[$i];  ?></option>
													<?php  } ?>
												</select>						
											</div>
											<div class="form-group">
												<label for="kids"><?php echo lang('lbl_register_kids'); ?></label>
												<select class="form-control" id="kids" name="kids">
													<option value="1"><?php echo lang('lbl_register_kids'); ?> - 1</option>
													<option value="2"><?php echo lang('lbl_register_kids'); ?> - 2</option>
													<option value="3"><?php echo lang('lbl_register_kids'); ?> - 3</option>
													<option value="4"><?php echo lang('lbl_register_kids'); ?> - 4</option>
													<option value="5"><?php echo lang('lbl_register_kids'); ?> - 5</option>
													<option value="6"><?php echo lang('lbl_register_kids'); ?> - 6</option>
													<option value="7"><?php echo lang('lbl_register_kids'); ?> - 7</option>
													<option value="None"><?php echo lang('lbl_register_none'); ?></option>
													<option value="One Day"><?php echo lang('lbl_register_one_day'); ?></option>
													<option value="I Don''t Want Kids"><?php echo lang('lbl_register_i_dont_want_kids'); ?></option>
												</select>
											</div>
											<div class="form-group">
												<label for="address"><?php echo lang('lbl_register_address'); ?></label>
												<textarea class="form-control" id="address" name="address"  placeholder="Address" rows="3"><?php echo set_value('address'); ?></textarea>
											</div>
										</div>
										<div class="form-group">
											<label for="exampleInputEmail1"><?php echo lang('lbl_register_date_preferances'); ?> :</label>
											<ul id="sortable" class="sortable" style="list-style:none">
												<li class="ui-state-default datepref" id="1" value="1" style="border:0px">
													<div class="Preference-icon">
													<img class="img-center" alt="#" src="Newassets/images/0001.png"></div><div class="text"><?php echo lang('lbl_register_fun'); ?></div></li>
												<li class="ui-state-default datepref " id="2" value="2"  style="border:0px">
													"text"><?php echo lang('lbl_register_food'); ?></div></li>
												<li class="ui-state-default datepref " id="3" value="3"  style="border:0px" >
													<div class="Preference-icon"><img class="img-center" alt="#" src="Newassets/images/0003.png"></div><div class="text"><?php echo lang('lbl_register_coffee'); ?></div></li>
												<li class="ui-state-default datepref " id="4" value="4"  style="border:0px">
													<div class="Preference-icon"><img class="img-center" alt="#" src="Newassets/images/0004.png"></div><div class="text"><?php echo lang('lbl_register_drink'); ?></div></li>
											</ul>
											<input type="hidden" id="datepref" name="datepref" value="1,2,3,4">
										</div>
										<div class="form-group">
											<label for="exampleInputEmail1"><?php echo lang('lbl_register_interested_in'); ?>:</label>
											<div class="row">
												<div class="col-sm-4">
													<div class="radio checked">
														<label  class="Intrested"><input type="radio" name="gender_pref" checked >
														<img class="img-fluid fmale" alt="#" src="<?php  echo base_url("images/fmail.png"); ?>">
														<img class="img-fluid fmale2" alt="#" src="<?php  echo base_url("images/fmail1.png"); ?>"></label>
													</div>
												</div>
												<div class="col-sm-3">  
													<div class="radio">
														<label  class="Intrested"> 
															<input type="radio"  name="gender_pref"  class="Intrested">
															<img class="img-fluid male2" alt="#" src="<?php  echo base_url("images/mail1.png") ?>">
															<img class="img-fluid male1" alt="#" src="<?php  echo base_url("images/mail.png") ?>">
														</label>
													</div>
												</div>
											</div>
										</div>	
										<div class="form-group">
											<label for="age_pref"><?php echo lang('lbl_register_age_preferances'); ?></label>
											<br>
											<br>
											<input type="hidden" class="form-control" name="age-min" id="age-min" value="<?php echo set_value('age-min'); ?>">
											<input type="hidden" class="form-control" name="age-max" id="age-max" value="<?php echo set_value('age-max'); ?>">
											<p>
											</p>
											<div id="age-Preference"></div>
										</div>
										<div class="form-group">
											<label for="age_pref"><?php echo lang('lbl_register_distance_preferances'); ?></label>
											<br/>
											<br/>
											<input type="hidden" class="form-control" name="dist-min" id="dist-min"  value="<?php echo set_value('dist-min'); ?>">
											<input type="hidden" class="form-control" name="dist-max" id="dist-max"  value="<?php echo set_value('dist-max'); ?>">
											<p>
											</p>
											<div id="Distance-Preference"></div>
										</div>
										<div class="form-group">
											<label for="ethnicity"><?php echo lang('lbl_register_ehtnticity'); ?></label>
											<select class="form-control" id="ethnicity" name="ethnicity">
												<?php 
												foreach($ethnicity as $eathni){?>
													<option value="<?php echo $eathni['id']; ?>"><?php echo $eathni['name']; ?></option>
												<?php }
												?>
											</select>
										</div>
										<div class="form-group">
											<label for="question"><?php echo lang('lbl_register_question'); ?></label>
											<select class="form-control" id="question" name="question">
												<?php 
												foreach($questions as $question){?>
													<option value="<?php echo $question['id']; ?>"><?php echo $question['question']; ?></option>
												<?php }
												?>
											</select>
										</div>
										<div class="form-group">
											<label for="question-ans"><?php echo lang('lbl_register_question_answer'); ?></label>
											<input type="text" class="form-control" id="question-ans" name="question-ans" value="<?php echo set_value('question-ans'); ?>">
										</div>
										<div class="form-group">
											<label for="access_loc"><?php echo lang('lbl_register_question_location'); ?></label>
											<input type="radio" id="access_loc" name="access_loc" value="1"><?php echo lang('lbl_register_question_location_yes'); ?> 
											<input type="radio" id="access_loc" name="access_loc" value="0"><?php echo lang('lbl_register_question_location_no'); ?> 
										</div>
										<div style="margin-bottom: 20px ;">
											<div class="g-recaptcha" data-sitekey="6LcodBkUAAAAAFZ0YnwJcR9t4na-vmgV8Cq2q5o4"></div>
										</div>
									</div>
									<div class="line"><hr></div>
									<div class="row">
										<div class="col-sm-9">
											<div class="checkbox">
												<label>
													<input type="checkbox"><?php echo lang('lbl_register_i_agree_to_the'); ?><span style="color:#2cc0d9"> <?php echo lang('lbl_register_tearms_agrement'); ?></span><?php echo lang('lbl_register_privacy_policy'); ?>
												</label>
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="submit" class="btn btn-default gradiant-btn" value="<?php echo lang('lbl_register_submit'); ?>">
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- .row -->
	</div><!-- .container -->
</div>
</div>
  <script>
$( function() {
		$( "#age-Preference" ).slider({
			range: true,
			min: 15,
			max: 60,
			values: [15,60],
			slide: function( event, ui ) {
				  if(ui.values[1] - ui.values[0] < 3){
					  return false;
				  }
				  else
				  {
					   var target = ui.handle || $('.ui-slider-handle'); 
					   var curValue = ui.value || initialValue;
					  $(target).html('<div class="tooltip top slider-tip"><div class="tooltip-arrow"></div><div class="tooltip-inner">' + curValue+ '</div></div>');
						$( "#agepref" ).html( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
						$( "#age-min" ).val(  ui.values[ 0 ] );
						$( "#age-max" ).val(  ui.values[ 1 ] );
				  }
			}
		});
		$( "#agepref" ).html( $( "#age-Preference" ).slider( "values", 0 ) +
		  " -  " + $( "#age-Preference" ).slider( "values", 1 ) );
		//set initial valu to field
		$( "#age-min" ).val(  $( "#age-Preference" ).slider( "values", 0 ));
		$( "#age-max" ).val( $( "#age-Preference" ).slider( "values", 1 ) );
		$( "#Distance-Preference" ).slider({
			  range: true,
			  min: 0,
			  max: 200,
			  values: [0,200],
			  slide: function( event, ui ) {
				  if(ui.values[1] - ui.values[0] < 10){
					  return false;
				  }
				  else
				  {
					   var target = ui.handle || $('.ui-slider-handle'); 
					   var curValue = ui.value || initialValue;
					   $(target).html('<div class="tooltip top slider-tip"><div class="tooltip-arrow"></div><div class="tooltip-inner">' + curValue+ '</div></div>');
						$( "#distpref" ).html( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
						$( "#dist-min" ).val(  ui.values[ 0 ] );
						$( "#dist-max" ).val(  ui.values[ 1 ] );
				  }
			  },
			  start: function( event, ui ) {
			  }
		});
		$(document).ready( function(){
			var sliderValues = [
				$( "#age-Preference" ).slider( "values", 0 ),
				$( "#age-Preference" ).slider( "values", 1 ),
				$( "#Distance-Preference" ).slider( "values", 0 ),
				$( "#Distance-Preference" ).slider( "values", 1 )
			];
			console.log(sliderValues);
			var tmp=0;
			$( '.ui-slider-handle' ).each(function( index ) {
				var target = $(this); 
				var curValue = sliderValues[tmp++];
				$(target).html('<div class="tooltip top slider-tip"><div class="tooltip-arrow"></div><div class="tooltip-inner">' + curValue+ '</div></div>');
			});
		});
		$( "#distpref" ).html( $( "#Distance-Preference" ).slider( "values", 0 ) +
		  " - " + $( "#Distance-Preference" ).slider( "values", 1 ) );
		//set initial valu to field
		$( "#dist-min" ).val(  $( "#Distance-Preference" ).slider( "values", 0 )  );
		$( "#dist-max" ).val(  $( "#Distance-Preference" ).slider( "values", 1 )  );
  } );
  </script>
<script>
$( ".sortable" ).sortable();
$( ".sortable" ).sortable({
  update: function( event, ui ) {
                var strItems = "";
                $("#sortable").children().each(function (i) {
                    var li = $(this);
                    strItems += li.attr("id")+",";
                });
				var strItems = strItems.substring(0, strItems.length-1);
				$("#datepref").val(strItems);
			}
});
</script>