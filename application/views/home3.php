<?php 
$lange= $this->admin_model->get_default_language();
$language=$lange['status'];
$languages = $this->session->userdata('site_lang');
$lg=$languages;
?>
<?php 
 $background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
 $background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
 $background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
 $background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
 $background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
 $background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
 $background8=$this->background_model->getbackgrounditem(array('id'=>'8'));
 ?>
<!--=================================
 banner -->
 <?php 
    $img;$text;
    if(!empty($sliders[1]['id'])){
        $img=$sliders[1]['image'];
        $text=$sliders[1]['title'];
        $t=$sliders[0]['title'];
    }else{
		if(!empty($sliders[0]['id'])){
			$img=$sliders[1]['image'];
			$text=$sliders[1]['title'];
			$t=$sliders[0]['title'];
		}
    }
    ?>
 <section id="main-slider" class="fullscreen">
  <div class="banner banner-3 bg-1" style="background-image: url(<?php echo base_url('Newassets/images/'.$img);?>);" data-gradient-red="4">
    <div class="slider-static">
      <div class="container">
        <div class="row flex justify-content-center">
          <div class="col-lg-7 col-md-6 align-self-center mb-5 xs-mb-2">
            <h1 class="animated3 text-white"><?php echo $t;?></h1> 
          </div>
          <div class="col-lg-5 col-md-6">
            <div class="banner-form">
              <h4><?php echo $text;?></h4>
              <form class="form-inner"> 
                    <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label"><?php echo lang('lbl_home_i_am_a'); ?></label>
                    <div class="col-sm-8 col-xs-8">
                    <select class="selectpicker">
                    <option selected=""><?php echo lang('lbl_home_men');?></option>
                    <option><?php echo lang('lbl_home_women'); ?></option>
                    </select>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label"><?php echo lang('lbl_home_seeking_a'); ?></label>
                    <div class="col-md-8 col-sm-8">
                    <select class="selectpicker">
                    <option><?php echo lang('lbl_home_men');?></option>
                    <option selected=""><?php echo lang('lbl_home_women');?></option>
                    </select>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label"><?php echo lang('lbl_home_from'); ?></label>
                    <div class="col-md-8 col-sm-8">
                    <select class="selectpicker">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                    <option>9</option>
                    <option>10</option>
                    <option>11</option>
                    <option>12</option>
                    <option>13</option>
                    <option>14</option>
                    <option selected="">15</option>
                    <option>16</option>
                    <option>17</option>
                    <option>18</option>
                    <option>19</option>
                    <option>20</option>
                    <option>21</option>
                    <option>22</option>
                    </select>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-md-4 col-sm-4 control-label"><?php echo lang('lbl_home_to'); ?></label>
                    <div class="col-md-8 col-xs-8">
                    <select class="selectpicker">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                    <option>9</option>
                    <option>10</option>
                    <option>11</option>
                    <option>12</option>
                    <option>13</option>
                    <option>14</option>
                    <option>15</option>
                    <option>16</option>
                    <option>17</option>
                    <option>18</option>
                    <option>19</option>
                    <option selected="">20</option>
                    <option>21</option>
                    <option>22</option>
                    </select>
                    </div>
                    </div>
                    <div class="form-group mb-0 text-right col-md-12">
                    <div class="form-group mb-0 text-right col-md-12"> <a class="button btn-lg btn-theme full-rounded animated right-icn"><span><?php echo lang('lbl_sidebar_search'); ?> <i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></a> </div>    
                    </div>
                    </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
  <!--=================================
 timeline-section -->
<section class="page-section-ptb text-center">
    <div class="container">
	<?php if(!empty($blocks[0]['id'])){?>
		<div class="row justify-content-center mb-5 xs-mb-3">
			<div class="col-lg-8">
				<h2 class="title divider-2 mb-3"><?php echo $blocks[0]['title']; ?></h2>
				<p class="lead xs-mt-3"><?php echo $blocks[0]['contain']; ?></p>
			</div>
		</div>
	<?php } ?>
		<div class="row">
		<?php if(!empty($blocks[1]['id'])){?>
		  <div class="col-md-4 xs-mb-3">
		  <div class="timeline-badge mb-2"><img class="img-center" src="<?php echo base_url('images/timeline/01.png'); ?>" alt=""/></div>
			  <h4 class="title divider-3 mb-3"><?php echo $blocks[1]['title']; ?></h4>
			  <p><?php echo $blocks[1]['contain']; ?></p>
		  </div>
		<?php } ?>
		<?php if(!empty($blocks[2]['id'])){?>  
		   <div class="col-md-4 xs-mb-3">
			 <div class="timeline-badge mb-2"><img class="img-center" src="<?php echo base_url('images/timeline/02.png'); ?>" alt=""/></div>
			  <h4 class="title divider-3 mb-3"><?php echo $blocks[2]['title']; ?></h4>
			  <p><?php echo $blocks[2]['contain']; ?></p>
		  </div>
		 	<?php } ?>
			<?php if(!empty($blocks[3]['id'])){?>  
		   <div class="col-md-4">
			 <div class="timeline-badge mb-2"><img class="img-center" src="<?php echo base_url('images/timeline/03.png'); ?>" alt=""/></div>
			  <h4 class="title divider-3 mb-3"><?php echo $blocks[3]['title']; ?></h4>
			  <p><?php echo $blocks[3]['contain']; ?></p>
		  </div>
			<?php } ?>
		</div>
    </div>
</section>
<!--=================================
 timeline-section -->
<!--=================================
story-slider-->
<section class="page-section-ptb grey-bg story-slider"> 
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-10 text-center">
       <h2 class="title divider"><?php echo lang('lbl_home_they_found_true_love');?></h2>
      </div>
  </div>
</div>
<div class="owl-carousel" data-nav-dots="true" data-items="5" data-md-items="4" data-sm-items="3" data-xs-items="2" data-space="30">
<?php
	foreach($stories as $story)
	{
		if(!empty($story['id'])){
		?>
		<div class="item">
			<div class="story-item">
            <div class="story-image clearfix">
			<?php
				if(!empty($story['image']))
				{
					$str=base_url("Newassets/images/thumbnail/".$story['image']);
					if(file_exists(DIRECTORY_PATH."Newassets/images/thumbnail/".$story['image']))
					{						
						?>
						<img class="img-fluid" src="<?php echo $str; ?>" alt="" >
						<?php
					}
					else
					{
						?>
						<img class="img-fluid" src="<?php echo base_url('images/story/01.jpg'); ?>" alt="">
						<?php
					}							
				}
				else
				{
					?><img class="img-fluid" src="<?php echo base_url('images/story/01.jpg'); ?>" alt="">
					<?php
				}
				?>
				<div class="story-link">
					<a href="<?php echo base_url('stories/stories_details/'.$story['id']);?>">
						<i class="glyph-icon flaticon-add"></i>
					</a>
				</div>				
               </div>
              <div class="story-details text-center">
                <h5 class="title divider-3"><?php echo $story['title']; ?></h5>                  
				<div class="about-des mt-3"><?php echo $story['excerpt']; ?></div>              
				</div>
          </div>
        </div>
		<?php
		}
	}
?>
   </div>
</section>
<!--=================================
story-slider-->
<!--=================================
Counter --> 
<section class="page-section-ptb bg bg-overlay-black-30 text-white bg text-center" style="background: url(<?php echo $background8['background_url']; ?>) no-repeat 0 0; background-size: cover;">
<div class="container"><div class="row">
     <div class="col-lg-3 col-md-3 col-sm-6 col-xx-12 text-center">
    <div class="counter fancy">
      <i class="glyph-icon flaticon-people-2"></i>
      <span id="total_users" class="timer" data-to="<?php echo $total_users; ?>" data-speed="10000"><?php echo $total_users; ?></span><label><?php echo lang('lbl_home_total_members'); ?></label>
    </div>
  </div>
   <div class="col-lg-3 col-md-3 col-sm-6 col-xx-12 text-center">
    <div class="counter fancy">
      <i class="glyph-icon flaticon-favorite"></i>
      <span id="online_users" class="timer" data-to="<?php echo $online_users; ?>" data-speed="10000"><?php echo $online_users; ?></span><label><?php echo lang('lbl_home_online_members'); ?></label>
    </div>
  </div>
   <div class="col-lg-3 col-md-3 col-sm-6 col-xx-12 text-center">
    <div class="counter fancy">
      <i class="glyph-icon flaticon-wedding-rings"></i>
      <span id="male_users" class="timer" data-to="<?php echo $male_users; ?>" data-speed="10000"><?php echo $male_users; ?></span><label><?php echo lang('lbl_home_men_online'); ?></label>
    </div>
  </div>
   <div class="col-lg-3 col-md-3 col-sm-6 col-xx-12 text-center">
    <div class="counter fancy">
      <i class="glyph-icon flaticon-love-birds"></i>
      <span id="female_users" class="timer" data-to="<?php echo $female_users; ?>" data-speed="10000"><?php echo $female_users; ?></span><label><?php echo lang('lbl_home_women_online'); ?></label>
    </div>
  </div>
</div></div></section>
<!--=================================
counter  -->
   <!--=================================
Blogs -->
<section class="page-section-ptb">
<div class="container">
	<?php if(!empty($blocks[5]['id'])){?>
    <div class="row justify-content-center mb-5 xs-mb-3">
        <div class="col-lg-10 text-center">
        <h2 class="title divider-2 mb-3"><?php echo $blocks[5]['title']; ?></h2>
            <p class="lead"><?php echo $blocks[5]['contain']; ?></p>
        </div>
    </div>
	<?php } ?>
	<div class="row post-article" id="ajax_table">
		<?php
		$cnt=1;$b_id;
		foreach($blogs as $blog)
		{	
			$b_id=$blog['id'];
			if(!empty($blog['id']))
			{
				if($cnt<=2)
				{ 
				   ?>
				   <div class="col-lg-6 col-md-6 col-sm-6">
					  <div class="post right-pos">
						  <div class="post-image clearfix">
					  <?php
					 if(!empty($blog['image']))
					 {
					  $str=base_url("Newassets/images/thumbnail/".$blog['image']);
					  if(file_exists(DIRECTORY_PATH."Newassets/images/thumbnail/".$blog['image']))
					  {      
					   ?>
					   <img class="img-fluid" src="<?php echo $str; ?>" alt="" >
					   <?php
					  }
					  else
					  {
					   ?>
					   <img class="img-fluid" src="<?php echo base_url('images/blog/01.jpg');?>" alt="">
					   <?php
					  }       
					 }
					 else
					 {
					  ?><img class="img-fluid" src="<?php echo base_url('images/blog/01.jpg');?>" alt="">
					  <?php
					 }
					 ?>
					</div>
					<div class="post-details">
					<div class="post-date"><?php echo date_format(date_create($blog['created_date']),"d"); ?><span><?php echo date_format(date_create($blog['created_date']),"M"); ?></span></div>
						  <div class="post-meta">
									  <a href="#"><i class="fa fa-user"></i> <?php echo $blog['author']; ?></a>
									  <a href="#"><i aria-hidden="true" class="fa fa-heart-o"></i>98 Like</a>
									  <a href="#"><i class="fa fa-comments-o last-child"></i>Comments</a>
						  </div>
						  <div class="post-title"><h5 class="title text-uppercase"><a href="<?php if($this->session->userdata('logged_in'))  echo base_url('blog/blog_details/'.$blog['id']); else "#";?>"><?php echo $blog['title']; ?></a></h5></div>
						  <div class="post-content"><p><?php echo substr($blog['description'], 0, 100); ?></p></div>
						  <a class="button" href="<?php if($this->session->userdata('logged_in'))  echo base_url('blog/blog_details/'.$blog['id']); else "#";?>"><?php echo lang('lbl_home_read_more');?></a>
					  </div>                
					  </div>  
					</div>
				<?php
				}
				$cnt++;
			}
		}
		?>
	</div>
	<div class="row mt-5 xs-mt-0">
		<div class="col-md-12 text-center">
		  <button class="button  btn-lg btn-theme full-rounded animated right-icn" id="load_more" data-val="0" data-type="<?php echo $b_id;?>"><span><?php echo lang('lbl_home_show_more');?><i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span>
		  <img style="display: none" id="loader" src="<?php echo str_replace('index.php','',base_url('Newassets/images/loader.GIF')); ?>">
		  </button>
		</div>
	</div>
 </div>
</section>
<!--=================================
Blogs -->   
<!--=================================
Testimonial--> 
<section class="page-section-ptb gray-bg">
   <div class="container">
    <div class="row justify-content-center">
		<div class="col-lg-8 text-center">
			<h2 class="title divider"><?php echo lang('lbl_home_testimonials'); ?></h2>
		</div>
	</div>
    <div class="row justify-content-center">
         <div class="col-lg-10"><div class="owl-carousel" data-nav-dots="true" data-items="1" data-md-items="1" data-sm-items="1">
			<?php
			foreach($testimonials as $testomonial)
			{
				if(!empty($testomonial['id'])){
    			?>
                <div class="item">
					<div class="testimonial left_pos">
                    <div class="testimonial-avatar">
                    <?php
					if(!empty($testomonial['author_image']))
					{
						$str=base_url("Newassets/images/thumbnail/".$testomonial['author_image']);
						if(file_exists(DIRECTORY_PATH."Newassets/images/thumbnail/".$testomonial['author_image']))
						{						
							?>
							<img src="<?php echo $str; ?>" alt=""/></div>
							<?php
						}
						else
						{
							?>
							<img src="<?php echo base_url('Newassets/images/thumbnail/thum-0.png'); ?>" alt=""></div>
							<?php
						}							
					}
					else
					{
						?><img src="<?php echo base_url('Newassets/images/thumbnail/thum-0.png'); ?>" alt=""/></div>
						<?php
					}
					?>
                    <div class="testimonial-info"><?php echo $testomonial['description']; ?></div>
                    <div class="author-info"> <strong><?php echo $testomonial['author']; ?></strong> </div>
					</div></div>
                <?php
				}
			}
			?>  
            </div>
    </div>
   </div>
</section>
<section class="page-section-ptb">
    <div class="container">
		<?php
		foreach($our_team_member as $team_member){
			if(!empty($team_member['id'])){
				?>
				<div class="row justify-content-center mb-5 sm-mb-3">
					<div class="col-lg-10 text-center">
						<h2 class="title divider mb-3"><?php echo $team_member['title']; ?></h2>
						<p class="lead"><?php  echo $team_member['contain']; ?></p>
					</div>
				</div>
			<?php
			}
		}
		?>
        <div class="row">
            <?php 
			foreach($members as $membr){ 
				if(!empty($membr['id'])){
				?>
				<div class="col-lg-3 col-md-6">
				<div class="team boxed">
						<div class="team-images"> 
							<img class="img-fluid" src="<?php echo base_url('Newassets/images/'.$membr['member_image']);?>" alt=""/> 
						</div>
						<div class="team-description">
						<h5 class="title"><a href="<?php echo base_url('team/singleteam/'.$membr['id']); ?>"><?php echo $membr['member_name'] ?></a></h5>
						<span><?php echo $membr['designation']; ?></span>
						<p><?php echo substr($membr['member_detail'], 0, 100).'...' ?></p>
						<div class="team-social-icon social-icons color-hover">
							<ul>
							<?php if($membr['facebook_url']!=''){ ?><li class="social-facebook"><a href="<?php echo $membr['facebook_url']; ?>"><i class="fa fa-facebook"></i></a></li><?php } ?>
					<?php if($membr['twitter_url']!=''){ ?><li class="social-twitter"><a href="<?php echo $membr['twitter_url']; ?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
					<?php if($membr['google_url']!=''){ ?><li class="social-gplus"><a href="<?php echo $membr['google_url']; ?>"><i class="fa fa-google-plus"></i></a></li><?php } ?>
					<?php if($membr['dribble_url']!=''){ ?><li class="social-dribbble"><a href="<?php echo $membr['dribble_url']; ?>"><i class="fa fa-dribbble"></i></a></li><?php } ?>
							</ul>
							</div>
						</div>
					</div>
				</div>
				<?php 
				}
			}
			?>
        </div>
    </div>
</section>
<!--=================================
 header -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript">
     $(document).ready(function(){
        $("#load_more").click(function(e){
            e.preventDefault();
			var page = $(this).data('val');
			var blog_id = $(this).data('type');
            getblog(page,blog_id);
        });
    });
    var getblog = function(page,blog_id){
        $("#loader").show();
        $.ajax({
            url:"<?php echo base_url() ?>user/getblog",
            type:'GET',
            data: {page:page,blog_id:blog_id}
        }).done(function(response){
            $("#ajax_table").append(response);
            $("#loader").hide();
            $('#load_more').data('val', ($('#load_more').data('val')+1));
            scroll();
			$("#load_more").hide();
        });
    };
    var scroll  = function(){
        $('html, body').animate({
            scrollTop: $('#load_more').offset().top
        }, 1000);
    };
</script>