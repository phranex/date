<div class="wrapper <?php echo (strcmp($_SESSION['site_lang'],"arabic")==0)?"arebic":""; ?>" >
	<?php
	/*
	From here display sidebar menu which is display on left side
	*/
	?>
    <div class="sidebar" data-color="purple" data-image="<?php echo base_url('images/sidebar-bg.png');?>">
		<div class="sidebar-wrapper">
			<div class="logo">
                <a href="<?php echo base_url();?>" class="simple-text">
                   <img src="<?php echo base_url('Newassets/images/logo.png')?>"  alt="">
                </a>
            </div>
			<ul class="nav">
				<li class="<?php echo ($this->uri->segment(2)=="dashboard")?"active":""; ?>">
					<a href="<?php echo base_url('user/dashboard'); ?>">
						<i class="fa fa-tachometer" aria-hidden="true"></i>
						<p><?php echo lang('lbl_sidebar_dashboard'); ?></p>
					</a>
				</li>               
				<li class="<?php echo ($this->uri->segment(2)=="friend")?"active":""; ?>">
					<a href="<?php echo base_url('friend'); ?>">
						<i class="fa fa-users" aria-hidden="true"></i>
						<p><?php echo lang('lbl_sidebar_friends'); ?></p>
					</a>
				</li>
				<li class="<?php echo ($this->uri->segment(2)=="search")?"active":""; ?>">
					<a href="<?php echo base_url().'search'; ?>">
						<i class="fa fa-search" aria-hidden="true"></i>
						<p><?php echo lang('lbl_sidebar_search'); ?></p></a>
				</li>
				<li class="<?php echo ($this->uri->segment(2)=="gallery")?"active":""; ?>">
					<a href="<?php echo base_url().'user/gallery'; ?>">
						<i class="fa fa-picture-o" aria-hidden="true"></i>
						<p><?php echo lang('lbl_sidebar_gallery'); ?></p>
					</a>
				</li>
				<li class="<?php echo ($this->uri->segment(2)=="notification")?"active":""; ?>">
					<a href="<?php echo base_url().'notification'; ?>"> 
						<i class="fa fa-bell" aria-hidden="true"></i>
						<p><?php echo lang('lbl_sidebar_notification'); ?></p>
					</a>
				</li>	
				<li class="<?php echo ($this->uri->segment(2)=="logout")?"active":""; ?>">
					<a href="<?php echo base_url().'user/logout'; ?>"> 
						<i class="fa fa-sign-out" aria-hidden="true"></i>
						<p><?php echo lang('lbl_sidebar_log_out'); ?></p>
					</a>
				</li>	
			</ul>
        </div>
    </div>
    <div class="main-panel">
		<nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell"></i>
                                    <b class="caret hidden-sm hidden-xs"></b>
                                    <span class="notification hidden-sm hidden-xs"></span> 
                                    <p class="hidden-lg hidden-md"><?php echo lang('lbl_sidebar_notification'); ?>s<b class="caret"></b></p>
                            </a>
                            <ul class="dropdown-menu notification-box text-center">
								<h4 class="text-center"><?php echo lang('lbl_sidebar_notification'); ?></h4>
								<?php 
								foreach($notification as $friend) { ?>
									<li>
										<ul>
											<li><div class="img-box">
												<?php if(!empty($friend['profile_image'])){ if(file_exists(DIRECTORY_PATH."/uploads/thumbnail/".$friend['profile_image']) or $friend['profile_image']!=""){ ?>                                
												<img src="<?php echo base_url("uploads/thumbnail/".$friend['profile_image']); ?>" class="img-center"  />
												<?php }}
													else
													{?>
														<img src="<?php echo base_url("/assets/images/default.png"); ?>" class="img-center"  />		
													<?php 
													}
													?>
												</div></li>										
											<li>
												<h5><a href="<?php echo base_url('friends/friend_profile/'.$friend['id']); ?>"><?php echo $friend['fname'].' '.$friend['lname']; ?></a></h5>
												<span><?php echo number_format($friend['distance'],2);  ?> <?php echo lang('lbl_sidebar_miles'); ?></span>
											</li>
											<li>
												<ul class="like-btn">
													<li><a href="<?php echo base_url().'friends/friend_decline/'.$friend['send_user_id']; ?>"><i class="fa fa-thumbs-o-down text-red" aria-hidden="true"></i></a></li>
													<li><a href="<?php echo base_url().'friends/friend_approved/'.$friend['send_user_id']; ?>"><i class="fa fa-thumbs-o-up text-green" aria-hidden="true"></i></a></li>
												</ul>
											</li>
										</ul>
									</li>
									<?php 
								}  ?>
                            </ul>
                        </li>
                        <li class="dropdown">
                           <a href="<?php echo base_url('user/profile'); ?>"><i class="fa fa-cog" aria-hidden="true"></i></a>
                        </li>
                        <li>
							<a class="profile-pic" href="#"> <img src="<?php if(!empty($_SESSION['profile_image'])){ echo base_url("uploads/thumbnail/".$_SESSION['profile_image']);} else{echo base_url("/assets/images/default.png");}?>"  alt="user-img" class="img-circle">
							<b><?php echo $_SESSION["fname"]; ?></b>
							</a>
                        </li>
					</ul>
                </div>
			</div> 
        </nav>