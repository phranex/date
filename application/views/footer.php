<!--footer section-->
<?php
$lange= $this->admin_model->get_default_language();
$language=$lange['status'];
$languages = $this->session->userdata('site_lang');
$lg=$languages;
?>
<!--Subscribe section-->
<?php
foreach($subscribe as $sub){
	if(!empty($sub['id'])){
		echo $sub['contain'];
	}
}
?>
<?php
foreach($footer_contact_us as $contact_us){
	if(!empty($contact_us['id'])){
		echo $contact_us['contain'];
	}
}
?>
<!--======footer -->
<footer class="text-white text-center">
	<div class="footer-widget">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="footer-logo mb-2">
					<?php
						if(!empty($header[2]['status']))
						{
							$str=base_url("Newassets/images/".$header[2]['status']);
							if(file_exists(DIRECTORY_PATH."Newassets/images/".$header[2]['status']))
							{						
								?>
								<img class="img-center" src="<?php echo $str; ?>" alt="">
								<?php
							}
							else
							{
								?>
								<img class="img-center"  src="images/thumbnail/01.jpg" alt="">
								<?php
							}							
						}
						else
						{
							?><img class="img-center"  src="images/thumbnail/01.jpg" alt="">
							<?php
						}
						?>
					</div>
					<div class="social-icons color-hover mb-1">
						<ul>
							<li class="social-facebook"><a href="<?php echo $header[6]['status']; ?>"><i class="fa fa-facebook"></i></a></li>
							<li class="social-twitter"><a href="<?php echo $header[7]['status']; ?>"><i class="fa fa-twitter"></i></a></li>
							<li class="social-dribbble"><a href="<?php echo $header[8]['status']; ?>"><i class="fa fa-dribbble"></i></a></li>
							<li class="social-gplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
							<li class="social-youtube"><a href="#"><i class="fa fa-youtube"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer> 
<?php $this->load->view('user/_crop'); ?>	
<!--=================================footer -->
<?php 
$this->FACEBOOK_KEY = $this->Common_model->get_key_configuration(array('key'=>'FACEBOOK_KEY'));
?>
<div id="back-to-top"><a class="top arrow" href="#top"><i class="fa fa-level-up"></i></a></div>
	</body>
	<!-- Bootstrap core CSS     -->		
	<?php 
	if($this->Common_model->is_rtl()){
		?>
		<link href="<?php  echo base_url("Newassets/css/bootstrap-rtl.min.css"); ?>" rel="stylesheet" />		
		<?php 
	}
	else{
		?>
		<link href="<?php  echo base_url("Newassets/css/bootstrap.min.css"); ?>" rel="stylesheet" />
		<?php
	}
	?>
	<link href="<?php  echo base_url("Newassets/css/bootstrap-slider.min.css"); ?>" rel="stylesheet" />
	<link href="<?php  echo base_url("Newassets/css/bootstrap-select.min.css"); ?>" rel="stylesheet" />
	<link href="<?php  echo base_url("Newassets/css/mega-menu/mega_menu.css"); ?>" rel="stylesheet" />
	<link href="<?php  echo base_url("Newassets/css/magnific-popup/magnific-popup.css"); ?>" rel="stylesheet" />
	<link href="<?php  echo base_url("Newassets/css/font-awesome.min.css"); ?>" rel="stylesheet" />
	<link href="<?php  echo base_url("Newassets/css/flaticon.css"); ?>" rel="stylesheet" />
	<link href="<?php  echo base_url("Newassets/css/animate.min.css"); ?>" rel="stylesheet"/>
	<link href="<?php  echo base_url("Newassets/css/general.css"); ?>" rel="stylesheet" />
	<link href="<?php  echo base_url("Newassets/css/style.css"); ?>" rel="stylesheet" />
	<link href="<?php  echo base_url("Newassets/css/owl.carousel.css"); ?>" rel="stylesheet" />
	<link href="<?php  echo base_url("Newassets/css/jquery-ui.min.css"); ?>" rel="stylesheet" />
	<link href="<?php  echo base_url("Newassets/crop/dist/cropper.min.css"); ?>" rel="stylesheet" />
	<link href="<?php  echo base_url("Newassets/crop/css/main.css"); ?>" rel="stylesheet" />
	
	<!--  Light Bootstrap Table core CSS    -->
	<!--     Fonts and icons     -->
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> 
	<link href="<?php  echo base_url("Newassets/css/pe-icon-7-stroke.css"); ?>" rel="stylesheet" />
	<!--Skin Load as per admin settings -->
	<?php $site_style=$header[19]['status']; ?>
	<?php if(file_exists(DIRECTORY_PATH."Newassets/css/skins/".$site_style.".css")) { ?>
		<link href="<?php  echo base_url("Newassets/css/skins/".$site_style.".css"); ?>" rel="stylesheet" />
	<?php } ?>

	<?php 
	if($this->Common_model->is_rtl()){
		?>			
		<link href="<?php  echo base_url("Newassets/css/rtl.css"); ?>" rel="stylesheet" /> 
		<?php 
	}
	?>
	<!--js -->
	<script>
		var BASE_URL='<?php echo base_url(); ?>';
		var FB_KEY='<?php echo $this->FACEBOOK_KEY;  ?>';
	</script>
	<script src="<?php echo base_url('Newassets/js/jquery.min.js');?>" type="text/javascript"></script>
	<script src="<?php echo base_url('Newassets/js/popper.min.js');?>" type="text/javascript"></script>
	
	<script src="<?php  echo base_url("Newassets/js/bootstrap.min.js"); ?>" type="text/javascript"></script>
	<script src="<?php  echo base_url("Newassets/js/bootstrap-slider.js"); ?>"  type="text/javascript"></script>
	<!--script src="<?php  echo base_url("Newassets/js/bootstrap-slider.min.js"); ?>" type="text/javascript"></script-->	
	<script src="<?php  echo base_url("Newassets/crop/dist/cropper.min.js"); ?>" type="text/javascript"></script>
	<script src="<?php  echo base_url("Newassets/crop/js/main.js"); ?>" type="text/javascript"></script>
	<script src="<?php  echo base_url("Newassets/js/gallery.min.js"); ?>" type="text/javascript"></script>
	<?php if(!empty($JS_Middle)) { ?>	
		<?php foreach($JS_Middle as $js) { ?>
			<script src="<?php  echo $js; ?>" type="text/javascript"></script>
		<?php } ?>
	<?php } ?>
	<!--   Core JS Files   -->
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
	<script src="<?php  echo base_url("Newassets/js/jquery.downCount.js"); ?>" type="text/javascript"></script>
	<script src="<?php  echo base_url("Newassets/js/bootstrap-notify.js"); ?>"  type="text/javascript"></script>
	<script src="<?php  echo base_url("Newassets/js/demo.js"); ?>"  type="text/javascript"></script>
	<script src="<?php  echo base_url("Newassets/js/bootstrap-select.js"); ?>"  type="text/javascript"></script>
	<script src="<?php  echo base_url("Newassets/js/bootstrap-select.min.js"); ?>"  type="text/javascript"></script>
	<script src="<?php  echo base_url("Newassets/js/magnific-popup/jquery.magnific-popup.min.js"); ?>"  type="text/javascript"></script>
	<script src="<?php  echo base_url("Newassets/js/chartist.min.js"); ?>"></script>
	<script src="<?php  echo base_url("Newassets/js/counter/jquery.countTo.js"); ?>"  type="text/javascript"></script>
	<script src="<?php  echo base_url("Newassets/js/light-bootstrap-dashboard.js"); ?>"  type="text/javascript"></script>
    <script src="<?php  echo base_url("Newassets/js/owl.carousel.min.js"); ?>"  type="text/javascript"></script>
	<?php 
	if($this->Common_model->is_rtl()){
		?>
		<script src="<?php  echo base_url("Newassets/js/custom-rtl.js"); ?>"></script>	
		<?php 
	}
	else{
		?>
		<script src="<?php  echo base_url("Newassets/js/custom.js"); ?>"></script>
		<?php
	}
	?>
	<script src="<?php  echo base_url("Newassets/js/jquery.appear.js"); ?>" type="text/javascript"></script>
	<script src="<?php  echo base_url("Newassets/js/mega-menu/mega_menu.js"); ?>" type="text/javascript"></script>
	<script src="<?php  echo base_url("Newassets/js/footer.min.js"); ?>" type="text/javascript"></script>
	<?php if(!empty($Additional_JS)) { ?>	
		<?php foreach($Additional_JS as $js) { ?>
			<script src="<?php  echo $js; ?>" type="text/javascript"></script>
		<?php } ?>
	<?php } ?>
</script> 
<!--footer section-->
</html>