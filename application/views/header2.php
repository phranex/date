<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
?>
<!-- header2 -->
<!DOCTYPE html>
<html lang="en"  <?php 
		if(!empty($this->session->userdata("rtl_lang"))){
			echo "dir='rtl'";
		}
		?>>
<head>
	    <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Cupid Love - Dating HTML5 Template" />
		<meta name="author" content="potenzaglobalsolutions.com" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<title>Cupid Love Web</title>
        <script src="<?php echo base_url('Newassets/js/jquery.min.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('Newassets/js/popper.min.js');?>" type="text/javascript"></script>
        <!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo base_url('images/favicon.ico');?>"/>		 
		<!-- Bootstrap core CSS     -->
		<?php 
		if(!empty($this->session->userdata("rtl_lang"))){
			?>
			<link href="<?php  echo base_url("Newassets/css/bootstrap-rtl.min.css"); ?>" rel="stylesheet" />
			<?php 	
		}
		else{
			?>
			<link href="<?php  echo base_url("Newassets/css/bootstrap.min.css"); ?>" rel="stylesheet" />
			<?php
		}
		?>		
		<link href="<?php  echo base_url("Newassets/css/bootstrap-slider.min.css"); ?>" rel="stylesheet" />
		<link href="<?php  echo base_url("Newassets/css/bootstrap-select.min.css"); ?>" rel="stylesheet" />
		<link href="<?php  echo base_url("Newassets/css/mega-menu/mega_menu.css"); ?>" rel="stylesheet" />
		<link href="<?php  echo base_url("Newassets/css/magnific-popup/magnific-popup.css"); ?>" rel="stylesheet" />
		<link href="<?php  echo base_url("Newassets/css/font-awesome.min.css"); ?>" rel="stylesheet" />
		<link href="<?php  echo base_url("Newassets/css/flaticon.css"); ?>" rel="stylesheet" />
		<link href="<?php  echo base_url("Newassets/css/animate.min.css"); ?>" rel="stylesheet"/>
		<link href="<?php  echo base_url("Newassets/css/general.css"); ?>" rel="stylesheet" />
		<link href="<?php  echo base_url("Newassets/css/style.css"); ?>" rel="stylesheet" />
		<link href="<?php  echo base_url("Newassets/css/owl.carousel.css"); ?>" rel="stylesheet" />
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<link href="<?php  echo base_url("Newassets/crop/dist/cropper.min.css"); ?>" rel="stylesheet" />
		<link href="<?php  echo base_url("Newassets/crop/css/main.css"); ?>" rel="stylesheet" />
		
		<!--  Light Bootstrap Table core CSS    -->
		<!--     Fonts and icons     -->
		<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> 
		<link href="<?php  echo base_url("Newassets/css/pe-icon-7-stroke.css"); ?>" rel="stylesheet" />
		<script src="<?php  echo base_url("Newassets/js/bootstrap.min.js"); ?>" type="text/javascript"></script>
		<script src="<?php  echo base_url("Newassets/js/bootstrap-slider.js"); ?>"  type="text/javascript"></script>
		<script src="<?php  echo base_url("Newassets/js/bootstrap-slider.min.js"); ?>" type="text/javascript"></script>	
		<script src="<?php  echo base_url("Newassets/crop/dist/cropper.min.js"); ?>" type="text/javascript"></script>
		<script src="<?php  echo base_url("Newassets/crop/js/main.js"); ?>" type="text/javascript"></script>
		<!--Skin Load as per admin settings -->
		<?php $site_style=$header[19]['status']; ?>
		<?php if(file_exists(DIRECTORY_PATH."Newassets/css/skins/".$site_style.".css")) { ?>
			<link href="<?php  echo base_url("Newassets/css/skins/".$site_style.".css"); ?>" rel="stylesheet" />
		<?php } ?>
		<?php 
		if(!empty($this->session->userdata("rtl_lang"))){
			?>
			<link href="<?php  echo base_url("Newassets/css/rtl.css"); ?>" rel="stylesheet" /> 
			<?php 	
		}
		?>
		<script>
		$(function(){
			$("#sel_lang").on("change",function(){
				$('#frm_lg').submit();
			});
		});
		</script>
	</head>
	<body id="main" <?php echo (isset($_GET['rtl'])) ? 'dir="rtl"' : ''; ?>>
<div id="preloader">
  <div class="clear-loading loading-effect"><img src="<?php echo base_url('images/loading.gif');?>" alt=""></div>
</div>
<header id="header" class="defualt">
<div class="topbar">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="topbar-left">
           <ul class="list-inline">
             <li><a href="mailto:support@website.com"><i class="fa fa-envelope-o"> </i> support@website.com </a></li>
             <li><a href="tel:(007)1234567890"><i class="fa fa-phone"></i> (007) 123 456 7890 </a></li> 
           </ul>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="topbar-right text-right">
           <ul class="list-inline social-icons color-hover">
             <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>   
             <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>   
             <li class="social-instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>   
             <li class="social-dribbble"><a href="#"><i class="fa fa-dribbble"></i></a></li>   
           </ul>
           <ul class="list-inline text-uppercase top-menu">
		   <?php
			if(!$this->session->userdata('logged_in'))
			{
			?>
             <li><a href="<?php echo base_url('register');?>"><?php echo lang('lbl_menu_name_register'); ?></a></li>
             <li><a href="<?php echo base_url('login');?>"><?php echo lang('lbl_menu_name_login'); ?></a></li>  
			<?php			 
			}
			?>
           </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!--=================================
 mega menu -->
<div class="menu">  
  <!-- menu start -->
   <nav id="menu" class="mega-menu">
    <!-- menu list items container -->
    <section class="menu-list-items">
     <div class="container"> 
      <div class="row"> 
       <div class="col-lg-12 col-md-12"> 
        <!-- menu logo -->
        <ul class="menu-logo">
            <li>
                <a href="<?php echo base_url();?>"><img src="<?php echo base_url('images/logo.png');?>" alt="logo"> </a>
            </li>
        </ul>
        <!-- menu links -->
        <ul class="menu-links">
            <!-- active class -->
            <li><a href="<?php echo base_url('user/index'); ?>"> <?php echo lang('lbl_menu_name_home'); ?></a></li>
            <li><a href="javascript:void(0)"><?php echo lang('lbl_menu_name_profiles'); ?> <i class="fa fa-angle-down fa-indicator"></i></a>
                 <!-- drop down multilevel  -->
                <ul class="drop-down-multilevel left-menu">
                    <li><a href="<?php echo base_url('user/dashboard'); ?>"><?php echo lang('lbl_sidebar_dashboard'); ?></a></li>
					<li><a href="<?php echo base_url('user/my_profile'); ?>"><?php echo lang('lbl_menu_name_my_profile'); ?></a></li>
					<li><a href="<?php echo base_url('user/my_preferance'); ?>"><?php echo lang('lbl_menu_name_my_preferances'); ?></a></li>
					<li><a href="<?php echo base_url('user/location'); ?>"><?php echo lang('lbl_location'); ?></a></li>
                    <li><a href="<?php echo base_url('friends'); ?>"><?php echo lang('lbl_sidebar_friends'); ?></a></li>
					<li><a href="<?php echo base_url('chat'); ?>"><?php echo lang('lbl_chat'); ?></a></li>	
                    <li><a href="<?php echo base_url('user/gallery'); ?>"><?php echo lang('lbl_sidebar_gallery'); ?></a></li>
                 </ul>
            </li>
			<li><a href="<?php echo base_url('stories'); ?>"><?php echo lang('lbl_menu_name_stories'); ?></a></li>
            <li><a href="<?php echo base_url('about'); ?>"><?php echo lang('lbl_menu_name_about'); ?></a></li>
            <li><a href="<?php echo base_url('team'); ?>"><?php echo lang('lbl_menu_name_team'); ?></a></li>
            <li><a href="javascript:void(0)"><?php echo lang('lbl_menu_name_pages'); ?> <i class="fa fa-angle-down fa-indicator"></i></a>
                 <!-- drop down multilevel  -->
                <ul class="drop-down-multilevel left-menu">
                    <li><a href="<?php echo base_url('error404'); ?>"><?php echo lang('lbl_menu_name_404_error'); ?></a></li>
					<li><a href="<?php echo base_url('comingsoon'); ?>"><?php echo lang('lbl_menu_name_coming_soon'); ?></a></li>
					<li><a href="<?php echo base_url('privacy'); ?>"><?php echo lang('lbl_menu_name_privacy_policy'); ?></a></li>
                    <li><a href="<?php echo base_url('Terms_conditions'); ?>"><?php echo lang('lbl_menu_name_terms_conditions'); ?></a></li>
                    <li><a href="<?php echo base_url('faq'); ?>"><?php echo lang('lbl_menu_name_faq'); ?></a></li>
                 </ul>
            </li>
            <li><a href="<?php echo base_url('blog');?>"> <?php echo lang('lbl_menu_name_blog'); ?></a></li>
            <li><a href="<?php echo base_url('contact');?>"> <?php echo lang('lbl_menu_name_contact'); ?></a></li>
			<?php if($this->session->userdata('logged_in')) {?>
			<li><a href="<?php echo base_url('search'); ?>"><?php echo lang('lbl_sidebar_search'); ?> <span class="search hidden-sm hidden-xs"></span></a></li>	<?php } ?>	
            <?php if($this->session->userdata('logged_in')) {?>
			<li><a href="<?php echo base_url('notification');?>"> <?php echo lang('lbl_menu_name_notification'); ?></a></li>
			<li><a href="<?php echo base_url().'user/logout'; ?>"><?php echo lang('lbl_sidebar_log_out'); ?></a></li><?php } ?>
        </ul>
       </div>
      </div>
     </div>
    </section>
   </nav>
  <!-- menu end -->
 </div>
</header>