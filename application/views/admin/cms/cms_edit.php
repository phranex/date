<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script>
$( function() {
    $( "#tabs" ).tabs();
});
</script>
<div class="page_wrapper">
	<div class="row padd-bottom"><div class="col-sm-12"><h1 class="page-header"><?php echo lang('lbl_admin_cms_edit'); ?></h1></div></div>
	<?php if (validation_errors()) : ?>
		<div class="col-md-12">
			<div class="alert alert-danger" role="alert">
				<?= validation_errors() ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if (isset($error)) : ?>
		<div class="col-md-12">
			<div class="alert alert-danger" role="alert">
				<?= $error ?>
			</div>
		</div>
	<?php endif; ?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><?php echo lang('lbl_admin_cms_edit'); ?></div>
				<div class="panel-body">
					<!-- /.Form start -->
					<form name="add_edit_user" id="add_edit_user" action="<?php echo base_url('admin/Cms/update');?>" enctype="multipart/form-data" method="post">
						<input class="form-control" type="hidden" name="id" id="id" value="<?php echo (!empty($edit_cms[0]['id'])) ? $edit_cms[0]['id'] : ""; ?>"/>
						<div class="form-group">
							<label class="control-label"><?php echo lang('lbl_admin_cms_title'); ?> </label>
							<div class="controls">    
								<input class="form-control" type="text" name="cmstitle" id="cmstitle" value="<?php echo (!empty($edit_cms[0]['title'])) ? $edit_cms[0]['title'] : ""; ?>"/>	
							</div>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo lang('lbl_admin_cms_slug'); ?> </label>
							<div class="controls">    
								<input class="form-control" type="text" name="cmsslug" id="cmsslug" value="<?php echo (!empty($edit_cms[0]['slug'])) ? $edit_cms[0]['slug'] : ""; ?>"/>	
							</div>
						</div>					
						<div class="form-group">
							<label class="control-label"><?php echo lang('lbl_admin_cms_content'); ?> </label>
							<div class="controls">  
								<textarea class="form-control" type="text" name="cmscontent" id="cmscontent"><?php echo (!empty($edit_cms[0]['content'])) ? $edit_cms[0]['content'] : ""; ?></textarea>	
							</div>
						</div>
						<div class="form-actions">
							<input type="submit" class="btn btn-primary" name="btn_submit" id="btn_submit" value="<?php echo lang('lbl_admin_language_edit_update'); ?>"/>
						</div>
					</form>
					<!-- /.form over -->
				</div>
			</div>
		</div>
			<!-- /.col-lg-3 col-md-6 -->
	</div>
</div>
<script>
	$("#cmstitle").keyup(function(){
		var Text = $('#cmstitle').val();
		Text = Text.replace(/[^a-z0-9-]/gi,'-');
		Text = Text.toLowerCase();
		Text = Text.replace('/\s/g','-');
		$("#cmsslug").val(Text);
		$('#cmsslug').attr("readonly", "readonly");
	});
</script>
<script>tinymce.init({ selector:'textarea' });</script>
<?php
function array_search_inner ($array, $attr, $val, $strict = FALSE) {
	// Error is input array is not an array
	if (!is_array($array)) return FALSE;
	// Loop the array
	foreach ($array as $key => $inner) {
		// Error if inner item is not an array (you may want to remove this line)
		if (!is_array($inner)) return FALSE;
		// Skip entries where search key is not present
		if (!isset($inner[$attr])) continue;
		if ($strict) {
			// Strict typing
			if ($inner[$attr] === $val) return $key;
		} else {
			// Loose typing
			if ($inner[$attr] == $val) return $key;
		}
	}
	// We didn't find it
	return NULL;
}
?>