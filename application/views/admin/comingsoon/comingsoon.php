<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script>
$( function() {
    $( "#tabs" ).tabs();
});
</script>
<div class="page_wrapper">
	<div class="row padd-bottom">
        <div class="col-sm-12">
            <h1 class="page-header"><?php echo lang('lbl_admin_comingsoon_list'); ?></h1>
        </div>
	</div>
	<?php if (validation_errors()) : ?>
		<div class="col-md-12">
			<div class="alert alert-danger" role="alert">
				<?= validation_errors() ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if (isset($error)) : ?>
		<div class="col-md-12">
			<div class="alert alert-danger" role="alert">
				<?= $error ?>
			</div>
		</div>
	<?php endif; ?>
	<?php
	/*
	here we can be send the notification from the admin to selected online users
	*/
	?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><?php echo lang('lbl_admin_comingsoon_list'); ?></div>
				<div class="panel-body">
					<!-- /.Form start -->
					<form name="add_edit_user" id="add_edit_user" action="<?php echo base_url('admin/comingsoon/comingsoon_update'); ?>" enctype="multipart/form-data" method="post">
						<div class="form-group">
							<label class="control-label"><?php echo lang('lbl_admin_comingsoon_title'); ?></label>
							<div class="controls">    
								<input class="form-control" type="text" name="comingsoon_title" id="" value="<?php echo (!empty($comingsoon[0]['title'])) ? $comingsoon[0]['title'] : ""; ?>"/>	
								<input class="form-control" type="hidden" name="id" id="id" value="<?php echo (!empty($comingsoon[0]['id'])) ? $comingsoon[0]['id'] : ""; ?>"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo lang('lbl_admin_comingsoon_date'); ?></label>
							<div class="controls">    
								<input class="form-control datepicker" type="text" name="comingsoon_date" id="" value="<?php echo (!empty($comingsoon[0]['date'])) ? $comingsoon[0]['date'] : ""; ?>"/>	
							</div>
						</div>
						<div class="form-group">
							<label for="religion"><?php echo lang('lbl_admin_comingsoon_banner'); ?></label>
							<div class="controls ">
								<input  type="file" name="comingsoon_image" id="" />
								</br>
								<?php if(!empty($comingsoon[0]['comingsoon_image'])) { ?>
								<img src="<?php echo base_url('Newassets/images/thumbnail/'.$comingsoon[0]['comingsoon_image']); ?>" height="100" width="100"/>
								<?php } ?>
							</div>													
						</div>
						<div class="form-actions">
							<input type="submit" class="btn btn-primary" name="btn_submit" id="btn_submit" value="<?php echo lang('lbl_admin_language_edit_update'); ?>"/>
						</div>
					</form>
					<!-- /.form over -->
				</div>
			</div>
		</div>	<!-- /.col-lg-3 col-md-6 -->
	</div>
</div>
<script>
$("#select_all").change(function(){  //"select all" change
    $(".chkbox").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
});
//".checkbox" change
$('.chkbox').change(function(){
    //uncheck "select all", if one of the listed checkbox item is unchecked
    if(false == $(this).prop("checked")){ //if this item is unchecked
        $("#select_all").prop('checked', false); //change "select all" checked status to false
    }
    //check "select all" if all checkbox items are checked
    if ($('.chkbox:checked').length == $('.chkbox').length ){
        $("#select_all").prop('checked', true);
    }
});
</script>
<?php
function array_search_inner ($array, $attr, $val, $strict = FALSE) {
	// Error is input array is not an array
	if (!is_array($array)) return FALSE;
	// Loop the array
	foreach ($array as $key => $inner) {
		// Error if inner item is not an array (you may want to remove this line)
		if (!is_array($inner)) return FALSE;
		// Skip entries where search key is not present
		if (!isset($inner[$attr])) continue;
		if ($strict) {
			// Strict typing
			if ($inner[$attr] === $val) return $key;
		} else {
			// Loose typing
			if ($inner[$attr] == $val) return $key;
		}
	}
	// We didn't find it
	return NULL;
}
?>