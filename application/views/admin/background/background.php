<div class="page_wrapper">
	<section>
		<div class="container-fluid text-center">
			<div class="row">
				<div class="col-sm-12 text-uppercase">
					<h3>Background Images</h3>
					<div><img class="img-center" src="<?php echo base_url('images/title-bdr.png'); ?>" alt=""></div>
				</div>
			</div>
		<?php
		if(!empty($_SESSION['success']))
		{
			echo "<h3 style='color:green'>$_SESSION[success]</h3>";
		}
		else if(!empty($_SESSION['fail']))
		{
			echo "<h4 style='color:red'>$_SESSION[fail]</h4>";
		}
		 if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
<?php 	endif; ?>
			<?php if (isset($error)) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= $error ?>
				</div>
			</div>
			<?php endif; ?>
			<?php
			/*
				From this page we can change the site logo on thw web app
			*/
			//echo "<pre>";print_r($background);die;
			?>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading"><?php echo lang('lbl_background'); ?></div>
						<div class="panel-body text-center">
						<!-- /.Form start -->
							<form name="add_edit_user" id="add_edit_user" action="<?php echo base_url('admin/background/update_background');  ?>" enctype="multipart/form-data" method="post">
								<div class="form-group">
									<div class="controls ">
										<input type="file" name="image_1" />
										<input type="hidden" name="bg_1" value="<?php echo $background[0]['id'];?>"/>		
									</div>
									<img src="<?php echo $background[0]['background_url'];?>" alt="bg_1" width="400px">
								</div>
								
								<div class="form-group">
									<div class="controls ">
										<input type="file" name="image_2" />
										<input type="hidden" name="bg_2" value="<?php echo $background[1]['id'];?>"/>		
									</div>
									<img src="<?php echo $background[1]['background_url'];?>" alt="bg_2" width="400px">
								</div>
								
								<div class="form-group">
									<div class="controls ">
										<input type="file" name="image_3" />
										<input type="hidden" name="bg_3" value="<?php echo $background[2]['id'];?>"/>		
									</div>
									<img src="<?php echo $background[2]['background_url'];?>" alt="bg_3" width="400px">
								</div>
								
								<div class="form-group">
									<div class="controls ">
										<input type="file" name="image_4" />
										<input type="hidden" name="bg_4" value="<?php echo $background[3]['id'];?>"/>		
									</div>
									<img src="<?php echo $background[3]['background_url'];?>" alt="bg_4" width="400px">
								</div>
								
								<div class="form-group">
									<div class="controls ">
										<input type="file" name="image_5" />
										<input type="hidden" name="bg_5" value="<?php echo $background[4]['id'];?>"/>		
									</div>
									<img src="<?php echo $background[4]['background_url'];?>" alt="bg_5" width="400px">
								</div>
								
								<div class="form-group">
									<div class="controls ">
										<input type="file" name="image_6" />
										<input type="hidden" name="bg_6" value="<?php echo $background[5]['id'];?>"/>		
									</div>
									<img src="<?php echo $background[5]['background_url'];?>" alt="bg_6" width="400px">
								</div>
								
								<div class="form-group">
									<div class="controls ">
										<input type="file" name="image_7" />
										<input type="hidden" name="bg_7" value="<?php echo $background[6]['id'];?>"/>		
									</div>
									<img src="<?php echo $background[6]['background_url'];?>" alt="bg_1" width="400px">
								</div>
								
								<!--div class="form-group">
									<div class="controls ">
										<input type="file" name="image_8" />
										<input type="hidden" name="bg_8" value="<?php echo $background[7]['id'];?>"/>		
									</div>
									<img src="<?php echo $background[7]['background_url'];?>" alt="bg_1" width="400px">
								</div-->
								
								<div class="form-actions">
									<input type="submit" class="btn btn-primary" name="btn_submit" id="btn_submit" value="<?php echo lang('lbl_admin_site_setting_submit'); ?>"/>
									&nbsp;
									<input type="button" name="back" value="<?php echo lang('lbl_admin_site_setting_cancel'); ?>" class="btn"/>
								</div>
							</form>
						<!-- /.form over -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$("document").ready(function(){
		$(".color").click(function(){
			$("#site_style").val($(this).attr("data-style"));
		});
	});
</script>