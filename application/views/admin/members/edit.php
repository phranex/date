<div class="page_wrapper">
	<div class="row padd-bottom"><div class="col-sm-12"><h1 class="page-header"><?php echo lang('lbl_admin_members_edit'); ?></h1></div></div>
	<?php if (validation_errors()) : ?>
		<div class="col-md-12">
			<div class="alert alert-danger" role="alert">
				<?= validation_errors() ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if (isset($error)) : ?>
		<div class="col-md-12">
			<div class="alert alert-danger" role="alert">
				<?= $error ?>
			</div>
		</div>
	<?php endif; ?>
	<?php
	/*
	here we can be send the notification from the admin to selected online users
	*/
	?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><?php echo lang('lbl_admin_members_edit'); ?></div>
				<div class="panel-body">
					<!-- /.Form start -->
					<form name="add_edit_user" id="add_edit_user" action="<?php echo base_url('admin/ourmembers/member_update'); ?>" enctype="multipart/form-data" method="post">
						<div class="form-group">
							<label class="control-label"><?php echo lang('lbl_admin_members_name'); ?> </label>
							<div class="controls">    
								<input class="form-control" type="text" name="member_name" id="authorname" value="<?php echo $member->member_name; ?>"/>	
								<input class="form-control" type="hidden" name="id" id="id" value="<?php echo $member->id; ?>"/>
							</div>
						</div>
                        <div class="form-group">
							<label class="control-label"><?php echo lang('lbl_admin_members_designation'); ?></label>
							<div class="controls">    
								<input class="form-control" type="text" name="designation" id="authorname" value="<?php echo $member->designation; ?>"/>	
							</div>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo lang('lbl_admin_members_detail'); ?> </label>
							<div class="controls">    
								<textarea class="form-control" type="text" name="member_detail" id="authordescrip"><?php echo $member->member_detail; ?></textarea>	
							</div>
						</div>	
						<div class="form-group">
							<label for="religion"><?php echo lang('lbl_admin_members_image'); ?> </label>
							<div class="controls ">
							<input  type="file" name="member_image" id="authorimage" />
							</br>
							<img src="<?php echo base_url('Newassets/images/thumbnail/'.$member->member_image); ?>" height="100" width="100"/></td>
						</div>
                        <div class="form-group">
							<label class="control-label"><?php echo lang('lbl_admin_members_facebook'); ?> </label>
							<div class="controls">    
								<input class="form-control" type="text" name="facebook_url" id="authorname" value="<?php echo $member->facebook_url; ?>"/>	
							</div>
						</div>
                        <div class="form-group">
							<label class="control-label"><?php echo lang('lbl_admin_members_twitter'); ?> </label>
							<div class="controls">    
								<input class="form-control" type="text" name="twitter_url" id="authorname" value="<?php echo $member->twitter_url; ?>"/>	
							</div>
						</div>
                        <div class="form-group">
							<label class="control-label"><?php echo lang('lbl_admin_members_google'); ?> </label>
							<div class="controls">    
								<input class="form-control" type="text" name="google_url" id="authorname" value="<?php echo $member->google_url; ?>"/>	
							</div>
						</div>
                        <div class="form-group">
							<label class="control-label"><?php echo lang('lbl_admin_members_dribble'); ?> </label>
							<div class="controls">    
								<input class="form-control" type="text" name="dribble_url" id="authorname" value="<?php echo $member->dribble_url; ?>"/>	
							</div>
						</div>													
						<div class="form-actions">
							<input type="submit" class="btn btn-primary" name="btn_submit" id="btn_submit" value="<?php echo lang('lbl_admin_language_edit_update'); ?>"/>
						</div>
					</form>
					<!-- /.form over -->
				</div>
			</div>
		</div>
		<!-- /.col-lg-3 col-md-6 -->
	</div>
</div>
<script>
$("#select_all").change(function(){  //"select all" change
    $(".chkbox").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
});
//".checkbox" change
$('.chkbox').change(function(){
    //uncheck "select all", if one of the listed checkbox item is unchecked
    if(false == $(this).prop("checked")){ //if this item is unchecked
        $("#select_all").prop('checked', false); //change "select all" checked status to false
    }
    //check "select all" if all checkbox items are checked
    if ($('.chkbox:checked').length == $('.chkbox').length ){
        $("#select_all").prop('checked', true);
    }
});
</script>