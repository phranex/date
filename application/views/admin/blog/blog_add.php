<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script>
$( function() {
    $( "#tabs" ).tabs();
});
</script>
<div class="page_wrapper">
	<div class="row padd-bottom"><div class="col-sm-12"><h1 class="page-header"><?php echo lang('lbl_admin_blog_add_tag'); ?></h1></div></div>
	<?php if (validation_errors()) : ?>
		<div class="col-md-12">
			<div class="alert alert-danger" role="alert">
				<?= validation_errors() ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if (isset($error)) : ?>
		<div class="col-md-12">
			<div class="alert alert-danger" role="alert">
				<?= $error ?>
			</div>
		</div>
	<?php endif; ?>
	<?php
	/*
	here we can be send the notification from the admin to selected online users
	*/
	?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><?php echo lang('lbl_admin_blog_add_tag'); ?></div>
				<div class="panel-body">
					<!-- /.Form start -->
					<form name="add_edit_user" id="add_edit_user" action="#" enctype="multipart/form-data" method="post">
						<div class="form-group">
							<label class="control-label"><?php echo lang('lbl_admin_blog_title'); ?></label>
							<div class="controls">    
									<input class="form-control" type="text" name="blogtitle" id="blogtitle" value=""/>	
							</div>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo lang('lbl_admin_blog_slug'); ?></label>
							<div class="controls">    
									<input class="form-control" type="text" name="blogslug" id="blogslug" value=""/>	
							</div>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo lang('lbl_admin_blog_author'); ?></label>
							<div class="controls">    
									<input class="form-control" type="text" name="authorname" id="authorname" value=""/>	
							</div>
						</div>
						<div class="form-group">
							<label for="religion"><?php echo lang('lbl_admin_blog_image'); ?></label>
							<div class="controls ">
								<input  type="file" name="blogimage" id="blogimage" />
							</div>						
							<div class="form-group">
								<label class="control-label"><?php echo lang('lbl_admin_blog_edit_description'); ?></label>
								<div class="controls">  
									<textarea class="form-control" type="text" name="blogdescrip" id="blogdescrip"></textarea>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<input type="submit" class="btn btn-primary" name="btn_submit" id="btn_submit" value="<?php echo lang('lbl_admin_language_add_submit'); ?>"/>
							<input type="button" name="back" value="<?php echo lang('lbl_admin_language_add_cancel'); ?>" class="btn"/>
						</div>
					</form>
					<!-- /.form over -->
				</div>
			</div>
		</div>
		<!-- /.col-lg-3 col-md-6 -->
	</div>
</div>
<script>
$("#blogtitle").focusout(function(){
    var title=$('#blogtitle').val().toLowerCase();
	$('#blogslug').val(title);
	$('#blogslug').attr("readonly", "readonly");
});
</script>