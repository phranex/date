<div class="page_wrapper">
	<div class="row padd-bottom"><div class="col-sm-12"><h1 class="page-header"><?php echo lang('lbl_admin_slider_edit'); ?></h1></div></div>
	<?php if (validation_errors()) : ?>
		<div class="col-md-12">
			<div class="alert alert-danger" role="alert">
				<?= validation_errors() ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if (isset($error)) : ?>
		<div class="col-md-12">
			<div class="alert alert-danger" role="alert">
				<?= $error ?>
			</div>
		</div>
	<?php endif; ?>
	<?php
	/*
	here we can be send the notification from the admin to selected online users
	*/
	?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><?php echo lang('lbl_admin_slider_edit'); ?></div>
				<div class="panel-body">
					<!-- /.Form start -->
					<form name="add_edit_user" id="add_edit_user" action="<?php echo base_url('admin/slider/slider_update'); ?>" enctype="multipart/form-data" method="post">
						<div class="form-group">
							<label class="control-label"><?php echo lang('lbl_admin_slider_title'); ?></label>
							<div class="controls"> 
								<textarea class="form-control" type="text" name="slidertitle" id="slidertitle"><?php echo $sliders['title']; ?></textarea>
								<input class="form-control" type="hidden" name="id" id="id" value="<?php echo $sliders['id']; ?>"/>	
							</div>
						</div>
						<div class="form-group">
							<label for="religion"><?php echo lang('lbl_admin_slider_image'); ?></label>
							<div class="controls ">
								<input  type="file" name="sliderimage" id="sliderimage" />
								</br>
								<img src="<?php echo base_url('Newassets/images/thumbnail/'.$sliders['image']); ?>" height="100" width="100"/></td>
							</div>	
						</div>
						<div class="form-actions">
							<input type="submit" class="btn btn-primary" name="btn_submit" id="btn_submit" value="<?php echo lang('lbl_admin_language_edit_update'); ?>"/>
						</div>
					</form>
					<!-- /.form over -->
				</div>
			</div>
		</div>
			<!-- /.col-lg-3 col-md-6 -->
	</div>
</div>