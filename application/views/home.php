<?php 
 $background2=$this->background_model->getbackgrounditem(array('id'=>'2'));
 $background3=$this->background_model->getbackgrounditem(array('id'=>'3'));
 $background4=$this->background_model->getbackgrounditem(array('id'=>'4'));
 $background5=$this->background_model->getbackgrounditem(array('id'=>'5'));
 $background6=$this->background_model->getbackgrounditem(array('id'=>'6'));
 $background7=$this->background_model->getbackgrounditem(array('id'=>'7'));
 ?>
<section id="home-slider" class="">
	<div id="main-slider" class="carousel carousel-fade" data-ride="carousel">
		<!-- Carousel inner -->
		<div class="carousel-inner">
			<?php
			$cnt=1;
			foreach($sliders as $slider)
			{
				if(!empty($slider['id'])){
					?>
					<div class="carousel-item bg-overlay-black-40 <?php echo ($cnt == 1)?'active':'';?>" style="background-size: cover;" data-gradient-red="4"> 
						<img class="img-fluid" src="<?php echo base_url('Newassets/images/'.$slider['image']); ?>" alt="slider">
						<div class="slider-content">
							<div class="container">
								<div class="row">
									<div class="col-md-12 <?php echo ($cnt==1) ?  'text-right': 'text-left' ; ?>">
										<?php if($cnt==1) { 
													$animate=2;
												}else if($cnt==2) { 
													$animate=7;
												}else {
													$animate=3;
												}
										?>
										<div class="slider-<?php echo $cnt; ?>">
											<h1 class="animated<?php echo $animate; ?> text-white"><?php echo $slider['title']; ?></h1>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php
				}
				$cnt++;
			}
			?>
		</div>
	</div>
</section>
  <!--=================================
 form-1 -->
<section class="form-1 py-3">
	<div class="container">
		<div class="banner-form">
			<form action="<?php echo base_url('search/search_by_condition')?>" method="POST">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group row">
							<label class="col-lg-4 col-md-12 control-label text-white"><?php echo lang('lbl_home_i_am_a'); ?></label>
							<div class="col-lg-8 col-md-12">
								<select name="find_from" class="selectpicker">
									<option selected="" value="male"><?php echo lang('lbl_home_men'); ?></option>
									<option value="female"><?php echo lang('lbl_home_women'); ?></option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group row">
							<label class="col-lg-5 col-md-12 control-label text-white"><?php echo lang('lbl_home_seeking_a'); ?></label>
							<div class="col-lg-7 col-md-12">
								<select name="find_to" class="selectpicker">
									<option value="male"><?php echo lang('lbl_home_men'); ?></option>
									<option selected="" value="female"><?php echo lang('lbl_home_women'); ?></option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="row">
							<div class="col-lg-7 col-md-6">
								<div class="form-group row">
									<label class="col-lg-4 col-md-12 control-label text-white"><?php echo lang('lbl_home_from'); ?></label>
									<div class="col-lg-8 col-md-12">
										<select name="age_from"  class="selectpicker">
											<?php for($from=15;$from<=60;$from++){ ?>
														<option value="<?php echo $from; ?>"><?php echo $from; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-5 col-md-6">
								<div class="form-group row">
									<label class="col-lg-2 col-md-12 control-label text-white pl-0"><?php echo lang('lbl_home_to'); ?></label>
									<div class="col-lg-10 col-md-12">
										<select name="age_to" class="selectpicker">
											<?php for($from=15;$from<=60;$from++){ ?>
														<option <?php echo (($from==20)?"Selected":"");?> value="<?php echo $from; ?>"><?php echo $from; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<button class="button btn-lg btn-theme full-rounded animated right-icn" type="submit">
							<span><?php echo lang('lbl_sidebar_search'); ?> <i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span>
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
<!--=================================
 form-1 -->
<!--=================================
 timeline-section -->
<section class="page-section-ptb position-relative timeline-section">
    <div class="container">
		<?php if(!empty($blocks[0]['id'])){?>
		<div class="row justify-content-center mb-5 xs-mb-3">
			<div class="col-lg-10 text-center">
				<h2 class="title divider mb-3"><?php echo $blocks[0]['title']; ?></h2>
				<p class="lead"><?php echo $blocks[0]['contain']; ?></p>
			</div>
		</div>
		<?php } ?>
		<div class="row justify-content-center ">
			<div class="col-lg-10 col-md-12">
				<ul class="timeline list-inline">
					<?php if(!empty($blocks[1]['id'])){?>
					<li>
						<div class="timeline-badge"><img class="img-fluid" src="<?php echo base_url('images/timeline/01.png'); ?>" alt=""></div>
						<div class="timeline-panel">
							<div class="timeline-heading text-center">
								<h4 class="timeline-title divider-3"><?php echo $blocks[1]['title']; ?></h4>
							</div>
							<div class="timeline-body">
								<p><?php echo $blocks[1]['contain']; ?></p>
							</div>
						</div>
					</li>
					<?php } ?>
					<?php if(!empty($blocks[2]['id'])){?>
					<li class="timeline-inverted">
						<div class="timeline-badge"><img class="img-fluid" src="<?php echo base_url('images/timeline/02.png'); ?>" alt=""></div>
						<div class="timeline-panel">
							<div class="timeline-heading text-center">
								<h4 class="timeline-title divider-3"><?php echo $blocks[2]['title']; ?></h4>
							</div>
							<div class="timeline-body">
								<p><?php echo $blocks[2]['contain']; ?></p>
							</div>
						</div>
					</li>
					<?php } ?>
					<?php if(!empty($blocks[3]['id'])){?>
					<li>
						<div class="timeline-badge"><img class="img-fluid" src="<?php echo base_url('images/timeline/03.png'); ?>" alt=""></div>
						<div class="timeline-panel">
							<div class="timeline-heading text-center">
								<h4 class="timeline-title divider-3"><?php echo $blocks[3]['title']; ?></h4>
							</div>
							<div class="timeline-body">
								<p><?php echo $blocks[3]['contain']; ?></p>
							</div>
						</div>
					</li>
					<?php } ?>
				</ul>
			</div>
		</div>
    </div>
</section>
<!--=================================
 timeline-section -->
<!--=================================
 our-app  -->
<section class="page-section-ptb pb-0 text-center our-app position-relative overflow-h" style="background: url(<?php echo $background4['background_url']; ?>) no-repeat 0 0; background-size: cover;">
	<div class="timeline-inner" style="background: url('<?php echo base_url(); ?>images/app-img.png');">
		<div class="container-fluid h-100">
			<div class="row justify-content-center mb-4">
				<div class="col-lg-8">
					<h2 class="title divider"><?php echo lang('lbl_home_download_our_app'); ?></h2>
				</div>
			</div>
			<div class="row justify-content-center justify-content-lg-start h-75">
				<div class="col-lg-8 col-md-12 align-self-center">
					<h2 class="title mb-3"><?php echo lang('lbl_home_want_at_find_match'); ?> <br><?php echo lang('lbl_home_get_our_app_now'); ?></h2>
					<a class="button btn-dark btn-lg  full-rounded"><i class="fa fa-apple" aria-hidden="true"></i><span><span><?php echo lang('lbl_home_available_on_the'); ?></span> <?php echo lang('lbl_home_app_store'); ?></span></a>
					<a class="button btn-lg full-rounded white-bg text-black"><i class="glyph-icon flaticon-play-button"></i><span><span><?php echo lang('lbl_home_get_it_on'); ?></span> <?php echo lang('lbl_home_google_play'); ?></span></a>
				</div>
			</div>
		</div>
	</div>
</section>
<!--=================================
 our-app  -->
<!--=================================
counter  -->
<section class="page-section-ptb text-white" style="background: url(<?php echo $background5['background_url']; ?>) no-repeat 0 0; background-size: cover;">
	<div class="container">
		<div class="row justify-content-center mb-5 xs-mb-3">
			<div class="col-lg-8 text-center">
			   <h2 class="title divider"><?php echo lang('lbl_home_animated_fun_facts'); ?></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 text-center">
				<div class="counter">
					<img src="<?php echo base_url('images/counter/01.png'); ?>" alt="">
					<span id="total_users" class="timer" data-to="<?php echo $total_users; ?>" data-speed="10000"><?php echo $total_users; ?></span><label><?php echo lang('lbl_home_total_members'); ?></label>
				</div>
			</div>
			<div class="col-md-3 text-center">
				<div class="counter">
					<img src="<?php echo base_url('images/counter/02.png'); ?>" alt="">
					<span id="online_users" class="timer" data-to="<?php echo $online_users; ?>" data-speed="10000" ><?php echo $online_users; ?></span><label><?php echo lang('lbl_home_online_members'); ?></label>
				</div>
			</div>
			<div class="col-md-3 text-center">
				<div class="counter">
					<img src="<?php echo base_url('images/counter/03.png'); ?>" alt="">
					<span id="male_users" class="timer" data-to="<?php echo $male_users; ?>" data-speed="10000"><?php echo $male_users; ?></span><label><?php echo lang('lbl_home_men_online'); ?></label>
				</div>
			</div>
			<div class="col-md-3 text-center">
				<div class="counter">
					<img src="<?php echo base_url('images/counter/04.png'); ?>" alt="">
					<span id="female_users" class="timer" data-to="<?php echo $female_users; ?>" data-speed="10000" ><?php echo $female_users; ?></span><label><?php echo lang('lbl_home_women_online'); ?></label>
				</div>
			</div>
		</div>
	</div>
</section>
<!--=================================
counter  -->
<!--=================================
story-slider-->
<section class="page-section-ptb grey-bg story-slider"> 
	<div class="container">
		<div class="row justify-content-center mb-2 xs-mb-0">
			<div class="col-lg-8 text-center">
				<h2 class="title divider"><?php echo lang('lbl_home_they_found_true_love'); ?></h2>
			</div>
		</div>
	</div>
	<div class="owl-carousel" data-nav-dots="true" data-items="5" data-lg-items="4" data-md-items="3" data-sm-items="2" data-space="30">
		<?php
			foreach($stories as $story)
			{
				if(!empty($story['id'])){
				?>
				 <div class="item">
					<div class="story-item">
						<div class="story-image clearfix">
							<?php
							if(!empty($story['image']))
							{
								$str=base_url("Newassets/images/thumbnail/".$story['image']);
								if(file_exists(DIRECTORY_PATH."Newassets/images/thumbnail/".$story['image']))
								{						
									?>
									<img class="img-fluid" src="<?php echo $str; ?>" alt="" ><!-- style="height:155px;width:300px;" -->
									<?php
								}
								else
								{
									?>
									<img class="img-fluid" src="<?php echo base_url('images/story/01.jpg'); ?>" alt="">
									<?php
								}							
							}
							else
							{
								?><img class="img-fluid" src="<?php echo base_url('images/story/01.jpg'); ?>" alt="">
								<?php
							}
							?>
							<div class="story-link">
								<a href="<?php echo base_url('stories/stories_details/'.$story['id']);?>">
									<i class="glyph-icon flaticon-add"></i>
								</a>
							</div>				
						</div>
						<div class="story-details text-center">
							<h5 class="title divider-3"><?php echo $story['title']; ?></h5>                  
							<div class="about-des mt-3"><?php echo $story['excerpt']; ?></div>              
						</div>
					</div>
				</div>
				<?php
				}
			}
		?>
   </div>
</section>
<!--=================================
story-slider-->
<!--=================================
video -->
<section class="page-section-ptb text-white bg-overlay-black-70 bg text-center" style="background: url(<?php echo $background3['background_url']; ?>) no-repeat 0 0; background-size: cover;">
	<div class="container">
		<?php if(!empty($blocks[4]['id'])){?>  
		<div class="row justify-content-center mb-5 xs-mb-3">
			<div class="col-lg-10">
				<h2 class="title divider mb-3"><?php echo $blocks[4]['title']; ?></h2>
				<p class="lead"><?php echo $blocks[4]['contain']; ?></p>
			</div>
		</div>
		<?php } ?>
		<div class="row">
			<div class="col-md-12">
				<h5 class="title mb-3"><?php echo lang('lbl_home_see_video'); ?></h5>
				<div class="popup-gallery">
				   <a href="<?php 
				   echo $header[8]['status']; ?>" class="play-btn popup-youtube"> <span><i class="glyph-icon flaticon-play-button"></i></span></a>
				</div>
			</div>
		</div>
	</div>
</section>
<!--=================================
video -->
<!--=================================
Blogs -->
<section class="page-section-ptb grey-bg our-blog-home">
	<div class="container">
		<?php if(!empty($blocks[5]['id'])){?>  
			<div class="row justify-content-center mb-5 xs-mb-2">
				<div class="col-lg-10 text-center">
				  <h2 class="title divider mb-3"><?php echo $blocks[5]['title']; ?></h2>
				  <p class="lead"><?php echo $blocks[5]['contain']; ?></p>
				</div>
			</div>
		<?php } ?>
		<div class="row post-article">
			<?php 
			$i=0;$b_id;
			foreach($blogs as $blog)
			{
				if(!empty($blog['id']))
				{
					$b_id=$blog['id'];
					if($i<=1)
					{
						?>
						<div class="col-md-6">
							<div class="post post-artical">
								<div class="post-image clearfix">
									<?php
									if(!empty($blog['image']))
									{
										$str=base_url("Newassets/images/blog/".$blog['image']);
										?>
										<img class="img-fluid" src="<?php echo $str; ?>" alt="">
										<?php           
									}
									else
									{
										?><img class="img-fluid" src="<?php echo base_url('images/blog/01.jpg');?>" alt="">
										<?php
									}
									?>
								</div>
								<div class="post-details">
									<div class="post-title mt-2">
										<h5 class="title text-uppercase mt-2">
										<a href="<?php echo base_url('blog/blog_details/'.$blog['id']);?>"><?php echo $blog['title']; ?></a>
										</h5>
									</div>
									<p><?php echo date_format(date_create($blog['created_date']),"F , Y"); ?><a href=""><?php echo $blog['author']; ?></a></p>          
									<div class="post-icon">
										<div class="post-content"><p><?php echo substr($blog['description'], 0, 100); ?></p></div>              
										<a class="button" href="<?php  echo base_url('blog/blog_details/'.$blog['id']); ?>"><?php echo lang('lbl_home_read_more');?></a>         
									</div>
								</div>                
							</div>  
						</div>
						<?php
						$i++;
					}
				}
			}?>
		</div>
		<div id="ajax_table">
			<div class="row post-article mt-5 xs-mt-0">   
				<?php
				$cnt=1;
				foreach($blogs as $blog)
				{	 
					$b_id=$blog['id'];
					if(!empty($blog['id']))
					{
						if($cnt>2 && $cnt<6)
						{ 
							?>
						   <div class="col-lg-4 col-md-6">
								<div class="post post-artical">
									<div class="post-image clearfix">
										<?php
										if(!empty($blog['image']))
										{
											$str=base_url("Newassets/images/blog/".$blog['image']);
											?>
											<img class="img-fluid" src="<?php echo $str; ?>" alt="">
											<?php
										}
										else
										{
											?><img class="img-fluid" src="<?php echo base_url('images/blog/01.jpg');?>" alt="">
											<?php
										}
										?>
										<div class="post-details">
											<div class="post-title mt-2">
												<h5 class="title text-uppercase mt-2"><a href="<?php  echo base_url('blog/blog_details/'.$blog['id']);?>"><?php echo $blog['title']; ?></a></h5>
											</div>
											<p><?php echo date_format(date_create($blog['created_date']),"F , Y"); ?> <?php echo lang('lbl_home_by'); ?> <a href=""><?php echo $blog['author']; ?></a></p>              
											<div class="post-icon">
												<div class="post-content"><p><?php echo substr($blog['description'], 0, 100); ?></p></div>              
												<a class="button" href="<?php echo base_url('blog/blog_details/'.$blog['id']);?>"><?php echo lang('lbl_home_read_more'); ?></a>      
											</div>
										</div>                
									 </div>  
								</div>
						   </div>
						   <?php
						}
						$cnt++;
					}
				}
				?>
			</div>
		</div>
		<div class="row mt-5 xs-mt-0">
			<div class="col-md-12 text-center">
				<button class="button  btn-lg btn-theme full-rounded animated right-icn" id="load_more" data-val="0" data-type="<?php echo $b_id;?>"><span><?php echo lang('lbl_home_show_more'); ?><i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span>
				<img style="display: none" id="loader" src="<?php echo str_replace('index.php','',base_url('Newassets/images/loader.GIF')); ?>">
				</button>
			</div>
		</div>
	</div>
</section>
<!--=================================
Blogs -->
<!--=================================
Testimonials -->
<section class="page-section-pt dark-bg text-white" style="background: url(<?php echo $background6['background_url']; ?>) no-repeat 0 0; background-size: cover;">
   <div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10 text-center">
				<h2 class="title divider"><?php echo lang('lbl_home_testimonials'); ?></h2>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-lg-10">
				<div class="owl-carousel" data-nav-arrow="true" data-items="1" data-md-items="1" data-sm-items="1">
					<?php
					foreach($testimonials as $testomonial)
					{
						if(!empty($testomonial['id'])){
						?>
						<div class="item"><div class="testimonial bottom_pos">
							<div class="testimonial-avatar">
							<?php
							if(!empty($testomonial['author_image']))
							{
								$str=base_url("Newassets/images/thumbnail/".$testomonial['author_image']);
								if(file_exists(DIRECTORY_PATH."Newassets/images/thumbnail/".$testomonial['author_image']))
								{						
									?>
									<img src="<?php echo $str; ?>" alt=""></div>
									<?php
								}
								else
								{
									?>
									<img src="<?php echo base_url('Newassets/images/thumbnail/thum-0.png'); ?>" alt=""></div>
									<?php
								}							
							}
							else
							{
								?><img src="<?php echo base_url('Newassets/images/thumbnail/thum-0.png'); ?>" alt=""></div>
								<?php
							}
							?>
							<div class="testimonial-info"><p><?php echo $testomonial['description']; ?></p>
								<div class="author-info"> <strong><?php echo $testomonial['author']; ?></strong> </div>
							</div>
						 </div>
						</div>
						<?php
						}
					}
					?>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="page-section-ptb grey-bg">
    <div class="container">
		<?php
		foreach($why_choose_us as $why_choose){
			if(!empty($why_choose['id'])){
			?>
			<div class="row justify-content-center mb-5 xs-mb-2">
				<div class="col-lg-10 text-center">
					<h2 class="title divider mb-3"><?php echo $why_choose['title']; ?></h2>
					<p class="lead"><?php echo $why_choose['contain']; ?></p>
				</div>
			</div>
			<?php 
			}
		}
		?>
        <div class="row justify-content-center mb-5">
            <div class="col-lg-10 text-center">
                <h4 class="title divider-3 text-uppercase"><?php echo lang('lbl_home_we_are_the_one'); ?></h4>
            </div>
        </div>
        <div class="row mb-5">
			<?php 
			foreach($members as $membr){ 
				if(!empty($membr['id'])){ ?>
					<div class="col-lg-3 col-md-6">
						<div class="team team-1">
							<div class="team-images"> 
								<img class="img-fluid" src="<?php echo base_url('Newassets/images/'.$membr['member_image']);?>" alt=""/> 
							</div>
							<div class="team-description">
								<div class="team-tilte">
									<h5 class="title"><a href="<?php echo base_url('team/singleteam/'.$membr['id']); ?>"><?php echo $membr['member_name'] ?></a></h5>
									<span><?php echo $membr['designation']; ?></span>
								</div>
								<p><?php echo substr($membr['member_detail'], 0, 100).'...' ?></p>  
								<div class="team-social-icon social-icons color-hover">
									<ul>
										<?php if($membr['facebook_url']!=''){ ?><li class="social-facebook"><a href="<?php echo $membr['facebook_url']; ?>"><i class="fa fa-facebook"></i></a></li><?php } ?>
										<?php if($membr['twitter_url']!=''){ ?><li class="social-twitter"><a href="<?php echo $membr['twitter_url']; ?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
										<?php if($membr['google_url']!=''){ ?><li class="social-gplus"><a href="<?php echo $membr['google_url']; ?>"><i class="fa fa-google-plus"></i></a></li><?php } ?>
										<?php if($membr['dribble_url']!=''){ ?><li class="social-dribbble"><a href="<?php echo $membr['dribble_url']; ?>"><i class="fa fa-dribbble"></i></a></li><?php } ?>
									</ul>
								</div>
							</div>
						</div>
					</div> 
					<?php 
				}
			}?>
		</div>
        <div class="row text-center">
			<?php
			foreach($team_inner_box1 as $inner_box1){
				if(!empty($inner_box1['id'])){
					?>
					<div class="col-md-4">
						<div class="about-cntn">
							<h5 class="title divider-3 text-uppercase mb-3"><?php echo $inner_box1['title']; ?></h5>
							<p><?php echo $inner_box1['contain']; ?></p>
						</div>
					</div>
					<?php
				}
			}
			?>
			<?php
			foreach($team_inner_box2 as $inner_box2){
				if(!empty($inner_box2['id'])){
					?>
					<div class="col-md-4">
						<div class="about-cntn">
							<h5 class="title divider-3 text-uppercase mb-3"><?php echo $inner_box2['title']; ?></h5>
							<p><?php echo $inner_box2['contain']; ?></p>
						</div>
					</div>
				<?php
				}
			}
			?>
			<?php
			foreach($team_inner_box3 as $inner_box3){
				if(!empty($inner_box3['id'])){
					?>
					<div class="col-md-4">
						<div class="about-cntn">
							<h5 class="title divider-3 text-uppercase mb-3"><?php echo $inner_box3['title']; ?></h5>
							<p><?php echo $inner_box3['contain']; ?></p>
						</div>
					</div>
					<?php
				}
			}
			?>
        </div>
    </div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#load_more").click(function(e){
            e.preventDefault();
			var page = $(this).data('val');
			var blog_id = $(this).data('type');
            getblog(page,blog_id);
        });
    });
    var getblog = function(page,blog_id){
        $("#loader").show();
        $.ajax({
            url:"<?php echo base_url() ?>user/getblog",
            type:'GET',
            data: {page:page,blog_id:blog_id}
        }).done(function(response){
            $("#ajax_table").append(response);
            $("#loader").hide();
            $('#load_more').data('val', ($('#load_more').data('val')+1));
            scroll();
			$("#load_more").hide();
        });
    };
    var scroll  = function(){
        $('html, body').animate({
            scrollTop: $('#load_more').offset().top
        }, 1000);
    };
</script>