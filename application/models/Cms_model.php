<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin_model class.
 * 
 * @extends CI_Model
 */
class Cms_model extends CI_Model {
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct()
    {
        parent::__construct();
        $this->table_name='cms';
    }
    public function insert($data)
    {
        if(empty($data))
            return false;
        else{
            if($this->db->insert($this->table_name,$data))
            {
                //data inserted
                return ($this->db->insert_id());
            }
            else
                return false;
        }
    }
    public function update($conditions=null,$data)
    {
        if(empty($data) || empty($conditions))
            return $this->db->insert('cms',$data);
        else{
            $this->db->where($conditions);
            return $this->db->update('cms',$data);
        }
    }
    public function delete($condition=null)
    {
        if(!$condition)
            return false;
        $this->db->where($condition);
        if($this->db->delete($this->table_name))
            return true;
    }
	public function getAll($condition=null)
	{
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->get('cms')->result_array();
	}
	public function getRecord($condition=null)
	{
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->get('cms')->result_array();
	}
}