<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin_model class.
 * 
 * @extends CI_Model
 */
class Comingsoon_model extends CI_Model {
    /**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
    public function getcomingsoon($condition=null)
	{
		$this->db->select('comingsoon.*');
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->get('comingsoon')->result_array();
	}
    public function updatecomingsoon($condition=null,$data)
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
            return $this->db->update('comingsoon',$data);
		}
		else{
			return $this->db->insert('comingsoon',$data);
		}
	}
}