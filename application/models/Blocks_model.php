<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin_model class.
 * 
 * @extends CI_Model
 */
class Blocks_model extends CI_Model {
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	public function fetch_block($condition=null)
	{
        $this->db->select('block.*');
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->get('block')->result_array();
	}
    public function get_fetch_block($condition=null)
	{
		$this->db->select('block.*');
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->get('block')->result_array();
	}
	public function update_block($condition=null,$data)
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
			return $this->db->update('block',$data);
		}
		else{
			return $this->db->insert('block',$data);
		}
	}
}