<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin_model class.
 * 
 * @extends CI_Model
 */
class Testimonial_model extends CI_Model {
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	public function fetch_testimonial($condition=null)
	{
		$this->db->select('testimonials.*');
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->get('testimonials')->result_array();
	}
	public function gettestimonial($condition=null)
	{
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->get('testimonials')->row_array();
	}
	public function updatetestimonial($condition=null,$data)
	{
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->update('testimonials',$data);
	}
	public function removetestimonial($condition=null)
	{
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->delete('testimonials');
	}
	public function settestomonial($data)
	{
		echo "<pre>";
			print_r($data);
		echo "</pre>";
		return $this->db->insert('testimonials',$data);
	}
}