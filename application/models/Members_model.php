<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin_model class.
 * 
 * @extends CI_Model
 */
class Members_model extends CI_Model {
    /**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
    public function getMembers($condition=null)
	{
        $this->db->select('members.*');
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->get('members')->result_array();
	}
    public function getMember($id)
	{
		$this->db->from('members');
		$this->db->where('id', $id);
		return $this->db->get()->row();
	}
    public function addMember($data)
	{
		return $this->db->insert('members',$data);
	}
    public function updateMember($condition=null,$data)
	{
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->update('members',$data);
	}
    public function deleteMember($id){
        $this->db->where('id', $id);
        return $this->db->delete('members');
    }
}