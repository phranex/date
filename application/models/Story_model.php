<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin_model class.
 * 
 * @extends CI_Model
 */
class Story_model extends CI_Model {
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	public function fetch_stories($condition=null)
	{
        $this->db->select('stories.*');
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->get('stories')->result_array();
	}
	public function getstoriesitem($condition=null)
	{
		$this->db->select('stories.*');
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->get('stories')->row_array();
	}
	public function updatestoriesitem($condition=null,$data)
	{
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->update('stories',$data);
	}
	public function removestoriesitem($condition=null)
	{
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->delete('stories');
	}
	public function setstoriesitem($data)
	{
		if(empty($data))
            return false;
        else{
            if($this->db->insert('stories',$data))
            {
                //data inserted
                return ($this->db->insert_id());
            }
            else
                return false;
        }
	}
}