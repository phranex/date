<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ejabberd_model extends CI_Model{

    public function __construct(){
        parent::__construct();
		$this->XMPP_ENABLE = $this->Common_model->get_key_configuration(array('key'=>'XMPP_ENABLE'));
		$this->XMPP_HOST = $this->Common_model->get_key_configuration(array('key'=>'XMPP_HOST'));
		$this->XMPP_SERVER = $this->Common_model->get_key_configuration(array('key'=>'XMPP_SERVER'));
		$this->XMPP_HTTP_BIND = 'http://'.$this->XMPP_SERVER.':5280/http-bind';
		$this->XMPP_DEFAULT_PASSWORD = $this->Common_model->get_key_configuration(array('key'=>'XMPP_DEFAULT_PASSWORD'));
		$this->FACEBOOK_KEY = $this->Common_model->get_key_configuration(array('key'=>'FACEBOOK_KEY'));
    }
    public function register($user_id){
        //get the user details from user id
        $user=$this->db->where('id',$user_id)->get('users')->row_array();
        $ejuser="";
        //now check if user has already register or not
        if(empty($user['ejuser'])){
            //register user
            $ejuser=$user['fname'][0].$user['lname'][0].$user['id'];
            //now create json field
            $result=$this->cURLRequest("register",array('user'=>$ejuser,'host'=>$this->XMPP_SERVER,'password'=>$this->XMPP_DEFAULT_PASSWORD));
            //update in the users table
            $this->db->where('id',$user_id)->update('users',array('ejuser'=>$ejuser));
        }
        else{
            $ejuser=$user['ejuser'];
        }
        return $ejuser;
    }
    public function add_rosteritem($user_id,$friend_id){
        //get the user details from user id
        $user=$this->db->where('id',$user_id)->get('users')->row_array();
        $friend=$this->db->where('id',$friend_id)->get('users')->row_array();
        $ejuser="";
        //now check if user has already register or not
        if(!empty($user['ejuser'])){
            //now create json field
            if(empty($friend['ejuser'])){
                $ejuser=$friend['fname'][0].$friend['lname'][0].$friend['id'];
                //now create json field
                $result=$this->cURLRequest("register",array('user'=>$ejuser,'host'=>$this->XMPP_SERVER,'password'=>$XMPP_DEFAULT_PASSWORD));
                //update in the users table
                $this->db->where('id',$friend_id)->update('users',array('ejuser'=>$ejuser));
                $friend['ejuser']=$ejuser;
            }
            $result=$this->cURLRequest("add_rosteritem",
                    array(
                        'localuser'=>$user['ejuser'],
                        'localserver'=>$this->XMPP_SERVER,
                        'user'=>$friend['ejuser'],
                        'server'=>$this->XMPP_SERVER,
                        "nick"=>$friend['fname'],
                        "group"=>"Friends",
                        "subs"=>"both"
                    )
            );
        }        
        return true;
    }
    public function sendMsg($data){
        $result=$this->cURLRequest("send_message",$data);
        return $result;
    }
    public function cURLRequest($command,$data){
        $url="http://".$this->XMPP_SERVER.":5280/api/".$command;
        $ch = curl_init( $url );
        # Setup request to send json via POST.
        $payload = json_encode( $data );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        $result = curl_exec($ch);
        if($errno = curl_errno($ch)) {
            $error_message = curl_strerror($errno);
            echo "cURL error ({$errno}):\n {$error_message}";
        }
        curl_close($ch);
        # Print response.
    }
}