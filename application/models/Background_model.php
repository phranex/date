<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Background_model class.
 * 
 * @extends CI_Model
 */
class Background_model extends CI_Model {
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	public function getbackgrounditem($condition=null)
	{
		if(!empty($condition)){
			$this->db->where($condition);
			return $this->db->get('background')->row_array();
		}else{
			return $this->db->get('background')->result_array();
		}
	}
	public function updatebackground($condition=null,$data)
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
			return $this->db->update('background',$data);
		}
		else{
			return $this->db->insert('background',$data);
		}
	}
}