<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin_model class.
 * 
 * @extends CI_Model
 */
class Slider_model extends CI_Model {
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	public function fetch_slider($condition=null)
	{
                $this->db->select('slider.*');
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->get('slider')->result_array();
	}
	public function getslideritem($condition=null)
	{
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->get('slider')->row_array();
	}
	public function updateslideritem($condition=null,$data)
	{
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->update('slider',$data);
	}
	public function removeslideritem($condition=null)
	{
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->delete('slider');
	}
	public function setslideritem($data)
	{
		return $this->db->insert('slider',$data);
	}
}