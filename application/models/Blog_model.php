<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_model extends CI_Model {

	public function __construct() {

		parent::__construct();

		$this->load->database();

	}

	public function fetch_blogs($condition=null)
	{

        $this->db->select('blog.*');

		if(!empty($condition))

			$this->db->where($condition);

		return $this->db->get('blog')->result_array();
	}

	public function getblogitem($condition=null)
	{
		$this->db->select('blog.*');

		if(!empty($condition))

			$this->db->where($condition);

		return $this->db->get('blog')->row_array();
	}

	public function updateblogitem($condition=null,$data)
	{
		if(!empty($condition))

			$this->db->where($condition);

		return $this->db->update('blog',$data);
	}

	public function removeblogitem($condition=null)
	{
		if(!empty($condition))

			$this->db->where($condition);

		return $this->db->delete('blog');
	}

	public function setblogitem($data)
	{
		if(empty($data))
            return false;
        else{
            if($this->db->insert('blog',$data))
            {
                //data inserted
               return ($this->db->insert_id());
            }
            else
                return false;
        }
	}
	public function count_total($condition=null) 
    {
		if(!empty($condition))

			$this->db->where($condition);

        $query = $this->db->get($this->db->dbprefix . 'blog');

        return $query->num_rows();
    }

    Public function get_my_data($condition=null,$limit=null,$offset=null) 
    {
		$this->db->select('blog.*');

		if(!empty($condition))

			$this->db->where($condition);

		if(!empty($offset))

			$query=$this->db->get('blog', $limit, $offset);

		else

			$query=$this->db->get('blog');

		return $query->result_array();		
    }

}