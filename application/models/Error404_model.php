<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin_model class.
 * 
 * @extends CI_Model
 */
class Error404_model extends CI_Model {
    /**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
    public function geterror404()
	{
		return $this->db->get('error_404')->result_array();
	}
    public function updateerror404($condition=null,$data)
	{
		if(!empty($condition))
		{
			$this->db->where($condition);
			return $this->db->update('error_404',$data);
		}
		else{
			return $this->db->insert('error_404',$data);
		}
	}
    public function fetch_error404($condition=null)
	{
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->get('error_404')->result_array();
	}
}