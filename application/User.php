<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'/PHPThumb/ThumbLib.inc.php';
/**
 * User class.
 * 
 * @extends CI_Controller
 */
class User extends CI_Controller {
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() 
	{
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
        $this->load->library('form_validation');
        $this->load->helper('form');
		$this->load->model('user_model');
		$this->load->model('SiteErrorLog');
		$this->load->model('blocks_model');
		$this->load->model('admin_model');
		$this->load->model('Pushnotification_model');
		$this->load->model('user_model');
		$this->load->model('story_model');
		$this->load->model('blog_model');
		$this->load->model('blocks_model');
        $this->load->model('members_model');
		$this->load->model('Cms_model');
		$this->load->library('pagination');
        $this->load->library('table');
		$this->load->model('Testimonial_model');
		$this->load->model('Slider_model');
        $this->load->model('Error404_model');
        $this->load->model('comingsoon_model');
        date_default_timezone_set('Asia/Kolkata'); 
		$data['header']=$this->admin_model->get_setting();
		//echo "<pre>";print_r($data);die;
		$is_comingsoon=$data['header'][18]['status'];
		$method=$this->router->fetch_method();
		if($is_comingsoon==1 && $method!='comingsoon'){
			redirect('user/comingsoon');
		}
	}
	public function index()
	{
		$lg = $this->session->userdata('site_lang');
		$lange= $this->admin_model->get_default_language();
		$language=$lange['status'];
		$value['header']=$this->admin_model->get_setting();
		$total=$this->admin_model->get_all();
		$total_count=count($total);
		$online=$this->admin_model->get_all_users();
		$online_count=count($online);
		$male=$this->admin_model->get_all_users(array('gender'=>'male'));
		$male_count=count($male);
		$female=$this->admin_model->get_all_users(array('gender'=>'female'));
		$female_count=count($female);
		$data['total_users']=  $total_count;
		$data['online_users']=  $online_count;
		$data['male_users']=  $male_count;
		$data['female_users']=  $female_count;
		$data['users']=$total;
		$data['blogs']=$this->blog_model->fetch_blogs();
		$data['testimonials']=$this->Testimonial_model->fetch_testimonial();
		$data['members']=$this->members_model->getMembers();
		$data['stories']=$this->story_model->fetch_stories();
		$data['blocks']=$this->blocks_model->fetch_block();
		$data['sliders']=$this->Slider_model->fetch_slider();
		$data['our_history'] = $this->blocks_model->get_fetch_block(array('block.id'=>12));
		$data['our_history_inner'] = $this->blocks_model->get_fetch_block(array('block.id'=>13));
		$data['why_choose_us'] =$this->blocks_model->get_fetch_block(array('block.id'=>15));
		$data['team_inner_box1'] =$this->blocks_model->get_fetch_block(array('block.id'=>7));
		$data['team_inner_box2'] = $this->blocks_model->get_fetch_block(array('block.id'=>8));
		$data['team_inner_box3'] = $this->blocks_model->get_fetch_block(array('block.id'=>9));
		$data['subscribe'] = $this->blocks_model->get_fetch_block(array('block.id'=>16));
		$data['footer_contact_us'] = $this->blocks_model->get_fetch_block(array('block.id'=>17));
		$data['our_team_member'] = $this->blocks_model->get_fetch_block(array('block.id'=>18));
		$data['homepage3_subscribe'] = $this->blocks_model->get_fetch_block(array('block.id'=>22));
		$lg=$this->lang->line('key');
		$value['val']=array(
			'lang'=>$lg,
		);
		if($value['header'][19]['status']==1){
		  $this->load->view('header',$value);			
		  $this->load->view('home',$data);
		  $this->load->view('footer',$value);
		}elseif($value['header'][19]['status']==2){
		  $this->load->view('header2',$value);		
		  $this->load->view('home2',$data);
		  $this->load->view('footer',$value);
		}else{
		  $this->load->view('header3',$value);			
		  $this->load->view('home3',$data);
		  $this->load->view('footer3',$value);
		}
	}
	/* Function Name fetch_gallery
	 * Returns Gallery detail 
	 * @param Number $user_id
	 * @return Array of Gallery Images detail of User
	 */
	public function fetch_gallery($user_id)
	{
		$logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }		
		return  $this->user_model->get_gallery_images(array("user_id "=>$user_id));
	}
	/* Function Name gallery
	 * Load The Gallery Page
	 */
 	public function gallery()
    {
		$data = new stdClass(); 
        $user_id = $this->session->userdata['user_id'];
        $data->userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
        $data->content = $this->fetch_gallery($user_id);
		$data->notification=$this->fetch_notification();
        $value['header']=$this->admin_model->get_setting();				
		$this->load->view('header',$value);      
		$this->load->view('user/dashboard/my_gallery',$data);
		$this->load->view('footer');
    }
	/* Function Name loginfb
	 * Function for Login with Facebook
	 */
	public function loginfb()
    {
		$this->load->helper('form');
		$this->load->library('form_validation');
		// set validation rules
		//$this->form_validation->set_rules('email', 'Email', 'trim|required');
			$name=$this->input->post("name");
            $fbid=$this->input->post("fbid"); 
		   $gender=$this->input->post("gender");
		   $email=$this->input->post("email");
		   $condition=array("fb_id"=>$fbid);
		   //Check for user already exists or not
		   $user=$this->user_model->get_user_where_row($condition);
		   if($user)
		   {
				// Login With facebook
				$_SESSION['user_id']      = (int)$user->id;
				$_SESSION['fname']      = (string)$user->fname;
				$_SESSION['lname']      = (string)$user->lname;
				$_SESSION['email']     = (string)$user->email;
				$_SESSION['logged_in']    = (bool)true;
				$_SESSION['is_confirmed'] = (bool)$user->is_confirmed;
				$_SESSION['is_admin']     = (bool)$user->is_admin;
				$_SESSION['profile_image']     = (string)$user->profile_image;
                echo "Login";
		   }
		   else
		   {
				// Register With facebook
				$n=explode(" ",$name);
				$pic = file_get_contents("https://graph.facebook.com/$fbid/picture?type=large");
				$filename = $fbid.".jpg";
				$path="uploads/".$filename;
				$path1="uploads/thumbnail/".$filename;
				file_put_contents($path, $pic);
				file_put_contents($path1, $pic);
				$data=array("fb_id"=>$fbid,"fname"=>$n[0],"lname"=>$n[1],"email"=>$email,"gender"=>$gender,"profile_image"=>$filename);
                $_SESSION['fb_id']      = $fbid;
                $_SESSION['fb_fname']      = $n[0];
                $_SESSION['fb_lname']      = $n[1];
                $_SESSION['fb_email']      = $email;
                $_SESSION['fb_gender']      = $gender;
                $_SESSION['fb_profile_image']      = $filename;
				echo "register";
		   }
    }
	/* Function Name block_user
	 * Function for block particular user
	 */
	public function block_user()
	{
		$user_id=$this->uri->segment(3);
		if ($this->user_model->blocked_user($user_id)) 
		{
			$user=$this->user_model->get_user($user_id);
			//$this->session->set_flashdata('success',lang('lbl_dashboard_success'));
            $this->session->set_flashdata('blocked_user', 'You have blocked '.$user->fname.' '.$user->lname);  
			redirect('User/dashboard/dashboard');
		}
	}
	public function report_user()
	{
		$user_id=$this->uri->segment(3);
		if ($this->user_model->report_user($user_id)) 
		{
			$user=$this->user_model->get_user($user_id);
			/*$this->session->set_flashdata('success',lang('lbl_search_report_success'));
			$this->session->set_flashdata('report_user', 'This profile reported to system.');*/
			echo $user->fname." ".$user->lname." ".lang('lbl_search_report');
			//redirect('User/dashboard/');
		}
	}
	/* Function Name terms
	 * Function for terms & Condition
	 * @param  file path $file
	 * @return Void
	 */
	public function terms()
	{
			$this->load->view('header');			
			$this->load->view('user/terms');
			$this->load->view('footer');
	}
	/* Function Name unlinkfile
	 * Function for deleteing image file from directory
	 * @param  file path $file
	 * @return Void
	 */
	public function unlinkfile($file=null)
	{
		if($file)
		{
			if(file_exists($file))
					unlink($file);
		}
	}
	/* Function Name edit_gallery_image
	 * Function for Change Gallery Image
	 * @return Void
	 */
	public function edit_gallery_image()
    {
		try
		{
			$logged_in_user = $this->session->userdata('user_id');
			if (!$logged_in_user)
			{
				redirect('login');
			}
			$imgkey=$_FILES['file']['name'];
			$image="img".$this->input->post("position");
			$user_id = $this->session->userdata['user_id'];
			$config['upload_path']= './uploads/';        
			$config['allowed_types']= 'gif|jpg|png|jpeg';
			$config['overwrite'] = TRUE;
			$fname = $user_id."_".uniqid();
			$config['file_name'] = $fname;
			$this->load->library('upload');
			$this->upload->initialize($config);
			$image_info = getimagesize($_FILES["file"]["tmp_name"]);
			$image_width = $image_info[0];
			$image_height = $image_info[1];
			/*echo($image_width."<br/>");
			echo($image_height);die;*/
			if($this->input->post("position"))
			{
				$position=$this->input->post("position");
			}
			else
			{
				$position="";
			}
			if ( ! $this->upload->do_upload('file'))
			{ 								
				$data['error'] = $this->upload->display_errors();  
				$this->session->set_flashdata('error', lang('lbl_gallery_error_img_update'));
				$user_id = $this->session->userdata['user_id'];
				$data["userdetail"]=$this->user_model->get_user_where_row(array("id "=>$user_id));
				$data['content'] = $this->user_model->get_gallery_images(array("user_id "=>$user_id));
				$this->load->view('header');
				$this->load->view('sidebar');
				$this->load->view('user/register/reg_image', $data);
				$this->load->view('footer');  
				//redirect("user/gallery");				
			}
			else
			{  
				$condition=array("user_id"=>$user_id,"img_key"=>$image);
				$file_data = $this->upload->data();
				$file_data = $this->upload->data();
				$image_detail= $this->user_model->get_gallery_images($condition);
				$unlinkimg="";
				// delete old Image
				if(!empty($image_detail[0]["img_url"]))
				{	 
					$unlinkimg=$image_detail[0]["img_url"];
					$this->unlinkfile(DIRECTORY_PATH."uploads/thumbnail/".$unlinkimg);
					$this->unlinkfile(DIRECTORY_PATH."uploads/".$unlinkimg);
				}
				//create image thumbnail
				$source_path = DIRECTORY_PATH.'uploads/'.$file_data["file_name"];
				$target_path = DIRECTORY_PATH.'uploads/thumbnail/'.$file_data["file_name"];
				$thumb = PhpThumbFactory::create($source_path);
				$thumb->adaptiveResize(280, 250);
				$thumb->save($target_path, 'jpg');
				if($image=="img1")
				{
					$condition=array("id"=>$user_id);
					$data=array("profile_image"=>$file_data["file_name"],"modified_date"=>date("Y-m-d H:i:s"));
					$data1=$this->user_model->get_user($user_id);
					if(!empty($data1->profile_image))
					{	 
						$unlinkimg=$data1->profile_image;
						$this->unlinkfile(DIRECTORY_PATH."uploads/thumbnail/".$unlinkimg);
						$this->unlinkfile(DIRECTORY_PATH."uploads/".$unlinkimg);
					}
					$this->user_model->update_users($data,$condition);
					$_SESSION['profile_image']=$file_data["file_name"];
					//redirect('user/gallery');
				}
				else
				{
					if($this->user_model->get_gallery_images($condition))
					{
						$data=array("img_url"=>$file_data["file_name"],"modified_date"=>date("Y-m-d H:i:s"),"imgposition"=>$position);
						$this->user_model->change__gallery_images($data,$condition);   
					}
					else
					{
						$data=array(
							"img_url"=>$file_data["file_name"],
							"user_id"=>$user_id,
							"img_key"=>$image,
							"imgposition"=>$position,
							"created_date"=>date("Y-m-d H:i:s"),
							"modified_date"=>date("Y-m-d H:i:s"));
						$this->user_model->add_gallery_images($data);
					}
				}
				$this->session->set_flashdata('success', lang('lbl_gallery_updatee_success'));
			}
        }
		catch(Exception $e)
		{
			$this->SiteErrorLog->addData("Edit Gallery Error :- ".$e->getMessage()."Invalid Image",$user_id);
			$this->session->set_flashdata('error', lang('lbl_gallery_error_update'));
		}
		//redirect("user/gallery");		
    }
	//Remove Profile Image Of User
    public function remove_profile_image($id)
    {
		try
		{
			$logged_in_user = $this->session->userdata('logged_in');
			$user_id = $this->session->userdata['user_id'];
			if (!$logged_in_user)
			{
				redirect('login');
			}
			$user=$this->user_model->get_user_where_row(array("id "=>$id));
			$condition=array("id"=>$id);
			$data=array("profile_image"=>"");
			$result=$this->user_model->update_users($data,$condition);
			if($result)
			{
				$this->unlinkfile(DIRECTORY_PATH."uploads/thumbnail/".$user->profile_image);
				$this->unlinkfile(DIRECTORY_PATH."uploads/".$user->profile_image);
				unset($_SESSION['profile_image']);			
			}
			else
			{
				throw new Exception('Image isn\'t Removed');
			}
			$this->session->set_flashdata('success', lang('lbl_gallery_remove_success'));
		}
		catch(Exception $e)
		{
			$this->SiteErrorLog->addData($e->getMessage()."Image isn't Removed",$user_id);
			$this->session->set_flashdata('error', lang('lbl_gallery_error_profile'));
		}redirect("user/gallery");
    } 
	// Remove Gallery Image Of User
    public function register_remove_gallery_image($id)
    {
		try
		{
			$logged_in_user = $this->session->userdata('logged_in');
			if (!$logged_in_user)
			{
				redirect('login');
			}
			$user_id = $this->session->userdata['user_id'];
			$condition=array("id"=>$id);
			$image_detail= $this->user_model->get_gallery_images($condition);
			$unlinkimg="";
			// delete old Image
			if(!empty($image_detail[0]["img_url"]))
			{
				$unlinkimg=$image_detail[0]["img_url"];
				$this->unlinkfile(DIRECTORY_PATH."uploads/thumbnail/".$unlinkimg);
				$this->unlinkfile(DIRECTORY_PATH."uploads/".$unlinkimg);
			}
			$data=array("img_url"=>"","modified_date"=>date("Y-m-d H:i:s"));
			$this->user_model->change__gallery_images($data,$condition); 
			$this->session->set_flashdata('success', lang('lbl_gallery_remove_success'));
		}
		catch(Exception $e)
		{
			$this->SiteErrorLog->addData("Remove Gallery Image Error :- ".$e->getMessage(),$user_id);
			$this->session->set_flashdata('error', lang('lbl_gallery_error_gallery'));
		}
		redirect("user/reg_image");
	}
	// Remove Gallery Image Of User
    public function remove_gallery_image($id)
    {
		try
		{
			$logged_in_user = $this->session->userdata('logged_in');
			if (!$logged_in_user)
			{
				redirect('login');
			}
			$user_id = $this->session->userdata['user_id'];
			$condition=array("id"=>$id);
			$image_detail= $this->user_model->get_gallery_images($condition);
			$unlinkimg="";
			// delete old Image
			if(!empty($image_detail[0]["img_url"]))
			{
				$unlinkimg=$image_detail[0]["img_url"];
				$this->unlinkfile(DIRECTORY_PATH."uploads/thumbnail/".$unlinkimg);
				$this->unlinkfile(DIRECTORY_PATH."uploads/".$unlinkimg);
			}
			$data=array("img_url"=>"","modified_date"=>date("Y-m-d H:i:s"));
			$this->user_model->change__gallery_images($data,$condition); 
			$this->session->set_flashdata('success', lang('lbl_gallery_remove_success'));
		}
		catch(Exception $e)
		{
			$this->SiteErrorLog->addData("Remove Gallery Image Error :- ".$e->getMessage(),$user_id);
			$this->session->set_flashdata('error', lang('lbl_gallery_error_gallery'));
		}
			redirect("user/gallery");
    }
	/**
	 * Function Name:captcha_validation.
	 * Its validating the Ceptcha Code 
	 */
	public function captcha_validation()
	{
		if(!empty($_POST))
		{
			try
			{	
				$data = array(
						'secret' => GOOGLE_SECRAT_KEY,
						'response' => $this->input->post("g-recaptcha-response"));
				$verify = curl_init();
				curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
				curl_setopt($verify, CURLOPT_POST, true);
				curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
				curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
				$response = curl_exec($verify);
				$check = json_decode($response);
				$this->form_validation->set_message('captcha_validation', lang('lbl_log_in_error_ceptcha'));
				if(!$check->success)
				{
					$error_array=(array)$check;
				 	if(in_array("invalid-input-secret",$error_array["error-codes"]) or in_array("missing-input-secret",$error_array["error-codes"]))
					{
						$error=implode(",",$error_array["error-codes"]);
						throw new Exception();
					}
				}
				return $check->success;
			}
			catch(Exception $e)
			{
				$this->SiteErrorLog->addData(lang('lbl_log_in_error_invalid_ceptcha'));
				return false;
			}
		}
	}
	public function reg_step2()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		// set validation rules
		$this->form_validation->set_rules('gender', lang('lbl_register_gender'), 'required');
		$this->form_validation->set_rules('day', lang('lbl_register_birthdate'), 'required|max_length[2]|callback_dob_validation');
		$this->form_validation->set_rules('month', lang('lbl_register_birthdate'), 'required|max_length[2]');
		$this->form_validation->set_rules('year', lang('lbl_register_birthdate'), 'required|max_length[4]');
		$this->form_validation->set_rules('about', lang('lbl_register_about'), 'trim|required|max_length[500]');
		$this->form_validation->set_rules('religion[]', lang('lbl_register_religion'), 'required');		
		$this->form_validation->set_rules('height', lang('lbl_register_height'), 'trim|required');				
		$this->form_validation->set_rules('kids', lang('lbl_register_kids'), 'trim|required');
		$this->form_validation->set_rules('address', lang('lbl_register_address'), 'trim|required');
		$this->form_validation->set_message('dob_validation', lang('lbl_profile_error_age'));

		if ($this->form_validation->run() === false) 
		{
			$data = new stdClass();
			$user_id = $this->session->userdata['user_id'];
			$data->religion = $this->user_model->get_all_religion(); 
			$data->ethnicity = $this->user_model->get_all_ethnicity();
			$data->datepreference = $this->user_model->get_all_datepreference();
			$data->questions = $this->user_model->get_all_questions();
			$data->userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
			$value['header']=$this->admin_model->get_setting();
			$this->load->view('header',$value);
			$this->load->view('user/register/reg_contact', $data);
			$this->load->view('footer');
		}
		else
		{
			$user_id = $this->session->userdata['user_id'];
			$address = $this->input->post('address');     
			$latitude =0;
            $longitude=0;
            $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key='.GOOGLE_API_KEY.'&address='.urlencode($address).'&sensor=true');
            // Convert the JSON to an array
            $geo = json_decode($geo, true);
            if ($geo['status'] == 'OK') 
			{
              // Get Lat & Long
				$latitude = $geo['results'][0]['geometry']['location']['lat'];
				$longitude = $geo['results'][0]['geometry']['location']['lng'];
            } 
			$birthdate=$this->input->post('year')."-".$this->input->post('month')."-".$this->input->post('day');
			$religion=implode(",",$this->input->post('religion'));
			$data=array(
				'gender'=>$this->input->post('gender'),
				'dob'=>$birthdate,
				'about'=>strip_tags($this->input->post('about')),
				'religion'=>$religion,
				'height'=>$this->input->post('height'),
				'kids'=>$this->input->post('kids'),
				'address'=>$this->input->post('address'),
				'location_lat'=>$latitude,
				'location_long'=>$longitude				
			);
			$user=$this->user_model->update_profile(array('id'=>$user_id),$data);
			if($user)
			{
				redirect('user/reg_preferance');
			}
			else
			{
				$this->session->set_flashdata('error','Record is Not inserted');
				$value['header']=$this->admin_model->get_setting();
				$this->load->view('header',$value);
				$this->load->view("user/register/reg_contact",$data);
				$this->load->view('footer');
			}
		}		
	}
	/**
	 * Function Name:Register.
	 * It is used to register the user 
	 * @return void
	 */	
    public function register() 
	{
		$step1 = $this->session->userdata('step1');
		if ($step1)
		{
			redirect('user/reg_image');
		}
		// load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');
		// set validation rules
		$this->form_validation->set_rules('fname', lang('lbl_register_firstname'), 'trim|required|max_length[50]|alpha',array("max_length"=>lang('lbl_validation_error_max_firstname'),
			"alpha"=>lang('lbl_validation_error_alpha_firstname')));
		$this->form_validation->set_rules('lname', lang('lbl_register_lastname'), 'trim|required|max_length[50]|alpha',array("max_length"=>lang('lbl_validation_error_max_lastname'),
			"alpha"=>lang('lbl_validation_error_alpha_lastname')));
        //$this->form_validation->set_rules('username', lang('lbl_register_username'), 'trim|required|alpha_numeric|min_length[4]|is_unique[users.username]', array('is_unique' => 'This username already exists. Please choose another one.'));
		$this->form_validation->set_rules('email', lang('lbl_register_email'), 'trim|required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', lang('lbl_register_password'), 'trim|required|min_length[6]');
		$this->form_validation->set_rules('password_confirm', lang('lbl_register_confirm_password'), 'trim|required|min_length[6]|matches[password]');
		$this->form_validation->set_rules('education', lang('lbl_register_education'), 'trim|required');
		$this->form_validation->set_rules('profession', lang('lbl_register_profession'), 'trim|required');
		// create the data object
		if ($this->form_validation->run() === false) 
		{	
			$value['header']=$this->admin_model->get_setting();				
			$this->load->view('header',$value);	
			$this->load->view('user/register/register');
			$this->load->view('footer');
		} 
		else 
		{	
			$id=$this->user_model->create_user($this->input->post());
			if ($id) 
			{
				// user creation ok
				$this->session->set_flashdata('register_success','Registration completed successfully.');
				$ids=$id;	
				$_SESSION['user_id']      = (int)$ids;
				$_SESSION['logged_in']    = (bool)true;
				$_SESSION['fname']      = (string)$this->input->post('fname');
				$_SESSION['lname']      = (string)$this->input->post('lname');
				$_SESSION['step1']      = (bool)true;
				if(XMPP_ENABLE){
					//register user
					$ejuser=$this->Ejabberd_model->register($id);
				}
				else{
					$ejuser=$data->userdetail->ejuser;
				}
				unset($_SESSION['fb_id']);
				redirect('user/reg_image');
			} 
			else 
			{
				// user creation failed, this should never happen
				$data->error = lang('lbl_register_error');
				$value['header']=$this->admin_model->get_setting();				
				$this->load->view('header',$value);				
				$this->load->view('user/register/register', $data);
				$this->load->view('footer');
			}
		}
	}
    public function registerfb() 
	{
		// load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');
		// set validation rules
		$this->form_validation->set_rules('fname', lang('lbl_register_firstname'), 'trim|required|max_length[50]|alpha',array("max_length"=>lang('lbl_validation_error_max_firstname'),
			"alpha"=>lang('lbl_validation_error_alpha_firstname')));
		$this->form_validation->set_rules('lname', lang('lbl_register_lastname'), 'trim|required|max_length[50]|alpha',array("max_length"=>lang('lbl_validation_error_max_lastname'),
			"alpha"=>lang('lbl_validation_error_alpha_lastname')));
        $this->form_validation->set_rules('username', lang('lbl_register_username'), 'trim|required|alpha_numeric|min_length[4]|is_unique[users.username]', array('is_unique' => 'This username already exists. Please choose another one.'));
		$this->form_validation->set_rules('email', lang('lbl_register_email'), 'trim|required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', lang('lbl_register_password'), 'trim|required|min_length[6]');
		$this->form_validation->set_rules('password_confirm', lang('lbl_register_confirm_password'), 'trim|required|min_length[6]|matches[password]');
		$this->form_validation->set_rules('education', lang('lbl_register_education'), 'trim|required');
		$this->form_validation->set_rules('profession', lang('lbl_register_profession'), 'trim|required');
		// create the data object
		if ($this->form_validation->run() === false) 
		{	
			$value['header']=$this->admin_model->get_setting();				
			$this->load->view('header',$value);	
			$this->load->view('user/register/registerfb');
			$this->load->view('footer');
		} 
		else 
		{			
			$id=$this->user_model->create_user($this->input->post());
			if ($id) 
			{
				// user creation ok
				$ids=$this->db->insert_id();
				$_SESSION['user_id']      = (int)$ids;
				$_SESSION['logged_in']    = (bool)true;
				$_SESSION['fname']      = (string)$this->input->post('fname');
				$_SESSION['lname']      = (string)$this->input->post('lname');
				redirect('user/reg_image');
			} 
			else 
			{
				// user creation failed, this should never happen
				$data->error = lang('lbl_register_error');
				// send error to the view
				$value['header']=$this->admin_model->get_setting();				
				$this->load->view('header',$value);				
				$this->load->view('user/register/registerfb', $data);
				$this->load->view('footer');
			}
		}
	}
	public function reg_step3()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		// set validation rules
		//$this->form_validation->set_rules('ethnicity[]', lang('lbl_register_ehtnticity'), 'required');
		$this->form_validation->set_rules('question-ans', lang('lbl_register_question_answer'), 'trim|required|max_length[100]');
		if ($this->form_validation->run() === false) 
		{
			$user_id = $this->session->userdata['user_id'];
			$data = new stdClass();
			$data->ethnicity = $this->user_model->get_all_ethnicity();
	        $data->datepreference = $this->user_model->get_all_datepreference();
	        $data->questions = $this->user_model->get_all_questions();
			$data->userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
			$value['header']=$this->admin_model->get_setting();
		    $this->load->view('header',$value);
			$this->load->view("user/register/reg_preferance",$data);
			$this->load->view('footer');
		}
		else
		{
			$user_id = $this->session->userdata['user_id'];
			//$ethnicity=implode(",",$this->input->post('ethnicity'));
			$data=array(
				'gender_pref'=>$this->input->post('gender_pref'),
				'date_pref'=>$this->input->post('datepref'),
				//'ethnicity'=>$ethnicity,
				'que_id'=>$this->input->post('question'),
				'que_ans'=>$this->input->post('question-ans'),
				'max_dist_pref'=>$this->input->post('dist-max'),
				'min_dist_pref'=>$this->input->post('dist-min'),
				'max_age_pref'=>$this->input->post('age-max'),
				'min_age_pref'=>$this->input->post('age-min'),
				'access_location'=>$this->input->post('access_loc')			
			);
			$user=$this->user_model->update_profile(array('id'=>$user_id),$data);
			if($user)
			{
				redirect('user/login');
			}
			else
			{
				$this->sessioon->set_flashdata('error','Record is Not inserted');
				$value['header']=$this->admin_model->get_setting();
				$this->load->view('header',$value);
				$this->load->view("user/register/reg_prefreance",$data);
				$this->load->view('footer');
			}
		}		
	}
    public function reg_image()
    {
		$step2 = $this->session->userdata('step2');
		if ($step2)
		{
			redirect('user/reg_contact');
		}
		$data = new stdClass(); 
		$user_id = $this->session->userdata['user_id'];
		$this->form_validation->set_rules('imgupload1','please select the image', 'trim|required');
		if ($this->form_validation->run() === false) 
		{
			$_SESSION['step2']      = (bool)true;
			$data->userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
			$data->content = $this->fetch_gallery($user_id);
			$value['header']=$this->admin_model->get_setting();
			$this->load->view('header',$value);
			$this->load->view("user/register/reg_image",$data);
			$this->load->view('footer');  
		}
		else
		{
			$data->userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
			$value['header']=$this->admin_model->get_setting();
			$this->load->view('header',$value);
			$this->load->view("user/register/reg_image",$data);
			$this->load->view('footer');
		}
    }
	public function my_profile()
	{		
		try
		{
			if (!$this->session->userdata['user_id'])
			{
				redirect('login');
			}			
			// validators
			// load form helper and validation library
			$this->load->helper('form');
			$this->load->library('form_validation');
			// set validation rules
			/*array("required"=>lang('lbl_register_error_day'),"numeric"=>lang('lbl_register_error_day_number'),"dob_validation"=>lang('lbl_validation_error_day'))*/
			
			$this->form_validation->set_rules('fname', lang('lbl_register_firstname'), 'required|max_length[50]|alpha',array("max_length"=>lang('lbl_validation_error_max_firstname'),
			"alpha"=>lang('lbl_validation_error_alpha_firstname')));
			$this->form_validation->set_rules('lname', lang('lbl_register_lastname'), 'required|max_length[50]|alpha',array(
					"max_length"=>lang('lbl_validation_error_max_lastname'),
					"alpha"=>lang('lbl_validation_error_alpha_lastname')
				)
			);
			$this->form_validation->set_rules('day', lang('lbl_register_error_day'), 'required|max_length[2]|numeric',array(
					"required"=>lang('lbl_validation_error_day'),
					"numeric"=>lang('lbl_register_error_day_number'),
					"max_length"=>lang('lbl_validation_error_day')
				)
			);
			$this->form_validation->set_rules('month', lang('lbl_register_error_month'), 
				'required|max_length[2]|numeric',
				array(
					"required"=>lang('lbl_validation_error_month'),
					"numeric"=>lang('lbl_register_error_month_number'),
					"max_length"=>lang('lbl_validation_error_month')
				)
			);
			$this->form_validation->set_rules('year', lang('lbl_register_error_year'),'required|numeric|max_length[4]|callback_valid_dob|callback_dob_validation',
				array(
					"required"=>lang('lbl_validation_error_year'),
					"numeric"=>lang('lbl_register_error_year_number'),
					"dob_validation"=>lang('lbl_profile_error_age'),
					"max_length"=>lang('lbl_validation_error_year')
				)
			);
			$this->form_validation->set_rules('about', lang('lbl_profile_about'), 'trim|required|max_length[500]');
			$this->form_validation->set_rules('height', lang('lbl_profile_height'), 'trim|required');
			$this->form_validation->set_rules('ethnicity', lang('lbl_profile_ehtnticity'), 'required');
			$this->form_validation->set_rules('religion', lang('lbl_profile_religion'), 'required');
			$this->form_validation->set_rules('address', lang('lbl_profile_address'), 'trim|required');
			$this->form_validation->set_rules('education', lang('lbl_profile_education'), 'trim|required');
			$this->form_validation->set_rules('profession', lang('lbl_profile_profession'), 'trim|required');
			$this->form_validation->set_rules('gender', lang('lbl_profile_gender'), 'required');
			//$this->form_validation->set_message('dob_validation', lang('lbl_register_error_dob'));
			$this->form_validation->set_message('valid_dob', lang('lbl_register_error_dob'));
			$email=$this->input->post("email");
			$id=$this->input->post("id");
			$user=$this->user_model->get_user($id);
			if($email!=$user->email){
				if(!empty($this->user_model->get_user_by_con(array("email"=>$email)))) {
					$this->form_validation->set_rules('email', lang('lbl_register_email'), 'is_unique[users.email]');
				}
				else{
					$this->form_validation->set_rules('email', lang('lbl_register_email'), 'trim|required|valid_email');
				}
			}
			//$this->form_validation->set_message('uniqemail',lang('lbl_validation_error_email'));
			if ($this->form_validation->run() === TRUE)
			{
				$dob=$_POST["year"]."-".$_POST["month"]."-".$_POST["day"];
				$address = $_POST['address'];            
                $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key='.GOOGLE_API_KEY.'&address='.urlencode($address).'&sensor=true');
				// Convert the JSON to an array
				$geo = json_decode($geo, true);
				if ($geo['status'] == 'OK') 
				{
				  // Get Lat & Long
					$latitude = $geo['results'][0]['geometry']['location']['lat'];
					$longitude = $geo['results'][0]['geometry']['location']['lng'];
				} 
				/*if(!empty($_POST["religion"]))
				{
					$religion=$_POST["religion"];
					$religions=implode(",",$religion);
				}
				if(!empty($_POST["ethnicity"]))
				{
					$ethnicity=$_POST["ethnicity"];
					$ethnicities=implode(",",$ethnicity);
				}*/	
				$data=array(
					'fname'=>$this->input->post('fname'),
					'lname'=>$this->input->post('lname'),
					'email'=>$this->input->post('email'),
					'dob'=>$dob,
					'ethnicity'=>$this->input->post('ethnicity'),
					'religion'=>$this->input->post('religion'),
					'about'=>strip_tags($this->input->post('about')),
					'gender'=>$this->input->post('gender'),
					'height'=>$this->input->post('height'),
					'address'=>$this->input->post('address'),
					'education'=>$this->input->post('education'),
					'profession'=>$this->input->post('profession'),
					'location_lat'=>$latitude,
					'location_long'=>$longitude,				
				);
				$user= $this->user_model->update_profile(array('id'=>$_POST["id"]),$data);
				$user_id = $this->user_model->get_user_id_from_email($email);
				$user    = $this->user_model->get_user($user_id);
				// set session user datas
				$_SESSION['user_id']      = (int)$user->id;
				$_SESSION['fname']      = (string)$user->fname;
				$_SESSION['lname']      = (string)$user->lname;
				$_SESSION['email']     = (string)$user->email;
				$_SESSION['logged_in']    = (bool)true;
				$_SESSION['is_confirmed'] = (bool)$user->is_confirmed;
				$_SESSION['is_admin']     = (bool)$user->is_admin;
				$_SESSION['profile_image']     = (string)$user->profile_image;
				$data  = $user;
				$this->session->set_flashdata('success', lang('lbl_profile_update_success'));
				// user login ok
				redirect('user/my_profile');
			}
			else
			{
			}
			$user_id = $this->session->userdata['user_id'];			
			$data['userdetail'] = $this->user_model->get_user($user_id);
			$data['ethnicity'] = $this->user_model->get_all_ethnicity();
			$data['religion'] = $this->user_model->get_all_religion();
			$value['header']=$this->admin_model->get_setting();
		}
		catch(Exception $e)
		{
			$this->session->set_flashdata('error', lang('lbl_profile_error_update').$e->getMessage());
		}
        $this->load->view('header',$value);
		$this->load->view("user/dashboard/my_profile",$data);
		$this->load->view('footer');
	}
	public function my_preferance()
	{
		$logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }	
		try
		{
			if (!$this->session->userdata['user_id'])
			{
				redirect('login');
			}			
			// validators
			// load form helper and validation library
			$this->load->helper('form');
			$this->load->library('form_validation');
			// set validation rules
			// $this->form_validation->set_rules('kids', lang('lbl_profile_kids'), 'trim|required');
			//$this->form_validation->set_rules('religion[]', lang('lbl_profile_religion'), 'required');
			//$this->form_validation->set_rules('ethnicity[]', lang('lbl_profile_ehtnticity'), 'required');
			$this->form_validation->set_rules('gender_pref',  lang('lbl_profile_interested_in'), 'trim|required');
			$this->form_validation->set_rules('question_ans', lang('lbl_profile_error_question_answer'), 'trim|required|max_length[100]');
			$this->form_validation->set_rules('access_loc', lang('lbl_register_firstname'), 'trim|required');
			if ($this->form_validation->run() === TRUE)
			{
				if(!empty($_POST["ethnicity"]))
				{
					$ethnicity=$_POST["ethnicity"];
					$ethnicities=implode(",",$ethnicity);
				}else{
					$ethnicity=0;
					$ethnicities=implode(",",$ethnicity);
				}
				if(!empty($_POST["religion"]))
				{
					$religion=$_POST["religion"];
					$religions=implode(",",$religion);
				}else{
					$religion=0;
					$religions=implode(",",$religion);
				}		
				$data=array(
					'gender_pref'=>$this->input->post('gender_pref'),
					'date_pref'=>$this->input->post('datepref'),
					'min_age_pref'=>$this->input->post('age-min'),
					'max_age_pref'=>$this->input->post('age-max'),
					'min_dist_pref'=>$this->input->post('dist-min'),
					'max_dist_pref'=>$this->input->post('dist-max'),
					'religion_pref'=>$religions,
					'ethnicity_pref'=>$ethnicities,
					'que_id'=>$this->input->post('question'),
					'que_ans'=>$this->input->post('question_ans'),
					'access_location'=>$this->input->post('access_loc')
				);
				$user= $this->user_model->update_profile(array('id'=>$_POST["id"]),$data);
				$this->session->set_flashdata('success', lang('lbl_profile_update_success'));
			}
			else
			{
				$this->session->set_flashdata('error', lang('lbl_profile_update_error'));
			}
			$user_id = $this->session->userdata['user_id'];
			$data['content'] = $this->user_model->get_user($user_id);
			$data['religion'] = $this->user_model->get_all_religion();
			$data['ethnicity'] = $this->user_model->get_all_ethnicity();
			$data['datepref'] = $this->user_model->get_new_datepreference($data['content']->date_pref);
			$data['datepreference'] = $this->user_model->get_all_datepreference();
			$data['questions'] = $this->user_model->get_all_questions();
			$value['header']=$this->admin_model->get_setting();
		}
		catch(Exception $e)
		{
			$this->session->set_flashdata('error', lang('lbl_profile_error_update').$e->getMessage());
		}
			$this->load->view('header',$value);
			$this->load->view("user/dashboard/my_preferance",$data);
			$this->load->view('footer');
	}
	public function reg_contact()
	{
		$step3 = $this->session->userdata('step3');
		if ($step3)
		{
			redirect('user/reg_step2');
		}
		$data = new stdClass();
		$user_id = $this->session->userdata['user_id'];
		$data->userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
		$data->content = $this->fetch_gallery($user_id);
		if(empty($data->userdetail->profile_image))
		{
			$this->session->set_flashdata('fail','At least Profile image must be uploaded');
			redirect('user/reg_image');
		}
		$_SESSION['step3']      = (bool)true;
        $data->religion = $this->user_model->get_all_religion(); 		
		$data->userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
		$value['header']=$this->admin_model->get_setting();
	    $this->load->view('header',$value);
		$this->load->view("user/register/reg_contact",$data);
		$this->load->view('footer');
	}
	public function reg_preferance()
	{
		$user_id = $this->session->userdata['user_id'];
		$data = new stdClass();
		$data->ethnicity = $this->user_model->get_all_ethnicity();
        $data->datepreference = $this->user_model->get_all_datepreference();
        $data->questions = $this->user_model->get_all_questions();
		$data->userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
		$value['header']=$this->admin_model->get_setting();
	    $this->load->view('header',$value);
		$this->load->view("user/register/reg_preferance",$data);
		$this->load->view('footer');
	}
	/**
	 * login function.
	 * 
	 * @access public
	 * @return void
	 */
	public function login() 
	{
		// load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');
		//echo "<pre>";print_r($_POST);
		/* set the code here for xmpp_addroster 	*/
		if ($this->session->userdata('logged_in'))
        {
            redirect('user/my_profile');
        }
        // create the data object
		$data = new stdClass();
		
		// set validation rules
		$this->form_validation->set_rules('username', lang('lbl_log_in_email'), 'required');
		$this->form_validation->set_rules('password', lang('lbl_log_in_password'), 'required');
		if(CAPTCHA_ENABLE)
		{ 
			$this->form_validation->set_rules('login', lang('lbl_register_error_ceptcha'), 'callback_captcha_validation');
		}
		if ($this->form_validation->run() == false) 
		{
			// validation not ok, send validation errors to the view
			$data=array("body_class"=>"main-bg");
			$lang['languages']=$this->fetch_language();
			$value['header']=$this->admin_model->get_setting();	
			$this->load->view('header',$value);
			$this->load->view('user/login/login',$lang);
			$this->load->view('footer');
		} 
		else 
		{
			// set variables from the form
			$email = $this->input->post('username');
			$password = $this->input->post('password');
			if ($this->user_model->resolve_user_login($email, $password)) 
			{
				$user_id = $this->user_model->get_user_id_from_email($email);
				$user    = $this->user_model->get_user($user_id);
				// set session user datas
				$_SESSION['user_id']      = (int)$user->id;
				$_SESSION['fname']      = (string)$user->fname;
				$_SESSION['lname']      = (string)$user->lname;
				$_SESSION['email']     = (string)$user->email;
				$_SESSION['logged_in']    = (bool)true;
				$_SESSION['is_confirmed'] = (bool)$user->is_confirmed;
				$_SESSION['is_admin']     = (bool)$user->is_admin;
				$_SESSION['profile_image']     = (string)$user->profile_image;
				$data  = $user;
				// user login ok
				redirect('user/my_profile');
			} 
			else 
			{
				// login failed
				$data->error = lang('lbl_log_in_error_wrong');
				$data->body_class="main-bg";
				$value['header']=$this->admin_model->get_setting();	
				$this->load->view('header',$value);
				$this->load->view('user/login/login', $data);
				$this->load->view('footer');
			}
		}
	}
	/**
	 * logout function.
	 * 
	 * @access public
	 * @return void
	 */
	public function logout() 
	{
        // create the data object
		$data = new stdClass();
		if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) 
		{
			// remove session datas
			foreach ($_SESSION as $key => $value) 
			{
				unset($_SESSION[$key]);
			}
			// user logout ok
			redirect('/user');
		} 
		else 
		{
			// there user was not logged in, we cannot logged him out,
			// redirect him to site root
			redirect('/user');
		}
	}
	/**
	 Function Name :dashboard.
	 * 
	 * @access public
	 * @return the list of array  gallery and notification
	 */
	public function dashboard()
	{
        $logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }
        // create the data object
        $user_id = $this->session->userdata['user_id'];
        $data['content'] = $this->user_model->get_user($user_id);		
		$data['question']=$this->user_model->get_question($data['content']->que_id);
        // load views
        // user logout ok
        $data["crt"]="dashboard";
		$data["enablechat"]=true;
		$data = new stdClass();
		$user_id = $this->session->userdata['user_id'];
		$data->userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
		
		/*
		* XMPP Register user
		*/
		if(XMPP_ENABLE && empty($data->ejuser)){
			//register user
			$ejuser=$this->Ejabberd_model->register($user_id);
		}
		else{
			$ejuser=$data->userdetail->ejuser;
		}
		/*
		* XMPP register ends
		*/

		if(empty($data->userdetail->profile_image))
		{
			redirect('user/reg_image');
		}
		else
		{
			$data->gallery=$this->fetch_gallery($user_id);
			$data->question = $this->user_model->get_question($data->userdetail->que_id);
			$value['header']=$this->admin_model->get_setting();
			$data->ejuser=$ejuser;
			$this->load->view('header',$value);
			$this->load->view('user/dashboard/dashboard',$data);
		}
		$this->load->view('footer');            
    }
	public function fetch_language()
	{
		return $this->user_model->fetch_language();
	}
	//Date Of Birth Validation
	public function dob_validation()
	{
		$day=$this->input->post('day');
		$month=$this->input->post('month');
		$year=$this->input->post('year');
		$dat=date_create($day."-".$month."-".$year);
		$curr=date_create(date('d-m-Y'));
		$diff=date_diff($dat,$curr);
		$yer=$diff->y;
		if($yer < 15 )
		{
			return false;
		}
		return true;
	}
	public function valid_dob()
	{
		$day=$this->input->post('day');
		$month=$this->input->post('month');
		$year=$this->input->post('year');
		if($day>31 || $month>12 || $month<1){
			return false;
		}
		else if(!checkdate($month,$day,$year)){
			return false;
		}
		else{
			return true;
		}
		
	}
	/**
	 Function Name :addr_line1.
	 * 
	 * @access private
	 * @return TRUE/FALSE for correct/incorrect address
	 */
	function addr_line1() 
	{
		try
		{
			$address=$this->input->post('address');
			$geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
			$geo = json_decode($geo, true);
			if($geo['status'] == 'OK') 
			{
				return TRUE;
			}
			elseif($geo['status']=="ZERO_RESULTS")
			{
				$this->form_validation->set_message('addr_line1', lang('lbl_register_error_invalid_email'));
				return false;
			}
			else
			{
				throw new Exception("Google Address Api Error :- ".$geo['status']);
			}
		}
		catch(Exception $e)
		{
			if(!empty($this->session->userdata['user_id']))
				$user_id = $this->session->userdata['user_id'];
			else
				$user_id=0;
			//echo 'Error : Google Captcha is Not Working';
			$this->SiteErrorLog->addData($e->getMessage(),$user_id);
			return false;
		}
	}
	/**
	 Function Name :profile.
	 * 
	 * @access public
	 * @return the information of login user and also update login user data
	 */
    public function profile()
	{
		try
		{
			$logged_in_user = $this->session->userdata('logged_in');
			if (!$logged_in_user)
			{
				redirect('login');
			}
			// validators
			// load form helper and validation library
			$this->load->helper('form');
			$this->load->library('form_validation');
			// set validation rules
			$this->form_validation->set_rules('fname', lang('lbl_register_firstname'), 'required|max_length[50]|alpha',array("max_length"=>lang('lbl_validation_error_max_firstname'),
			"alpha"=>lang('lbl_validation_error_alpha_firstname')));
			$this->form_validation->set_rules('lname', lang('lbl_register_lastname'), 'required|max_length[50]|alpha',array("max_length"=>lang('lbl_validation_error_max_lastname'),
			"alpha"=>lang('lbl_validation_error_alpha_lastname')));
			$this->form_validation->set_rules('day', lang('lbl_register_error_day'), 'required|max_length[2]|callback_dob_validation');
			$this->form_validation->set_rules('month', lang('lbl_register_error_month'), 'required|max_length[2]');
			$this->form_validation->set_rules('year', lang('lbl_register_error_year'), 'required|max_length[4]');
			$this->form_validation->set_rules('about', lang('lbl_profile_about'), 'trim|required|max_length[500]');
			$this->form_validation->set_rules('gender', lang('lbl_profile_gender'), 'required');
			$this->form_validation->set_rules('education', lang('lbl_profile_education'), 'trim|required');
			$this->form_validation->set_rules('profession', lang('lbl_profile_profession'), 'trim|required');
			$this->form_validation->set_rules('height', lang('lbl_profile_height'), 'trim|required');
			$this->form_validation->set_rules('kids', lang('lbl_profile_kids'), 'trim|required');
			$this->form_validation->set_rules('religion[]', lang('lbl_profile_religion'), 'required');
			//$this->form_validation->set_rules('ethnicity[]', lang('lbl_profile_ehtnticity'), 'required');
			$this->form_validation->set_rules('address', lang('lbl_profile_address'), 'trim|required|callback_addr_line1');
			$this->form_validation->set_rules('gender_pref', '\'Intrested in\'', 'trim|required');
			$this->form_validation->set_rules('question_ans', lang('lbl_profile_error_question_answer'), 'trim|required|max_length[100]');
			$this->form_validation->set_rules('access_loc', lang('lbl_register_firstname'), 'trim|required');
			$this->form_validation->set_message('dob_validation', lang('lbl_profile_error_age'));
			if ($this->form_validation->run() === TRUE)
			{
				/*if(!empty($_POST["ethnicity"]))
				{
					$ethnicity=$_POST["ethnicity"];
					$_POST["ethnicity"]=implode(",",$ethnicity);
				}*/
				if(!empty($_POST["religion"]))
				{
					$religion=$_POST["religion"];
					$_POST["religion"]=implode(",",$religion);
				}			
				$user= $this->user_model->update_profile($this->input->post());
				$this->session->set_flashdata('success', lang('lbl_profile_update_success'));
			}
			$user_id = $this->session->userdata['user_id'];
			$data['content'] = $this->user_model->get_user($user_id);
			$data['religion'] = $this->user_model->get_all_religion();
			//$data['ethnicity'] = $this->user_model->get_all_ethnicity();
			$data['datepreference'] = $this->user_model->get_all_datepreference();
			$data['questions'] = $this->user_model->get_all_questions();
			$data['notification']=$this->fetch_notification();
			$data['gallery']=$this->fetch_gallery($user_id);
			$data["crt"]="profile";
		}
		catch(Exception $e)
		{
			$this->session->set_flashdata('error', lang('lbl_profile_error_update').$e->getMessage());
		}
			$this->load->view('header');
			$this->load->view('sidebar', $data);
			$this->load->view('user/dashboard/profile');
			$this->load->view('footer');
    }
	/**
	 Function Name :perference.
	 * 
	 * @access public
	 * @return the information of login user and also update login user data
	 */
    public function perference()
	{
        $logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }
        // validators
        // load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('kids', 'Height', 'trim|required');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('age-min', 'Age Minimum', 'trim|required');
		$this->form_validation->set_rules('age-max', 'Age Maximum', 'trim|required');
		$this->form_validation->set_rules('dist-min', 'Distance Minimum', 'trim|required');
		$this->form_validation->set_rules('dist-max', 'Distance Maximum', 'trim|required');
        $this->form_validation->set_rules('access_loc', 'Acess location', 'trim|required');
        if ($this->form_validation->run() === TRUE)
        {
            $user    = $this->user_model->update_perference($this->input->post());
            redirect('/user/dashboard');
        }
        // create the data object
        $user_id = $this->session->userdata['user_id'];
        $data['content'] = $this->user_model->get_user($user_id);
        $data['religion'] = $this->user_model->get_all_religion();
        $data['ethnicity'] = $this->user_model->get_all_ethnicity();
        $data['datepreference'] = $this->user_model->get_new_datepreference($data['content']->date_pref);
        $data["crt"]="perference";
        // load views
        // user logout ok
		$this->load->view('header');
        $this->load->view('sidebar', $data);
	    $this->load->view('user/dashboard/perference');
		$this->load->view('footer');
    }
	/**
	 Function Name :search.
	 * 
	 * @access public
	 * @return the users list match with login user preferences
	 */
    /*public function search()
	{
        $logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }
        // create the data object
        $user_id = $this->session->userdata['user_id'];
        $data['content'] = $this->user_model->get_search_prefences($user_id);
		$data['notification']=$this->fetch_notification();
		$data['gallery']=$this->fetch_gallery($user_id);
        $data["crt"]="search";
		$value['header']=$this->admin_model->get_setting();	
		$this->load->view('header',$value);
	    $this->load->view('user/dashboard/search',$data);
		$this->load->view('footer');
    }*/
	/**
	 Function Name :search by condition.
	 * 
	 * @access public
	 * @return the users list match with login user preferences
	 */
    /*public function search_by_condition()
	{
        $logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }
        // create the data object
        $user_id = $this->session->userdata['user_id'];
        $data['content'] = $this->user_model->get_search_prefences_by_condition($user_id,$this->input->post("find_to"),$this->input->post("age_from"),$this->input->post("age_to"));
		$data['notification']=$this->fetch_notification();
		$data['gallery']=$this->fetch_gallery($user_id);
        $data["crt"]="search";
		$value['header']=$this->admin_model->get_setting();	
		$this->load->view('header',$value);
	    $this->load->view('user/dashboard/search',$data);
		$this->load->view('footer');
    }*/
	/**
	 Function Name :friend_like.
	 * Function is user to like perticular selected user
	 * @access public
	 * @return void
	 */
    /*public function friend_like($fid)
	{
		$logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
			redirect('login');
            //redirect('login');
        }
        $user_id = $this->session->userdata['user_id'];
        $friend_id = $this->uri->segment(3);
		$chk=$this->user_model->get_friend($user_id,$friend_id);
        if(empty($chk))
		{
			$friend=$this->user_model->get_user($friend_id);
			$already=$this->user_model->is_already_friend($user_id,$friend_id);
			if(empty($already)){
				$data['content'] = $this->user_model->friend_like($user_id,$friend_id);
				//$this->session->set_flashdata('add_friend', 'Request sent to '.$friend->fname.' '.$friend->lname);
				/* add user in the roster and subscribe user * /
				$user=$this->user_model->get_user($user_id);
				
				if(XMPP_ENABLE)
				{
					$this->Ejabberd_model->add_rosteritem($user_id,$friend_id);
					/*try
					{
						$user_connection=array(
							'hostname'	=> XMPP_HOST,
							'username'	=> $user_id.'_'.strtolower(trim($user->fname)).'_cl_new@'.XMPP_SERVER,
							'password'	=> XMPP_DEFAULT_PASSWORD
						);
						$user_xmpp=new Radix_XMPP($user_connection);
						//add user to the roster 
						$user_xmpp->roster($friend_id.'_'.strtolower(trim($friend->fname)).'_cl_new@'.XMPP_SERVER);
					}
					catch(Exception $e){
						$this->session->set_flashdata('error', lang('lbl_notification_error_xmpp').$e->getMessage());
					}* /
				}
					//send push notification
				try
				{
					$ludetail=$this->user_model->get_user_array($user_id);
					$udetail=$this->user_model->get_user_array($friend_id);
					$device_token=$this->user_model->get_device_token($friend_id);
					$fdetail["friend_profile"]=$ludetail["profile_image"];
					$fdetail["friend_Fname"]=$ludetail["fname"];
					$fdetail["friend_Lname"]=$ludetail["lname"];
					$fdetail["friendid"]=$ludetail["id"];
					foreach($device_token as $device)
					{
						if($device['device']=="ios")
						{
							try
							{
							$status=$this->Pushnotification_model->send_push($device['device_token'], "Cupid Love",($udetail["notificationcounter"]+1) , $ludetail["fname"]." has Liked you",$type=1,$fdetail);
							}
							catch(Exception $e)
							{
								//$this->session->set_flashdata('error', lang('lbl_notification_error_xmpp').$e->getMessage());
							}
						}
						elseif($device['device']=="android")
							$this->Pushnotification_model->send_push_android($device['device_token'], "Cupid Love",($udetail["notificationcounter"]+1) , $ludetail["fname"]." has Liked you",$type=1,$fdetail);
					}
				}
				catch(Exception $e)
				{
					echo lang('lbl_notification_error_notification');
					//$this->session->set_flashdata('error', lang('lbl_notification_error_notification').$e->getMessage());
				}
			}			
		}
		echo 'Request sent to '.$friend->fname.' '.$friend->lname;
		//redirect('user/search');
		
    }*/
	/**
	 Function Name :friend_dislike.
	 * Function is user to dis-like particular selected user
	 * @access public
	 * @return void
	 */
   /* public function friend_dislike($fid)
	{
        $logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
			redirect('login');
            //redirect('login');
        }
        // create the data object
        $user_id = $this->session->userdata['user_id'];		
        $friend_id = $this->uri->segment(3);
		$friend=$this->user_model->get_user($friend_id);
		$already=$this->user_model->is_already_friend($user_id,$friend_id);
		if(empty($already)){
			$this->user_model->delete_friend($user_id,$friend_id);
		}
        $data['content'] = $this->user_model->friend_dislike($user_id,$friend_id);
		//$this->session->set_flashdata('dislike_friend', 'You have ignored '.$friend->fname.' '.$friend->lname);
		echo 'You have ignored '.$friend->fname.' '.$friend->lname;
        //redirect('user/search');
    }*/
	/**
	 Function Name :getDistance.
	 * Function is used to fetch distance from latitude and longitude
	 * @access public
	 * @return void
	 */
	public function getDistance($gps1, $gps2)
	{
		$lat1 = $gps1['lat'];
		$lon1 = $gps1['lng'];
		$lat2 = $gps2['lat'];
		$lon2 = $gps2['lng']; 
		$theta = $lon1-$lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +
		cos(deg2rad($lat1)) * cos(deg2rad($lat2)) *
		cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		return ($miles * 0.8684);
	}
	/**
	 Function Name :notification.
	 * Function is used to fetch login user notification
	 * @access public
	 * @return void
	 */
    /*public function notification()
	{
        $logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }
        // create the data object
		$user_id = $this->session->userdata['user_id'];
        $data['notification'] = $this->fetch_notification();		
        $data["crt"]="notification";
        // load views
		$value['header']=$this->admin_model->get_setting();
	    $this->load->view('header',$value);
	    $this->load->view('user/dashboard/notification', $data);
		$this->load->view('footer');
    }*/
	/*public function stories()
    {
		$i=0;
		$results3=$this->story_model->fetch_stories(array('parent_id'=>0));
		$stories=array();
		foreach($results3 as $story){
			$stories[$i][$story['name']]=$story;
			$childs=$this->story_model->fetch_stories(array('parent_id'=>$story['id']));
			foreach($childs as $child){
				$stories[$i][$child['name']]=$child;
			}
			$i++;
		}
        $data['stories']=$stories;
        $value['header']=$this->admin_model->get_setting();		
		$value['notification'] = $this->fetch_notification();
	    $this->load->view('header',$value);
		$this->load->view("user/dashboard/stories",$data);
		$this->load->view('footer');
    }*/
	/**
	 Function Name :friend_approved.
	 * Function is used to approve notification who is send me friend request
	 * @access public
	 * @return void
	 */
    /*public function friend_approved($fid)
	{
        $logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }
        // create the data object
        $user_id = $this->session->userdata['user_id'];
        $friend_id = $this->uri->segment(3);
		$friend=$this->user_model->get_user($friend_id);
		/*$already=$this->user_model->is_already_friend($user_id,$friend_id);
		if(!empty($already)){
			$this->user_model->delete_friend($user_id,$friend_id);
		}* /
        $data['content'] = $this->user_model->friend_approved($user_id,$friend_id);
        $this->session->set_flashdata('friend_approved', $friend->fname.' '.$friend->lname.' added as your friend');
		$user=$this->user_model->get_user($user_id);
		echo $friend->fname.' '.$friend->lname.' added as your friend';
		if(XMPP_ENABLE)
		{
			$this->Ejabberd_model->add_rosteritem($user_id,$friend_id);
			/* try
			{
				$user_connection=array(
					'hostname'	=> XMPP_HOST,
					'username'	=> $user_id.'_'.strtolower(trim($user->fname)).'_cl_new@'.XMPP_SERVER,
					'password'	=> XMPP_DEFAULT_PASSWORD
				);
				$user_xmpp=new Radix_XMPP($user_connection);
				//add user to the roster 
				$user_xmpp->subscribed($friend_id.'_'.strtolower(trim($friend->fname)).'_cl_new@'.XMPP_SERVER);
				$user_xmpp->roster($friend_id.'_'.strtolower(trim($friend->fname)).'_cl_new@'.XMPP_SERVER);
				$friend_connection=array(
					'hostname'	=> XMPP_HOST,
					'username'	=> $friend_id.'_'.strtolower(trim($friend->fname)).'_cl_new@'.XMPP_SERVER,
					'password'	=> XMPP_DEFAULT_PASSWORD);
				$user_xmpp=new Radix_XMPP($friend_connection);
				$user_xmpp->subscribed($user_id.'_'.strtolower(trim($user->fname)).'_cl_new@'.XMPP_SERVER);
			}
			catch(Exception $e)			
			{
				$this->session->set_flashdata('error', lang('lbl_notification_error_xmpp').$e->getMessage());
			}* /
		}
		//send push notification
		try
		{
			$ludetail=$this->user_model->get_user_array($user_id);
			$device_token=$this->user_model->get_device_token($friend_id);
			$udetail=$this->user_model->get_user_array($friend_id);
			if($ludetail["profile_image"]=="")
				$fdetail["friend_profile"]="default.png";
			else
				$fdetail["friend_profile"]=$ludetail["profile_image"];
			$fdetail["friend_Fname"]=$ludetail["fname"];
			$fdetail["friend_Lname"]=$ludetail["lname"];
			$fdetail["friendid"]=$ludetail["id"];
			foreach($device_token as $device)
			{
				if($device['device']=="ios")
					$this->Pushnotification_model->send_push($device["device_token"], "Cupid Love",($udetail["notificationcounter"]+1) , "You are matched With ".$ludetail["fname"],$type=2,$fdetail);
				elseif($device['device']=="android")
					$this->Pushnotification_model->send_push_android($device["device_token"], "Cupid Love",($udetail["notificationcounter"]+1) , "You are matched With ".$ludetail["fname"],$type=2,$fdetail);
			}	
		}
		catch(Exception $e)
		{
			$this->session->set_flashdata('error',  lang('lbl_notification_error_notification').$e->getMessage());
		}
		//redirect('user/notification');
    }
	/**
	 Function Name :friend.
	 * Function is used to fetch login users friend
	 * @access public
	 * @return users information
	 * /
	public function friend()
	{
        $logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
			redirect('login');
        }
        // create the data object
		$user_id = $this->session->userdata['user_id'];
		$userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
		/*
		* XMPP Register user
		* /
		if(XMPP_ENABLE && empty($userdetail->ejuser)){
			//register user
			$ejuser=$this->Ejabberd_model->register($user_id);
		}
		else{
			$ejuser=$userdetail->ejuser;
		}
		/*
		* XMPP register ends
		* /

        $data['content'] = $this->user_model->get_friends_list($user_id);
		$data['notification']=$this->fetch_notification();
		$data['gallery']=$this->fetch_gallery($user_id);
		$data["crt"]="friends";   
		$data["ejuser"]=$ejuser;
        // load views
		$value['header']=$this->admin_model->get_setting();	
		$this->load->view('header',$value);
	    $this->load->view('user/dashboard/friends',$data);
		$this->load->view('footer');
    }
	/**
	 Function Name :friend_decline.
	 * Function is used reject user friend request who send me
	 * /
    public function friend_decline($id)
    {
		$id=$this->uri->segment(3);
        $logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }
		$rid=$this->session->userdata('user_id');
        $this->user_model->friend_decline($rid,$id);
        $this->session->set_flashdata('friend_decline', 'You have declined friend request');                
       	echo 'You have declined friend request';
       	//redirect('user/dashboard');
    }
	/**
	 Function Name :friend_profile.
	 * Function is used fetch selected friend profile information
	 * @access public
	 * @return single user information
	 * /
    public function friend_profile($fid)
	{
        $logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }
        // create the data object
        $user_id = $this->session->userdata['user_id'];
        $friend_id = $this->uri->segment(3);
        $data['content'] = $this->user_model->get_friend_profile($user_id,$friend_id);
		$data['gallery']=$this->fetch_gallery($friend_id);
		$dat['notification'] = $this->fetch_notification();
        if($data['content']=='')
		{
			redirect('user/friend');
        }
        // load views
        // user logout ok
		$this->load->view('header');        
	    $this->load->view('user/dashboard/friend_profile', $data);
		$this->load->view('footer');
    }*/
	/**
	 Function Name :user_profile.
	 * Function is used fetch selected user profile information
	 * @access public
	 * @return single user information
	 */
	public function user_profile()
	{
        $logged_in_user = $this->session->userdata('logged_in');         
        if (!$logged_in_user)
        {
            redirect('login');
        }
        // create the data object
        $user_id = $this->uri->segment(3);
        $data['content'] = $this->user_model->get_user_profile($user_id);
		$data['gallery']=$this->fetch_gallery($user_id);
        if($data['content']=='')
		{
			redirect('user/friend');
        }
        // load views
        // user logout ok
		$value['header']=$this->admin_model->get_setting();
	    $this->load->view('header',$value);
	    $this->load->view('user/dashboard/user_profile', $data);
		$this->load->view('footer');
	}
	//change password start.
	public function forgot_pass()
    {
			$logged_in_user = $this->session->userdata('logged_in');         
			if (!empty($_SESSION['user_id']))
			{
				redirect('login');
			}
			$value['header']=$this->admin_model->get_setting();
			$this->load->view('header',$value);
			$this->load->view('user/forgot_password/forgot_pass');
			$this->load->view('footer');                    
    }
    public function check_email()
    {
       $email=$this->input->post("username");
        if(!empty($email))
        {
            $id=$this->user_model->get_user_id_from_email($email);                        
		    if(!empty($id))
            {
                $token=md5(uniqid(rand(), true));
                if($this->user_model->set_token($token,$id))
                {                    
					$link="For Reset Your Password.<a href='".base_url()."user/reset_pass/".$token."/".$id."'>Click Here</a>";
					$this->email->from('info@themes.potenzaglobalsolutions.com', 'Cupid Love');
					$this->email->to($email);
					$this->email->subject('Reset Password');
					$this->email->message($link);
					$this->email->set_mailtype('html');
					if($this->email->send()){
                        $this->session->set_flashdata('success',lang('lbl_forgot_password_mail_success'));                                                                      
                    }
                    else
                        $this->session->set_flashdata('fail',lang('lbl_forgot_password_error_mail'));
					    $this->common_msg();				   
                }
            }
            else
            {
               $this->session->set_flashdata('fail',lang('lbl_forgot_password_error_mail'));
               redirect("/user/forgot_pass");
            } 
        }
		else
		{
		   $this->session->set_flashdata('fail',lang('lbl_forgot_password_error_mail'));
		   redirect("/user/forgot_pass");
		} 
    }
	//Reset Password Function
    public function reset_pass($token,$id)
    {
	   $user=$this->user_model->get_user_where_row(array("pass_token"=>$token,"id"=>$id));
        if(!empty($user))
        {
            if($user->id==$id&&$user->pass_token==$token)
            {
				$value['header']=$this->admin_model->get_setting();
				$this->load->view('header',$value);
				$this->load->view("user/forgot_password/reset_pass",array("id"=>$id,"token"=>$token,'email'=>$user->email));   
				$this->load->view('footer');  				
            }
            else
            {
                $this->session->set_flashdata('fail',lang('lbl_common_unauthorized'));
                $this->common_msg();
            }            
        }
        else
        {
            $this->session->set_flashdata('fail',lang('lbl_common_link_expired'));						      
			$this->common_msg();
        }           
    }
	//Change Password Function
    public function change_pass()
    {
    	$this->load->library('form_validation'); 
		$this->form_validation->set_rules('reset_password', lang('lbl_register_password'), 'trim|required|min_length[6]');
		$this->form_validation->set_rules('confirm_password', lang('lbl_register_confirm_password'), 'trim|required|matches[reset_password]');
		if ($this->form_validation->run() === false) 
		{
	        $id=$this->input->post("id");
	        $token=$this->input->post("token");
			$this->reset_pass($token,$id);
	    }
	    else{
	    	$id=$this->input->post("id");
	        $token=$this->input->post("token");
	        $pass=$this->input->post("reset_password");
	        $cpass=$this->input->post("confirm_password");
	        if($pass==$cpass)
	        {
	            if($this->user_model->update_password(array("id"=>$id),$pass))
	            {
	                $this->session->set_flashdata('success',lang('lbl_log_in_success') ); 
	                $this->user_model->unset_token($id);    
	                redirect('user/reset_pass/'.$token.'/'.$id);            
	            }
	            else {
	                $this->session->set_flashdata('fail',lang('lbl_log_in_error'));
	                redirect('login'); 
	            }
	        }else{
	            $this->session->set_flashdata('fail','Please enter same password');
	        }
	   	}
    }	
	//After the completing every reset password function this function message is displayed
    public function common_msg()
    {
        $value['header']=$this->admin_model->get_setting();
		$this->load->view('header',$value);
		$this->load->view("user/forgot_password/common_msg");
		$this->load->view('footer');  
    }
	/*public function blog($author=null)
    {   
        $logged_in_user = $this->session->userdata('logged_in');
		if(!empty($author) && !is_numeric($author)){
			$condition=array('blog.parent_id'=>0,'blog.author'=>str_replace("_"," ",$author));
			$base_url=site_url('user/blog/'.str_replace(" ","_",$author));
		}
		else{
			$condition=array('blog.parent_id'=>0);
			$base_url=site_url('user/blog');
		}
		$total_rows = $this->blog_model->count_total($condition);
        $config['base_url'] = $base_url;    // url of the page
        $config['total_rows'] = $total_rows;//get total number of records 
        $config['per_page'] = 2;  // define how many records on page
        $config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = lang('lbl_next').' <span aria-hidden="true">&raquo;</span>';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '<span aria-hidden="true">&laquo;</span> '.lang('lbl_prev');
        $config['prev_tag_open'] = '<li class="previous">';
        $config['prev_tag_close'] = '</li>';
        $config['page_query_string'] = FALSE;
        $this->pagination->initialize($config);
        if(!empty($author) && !is_numeric($author)) 
        	$page = ($this->uri->segment(4))? $this->uri->segment(4) : 0;
        else
        	$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
		$i=0;
		$results1=$this->blog_model->get_my_data($condition,$config['per_page'],$page);
		/*if(!empty($author)){
			echo $this->db->last_query()."|".$this->uri->segment(3);
		}* /
		$db_blog=array();
		foreach($results1 as $blog){
			$db_blog[$i][$blog['name']]=$blog;
			if(!empty($author) && !is_numeric($author)){
				$condition=array('blog.parent_id'=>$blog['id'],'blog.author'=>$author);
			}
			else{
				$condition=array('blog.parent_id'=>$blog['id']);
			}
			$childs=$this->blog_model->get_my_data($condition);
			foreach($childs as $child){
				$db_blog[$i][$child['name']]=$child;
			}
			$i++;
		}
        $data=array(
			'db_blog'=>$db_blog,
            'total_rows'=>$total_rows,
        );
        $value['header']=$this->admin_model->get_setting();				
		$this->load->view('header',$value);	
		$this->load->view("user/dashboard/blog",$data);
		$this->load->view('footer');
    }*/
	/*
	public function contact()
    { 
		$x=0;
		$results4=$this->blocks_model->fetch_block(array('parent_id'=>0));
		$blocks=array();
		foreach($results4 as $block){
			$blocks[$x][$block['name']]=$block;
			$childs=$this->blocks_model->fetch_block(array('parent_id'=>$block['id']));
			foreach($childs as $child){
				$blocks[$x][$child['name']]=$child;
			}
			$x++;
		}
		$data['blocks']=$blocks;
		$data['contact']=$this->user_model->get_site_setting();
        $value['header']=$this->admin_model->get_setting();				
		$this->load->view('header',$value);
		$this->load->view("user/dashboard/contact",$data);
		$this->load->view('footer');
    }*/
	/* public function blog_details($id)
    { 	
		$logged_in_user = $this->session->userdata('logged_in');
		$j=0;
		$results=$this->blog_model->getblogitem(array('blog.id'=>$id));
		
		$blog_details=array();
			$blog_details[$j][$results['name']]=$results;
			$childs=$this->blog_model->getblogitem(array('parent_id'=>$blog['id']));
			foreach($childs as $child){
				$blog_details[$j][$child['name']]=$child;
			}
			$j++;
		$data['blog_details']=$blog_details;
	
        $value['header']=$this->admin_model->get_setting();				
		$this->load->view('header',$value);
		$this->load->view("user/dashboard/blog_details",$data);
		$this->load->view('footer');
    }*/
	/*public function contact_insert()
    {
        $ispost=$this->input->method(TRUE);
        if($ispost=='POST')
        {
            //allow only post method here
            //do the form validation here 
				$this->load->helper('form');
                $this->load->library('form_validation');
                $this->form_validation->set_rules('name', lang('lbl_contact_name'), 'trim|required');
                $this->form_validation->set_rules('email', lang('lbl_register_email'), 'trim|required|valid_email');
                $this->form_validation->set_rules('subject',lang('lbl_contact_subject'), 'trim|required');
                $this->form_validation->set_rules('message',lang('lbl_contact_comment'), 'trim|required');
				$data['contact']=$this->user_model->get_site_setting();
				$x=0;
				$results4=$this->blocks_model->fetch_block(array('parent_id'=>0));
				$blocks=array();
				foreach($results4 as $block){
					$blocks[$x][$block['name']]=$block;
					$childs=$this->blocks_model->fetch_block(array('parent_id'=>$block['id']));
					foreach($childs as $child){
						$blocks[$x][$child['name']]=$child;
					}
					$x++;
				}
				$data['blocks']=$blocks;
				if ($this->form_validation->run() === false) 
				{
					$value['header']=$this->admin_model->get_setting();				
					$this->load->view('header',$value);					
					$this->load->view('user/dashboard/contact', $data);
					$this->load->view('footer');
				} 
                else
                {
                        $dbdata=array(
                            'name'=>$this->input->post('name'),
                            'email'=>$this->input->post('email'),
                            'subject'=>$this->input->post('subject'),
                            'message'=>$this->input->post('message'),
                        );
                        $id=$this->user_model->insert_contact($dbdata);
                        if($id)
                        {
                            //echo success;   //set flash data for success and redirect to index
                            $this->session->set_flashdata('success',lang("lbl_contact_success"));
							redirect('user/contact');
                        }
                        else{
                            //echo fail; //set flash data and redirect back to add
                            $this->session->set_flashdata('fail',lang("lbl_error"));
                            redirect('user/contact');
                        }
                  }
          }
          else
          {
            //set the error message here and redirect to the index page
            $this->session->set_flashdata('error','Something Went Wrong!');
            redirect('user/contact');
          }
	}*/
    /*public function team_contact_insert()
    {
        $team_id = $this->input->post('team_id');
        $ispost=$this->input->method(TRUE);
        if($ispost=='POST')
        {
            //allow only post method here
            //do the form validation here 
                $this->load->library('form_validation');
                $this->form_validation->set_rules('name', 'Name', 'required');
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
                $this->form_validation->set_rules('subject', 'Subject', 'required');
                $this->form_validation->set_rules('message', 'Comment', 'required');
				$data['contact']=$this->user_model->get_site_setting();
				$x=0;
				$results4=$this->blocks_model->fetch_block(array('parent_id'=>0));
				$blocks=array();
				foreach($results4 as $block){
					$blocks[$x][$block['name']]=$block;
					$childs=$this->blocks_model->fetch_block(array('parent_id'=>$block['id']));
					foreach($childs as $child){
						$blocks[$x][$child['name']]=$child;
					}
					$x++;
				}
				$data['blocks']=$blocks;
				if ($this->form_validation->run() === false) 
				{
					$value['header']=$this->admin_model->get_setting();
					$g=0;
					$results13=$this->blocks_model->get_fetch_block(array('block.id'=>17));
					$footer_contact_us=array();
					foreach($results13 as $contact_us){
						$footer_contact_us[$g][$contact_us['name']]=$contact_us;
						$childs=$this->blocks_model->get_fetch_block(array('parent_id'=>$contact_us['id']));
						foreach($childs as $child){
							$footer_contact_us[$g][$child['name']]=$child;
						}
						$g++;
					}
					$data['footer_contact_us'] = $footer_contact_us;
					$h=0;
					$results14=$this->blocks_model->get_fetch_block(array('block.id'=>19));
					$our_activity_section=array();
					foreach($results14 as $our_activity){
						$our_activity_section[$h][$our_activity['name']]=$our_activity;
						$childs=$this->blocks_model->get_fetch_block(array('parent_id'=>$our_activity['id']));
						foreach($childs as $child){
							$our_activity_section[$h][$child['name']]=$child;
						}
						$h++;
					}
					$data['our_activity_section'] = $our_activity_section;
                    $data['member']=$this->members_model->getMember($team_id);
                    if (empty($data['member'])) {
                        redirect('user/team');
                    }else{
            	    $this->load->view('header',$value);			
            		$this->load->view('user/dashboard/singleteam',$data);
            		$this->load->view('footer',$value);
                    }
				} 
                else
                {
                        $dbdata=array(
                            'name'=>$this->input->post('name'),
                            'email'=>$this->input->post('email'),
                            'subject'=>$this->input->post('subject'),
                            'message'=>$this->input->post('message'),
                        );
                        $id=$this->user_model->insert_contact($dbdata);
                        if($id)
                        {
                            //echo success;   //set flash data for success and redirect to index
                            $this->session->set_flashdata('success',lang("lbl_contact_success"));
							redirect('user/singleteam/'.$team_id);
                        }
                        else{
                            //echo fail; //set flash data and redirect back to add
                            $this->session->set_flashdata('fail',lang("lbl_error"));
                            redirect('user/singleteam/'.$team_id);
                        }
                  }
          }
          else
          {
            //set the error message here and redirect to the index page
            $this->session->set_flashdata('error','Something Went Wrong!');
            redirect('user/singleteam/'.$team_id);
          }
	}*/
	public function display_cms($alias_name)
	{
		$data['cms']=$this->Cms_model->getRecord(array('slug'=>$alias_name));
		if(!empty($data['cms']))
		{
			 $value['header']=$this->admin_model->get_setting();				
			$this->load->view('header',$value);
			$this->load->view('user/dashboard/cms_display',$data);
			$this->load->view('footer');
		}
	}
	/*public function stories_details($id)
    { 
		$j=0;
		$results=$this->story_model->getstoriesitem(array('stories.id'=>$id));
		$story=array();
			$story[$j][$results['name']]=$results;
			$childs=$this->story_model->getstoriesitem(array('stories.parent_id'=>$results['id']));
			foreach($childs as $child){
				$story[$j][$child['name']]=$child;
			}
			$j++;
        $data['story']=$story;
		$value['header']=$this->admin_model->get_setting();				
		$this->load->view('header',$value);
		$this->load->view("user/dashboard/stories_details",$data);
		$this->load->view('footer');
    }*/
   /* public function error404()
    { 	
		$j=0;
		$results=$this->admin_model->geterror404(array('parent_id'=>0));
		$error404=array();
		foreach($results as $error){
			$error404[$j][$error['name']]=$error;
			$childs=$this->admin_model->geterror404(array('parent_id'=>$error['id']));
			foreach($childs as $child){
				$error404[$j][$child['name']]=$child;
			}
			$j++;
		}
        $data['error404']=$error404;
        $value['header']=$this->admin_model->get_setting();				
        $this->load->view('header',$value);
		$this->load->view("user/dashboard/error404",$data);
		$this->load->view('footer');
    }*/
	/*
    public function comingsoon()
    { 	
		$k=0;
		$results1=$this->comingsoon_model->getcomingsoon(array('parent_id'=>0));
		$comingsoon=array();
		foreach($results1 as $page){
			$comingsoon[$k][$page['name']]=$page;
			$childs1=$this->comingsoon_model->getcomingsoon(array('parent_id'=>$page['id']));
			foreach($childs1 as $child1){
				$comingsoon[$k][$child1['name']]=$child1;
			}
			$k++;
		}
        $data['comingsoon']=$comingsoon;
        $value['header']=$this->admin_model->get_setting();				
        $this->load->view('header',$value);
		$this->load->view("user/dashboard/comingsoon",$data);
		$this->load->view('footer');
    }*/
    /*public function privacy()
	{
		$g=0;
		$results13=$this->blocks_model->get_fetch_block(array('block.id'=>11));
		$privacy=array();
		foreach($results13 as $privacy_policy){
			$privacy[$g][$privacy_policy['name']]=$privacy_policy;
			$childs=$this->blocks_model->get_fetch_block(array('parent_id'=>$privacy_policy['id']));
			foreach($childs as $child){
				$privacy[$g][$child['name']]=$child;
			}
			$g++;
		}
		$data['privacy'] = $privacy;
        $value['header']=$this->admin_model->get_setting();				
        $this->load->view('header',$value);
		$this->load->view("user/dashboard/privacy",$data);
		$this->load->view('footer');
    }*/
   /* public function terms_condition()
	{
		$g=0;
		$results13=$this->blocks_model->get_fetch_block(array('block.id'=>14));
		$terms=array();
		foreach($results13 as $termscondition){
			$terms[$g][$termscondition['name']]=$termscondition;
			$childs=$this->blocks_model->get_fetch_block(array('parent_id'=>$termscondition['id']));
			foreach($childs as $child){
				$terms[$g][$child['name']]=$child;
			}
			$g++;
		}
		$data['terms'] = $terms;
        $value['header']=$this->admin_model->get_setting();				
        $this->load->view('header',$value);
		$this->load->view("user/dashboard/terms",$data);
		$this->load->view('footer');
    }*/
   /* public function about()
	{
		$results=$this->blog_model->fetch_blogs(array('parent_id'=>0));
		$blogs=array();
		$i=0;$j=0;$k=0;
		foreach($results as $blog){
			$blogs[$i][$blog['name']]=$blog;
			$childs=$this->blog_model->fetch_blogs(array('parent_id'=>$blog['id']));
			foreach($childs as $child){
				$blogs[$i][$child['name']]=$child;
			}
			$i++;
		}
		$results1=$this->Testimonial_model->fetch_testimonial(array('parent_id'=>0));
		$testimonials=array();
		foreach($results1 as $testimonial){
			$testimonials[$j][$testimonial['name']]=$testimonial;
			$childs=$this->Testimonial_model->fetch_testimonial(array('parent_id'=>$testimonial['id']));
			foreach($childs as $child){
				$testimonials[$j][$child['name']]=$child;
			}
			$j++;
		}
		$results2=$this->members_model->getMembers(array('parent_id'=>0));
		$members=array();
		foreach($results2 as $member){
			$members[$k][$member['name']]=$member;
			$childs=$this->members_model->getMembers(array('parent_id'=>$member['id']));
			foreach($childs as $child){
				$members[$k][$child['name']]=$child;
			}
			$k++;
		}
		$l=0;
		$results3=$this->story_model->fetch_stories(array('parent_id'=>0));
		$stories=array();
		foreach($results3 as $story){
			$stories[$l][$story['name']]=$story;
			$childs=$this->story_model->fetch_stories(array('parent_id'=>$story['id']));
			foreach($childs as $child){
				$stories[$l][$child['name']]=$child;
			}
			$l++;
		}
		$x=0;
		$results4=$this->blocks_model->fetch_block(array('parent_id'=>0));
		$blocks=array();
		foreach($results4 as $block){
			$blocks[$x][$block['name']]=$block;
			$childs=$this->blocks_model->fetch_block(array('parent_id'=>$block['id']));
			foreach($childs as $child){
				$blocks[$x][$child['name']]=$child;
			}
			$x++;
		}
		$y=0;
		$results5=$this->Slider_model->fetch_slider(array('parent_id'=>0));
		$sliders=array();
		foreach($results5 as $slider){
			$sliders[$y][$slider['name']]=$slider;
			$childs=$this->Slider_model->fetch_slider(array('parent_id'=>$slider['id']));
			foreach($childs as $child){
				$sliders[$y][$child['name']]=$child;
			}
			$y++;
		}
		$value['header']=$this->admin_model->get_setting();
		$total=$this->admin_model->get_all();
		$total_count=count($total);
		$online=$this->admin_model->get_all_users();
		$online_count=count($online);
		$male=$this->admin_model->get_all_users(array('gender'=>'male'));
		$male_count=count($male);
		$female=$this->admin_model->get_all_users(array('gender'=>'female'));
		$female_count=count($female);
		$data['total_users']=  $total_count;
		$data['online_users']=  $online_count;
		$data['male_users']=  $male_count;
		$data['female_users']=  $female_count;
		$data['users']=$total;
		$data['stories']=$stories;
		$data['blogs']=$blogs;
		$data['testimonials']=$testimonials;
		$data['blocks']=$blocks;
		$data['sliders']=$sliders;
        $data['members']=$members;
		$z=0;
		$results6=$this->blocks_model->get_fetch_block(array('block.id'=>12));
		$our_history=array();
		foreach($results6 as $history){
			$our_history[$z][$history['name']]=$history;
			$childs=$this->blocks_model->get_fetch_block(array('parent_id'=>$history['id']));
			foreach($childs as $child){
				$our_history[$z][$child['name']]=$child;
			}
			$z++;
		}
		$data['our_history'] = $our_history;
		$a=0;
		$results7=$this->blocks_model->get_fetch_block(array('block.id'=>13));
		$our_history_inner=array();
		foreach($results7 as $history_inner){
			$our_history_inner[$a][$history_inner['name']]=$history_inner;
			$childs=$this->blocks_model->get_fetch_block(array('parent_id'=>$history_inner['id']));
			foreach($childs as $child){
				$our_history_inner[$a][$child['name']]=$child;
			}
			$a++;
		}
		$data['our_history_inner'] = $our_history_inner;
		$b=0;
		$results8=$this->blocks_model->get_fetch_block(array('block.id'=>15));
		$why_choose_us=array();
		foreach($results8 as $why_choose){
			$why_choose_us[$b][$why_choose['name']]=$why_choose;
			$childs=$this->blocks_model->get_fetch_block(array('parent_id'=>$why_choose['id']));
			foreach($childs as $child){
				$why_choose_us[$b][$child['name']]=$child;
			}
			$b++;
		}
		$data['why_choose_us'] = $why_choose_us;
		$c=0;
		$results9=$this->blocks_model->get_fetch_block(array('block.id'=>7));
		$team_inner_box1=array();
		foreach($results9 as $inner_box1){
			$team_inner_box1[$c][$inner_box1['name']]=$inner_box1;
			$childs=$this->blocks_model->get_fetch_block(array('parent_id'=>$inner_box1['id']));
			foreach($childs as $child){
				$team_inner_box1[$c][$child['name']]=$child;
			}
			$c++;
		}
		$data['team_inner_box1'] = $team_inner_box1;
		$d=0;
		$results10=$this->blocks_model->get_fetch_block(array('block.id'=>8));
		$team_inner_box2=array();
		foreach($results10 as $inner_box2){
			$team_inner_box2[$d][$inner_box2['name']]=$inner_box2;
			$childs=$this->blocks_model->get_fetch_block(array('parent_id'=>$inner_box2['id']));
			foreach($childs as $child){
				$team_inner_box2[$d][$child['name']]=$child;
			}
			$d++;
		}
		$data['team_inner_box2'] = $team_inner_box2;
		$e=0;
		$results11=$this->blocks_model->get_fetch_block(array('block.id'=>9));
		$team_inner_box3=array();
		foreach($results11 as $inner_box3){
			$team_inner_box3[$e][$inner_box3['name']]=$inner_box3;
			$childs=$this->blocks_model->get_fetch_block(array('parent_id'=>$inner_box3['id']));
			foreach($childs as $child){
				$team_inner_box3[$e][$child['name']]=$child;
			}
			$e++;
		}
		$data['team_inner_box3'] = $team_inner_box3;
		$f=0;
		$results12=$this->blocks_model->get_fetch_block(array('block.id'=>16));
		$subscribe=array();
		foreach($results12 as $subscribe_btn){
			$subscribe[$f][$subscribe_btn['name']]=$subscribe_btn;
			$childs=$this->blocks_model->get_fetch_block(array('parent_id'=>$subscribe_btn['id']));
			foreach($childs as $child){
				$subscribe[$f][$child['name']]=$child;
			}
			$f++;
		}
		$data['subscribe'] = $subscribe;
		$g=0;
		$results13=$this->blocks_model->get_fetch_block(array('block.id'=>17));
		$footer_contact_us=array();
		foreach($results13 as $contact_us){
			$footer_contact_us[$g][$contact_us['name']]=$contact_us;
			$childs=$this->blocks_model->get_fetch_block(array('parent_id'=>$contact_us['id']));
			foreach($childs as $child){
				$footer_contact_us[$g][$child['name']]=$child;
			}
			$g++;
		}
		$data['footer_contact_us'] = $footer_contact_us;
		$this->load->view('header',$value);			
		$this->load->view('user/dashboard/about',$data);
		$this->load->view('footer',$value);
	}*/
   /* public function team()
	{
	    $value['header']=$this->admin_model->get_setting();
		$g=0;
		$results13=$this->blocks_model->get_fetch_block(array('block.id'=>17));
		$footer_contact_us=array();
		foreach($results13 as $contact_us){
			$footer_contact_us[$g][$contact_us['name']]=$contact_us;
			$childs=$this->blocks_model->get_fetch_block(array('parent_id'=>$contact_us['id']));
			foreach($childs as $child){
				$footer_contact_us[$g][$child['name']]=$child;
			}
			$g++;
		}
		$data['footer_contact_us'] = $footer_contact_us;
		$h=0;
		$results14=$this->blocks_model->get_fetch_block(array('block.id'=>18));
		$our_team_member=array();
		foreach($results14 as $team_member){
			$our_team_member[$h][$team_member['name']]=$team_member;
			$childs=$this->blocks_model->get_fetch_block(array('parent_id'=>$team_member['id']));
			foreach($childs as $child){
				$our_team_member[$h][$child['name']]=$child;
			}
			$h++;
		}
		$data['our_team_member'] = $our_team_member;
		$k=0;
		$results2=$this->members_model->getMembers(array('parent_id'=>0));
		$members=array();
		foreach($results2 as $member){
			$members[$k][$member['name']]=$member;
			$childs=$this->members_model->getMembers(array('parent_id'=>$member['id']));
			foreach($childs as $child){
				$members[$k][$child['name']]=$child;
			}
			$k++;
		}
        $data['members']=$members;
	    $this->load->view('header',$value);			
		$this->load->view('user/dashboard/team',$data);
		$this->load->view('footer',$value);
	}*/
    /*public function singleteam($id)
	{
	    $value['header']=$this->admin_model->get_setting();
		$g=0;
		$results13=$this->blocks_model->get_fetch_block(array('block.id'=>17));
		$footer_contact_us=array();
		foreach($results13 as $contact_us){
			$footer_contact_us[$g][$contact_us['name']]=$contact_us;
			$childs=$this->blocks_model->get_fetch_block(array('parent_id'=>$contact_us['id']));
			foreach($childs as $child){
				$footer_contact_us[$g][$child['name']]=$child;
			}
			$g++;
		}
		$data['footer_contact_us'] = $footer_contact_us;
		$h=0;
		$results14=$this->blocks_model->get_fetch_block(array('block.id'=>19));
		$our_activity_section=array();
		foreach($results14 as $our_activity){
			$our_activity_section[$h][$our_activity['name']]=$our_activity;
			$childs=$this->blocks_model->get_fetch_block(array('parent_id'=>$our_activity['id']));
			foreach($childs as $child){
				$our_activity_section[$h][$child['name']]=$child;
			}
			$h++;
		}
		$data['our_activity_section'] = $our_activity_section;
        $data['member']=$this->members_model->getMember($id);
        if (empty($data['member'])) {
            redirect('user/team');
        }else{
	    $this->load->view('header',$value);			
		$this->load->view('user/dashboard/singleteam',$data);
		$this->load->view('footer',$value);
        }
	}*/
	public function getblog()
	{
		$page =  $_GET['page'];
		$blog_id=$_GET['blog_id'];
        $blogs = $this->user_model->getblog($page,$blog_id);
		$cnt=1;
		$flag=true;
		foreach($blogs as $blog)
		{
			if($flag==true)
			{
				echo '<div class="row post-article mt-5  xs-mt-3">';
				$flag=false;
			}
			if($cnt>5)
			{
		   echo '<div class="col-lg-4 col-md-4 col-sm-6 mb-5 xs-mb-3">';
			echo '<div class="post post-artical">';
			 echo '<div class="post-image clearfix">';
			 if(!empty($blog->image))
			 {
			  $str=base_url("Newassets/images/blog/".$blog->image);
              $str1=base_url('images/blog/01.jpg');
			  if(file_exists(DIRECTORY_PATH."Newassets/images/blog/".$blog->image))
			  {      
				echo '<img class="img-fluid" src="'.$str.'" alt="">';
			  }
			  else
			  { 
			    echo '<img class="img-fluid" src="'.$str1.'" alt="">';
			  }       
			 }
			 else
			 {
			  echo '<img class="img-fluid" src="'.$str1.'" alt="">';
			 }
			 echo '<div class="post-details">';
				echo '<div class="post-title mt-2">';
				   echo '<h5 class="title text-uppercase mt-2">';
					echo '<a href="';
						echo base_url('user/blog_details/'.$blog->id);
					echo '">'.$blog->title.'</a></h5></div>';
			 echo '<p>';echo date_format(date_create($blog->created_date),"F , Y"); echo ' by <a href="#">';echo $blog->author;
			  echo '</a></p>';              
			   echo '<div class="post-icon">';
			    echo '<div class="post-content"><p>';echo substr($blog->description, 0, 100); echo '</p></div>';              
				 echo '<a class="button" href="';
					 echo base_url('user/blog_details/'.$blog->id); 
				echo '">Read More</a>';      
			  echo '</div>';
			  echo '</div>';                
			 echo '</div>';  
		   echo '</div>';
		  echo '</div>';  
			}   
		    $cnt++;
		}
		echo '</div>';
    }
   /* public function faq()
	{
		$g=0;
		$results13=$this->blocks_model->get_fetch_block(array('block.id'=>20));
		$faq=array();
		foreach($results13 as $faq1){
			$faq[$g][$faq1['name']]=$faq1;
			$childs=$this->blocks_model->get_fetch_block(array('parent_id'=>$faq1['id']));
			foreach($childs as $child){
				$faq[$g][$child['name']]=$child;
			}
			$g++;
		}
		$data['faq'] = $faq;
		$g=0;
		$results14=$this->blocks_model->get_fetch_block(array('block.id'=>21));
		$faq_questions=array();
		foreach($results14 as $faq2){
			$faq_questions[$g][$faq2['name']]=$faq2;
			$childs=$this->blocks_model->get_fetch_block(array('parent_id'=>$faq2['id']));
			foreach($childs as $child){
				$faq_questions[$g][$child['name']]=$child;
			}
			$g++;
		}
		$data['faq_questions'] = $faq_questions;
        $value['header']=$this->admin_model->get_setting();				
        $this->load->view('header',$value);
		$this->load->view("user/dashboard/faq",$data);
		$this->load->view('footer');
    }*/
    /*function for online users*/
	//getting all online session data
	public function get_all_session_data(){
		$query=$this->user_model->get_all_session_data();
		$user = array();
		$j=0;$m=0;$f=0;
		foreach ($query as $row)
		{
		    //$udata = unserialize($row->data);
		    if( strpos( $row['data'], "logged_in" ) !== false ) {
		    	$strArray = explode(';',$row['data']);
			    $j+=1;
			    foreach ($strArray as $value) {
			    	 if( strpos( $value, "email" ) !== false ) {
			    	 	$email=explode(':',$value);
			    	 	$all_email[]=str_replace('"', "", $email[2]);
			    	 }
			    }
			}
		}
		$all_email=array_unique($all_email);
		foreach ($all_email as $email)
		{
			$checkgen=$this->user_model->get_user_by_con(array("email"=>$email));
    	 	if($checkgen->gender=="male"){
				$m+=1;
    	 	}
    	 	else{
    	 		$f+=1;
    	 	}
		}
		$total_users=$this->user_model->get_total_users();
		echo json_encode(array("total_users"=>$total_users,"total_user_online"=>sizeof($all_email),"men_onlie"=>$m,"female_online"=>$f));
	}
	public function fetch_notification()
	{
		$logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
			redirect('login');
        }
        // create the data object
        $user_id = $this->session->userdata['user_id'];
        $userdetail = $this->user_model->get_user($user_id);
		$data = $this->user_model->get_notification($user_id,$userdetail->location_lat,$userdetail->location_long);
        return $data;
	}
 }