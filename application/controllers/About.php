<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'/PHPThumb/ThumbLib.inc.php';
/**
 * About class.
 * 
 * @extends CI_Controller
 */
class About extends CI_Controller {
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() 
	{
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
        $this->load->library('form_validation');
        $this->load->helper('form');
		$this->load->model('user_model');
		$this->load->model('SiteErrorLog');
		$this->load->model('blocks_model');
		$this->load->model('admin_model');
		$this->load->model('Pushnotification_model');
		$this->load->model('story_model');
		$this->load->model('blog_model');
		$this->load->model('blocks_model');
        $this->load->model('members_model');
		$this->load->model('Cms_model');
		$this->load->library('pagination');
        $this->load->library('table');
		$this->load->model('Testimonial_model');
		$this->load->model('Slider_model');
        $this->load->model('Error404_model');
        $this->load->model('comingsoon_model');
		$this->load->model('background_model');
        date_default_timezone_set('Asia/Kolkata'); 
		$data['header']=$this->admin_model->get_setting();
		$is_comingsoon=$data['header'][18]['status'];
		$method=$this->router->fetch_method();
		if($is_comingsoon==1 && $method!='comingsoon'){
			redirect('user/comingsoon');
		}
	}
	public function index()
    {
		$value['header']=$this->admin_model->get_setting();
		$total=$this->admin_model->get_all();
		$total_count=count($total);
		$online=$this->admin_model->get_all_users();
		$online_count=count($online);
		$male=$this->admin_model->get_all_users(array('gender'=>'male'));
		$male_count=count($male);
		$female=$this->admin_model->get_all_users(array('gender'=>'female'));
		$female_count=count($female);
		$data['total_users']=  $total_count;
		$data['online_users']=  $online_count;
		$data['male_users']=  $male_count;
		$data['female_users']=  $female_count;
		$data['users']=$total;
		$data['stories']=$this->story_model->fetch_stories();
		$data['blogs']=$this->blog_model->fetch_blogs();
		$data['testimonials']=$this->Testimonial_model->fetch_testimonial();
		$data['blocks']=$this->blocks_model->fetch_block();
		$data['sliders']=$this->Slider_model->fetch_slider();
        $data['members']=$this->members_model->getMembers();
		$data['our_history'] = $this->blocks_model->get_fetch_block(array('block.id'=>12));
		$data['our_history_inner'] = $this->blocks_model->get_fetch_block(array('block.id'=>13));
		$data['why_choose_us'] = $this->blocks_model->get_fetch_block(array('block.id'=>15));
		$data['team_inner_box1'] = $this->blocks_model->get_fetch_block(array('block.id'=>7));
		$data['team_inner_box2'] = $this->blocks_model->get_fetch_block(array('block.id'=>8));
		$data['team_inner_box3'] = $this->blocks_model->get_fetch_block(array('block.id'=>9));
		$data['subscribe'] = $this->blocks_model->get_fetch_block(array('block.id'=>16));
		$data['footer_contact_us'] = $this->blocks_model->get_fetch_block(array('block.id'=>17));
		$this->load->view('header',$value);			
		$this->load->view('user/dashboard/about',$data);
		$this->load->view('footer',$value);
	}
	
 }