<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'/PHPThumb/ThumbLib.inc.php';

/*Crop Avtar Class */
class CropAvatar {
	private $src;
	private $data;
	private $dst;
	private $type;
	private $extension;
	private $msg;
	private $upload_path;
  
	function __construct($src, $data, $file,$upload_path) {
	  $this->setUploadPath($upload_path);
	  $this -> setSrc($src);
	  $this -> setData($data);
	  $this -> setFile($file);
	  $this -> crop($this -> src, $this -> dst, $this -> data);
	}
   private function setUploadPath($upload_path){
		$this -> upload_path = $upload_path;
   }
	private function setSrc($src) {
	  if (!empty($src)) {
		$type = exif_imagetype($src);
  
		if ($type) {
		  $this -> src = $src;
		  $this -> type = $type;
		  $this -> extension = image_type_to_extension($type);
		  $this -> setDst();
		}
	  }
	}
  
	private function setData($data) {
	  if (!empty($data)) {
		$this -> data = json_decode(stripslashes($data));
	  }
	}
  
	private function setFile($file) {
	  $errorCode = $file['error'];
  
	  if ($errorCode === UPLOAD_ERR_OK) {
		$type = exif_imagetype($file['tmp_name']);
  
		if ($type) {
		  $extension = image_type_to_extension($type);
		  //$src = FCPATH .'assets/images/student/avtar/' . date('YmdHis') . '.original' . $extension;
		  $src= $this -> upload_path . date('YmdHis') . '.original' . $extension;
		  if ($type == IMAGETYPE_GIF || $type == IMAGETYPE_JPEG || $type == IMAGETYPE_PNG) {
  
			if (file_exists($src)) {
			  unlink($src);
			}
  
			$result = move_uploaded_file($file['tmp_name'], $src);
  
			if ($result) {
			  $this -> src = $src;
			  $this -> type = $type;
			  $this -> extension = $extension;
			  $this -> setDst();
			} else {
			   $this -> msg = 'Failed to save file';
			}
		  } else {
			$this -> msg = 'Please upload image with the following types: JPG, PNG, GIF';
		  }
		} else {
		  $this -> msg = 'Please upload image file';
		}
	  } else {
		$this -> msg = $this -> codeToMessage($errorCode);
	  }
	}
  
	private function setDst() {
	  //$this -> dst = FCPATH .'assets/images/student/avtar/' . date('YmdHis') . '.png';
	  $this->dst= $this -> upload_path. date('YmdHis') . '.png';
	}
  
	private function crop($src, $dst, $data) {
	  if (!empty($src) && !empty($dst) && !empty($data)) {
		switch ($this -> type) {
		  case IMAGETYPE_GIF:
			$src_img = imagecreatefromgif($src);
			break;
  
		  case IMAGETYPE_JPEG:
			$src_img = imagecreatefromjpeg($src);
			break;
  
		  case IMAGETYPE_PNG:
			$src_img = imagecreatefrompng($src);
			break;
		}
  
		if (!$src_img) {
		  $this -> msg = "Failed to read the image file";
		  return;
		}
  
		$size = getimagesize($src);
		$size_w = $size[0]; // natural width
		$size_h = $size[1]; // natural height
  
		$src_img_w = $size_w;
		$src_img_h = $size_h;
  
		$degrees = $data -> rotate;
  
		// Rotate the source image
		if (is_numeric($degrees) && $degrees != 0) {
		  // PHP's degrees is opposite to CSS's degrees
		  $new_img = imagerotate( $src_img, -$degrees, imagecolorallocatealpha($src_img, 0, 0, 0, 127) );
  
		  imagedestroy($src_img);
		  $src_img = $new_img;
  
		  $deg = abs($degrees) % 180;
		  $arc = ($deg > 90 ? (180 - $deg) : $deg) * M_PI / 180;
  
		  $src_img_w = $size_w * cos($arc) + $size_h * sin($arc);
		  $src_img_h = $size_w * sin($arc) + $size_h * cos($arc);
  
		  // Fix rotated image miss 1px issue when degrees < 0
		  $src_img_w -= 1;
		  $src_img_h -= 1;
		}
  
		$tmp_img_w = $data -> width;
		$tmp_img_h = $data -> height;
		$dst_img_w = 206;
		$dst_img_h = 206;
  
		$src_x = $data -> x;
		$src_y = $data -> y;
  
		if ($src_x <= -$tmp_img_w || $src_x > $src_img_w) {
		  $src_x = $src_w = $dst_x = $dst_w = 0;
		} else if ($src_x <= 0) {
		  $dst_x = -$src_x;
		  $src_x = 0;
		  $src_w = $dst_w = min($src_img_w, $tmp_img_w + $src_x);
		} else if ($src_x <= $src_img_w) {
		  $dst_x = 0;
		  $src_w = $dst_w = min($tmp_img_w, $src_img_w - $src_x);
		}
  
		if ($src_w <= 0 || $src_y <= -$tmp_img_h || $src_y > $src_img_h) {
		  $src_y = $src_h = $dst_y = $dst_h = 0;
		} else if ($src_y <= 0) {
		  $dst_y = -$src_y;
		  $src_y = 0;
		  $src_h = $dst_h = min($src_img_h, $tmp_img_h + $src_y);
		} else if ($src_y <= $src_img_h) {
		  $dst_y = 0;
		  $src_h = $dst_h = min($tmp_img_h, $src_img_h - $src_y);
		}
  
		// Scale to destination position and size
		$ratio = $tmp_img_w / $dst_img_w;
		$dst_x /= $ratio;
		$dst_y /= $ratio;
		$dst_w /= $ratio;
		$dst_h /= $ratio;
  
		$dst_img = imagecreatetruecolor($dst_img_w, $dst_img_h);
  
		// Add transparent background to destination image
		imagefill($dst_img, 0, 0, imagecolorallocatealpha($dst_img, 0, 0, 0, 127));
		imagesavealpha($dst_img, true);
  
		$result = imagecopyresampled($dst_img, $src_img, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
  
		if ($result) {
		  if (!imagepng($dst_img, $dst)) {
			$this -> msg = "Failed to save the cropped image file";
		  }
		} else {
		  $this -> msg = "Failed to crop the image file";
		}
  
		imagedestroy($src_img);
		imagedestroy($dst_img);
	  }
	}
  
	private function codeToMessage($code) {
	  $errors = array(
		UPLOAD_ERR_INI_SIZE =>'The uploaded file exceeds the upload_max_filesize directive in php.ini',
		UPLOAD_ERR_FORM_SIZE =>'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
		UPLOAD_ERR_PARTIAL =>'The uploaded file was only partially uploaded',
		UPLOAD_ERR_NO_FILE =>'No file was uploaded',
		UPLOAD_ERR_NO_TMP_DIR =>'Missing a temporary folder',
		UPLOAD_ERR_CANT_WRITE =>'Failed to write file to disk',
		UPLOAD_ERR_EXTENSION =>'File upload stopped by extension',
	  );
  
	  if (array_key_exists($code, $errors)) {
		return $errors[$code];
	  }
  
	  return 'Unknown upload error';
	}
  
	public function getResult() {
	  return !empty($this -> data) ? $this -> dst : $this -> src;
	}
  
	public function getMsg() {
	  return $this -> msg;
	}
  }
  /* Ends */
/**
 * User class.
 * 
 * @extends CI_Controller
 */
class User extends CI_Controller {
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() 
	{
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
        $this->load->library('form_validation');
        $this->load->helper('form');
		$this->load->model('user_model');
		$this->load->model('SiteErrorLog');
		$this->load->model('blocks_model');
		$this->load->model('admin_model');
		$this->load->model('Pushnotification_model');
		$this->load->model('user_model');
		$this->load->model('story_model');
		$this->load->model('blog_model');
		$this->load->model('blocks_model');
        $this->load->model('members_model');
		$this->load->model('Cms_model');
		$this->load->library('pagination');
        $this->load->library('table');
		$this->load->model('Testimonial_model');
		$this->load->model('Slider_model');
        $this->load->model('Error404_model');
        $this->load->model('comingsoon_model');
		$this->load->model('Common_model');
		$this->load->model('background_model');
        date_default_timezone_set('Asia/Kolkata'); 
		$data['header']=$this->admin_model->get_setting();
		//echo "<pre>";print_r($data);die;
		$is_comingsoon=$data['header'][18]['status'];
		$method=$this->router->fetch_method();
		if($is_comingsoon==1 && $method!='comingsoon'){
			redirect('user/comingsoon');
		}
	}
	public function index()
	{
		$lg = $this->session->userdata('site_lang');
		$lange= $this->admin_model->get_default_language();
		$language=$lange['status'];
		$value['header']=$this->admin_model->get_setting();
		$total=$this->admin_model->get_all();
		$total_count=count($total);
		$online=$this->admin_model->get_all_users();
		$online_count=count($online);
		$male=$this->admin_model->get_all_users(array('gender'=>'male'));
		$male_count=count($male);
		$female=$this->admin_model->get_all_users(array('gender'=>'female'));
		$female_count=count($female);
		$data['total_users']=  $total_count;
		$data['online_users']=  $online_count;
		$data['male_users']=  $male_count;
		$data['female_users']=  $female_count;
		$data['users']=$total;
		$data['blogs']=$this->blog_model->fetch_blogs();
		$data['testimonials']=$this->Testimonial_model->fetch_testimonial();
		$data['members']=$this->members_model->getMembers();
		$data['stories']=$this->story_model->fetch_stories();
		$data['blocks']=$this->blocks_model->fetch_block();
		$data['sliders']=$this->Slider_model->fetch_slider();
		$data['our_history'] = $this->blocks_model->get_fetch_block(array('block.id'=>12));
		$data['our_history_inner'] = $this->blocks_model->get_fetch_block(array('block.id'=>13));
		$data['why_choose_us'] =$this->blocks_model->get_fetch_block(array('block.id'=>15));
		$data['team_inner_box1'] =$this->blocks_model->get_fetch_block(array('block.id'=>7));
		$data['team_inner_box2'] = $this->blocks_model->get_fetch_block(array('block.id'=>8));
		$data['team_inner_box3'] = $this->blocks_model->get_fetch_block(array('block.id'=>9));
		$data['subscribe'] = $this->blocks_model->get_fetch_block(array('block.id'=>16));
		$data['footer_contact_us'] = $this->blocks_model->get_fetch_block(array('block.id'=>17));
		$data['our_team_member'] = $this->blocks_model->get_fetch_block(array('block.id'=>18));
		$data['homepage3_subscribe'] = $this->blocks_model->get_fetch_block(array('block.id'=>22));
		$lg=$this->lang->line('key');
		$value['val']=array(
			'lang'=>$lg,
		);
		/*echo"<pre>";
		print_r($value['header']);
		die;*/
		//if($value['header'][20]['status']==1){
		  $this->load->view('header',$value);			
		  $this->load->view('home',$data);
		  $this->load->view('footer',$value);
		/*}elseif($value['header'][20]['status']==2){
		  $this->load->view('header2',$value);		
		  $this->load->view('home2',$data);
		  $this->load->view('footer',$value);
		}else{
		  $this->load->view('header3',$value);			
		  $this->load->view('home3',$data);
		  $this->load->view('footer3',$value);
		}*/
	}
	/* Function Name fetch_gallery
	 * Returns Gallery detail 
	 * @param Number $user_id
	 * @return Array of Gallery Images detail of User
	 */
	public function fetch_gallery($user_id)
	{
		$logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }		
		return  $this->user_model->get_gallery_images(array("user_id "=>$user_id));
	}
	/* Function Name gallery
	 * Load The Gallery Page
	 */
 	public function gallery()
    {
		$data = new stdClass(); 
        $user_id = $this->session->userdata['user_id'];
        $data->userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
        $data->content = $this->fetch_gallery($user_id);
		$data->notification=$this->fetch_notification();
        $value['header']=$this->admin_model->get_setting();				
		$this->load->view('header',$value);      
		$this->load->view('user/dashboard/my_gallery',$data);
		$this->load->view('footer');
    }
	/* Function Name loginfb
	 * Function for Login with Facebook
	 */
	public function loginfb()
    {
		$this->load->helper('form');
		$this->load->library('form_validation');
		// set validation rules
		//$this->form_validation->set_rules('email', 'Email', 'trim|required');
			$name=$this->input->post("name");
            $fbid=$this->input->post("fbid"); 
		   $gender=$this->input->post("gender");
		   $email=$this->input->post("email");
		   $condition=array("fb_id"=>$fbid);
		   //Check for user already exists or not
		   $user=$this->user_model->get_user_where_row($condition);
		   if($user)
		   {
				// Login With facebook
				$_SESSION['user_id']      = (int)$user->id;
				$_SESSION['fname']      = (string)$user->fname;
				$_SESSION['lname']      = (string)$user->lname;
				$_SESSION['email']     = (string)$user->email;
				$_SESSION['logged_in']    = (bool)true;
				$_SESSION['is_confirmed'] = (bool)$user->is_confirmed;
				$_SESSION['is_admin']     = (bool)$user->is_admin;
				$_SESSION['profile_image']     = (string)$user->profile_image;
                echo "Login";
		   }
		   else
		   {
				// Register With facebook
				$n=explode(" ",$name);
				$pic = file_get_contents("https://graph.facebook.com/$fbid/picture?type=large");
				$filename = $fbid.".jpg";
				$path="uploads/".$filename;
				$path1="uploads/thumbnail/".$filename;
				file_put_contents($path, $pic);
				file_put_contents($path1, $pic);
				$data=array("fb_id"=>$fbid,"fname"=>$n[0],"lname"=>$n[1],"email"=>$email,"gender"=>$gender,"profile_image"=>$filename);
                $_SESSION['fb_id']      = $fbid;
                $_SESSION['fb_fname']      = $n[0];
                $_SESSION['fb_lname']      = $n[1];
                $_SESSION['fb_email']      = $email;
                $_SESSION['fb_gender']      = $gender;
                $_SESSION['fb_profile_image']      = $filename;
				echo "register";
		   }
    }
	/* Function Name block_user
	 * Function for block particular user
	 */
	public function block_user()
	{
		$user_id=$this->uri->segment(3);
		if ($this->user_model->blocked_user($user_id)) 
		{
			$user=$this->user_model->get_user($user_id);
			//$this->session->set_flashdata('success',lang('lbl_dashboard_success'));
            $this->session->set_flashdata('blocked_user', 'You have blocked '.$user->fname.' '.$user->lname);  
			redirect('User/dashboard/dashboard');
		}
	}
	public function report_user()
	{
		$user_id=$this->uri->segment(3);
		if ($this->user_model->report_user($user_id)) 
		{
			$user=$this->user_model->get_user($user_id);
			/*$this->session->set_flashdata('success',lang('lbl_search_report_success'));
			$this->session->set_flashdata('report_user', 'This profile reported to system.');*/
			echo $user->fname." ".$user->lname." ".lang('lbl_search_report');
			//redirect('User/dashboard/');
		}
	}
	/* Function Name terms
	 * Function for terms & Condition
	 * @param  file path $file
	 * @return Void
	 */
	public function terms()
	{
			$this->load->view('header');			
			$this->load->view('user/terms');
			$this->load->view('footer');
	}
	/* Function Name unlinkfile
	 * Function for deleteing image file from directory
	 * @param  file path $file
	 * @return Void
	 */
	public function unlinkfile($file=null)
	{
		if($file)
		{
			if(file_exists($file))
					unlink($file);
		}
	}
	public function galleryprofile()
	{
		$user_id=$this->session->userdata('user_id');
        $ispost=$this->input->method(TRUE);
		$image="img".$this->input->post("avatar_pos");
		if($this->input->post("avatar_pos"))
		{
			$position=$this->input->post("avatar_pos");
		}
		else
		{
			$position="";
		}
		$imgkey=$_FILES['avatar_file']['name'];
		//echo "<pre>";print_r($_POST);print_r($_FILES);die;
		$crop = new CropAvatar(
		  isset($_POST['avatar_src']) ? $_POST['avatar_src'] : null,
		  isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null,
		  isset($_FILES['avatar_file']) ? $_FILES['avatar_file'] : null,
		  'uploads/'
		);
		
		$response = array(
		  'state'  => 200,
		  'message' => $crop->getMsg(),
		  'result' => $crop->getResult()
		);
		//echo "<pre>";print_r($response);die;
		if($response['state']==200){

			$mainurl=$this->config->item('base_url');
			$img_url= $mainurl."".$response['result'];
			$filename = str_replace("uploads/","",$response['result']); 
			//copy file 
			copy("./uploads/".$filename,"./uploads/thumbnail/".$filename);
			$condition=array("user_id"=>$user_id,"img_key"=>$image);				
			if($image=="img1")
			{
				$condition=array("id"=>$user_id);
				$data=array("profile_image"=>$filename,"modified_date"=>date("Y-m-d H:i:s"));
				
				$user = $this->user_model->update_users($data,$condition);
				$_SESSION['profile_image']=$filename;
			}
			else
			{
				
				if($this->user_model->get_gallery_images($condition))
				{
					$data=array("img_url"=>$filename,"modified_date"=>date("Y-m-d H:i:s"),"imgposition"=>$position);
					$user = $this->user_model->change__gallery_images($data,$condition);
				}
				else
				{
					$data=array(
						"img_url"=>$filename,
						"user_id"=>$user_id,
						"img_key"=>$image,
						"imgposition"=>$position,
						"created_date"=>date("Y-m-d H:i:s"),
						"modified_date"=>date("Y-m-d H:i:s")
					);
					$user = $this->user_model->add_gallery_images($data);
				}
			}
			
			if($user)
			{
				$response = array(
				  'position'=> $position,
				  'state'  => 200,
				  'message' => $crop->getMsg(),
				  'result' => $img_url,
				);
			}
			else
			{
				$response = array(
				  'position'=> $position,
				  'state'  => 200,
				  'message' => $crop->getMsg(),
				  'result' => $crop->getResult()
				);
			}
		}
		else
		{
			$response = array(
				'position'=> $position,
				'state'  => 200,
				'message' => $crop->getMsg(),
				'result' => $crop->getResult()
			);
		}
		echo json_encode($response);
		die;
	}

	public function ajaxgalleryprofile()
	{
		$user_id=$this->session->userdata('user_id');
        $ispost=$this->input->method(TRUE);
		$image="img".$this->input->post("avatar_pos");
		if($this->input->post("avatar_pos"))
		{
			$position=$this->input->post("avatar_pos");
		}
		else
		{
			$position="";
		}
		$imgkey=$_FILES['avatar_file']['name'];
		//echo "<pre>";print_r($_POST);print_r($_FILES);die;
		$crop = new CropAvatar(
		  isset($_POST['avatar_src']) ? $_POST['avatar_src'] : null,
		  isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null,
		  isset($_FILES['avatar_file']) ? $_FILES['avatar_file'] : null,
		  'uploads/thumbnail/'
		);
		
		$response = array(
		  'state'  => 200,
		  'message' => $crop->getMsg(),
		  'result' => $crop->getResult()
		);
		//echo "<pre>";print_r($response);die;
		if($response['state']==200){

			$mainurl=$this->config->item('base_url');
			$img_url= $mainurl."".$response['result'];
			$filename = str_replace("uploads/thumbnail/","",$response['result']); 
			$condition=array("user_id"=>$user_id,"img_key"=>$image);				
			if($image=="img1")
			{
				$condition=array("id"=>$user_id);
				$data=array("profile_image"=>$filename,"modified_date"=>date("Y-m-d H:i:s"));
				/*$data1=$this->user_model->get_user($user_id);
				if(!empty($data1->profile_image))
				{	 
					$unlinkimg=$data1->profile_image;
					$this->unlinkfile(DIRECTORY_PATH."uploads/thumbnail/".$unlinkimg);
					$this->unlinkfile(DIRECTORY_PATH."uploads/".$unlinkimg);
				}*/
				$user = $this->user_model->update_users($data,$condition);
				$_SESSION['profile_image']=$imgkey;
			}
			else
			{
				
				if($this->user_model->get_gallery_images($condition))
				{
					$data=array("img_url"=>$filename,"modified_date"=>date("Y-m-d H:i:s"),"imgposition"=>$position);
					$user = $this->user_model->change__gallery_images($data,$condition);
				}
				else
				{
					$data=array(
						"img_url"=>$filename,
						"user_id"=>$user_id,
						"img_key"=>$image,
						"imgposition"=>$position,
						"created_date"=>date("Y-m-d H:i:s"),
						"modified_date"=>date("Y-m-d H:i:s")
					);
					$user = $this->user_model->add_gallery_images($data);
				}
			}
			
			if($user)
			{
				$response = array(
				  'state'  => 200,
				  'message' => $crop->getMsg(),
				  'result' => $img_url,
				);
			}
			else
			{
				$response = array(
				  'state'  => 200,
				  'message' => $crop->getMsg(),
				  'result' => $crop->getResult()
				);
			}
		}
		else
		{
			$response = array(
				'state'  => 200,
				'message' => $crop->getMsg(),
				'result' => $crop->getResult()
			);
		}
		echo json_encode($response);
		die;
	}
	/* Function Name edit_gallery_image
	 * Function for Change Gallery Image
	 * @return Void
	 */
	public function edit_gallery_image()
    {
		try
		{
			$logged_in_user = $this->session->userdata('user_id');
			if (!$logged_in_user)
			{
				redirect('login');
			}
			$imgkey=$_FILES['file']['name'];
			$image="img".$this->input->post("position");
			$user_id = $this->session->userdata['user_id'];
			$config['upload_path']= './uploads/';        
			$config['allowed_types']= 'gif|jpg|png|jpeg';
			$config['overwrite'] = TRUE;
			$fname = $user_id."_".uniqid();
			$config['file_name'] = $fname;
			$this->load->library('upload');
			$this->upload->initialize($config);
			$image_info = getimagesize($_FILES["file"]["tmp_name"]);
			$image_width = $image_info[0];
			$image_height = $image_info[1];
			/*echo($image_width."<br/>");
			echo($image_height);die;*/
			if($this->input->post("position"))
			{
				$position=$this->input->post("position");
			}
			else
			{
				$position="";
			}
			if ( ! $this->upload->do_upload('file'))
			{ 								
				$data['error'] = $this->upload->display_errors();  
				$this->session->set_flashdata('error', lang('lbl_gallery_error_img_update'));
				$user_id = $this->session->userdata['user_id'];
				$data["userdetail"]=$this->user_model->get_user_where_row(array("id "=>$user_id));
				$data['content'] = $this->user_model->get_gallery_images(array("user_id "=>$user_id));
				$this->load->view('header');
				$this->load->view('sidebar');
				$this->load->view('user/register/reg_image', $data);
				$this->load->view('footer');  
				//redirect("user/gallery");				
			}
			else
			{  
				$condition=array("user_id"=>$user_id,"img_key"=>$image);
				$file_data = $this->upload->data();
				$image_detail= $this->user_model->get_gallery_images($condition);
				$unlinkimg="";
				// delete old Image
				if(!empty($image_detail[0]["img_url"]))
				{	 
					$unlinkimg=$image_detail[0]["img_url"];
					$this->unlinkfile(DIRECTORY_PATH."uploads/thumbnail/".$unlinkimg);
					$this->unlinkfile(DIRECTORY_PATH."uploads/".$unlinkimg);
				}
				//create image thumbnail
				$source_path = DIRECTORY_PATH.'uploads/'.$file_data["file_name"];
				$target_path = DIRECTORY_PATH.'uploads/thumbnail/'.$file_data["file_name"];
				$thumb = PhpThumbFactory::create($source_path);
				$thumb->adaptiveResize(280, 250);
				$thumb->save($target_path, 'jpg');
				if($image=="img1")
				{
					$condition=array("id"=>$user_id);
					$data=array("profile_image"=>$file_data["file_name"],"modified_date"=>date("Y-m-d H:i:s"));
					$data1=$this->user_model->get_user($user_id);
					if(!empty($data1->profile_image))
					{	 
						$unlinkimg=$data1->profile_image;
						$this->unlinkfile(DIRECTORY_PATH."uploads/thumbnail/".$unlinkimg);
						$this->unlinkfile(DIRECTORY_PATH."uploads/".$unlinkimg);
					}
					$this->user_model->update_users($data,$condition);
					$_SESSION['profile_image']=$file_data["file_name"];
					//redirect('user/gallery');
				}
				else
				{
					if($this->user_model->get_gallery_images($condition))
					{
						$data=array("img_url"=>$file_data["file_name"],"modified_date"=>date("Y-m-d H:i:s"),"imgposition"=>$position);
						$this->user_model->change__gallery_images($data,$condition);   
					}
					else
					{
						$data=array(
							"img_url"=>$file_data["file_name"],
							"user_id"=>$user_id,
							"img_key"=>$image,
							"imgposition"=>$position,
							"created_date"=>date("Y-m-d H:i:s"),
							"modified_date"=>date("Y-m-d H:i:s"));
						$this->user_model->add_gallery_images($data);
					}
				}
				$this->session->set_flashdata('success', lang('lbl_gallery_updatee_success'));
			}
        }
		catch(Exception $e)
		{
			$this->SiteErrorLog->addData("Edit Gallery Error :- ".$e->getMessage()."Invalid Image",$user_id);
			$this->session->set_flashdata('error', lang('lbl_gallery_error_update'));
		}
		//redirect("user/gallery");		
    }
	//Remove Profile Image Of User
    public function remove_profile_image($id)
    {
		try
		{
			$logged_in_user = $this->session->userdata('logged_in');
			$user_id = $this->session->userdata['user_id'];
			if (!$logged_in_user)
			{
				redirect('login');
			}
			$user=$this->user_model->get_user_where_row(array("id "=>$id));
			$condition=array("id"=>$id);
			$data=array("profile_image"=>"");
			$result=$this->user_model->update_users($data,$condition);
			if($result)
			{
				$this->unlinkfile(DIRECTORY_PATH."uploads/thumbnail/".$user->profile_image);
				$this->unlinkfile(DIRECTORY_PATH."uploads/".$user->profile_image);
				unset($_SESSION['profile_image']);			
			}
			else
			{
				throw new Exception('Image isn\'t Removed');
			}
			$this->session->set_flashdata('success', lang('lbl_gallery_remove_success'));
		}
		catch(Exception $e)
		{
			$this->SiteErrorLog->addData($e->getMessage()."Image isn't Removed",$user_id);
			$this->session->set_flashdata('error', lang('lbl_gallery_error_profile'));
		}redirect("user/gallery");
    } 
	// Remove Gallery Image Of User
    public function register_remove_gallery_image($id)
    {
		try
		{
			$logged_in_user = $this->session->userdata('logged_in');
			if (!$logged_in_user)
			{
				redirect('login');
			}
			$user_id = $this->session->userdata['user_id'];
			$condition=array("id"=>$id);
			$image_detail= $this->user_model->get_gallery_images($condition);
			$unlinkimg="";
			// delete old Image
			if(!empty($image_detail[0]["img_url"]))
			{
				$unlinkimg=$image_detail[0]["img_url"];
				$this->unlinkfile(DIRECTORY_PATH."uploads/thumbnail/".$unlinkimg);
				$this->unlinkfile(DIRECTORY_PATH."uploads/".$unlinkimg);
			}
			$data=array("img_url"=>"","modified_date"=>date("Y-m-d H:i:s"));
			$this->user_model->change__gallery_images($data,$condition); 
			$this->session->set_flashdata('success', lang('lbl_gallery_remove_success'));
		}
		catch(Exception $e)
		{
			$this->SiteErrorLog->addData("Remove Gallery Image Error :- ".$e->getMessage(),$user_id);
			$this->session->set_flashdata('error', lang('lbl_gallery_error_gallery'));
		}
		redirect("user/reg_image");
	}
	// Remove Gallery Image Of User
	public function ajax_remove_gallery_image()
    {
		$imgposition = $this->input->post('position');
		try
		{
			$logged_in_user = $this->session->userdata('logged_in');
			if (!$logged_in_user)
			{
				redirect('login');
			}
			$user_id = $this->session->userdata['user_id'];
			$condition=array("imgposition"=>$imgposition);
			$image_detail= $this->user_model->get_gallery_images($condition);
			$unlinkimg="";
			// delete old Image
			if(!empty($image_detail[0]["img_url"]))
			{
				$unlinkimg=$image_detail[0]["img_url"];
				$this->unlinkfile(DIRECTORY_PATH."uploads/thumbnail/".$unlinkimg);
				$this->unlinkfile(DIRECTORY_PATH."uploads/".$unlinkimg);
			}
			$data=array("img_url"=>"","modified_date"=>date("Y-m-d H:i:s"));
			$this->user_model->change__gallery_images($data,$condition); 
		}
		catch(Exception $e)
		{
			$this->SiteErrorLog->addData("Remove Gallery Image Error :- ".$e->getMessage(),$user_id);
		}
		if($imgposition == 2){
			$url = base_url('images/step/02.png');
		}
		if($imgposition == 3){
			$url = base_url('images/step/03.png');
		}
		if($imgposition == 4){
			$url = base_url('images/step/04.png');
		}
		if($imgposition == 5){
			$url = base_url('images/step/05.png');
		}
		if($imgposition == 6){
			$url = base_url('images/step/06.png');
		}
		$response = array(
			'url'=>$url
		);
		echo json_encode($response);
		die;
		
    }
    public function remove_gallery_image($id)
    {
		try
		{
			$logged_in_user = $this->session->userdata('logged_in');
			if (!$logged_in_user)
			{
				redirect('login');
			}
			$user_id = $this->session->userdata['user_id'];
			$condition=array("id"=>$id);
			$image_detail= $this->user_model->get_gallery_images($condition);
			$unlinkimg="";
			// delete old Image
			if(!empty($image_detail[0]["img_url"]))
			{
				$unlinkimg=$image_detail[0]["img_url"];
				$this->unlinkfile(DIRECTORY_PATH."uploads/thumbnail/".$unlinkimg);
				$this->unlinkfile(DIRECTORY_PATH."uploads/".$unlinkimg);
			}
			$data=array("img_url"=>"","modified_date"=>date("Y-m-d H:i:s"));
			$this->user_model->change__gallery_images($data,$condition); 
			$this->session->set_flashdata('success', lang('lbl_gallery_remove_success'));
		}
		catch(Exception $e)
		{
			$this->SiteErrorLog->addData("Remove Gallery Image Error :- ".$e->getMessage(),$user_id);
			$this->session->set_flashdata('error', lang('lbl_gallery_error_gallery'));
		}
		redirect("user/gallery");
    }
	/**
	 * Function Name:captcha_validation.
	 * Its validating the Ceptcha Code 
	 */
	public function captcha_validation()
	{
		if(!empty($_POST))
		{
			try
			{	
				$data = array(
						'secret' => GOOGLE_SECRAT_KEY,
						'response' => $this->input->post("g-recaptcha-response"));
				$verify = curl_init();
				curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
				curl_setopt($verify, CURLOPT_POST, true);
				curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
				curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
				$response = curl_exec($verify);
				$check = json_decode($response);
				$this->form_validation->set_message('captcha_validation', lang('lbl_log_in_error_ceptcha'));
				if(!$check->success)
				{
					$error_array=(array)$check;
				 	if(in_array("invalid-input-secret",$error_array["error-codes"]) or in_array("missing-input-secret",$error_array["error-codes"]))
					{
						$error=implode(",",$error_array["error-codes"]);
						throw new Exception();
					}
				}
				return $check->success;
			}
			catch(Exception $e)
			{
				$this->SiteErrorLog->addData(lang('lbl_log_in_error_invalid_ceptcha'));
				return false;
			}
		}
	}
	public function reg_step2()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		// set validation rules
		$this->form_validation->set_rules('gender', lang('lbl_register_gender'), 'required');
		$this->form_validation->set_rules('day', lang('lbl_register_birthdate'), 'required|max_length[2]|callback_dob_validation');
		$this->form_validation->set_rules('month', lang('lbl_register_birthdate'), 'required|max_length[2]');
		$this->form_validation->set_rules('year', lang('lbl_register_birthdate'), 'required|max_length[4]');
		$this->form_validation->set_rules('about', lang('lbl_register_about'), 'trim|required|max_length[500]');
		$this->form_validation->set_rules('ethnicity', lang('lbl_register_ehtnticity'), 'required');
		$this->form_validation->set_rules('religion', lang('lbl_register_religion'), 'required');	
		$this->form_validation->set_rules('height', lang('lbl_register_height'), 'trim|required');				
		$this->form_validation->set_rules('kids', lang('lbl_register_kids'), 'trim|required');
		$this->form_validation->set_rules('address', lang('lbl_register_address'), 'trim|required');
		$this->form_validation->set_message('dob_validation', lang('lbl_profile_error_age'));

		if ($this->form_validation->run() === false) 
		{
			$data = new stdClass();
			$user_id = $this->session->userdata['user_id'];
			$data->religion = $this->user_model->get_all_religion(); 
			$data->ethnicity = $this->user_model->get_all_ethnicity();
			$data->datepreference = $this->user_model->get_all_datepreference();
			$data->questions = $this->user_model->get_all_questions();
			$data->userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
	    	$data->googleapiskey = $this->user_model->get_googleapiskey();
			$value['header']=$this->admin_model->get_setting();
			$this->load->view('header',$value);
			$this->load->view('user/register/reg_contact', $data);
			$this->load->view('footer');
		}
		else
		{
			$user_id = $this->session->userdata['user_id'];
			$address = $this->input->post('address');     
			$latitude =0;
            $longitude=0;
            $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key='.GOOGLE_API_KEY.'&address='.urlencode($address).'&sensor=true');
            // Convert the JSON to an array
            $geo = json_decode($geo, true);
            if ($geo['status'] == 'OK') 
			{
              // Get Lat & Long
				$latitude = $geo['results'][0]['geometry']['location']['lat'];
				$longitude = $geo['results'][0]['geometry']['location']['lng'];
            } 
			$birthdate=$this->input->post('year')."-".$this->input->post('month')."-".$this->input->post('day');
			//$religion=implode(",",$this->input->post('religion'));
			$data=array(
				'gender'=>$this->input->post('gender'),
				'dob'=>$birthdate,
				'about'=>strip_tags($this->input->post('about')),
				'ethnicity'=>$this->input->post('ethnicity'),
				'religion'=>$this->input->post('religion'),
				'height'=>$this->input->post('height'),
				'kids'=>$this->input->post('kids'),
				'address'=>$this->input->post('address'),
				'location_lat'=>$latitude,
				'location_long'=>$longitude				
			);
			$user=$this->user_model->update_profile(array('id'=>$user_id),$data);
			if($user)
			{
				redirect('user/reg_preferance');
			}
			else
			{
				$this->session->set_flashdata('error','Record is Not inserted');
	    		$data['googleapiskey'] = $this->user_model->get_googleapiskey();
				$value['header']=$this->admin_model->get_setting();
				$this->load->view('header',$value);
				$this->load->view("user/register/reg_contact",$data);
				$this->load->view('footer');
			}
		}		
	}
	/**
	 * Function Name:Register.
	 * It is used to register the user 
	 * @return void
	 */	
    public function register() 
	{
		$step1 = $this->session->userdata('step1');
		if ($step1)
		{
			redirect('user/reg_image');
		}
		//add js file 
		$value['Additional_JS']=array(
			'http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js',
			base_url('Newassets/js/register.js'),
		);

		// load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');
		// set validation rules
		$this->form_validation->set_rules('fname', lang('lbl_register_firstname'), 'trim|required|max_length[50]|alpha',array("max_length"=>lang('lbl_validation_error_max_firstname'),
			"alpha"=>lang('lbl_validation_error_alpha_firstname')));
		$this->form_validation->set_rules('lname', lang('lbl_register_lastname'), 'trim|required|max_length[50]|alpha',array("max_length"=>lang('lbl_validation_error_max_lastname'),
			"alpha"=>lang('lbl_validation_error_alpha_lastname')));
        //$this->form_validation->set_rules('username', lang('lbl_register_username'), 'trim|required|alpha_numeric|min_length[4]|is_unique[users.username]', array('is_unique' => 'This username already exists. Please choose another one.'));
		$this->form_validation->set_rules('email', lang('lbl_register_email'), 'trim|required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', lang('lbl_register_password'), 'trim|required|min_length[6]');
		$this->form_validation->set_rules('password_confirm', lang('lbl_register_confirm_password'), 'trim|required|min_length[6]|matches[password]');
		$this->form_validation->set_rules('education', lang('lbl_register_education'), 'trim|required');
		$this->form_validation->set_rules('profession', lang('lbl_register_profession'), 'trim|required');
		// create the data object
		if ($this->form_validation->run() === false) 
		{	
			$value['header']=$this->admin_model->get_setting();				
			$this->load->view('header',$value);	
			$this->load->view('user/register/register');
			$this->load->view('footer');
		} 
		else 
		{	
			$id=$this->user_model->create_user($this->input->post());
			if ($id) 
			{
				// user creation ok
				$this->session->set_flashdata('register_success','Registration completed successfully.');
				$ids=$id;	
				$_SESSION['user_id']      = (int)$ids;
				$_SESSION['logged_in']    = (bool)true;
				$_SESSION['fname']      = (string)$this->input->post('fname');
				$_SESSION['lname']      = (string)$this->input->post('lname');
				$_SESSION['step1']      = (bool)true;
				$this->XMPP_ENABLE = $this->Common_model->get_key_configuration(array('key'=>'XMPP_ENABLE'));
				if($this->XMPP_ENABLE == "true"){
					//register user
					$ejuser=$this->Ejabberd_model->register($id);
				}
				else{
					$ejuser=$data->userdetail->ejuser;
				}
				unset($_SESSION['fb_id']);
				redirect('user/reg_image');
			} 
			else 
			{
				// user creation failed, this should never happen
				$data->error = lang('lbl_register_error');
				$value['header']=$this->admin_model->get_setting();		

				$this->load->view('header',$value);				
				$this->load->view('user/register/register', $data);
				$this->load->view('footer');
			}
		}
	}
    public function registerfb() 
	{
		// load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');
		// set validation rules
		$this->form_validation->set_rules('fname', lang('lbl_register_firstname'), 'trim|required|max_length[50]|alpha',array("max_length"=>lang('lbl_validation_error_max_firstname'),
			"alpha"=>lang('lbl_validation_error_alpha_firstname')));
		$this->form_validation->set_rules('lname', lang('lbl_register_lastname'), 'trim|required|max_length[50]|alpha',array("max_length"=>lang('lbl_validation_error_max_lastname'),
			"alpha"=>lang('lbl_validation_error_alpha_lastname')));
        $this->form_validation->set_rules('username', lang('lbl_register_username'), 'trim|required|alpha_numeric|min_length[4]|is_unique[users.username]', array('is_unique' => 'This username already exists. Please choose another one.'));
		$this->form_validation->set_rules('email', lang('lbl_register_email'), 'trim|required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', lang('lbl_register_password'), 'trim|required|min_length[6]');
		$this->form_validation->set_rules('password_confirm', lang('lbl_register_confirm_password'), 'trim|required|min_length[6]|matches[password]');
		$this->form_validation->set_rules('education', lang('lbl_register_education'), 'trim|required');
		$this->form_validation->set_rules('profession', lang('lbl_register_profession'), 'trim|required');
		// create the data object
		if ($this->form_validation->run() === false) 
		{	
			$value['header']=$this->admin_model->get_setting();				
			$this->load->view('header',$value);	
			$this->load->view('user/register/registerfb');
			$this->load->view('footer');
		} 
		else 
		{			
			$id=$this->user_model->create_user($this->input->post());
			if ($id) 
			{
				// user creation ok
				$ids=$this->db->insert_id();
				$_SESSION['user_id']      = (int)$ids;
				$_SESSION['logged_in']    = (bool)true;
				$_SESSION['fname']      = (string)$this->input->post('fname');
				$_SESSION['lname']      = (string)$this->input->post('lname');
				redirect('user/reg_image');
			} 
			else 
			{
				// user creation failed, this should never happen
				$data->error = lang('lbl_register_error');
				// send error to the view
				$value['header']=$this->admin_model->get_setting();				
				$this->load->view('header',$value);				
				$this->load->view('user/register/registerfb', $data);
				$this->load->view('footer');
			}
		}
	}
	public function reg_step3()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		// set validation rules
		//$this->form_validation->set_rules('ethnicity[]', lang('lbl_register_ehtnticity'), 'required');
		$this->form_validation->set_rules('question-ans', lang('lbl_register_question_answer'), 'trim|required|max_length[100]');
		if ($this->form_validation->run() === false) 
		{
			$user_id = $this->session->userdata['user_id'];
			$data = new stdClass();
			$data->ethnicity = $this->user_model->get_all_ethnicity();
	        $data->datepreference = $this->user_model->get_all_datepreference();
	        $data->questions = $this->user_model->get_all_questions();
			$data->userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
			$value['header']=$this->admin_model->get_setting();
		    $this->load->view('header',$value);
			$this->load->view("user/register/reg_preferance",$data);
			$this->load->view('footer');
		}
		else
		{
			$user_id = $this->session->userdata['user_id'];
			//$ethnicity=implode(",",$this->input->post('ethnicity'));
			$data=array(
				'gender_pref'=>$this->input->post('gender_pref'),
				'date_pref'=>$this->input->post('datepref'),
				//'ethnicity'=>$ethnicity,
				'que_id'=>$this->input->post('question'),
				'que_ans'=>$this->input->post('question-ans'),
				'max_dist_pref'=>$this->input->post('dist-max'),
				'min_dist_pref'=>$this->input->post('dist-min'),
				'max_age_pref'=>$this->input->post('age-max'),
				'min_age_pref'=>$this->input->post('age-min'),
				'access_location'=>$this->input->post('access_loc')			
			);
			$user=$this->user_model->update_profile(array('id'=>$user_id),$data);
			if($user)
			{
				redirect('user/login');
			}
			else
			{
				$this->sessioon->set_flashdata('error','Record is Not inserted');
				$value['header']=$this->admin_model->get_setting();
				$this->load->view('header',$value);
				$this->load->view("user/register/reg_prefreance",$data);
				$this->load->view('footer');
			}
		}		
	}
    public function reg_image()
    {
		/*$step2 = $this->session->userdata('step2');
		if ($step2)
		{
			redirect('user/reg_contact');
		}*/
		$data = new stdClass(); 
		$user_id = $this->session->userdata['user_id'];
		$this->form_validation->set_rules('imgupload1','please select the image', 'trim|required');
		if ($this->form_validation->run() === false) 
		{
			$_SESSION['step2']      = (bool)true;
			$data->userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
			$data->content = $this->fetch_gallery($user_id);
			$value['header']=$this->admin_model->get_setting();
			$this->load->view('header',$value);
			$this->load->view("user/register/reg_image",$data);
			$this->load->view('footer');  
		}
		else
		{
			$data->userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
			$value['header']=$this->admin_model->get_setting();
			$this->load->view('header',$value);
			$this->load->view("user/register/reg_image",$data);
			$this->load->view('footer');
		}
    }
	public function my_profile()
	{		
		try
		{
			if (!$this->session->userdata['user_id'])
			{
				redirect('login');
			}			
			// validators
			// load form helper and validation library
			$this->load->helper('form');
			$this->load->library('form_validation');
			// set validation rules
			/*array("required"=>lang('lbl_register_error_day'),"numeric"=>lang('lbl_register_error_day_number'),"dob_validation"=>lang('lbl_validation_error_day'))*/
			
			$this->form_validation->set_rules('fname', lang('lbl_register_firstname'), 'required|max_length[50]|alpha',array("max_length"=>lang('lbl_validation_error_max_firstname'),
			"alpha"=>lang('lbl_validation_error_alpha_firstname')));
			$this->form_validation->set_rules('lname', lang('lbl_register_lastname'), 'required|max_length[50]|alpha',array(
					"max_length"=>lang('lbl_validation_error_max_lastname'),
					"alpha"=>lang('lbl_validation_error_alpha_lastname')
				)
			);
			$this->form_validation->set_rules('day', lang('lbl_register_error_day'), 'required|max_length[2]|numeric',array(
					"required"=>lang('lbl_validation_error_day'),
					"numeric"=>lang('lbl_register_error_day_number'),
					"max_length"=>lang('lbl_validation_error_day')
				)
			);
			$this->form_validation->set_rules('month', lang('lbl_register_error_month'), 
				'required|max_length[2]|numeric',
				array(
					"required"=>lang('lbl_validation_error_month'),
					"numeric"=>lang('lbl_register_error_month_number'),
					"max_length"=>lang('lbl_validation_error_month')
				)
			);
			$this->form_validation->set_rules('year', lang('lbl_register_error_year'),'required|numeric|max_length[4]|callback_valid_dob|callback_dob_validation',
				array(
					"required"=>lang('lbl_validation_error_year'),
					"numeric"=>lang('lbl_register_error_year_number'),
					"dob_validation"=>lang('lbl_profile_error_age'),
					"max_length"=>lang('lbl_validation_error_year')
				)
			);
			$this->form_validation->set_rules('about', lang('lbl_profile_about'), 'trim|required|max_length[500]');
			$this->form_validation->set_rules('height', lang('lbl_profile_height'), 'trim|required');
			$this->form_validation->set_rules('ethnicity', lang('lbl_profile_ehtnticity'), 'required');
			$this->form_validation->set_rules('religion', lang('lbl_profile_religion'), 'required');
			$this->form_validation->set_rules('address', lang('lbl_profile_address'), 'trim|required');
			$this->form_validation->set_rules('education', lang('lbl_profile_education'), 'trim|required');
			$this->form_validation->set_rules('profession', lang('lbl_profile_profession'), 'trim|required');
			$this->form_validation->set_rules('gender', lang('lbl_profile_gender'), 'required');
			//$this->form_validation->set_message('dob_validation', lang('lbl_register_error_dob'));
			$this->form_validation->set_message('valid_dob', lang('lbl_register_error_dob'));
			$email=$this->input->post("email");
			$id=$this->input->post("id");
			$user=$this->user_model->get_user($id);
			if($email!=$user->email){
				if(!empty($this->user_model->get_user_by_con(array("email"=>$email)))) {
					$this->form_validation->set_rules('email', lang('lbl_register_email'), 'is_unique[users.email]');
				}
				else{
					$this->form_validation->set_rules('email', lang('lbl_register_email'), 'trim|required|valid_email');
				}
			}
			//$this->form_validation->set_message('uniqemail',lang('lbl_validation_error_email'));
			if ($this->form_validation->run() === TRUE)
			{
				$dob=$_POST["year"]."-".$_POST["month"]."-".$_POST["day"];
				$address = $_POST['address'];            
                $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key='.GOOGLE_API_KEY.'&address='.urlencode($address).'&sensor=true');
				// Convert the JSON to an array
				$geo = json_decode($geo, true);
				if ($geo['status'] == 'OK') 
				{
				  // Get Lat & Long
					$latitude = $geo['results'][0]['geometry']['location']['lat'];
					$longitude = $geo['results'][0]['geometry']['location']['lng'];
				} 
				/*if(!empty($_POST["religion"]))
				{
					$religion=$_POST["religion"];
					$religions=implode(",",$religion);
				}
				if(!empty($_POST["ethnicity"]))
				{
					$ethnicity=$_POST["ethnicity"];
					$ethnicities=implode(",",$ethnicity);
				}*/	
				$data=array(
					'fname'=>$this->input->post('fname'),
					'lname'=>$this->input->post('lname'),
					'email'=>$this->input->post('email'),
					'dob'=>$dob,
					'ethnicity'=>$this->input->post('ethnicity'),
					'religion'=>$this->input->post('religion'),
					'about'=>strip_tags($this->input->post('about')),
					'gender'=>$this->input->post('gender'),
					'height'=>$this->input->post('height'),
					'address'=>$this->input->post('address'),
					'education'=>$this->input->post('education'),
					'profession'=>$this->input->post('profession'),
					'location_lat'=>$latitude,
					'location_long'=>$longitude,				
				);
				$user= $this->user_model->update_profile(array('id'=>$_POST["id"]),$data);
				$user_id = $this->user_model->get_user_id_from_email($email);
				$user    = $this->user_model->get_user($user_id);
				// set session user datas
				$_SESSION['user_id']      = (int)$user->id;
				$_SESSION['fname']      = (string)$user->fname;
				$_SESSION['lname']      = (string)$user->lname;
				$_SESSION['email']     = (string)$user->email;
				$_SESSION['logged_in']    = (bool)true;
				$_SESSION['is_confirmed'] = (bool)$user->is_confirmed;
				$_SESSION['is_admin']     = (bool)$user->is_admin;
				$_SESSION['profile_image']     = (string)$user->profile_image;
				$data  = $user;
				$this->session->set_flashdata('success', lang('lbl_profile_update_success'));
				// user login ok
				redirect('user/my_profile');
			}
			else
			{
			}
			$user_id = $this->session->userdata['user_id'];			
			$data['userdetail'] = $this->user_model->get_user($user_id);
			$data['ethnicity'] = $this->user_model->get_all_ethnicity();
			$data['religion'] = $this->user_model->get_all_religion();
			$value['header']=$this->admin_model->get_setting();
		}
		catch(Exception $e)
		{
			$this->session->set_flashdata('error', lang('lbl_profile_error_update').$e->getMessage());
		}
        $this->load->view('header',$value);
		$this->load->view("user/dashboard/my_profile",$data);
		$this->load->view('footer');
	}
	public function my_preferance()
	{
		$logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }	
		try
		{
			if (!$this->session->userdata['user_id'])
			{
				redirect('login');
			}			
			// validators
			// load form helper and validation library
			$this->load->helper('form');
			$this->load->library('form_validation');
			// set validation rules
			// $this->form_validation->set_rules('kids', lang('lbl_profile_kids'), 'trim|required');
			//$this->form_validation->set_rules('religion[]', lang('lbl_profile_religion'), 'required');
			//$this->form_validation->set_rules('ethnicity[]', lang('lbl_profile_ehtnticity'), 'required');
			$this->form_validation->set_rules('gender_pref',  lang('lbl_profile_interested_in'), 'trim|required');
			$this->form_validation->set_rules('question_ans', lang('lbl_profile_error_question_answer'), 'trim|required|max_length[100]');
			$this->form_validation->set_rules('access_loc', lang('lbl_register_firstname'), 'trim|required');
			if ($this->form_validation->run() === TRUE)
			{
				if(!empty($_POST["ethnicity"]))
				{
					$ethnicity=$_POST["ethnicity"];
					$ethnicities=implode(",",$ethnicity);
				}else{
					$ethnicity=0;
					$ethnicities=implode(",",$ethnicity);
				}
				if(!empty($_POST["religion"]))
				{
					$religion=$_POST["religion"];
					$religions=implode(",",$religion);
				}else{
					$religion=0;
					$religions=implode(",",$religion);
				}		
				$data=array(
					'gender_pref'=>$this->input->post('gender_pref'),
					'date_pref'=>$this->input->post('datepref'),
					'min_age_pref'=>$this->input->post('age-min'),
					'max_age_pref'=>$this->input->post('age-max'),
					'min_dist_pref'=>$this->input->post('dist-min'),
					'max_dist_pref'=>$this->input->post('dist-max'),
					'religion_pref'=>$religions,
					'ethnicity_pref'=>$ethnicities,
					'que_id'=>$this->input->post('question'),
					'que_ans'=>$this->input->post('question_ans'),
					'access_location'=>$this->input->post('access_loc')
				);
				$user= $this->user_model->update_profile(array('id'=>$_POST["id"]),$data);
				$this->session->set_flashdata('success', lang('lbl_profile_update_success'));
			}
			else
			{
				$this->session->set_flashdata('error', lang('lbl_profile_update_error'));
			}
			$user_id = $this->session->userdata['user_id'];
			$data['content'] = $this->user_model->get_user($user_id);
			$data['religion'] = $this->user_model->get_all_religion();
			$data['ethnicity'] = $this->user_model->get_all_ethnicity();
			$data['datepref'] = $this->user_model->get_new_datepreference($data['content']->date_pref);
			$data['datepreference'] = $this->user_model->get_all_datepreference();
			$data['questions'] = $this->user_model->get_all_questions();
			$value['header']=$this->admin_model->get_setting();
			$data['JS_Middle']=array(
				base_url('Newassets/js/mypreference.js'),
			);
		}
		catch(Exception $e)
		{
			$this->session->set_flashdata('error', lang('lbl_profile_error_update').$e->getMessage());
		}
			$this->load->view('header',$value);
			$this->load->view("user/dashboard/my_preferance",$data);
			$this->load->view('footer');
	}
	public function reg_contact()
	{
		$step3 = $this->session->userdata('step3');
		if ($step3)
		{
			redirect('user/reg_step2');
		}
		$data = new stdClass();
		$user_id = $this->session->userdata['user_id'];
		$data->userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
		$data->content = $this->fetch_gallery($user_id);
		if(empty($data->userdetail->profile_image))
		{
			$this->session->set_flashdata('fail','At least Profile image must be uploaded');
			redirect('user/reg_image');
		}
		$_SESSION['step3']      = (bool)true;
        $data->religion = $this->user_model->get_all_religion();
		$data->ethnicity = $this->user_model->get_all_ethnicity();
		$data->userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
		$value['header']=$this->admin_model->get_setting();
	    $this->load->view('header',$value);
		$this->load->view("user/register/reg_contact",$data);
		$this->load->view('footer');
	}
	public function reg_preferance()
	{
		$user_id = $this->session->userdata['user_id'];
		$data = new stdClass();
		$data->ethnicity = $this->user_model->get_all_ethnicity();
        $data->datepreference = $this->user_model->get_all_datepreference();
        $data->questions = $this->user_model->get_all_questions();
		$data->userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
		$value['header']=$this->admin_model->get_setting();
		$footer['JS_Middle']=array( base_url('Newassets/js/mypreference.js'));	
	    $this->load->view('header',$value);
		$this->load->view("user/register/reg_preferance",$data);
		$this->load->view('footer',$footer);
	}
	/**
	 * login function.
	 * 
	 * @access public
	 * @return void
	 */
	public function login() 
	{
		// load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');
		//echo "<pre>";print_r($_POST);
		if ($this->session->userdata('logged_in'))
        {
            redirect('search');
		}
		//add js file 
		$value['Additional_JS']=array(
			'http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js',
			base_url('Newassets/js/login.js'),
		);
        // create the data object
		$data = new stdClass();
		
		// set validation rules
		$this->form_validation->set_rules('username', lang('lbl_log_in_email'), 'required');
		$this->form_validation->set_rules('password', lang('lbl_log_in_password'), 'required');
		if(CAPTCHA_ENABLE)
		{ 
			$this->form_validation->set_rules('login', lang('lbl_register_error_ceptcha'), 'callback_captcha_validation');
		}
		if ($this->form_validation->run() == false) 
		{
			// validation not ok, send validation errors to the view
			$data=array("body_class"=>"main-bg");
			$lang['languages']=$this->fetch_language();
			$value['header']=$this->admin_model->get_setting();	
			$this->load->view('header',$value);
			$this->load->view('user/login/login',$lang);
			$this->load->view('footer');
		} 
		else 
		{
			// set variables from the form
			$email = $this->input->post('username');
			$password = $this->input->post('password');
			if ($this->user_model->resolve_user_login($email, $password)) 
			{
				$user_id = $this->user_model->get_user_id_from_email($email);
				$user    = $this->user_model->get_user($user_id);
				// set session user datas
				$_SESSION['user_id']      = (int)$user->id;
				$_SESSION['fname']      = (string)$user->fname;
				$_SESSION['lname']      = (string)$user->lname;
				$_SESSION['email']     = (string)$user->email;
				$_SESSION['logged_in']    = (bool)true;
				$_SESSION['is_confirmed'] = (bool)$user->is_confirmed;
				$_SESSION['is_admin']     = (bool)$user->is_admin;
				$_SESSION['profile_image']     = (string)$user->profile_image;
				$data  = $user;
				// user login ok
				redirect('search');
			} 
			else 
			{
				// login failed
				$data->error = lang('lbl_log_in_error_wrong');
				$data->body_class="main-bg";
				$value['header']=$this->admin_model->get_setting();	
				$this->load->view('header',$value);
				$this->load->view('user/login/login', $data);
				$this->load->view('footer');
			}
		}
	}
	/**
	 * logout function.
	 * 
	 * @access public
	 * @return void
	 */
	public function logout() 
	{
        // create the data object
		$data = new stdClass();
		if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) 
		{
			// remove session datas
			foreach ($_SESSION as $key => $value) 
			{
				unset($_SESSION[$key]);
			}
			// user logout ok
			redirect('/user');
		} 
		else 
		{
			// there user was not logged in, we cannot logged him out,
			// redirect him to site root
			redirect('/user');
		}
	}
	/**
	 Function Name :dashboard.
	 * 
	 * @access public
	 * @return the list of array  gallery and notification
	 */
	public function dashboard()
	{
        $logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }
        // create the data object
        $user_id = $this->session->userdata['user_id'];
        $data['content'] = $this->user_model->get_user($user_id);		
		$data['question']=$this->user_model->get_question($data['content']->que_id);
        // load views
        // user logout ok
        $data["crt"]="dashboard";
		$data["enablechat"]=true;
		$data = new stdClass();
		$user_id = $this->session->userdata['user_id'];
		$data->userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
		
		/*
		* XMPP Register user
		*/
		if($this->XMPP_ENABLE == 'true' && empty($data->ejuser)){
			//register user
			$ejuser=$this->Ejabberd_model->register($user_id);
		}
		else{
			$ejuser=$data->userdetail->ejuser;
		}
		/*
		* XMPP register ends
		*/

		if(empty($data->userdetail->profile_image))
		{
			redirect('user/reg_image');
		}
		else
		{
			$data->gallery=$this->fetch_gallery($user_id);
			$data->question = $this->user_model->get_question($data->userdetail->que_id);
			$value['header']=$this->admin_model->get_setting();
			$data->ejuser=$ejuser;
			$this->load->view('header',$value);
			$this->load->view('user/dashboard/dashboard',$data);
		}
		$this->load->view('footer');            
    }
	public function fetch_language()
	{
		return $this->user_model->fetch_language();
	}
	//Date Of Birth Validation
	public function dob_validation()
	{
		$day=$this->input->post('day');
		$month=$this->input->post('month');
		$year=$this->input->post('year');
		$dat=date_create($day."-".$month."-".$year);
		$curr=date_create(date('d-m-Y'));
		$diff=date_diff($dat,$curr);
		$yer=$diff->y;
		if($yer < 15 )
		{
			return false;
		}
		return true;
	}
	public function valid_dob()
	{
		$day=$this->input->post('day');
		$month=$this->input->post('month');
		$year=$this->input->post('year');
		if($day>31 || $month>12 || $month<1){
			return false;
		}
		else if(!checkdate($month,$day,$year)){
			return false;
		}
		else{
			return true;
		}
		
	}
	/**
	 Function Name :addr_line1.
	 * 
	 * @access private
	 * @return TRUE/FALSE for correct/incorrect address
	 */
	function addr_line1() 
	{
		try
		{
			$address=$this->input->post('address');
			$geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
			$geo = json_decode($geo, true);
			if($geo['status'] == 'OK') 
			{
				return TRUE;
			}
			elseif($geo['status']=="ZERO_RESULTS")
			{
				$this->form_validation->set_message('addr_line1', lang('lbl_register_error_invalid_email'));
				return false;
			}
			else
			{
				throw new Exception("Google Address Api Error :- ".$geo['status']);
			}
		}
		catch(Exception $e)
		{
			if(!empty($this->session->userdata['user_id']))
				$user_id = $this->session->userdata['user_id'];
			else
				$user_id=0;
			//echo 'Error : Google Captcha is Not Working';
			$this->SiteErrorLog->addData($e->getMessage(),$user_id);
			return false;
		}
	}
	/**
	 Function Name :profile.
	 * 
	 * @access public
	 * @return the information of login user and also update login user data
	 */
    public function profile()
	{
		try
		{
			$logged_in_user = $this->session->userdata('logged_in');
			if (!$logged_in_user)
			{
				redirect('login');
			}
			// validators
			// load form helper and validation library
			$this->load->helper('form');
			$this->load->library('form_validation');
			// set validation rules
			$this->form_validation->set_rules('fname', lang('lbl_register_firstname'), 'required|max_length[50]|alpha',array("max_length"=>lang('lbl_validation_error_max_firstname'),
			"alpha"=>lang('lbl_validation_error_alpha_firstname')));
			$this->form_validation->set_rules('lname', lang('lbl_register_lastname'), 'required|max_length[50]|alpha',array("max_length"=>lang('lbl_validation_error_max_lastname'),
			"alpha"=>lang('lbl_validation_error_alpha_lastname')));
			$this->form_validation->set_rules('day', lang('lbl_register_error_day'), 'required|max_length[2]|callback_dob_validation');
			$this->form_validation->set_rules('month', lang('lbl_register_error_month'), 'required|max_length[2]');
			$this->form_validation->set_rules('year', lang('lbl_register_error_year'), 'required|max_length[4]');
			$this->form_validation->set_rules('about', lang('lbl_profile_about'), 'trim|required|max_length[500]');
			$this->form_validation->set_rules('gender', lang('lbl_profile_gender'), 'required');
			$this->form_validation->set_rules('education', lang('lbl_profile_education'), 'trim|required');
			$this->form_validation->set_rules('profession', lang('lbl_profile_profession'), 'trim|required');
			$this->form_validation->set_rules('height', lang('lbl_profile_height'), 'trim|required');
			$this->form_validation->set_rules('kids', lang('lbl_profile_kids'), 'trim|required');
			$this->form_validation->set_rules('religion[]', lang('lbl_profile_religion'), 'required');
			//$this->form_validation->set_rules('ethnicity[]', lang('lbl_profile_ehtnticity'), 'required');
			$this->form_validation->set_rules('address', lang('lbl_profile_address'), 'trim|required|callback_addr_line1');
			$this->form_validation->set_rules('gender_pref', '\'Intrested in\'', 'trim|required');
			$this->form_validation->set_rules('question_ans', lang('lbl_profile_error_question_answer'), 'trim|required|max_length[100]');
			$this->form_validation->set_rules('access_loc', lang('lbl_register_firstname'), 'trim|required');
			$this->form_validation->set_message('dob_validation', lang('lbl_profile_error_age'));
			if ($this->form_validation->run() === TRUE)
			{
				/*if(!empty($_POST["ethnicity"]))
				{
					$ethnicity=$_POST["ethnicity"];
					$_POST["ethnicity"]=implode(",",$ethnicity);
				}*/
				if(!empty($_POST["religion"]))
				{
					$religion=$_POST["religion"];
					$_POST["religion"]=implode(",",$religion);
				}			
				$user= $this->user_model->update_profile($this->input->post());
				$this->session->set_flashdata('success', lang('lbl_profile_update_success'));
			}
			$user_id = $this->session->userdata['user_id'];
			$data['content'] = $this->user_model->get_user($user_id);
			$data['religion'] = $this->user_model->get_all_religion();
			//$data['ethnicity'] = $this->user_model->get_all_ethnicity();
			$data['datepreference'] = $this->user_model->get_all_datepreference();
			$data['questions'] = $this->user_model->get_all_questions();
			$data['notification']=$this->fetch_notification();
			$data['gallery']=$this->fetch_gallery($user_id);
			$data["crt"]="profile";
		}
		catch(Exception $e)
		{
			$this->session->set_flashdata('error', lang('lbl_profile_error_update').$e->getMessage());
		}
			$this->load->view('header');
			$this->load->view('sidebar', $data);
			$this->load->view('user/dashboard/profile');
			$this->load->view('footer');
    }
	/**
	 Function Name :perference.
	 * 
	 * @access public
	 * @return the information of login user and also update login user data
	 */
    public function perference()
	{
        $logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }
        // validators
        // load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('kids', 'Height', 'trim|required');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('age-min', 'Age Minimum', 'trim|required');
		$this->form_validation->set_rules('age-max', 'Age Maximum', 'trim|required');
		$this->form_validation->set_rules('dist-min', 'Distance Minimum', 'trim|required');
		$this->form_validation->set_rules('dist-max', 'Distance Maximum', 'trim|required');
        $this->form_validation->set_rules('access_loc', 'Acess location', 'trim|required');
        if ($this->form_validation->run() === TRUE)
        {
            $user    = $this->user_model->update_perference($this->input->post());
            redirect('/user/dashboard');
        }
        // create the data object
        $user_id = $this->session->userdata['user_id'];
        $data['content'] = $this->user_model->get_user($user_id);
        $data['religion'] = $this->user_model->get_all_religion();
        $data['ethnicity'] = $this->user_model->get_all_ethnicity();
        $data['datepreference'] = $this->user_model->get_new_datepreference($data['content']->date_pref);
        $data["crt"]="perference";
        // load views
        // user logout ok
		$this->load->view('header');
        $this->load->view('sidebar', $data);
	    $this->load->view('user/dashboard/perference');
		$this->load->view('footer');
    }
	/**
	 Function Name :getDistance.
	 * Function is used to fetch distance from latitude and longitude
	 * @access public
	 * @return void
	 */
	public function getDistance($gps1, $gps2)
	{
		$lat1 = $gps1['lat'];
		$lon1 = $gps1['lng'];
		$lat2 = $gps2['lat'];
		$lon2 = $gps2['lng']; 
		$theta = $lon1-$lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +
		cos(deg2rad($lat1)) * cos(deg2rad($lat2)) *
		cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		return ($miles * 0.8684);
	}
	/**
	 Function Name :user_profile.
	 * Function is used fetch selected user profile information
	 * @access public
	 * @return single user information
	 */
	public function user_profile()
	{
        $logged_in_user = $this->session->userdata('logged_in');         
        if (!$logged_in_user)
        {
            redirect('login');
        }
        // create the data object
        $user_id = $this->uri->segment(3);
        $data['content'] = $this->user_model->get_user_profile($user_id);
		$data['gallery']=$this->fetch_gallery($user_id);
        if($data['content']=='')
		{
			redirect('user/friend');
        }
        // load views
        // user logout ok
		$value['header']=$this->admin_model->get_setting();
	    $this->load->view('header',$value);
	    $this->load->view('user/dashboard/user_profile', $data);
		$this->load->view('footer');
	}
	//change password start.
	public function forgot_pass()
    {
			$logged_in_user = $this->session->userdata('logged_in');         
			if (!empty($_SESSION['user_id']))
			{
				redirect('login');
			}
			$value['header']=$this->admin_model->get_setting();
			$this->load->view('header',$value);
			$this->load->view('user/forgot_password/forgot_pass');
			$this->load->view('footer');                    
    }
    public function check_email()
    {
       $email=$this->input->post("username");
        if(!empty($email))
        {
            $id=$this->user_model->get_user_id_from_email($email);                        
		    if(!empty($id))
            {
                $token=md5(uniqid(rand(), true));
                if($this->user_model->set_token($token,$id))
                {                    
					$link="For Reset Your Password.<a href='".base_url()."user/reset_pass/".$token."/".$id."'>Click Here</a>";
					$this->email->from('info@themes.potenzaglobalsolutions.com', 'Cupid Love');
					$this->email->to($email);
					$this->email->subject('Reset Password');
					$this->email->message($link);
					$this->email->set_mailtype('html');
					if($this->email->send()){
                        $this->session->set_flashdata('success',lang('lbl_forgot_password_mail_success'));                                                                      
                    }
                    else
                        $this->session->set_flashdata('fail',lang('lbl_forgot_password_error_mail'));
					    $this->common_msg();				   
                }
            }
            else
            {
               $this->session->set_flashdata('fail',lang('lbl_forgot_password_error_mail'));
               redirect("/user/forgot_pass");
            } 
        }
		else
		{
		   $this->session->set_flashdata('fail',lang('lbl_forgot_password_error_mail'));
		   redirect("/user/forgot_pass");
		} 
    }
	//Reset Password Function
    public function reset_pass($token,$id)
    {
	   $user=$this->user_model->get_user_where_row(array("pass_token"=>$token,"id"=>$id));
        if(!empty($user))
        {
            if($user->id==$id&&$user->pass_token==$token)
            {
				$value['header']=$this->admin_model->get_setting();
				$this->load->view('header',$value);
				$this->load->view("user/forgot_password/reset_pass",array("id"=>$id,"token"=>$token,'email'=>$user->email));   
				$this->load->view('footer');  				
            }
            else
            {
                $this->session->set_flashdata('fail',lang('lbl_common_unauthorized'));
                $this->common_msg();
            }            
        }
        else
        {
            $this->session->set_flashdata('fail',lang('lbl_common_link_expired'));						      
			$this->common_msg();
        }           
    }
	//Change Password Function
    public function change_pass()
    {
    	$this->load->library('form_validation'); 
		$this->form_validation->set_rules('reset_password', lang('lbl_register_password'), 'trim|required|min_length[6]');
		$this->form_validation->set_rules('confirm_password', lang('lbl_register_confirm_password'), 'trim|required|matches[reset_password]');
		if ($this->form_validation->run() === false) 
		{
	        $id=$this->input->post("id");
	        $token=$this->input->post("token");
			$this->reset_pass($token,$id);
	    }
	    else{
	    	$id=$this->input->post("id");
	        $token=$this->input->post("token");
	        $pass=$this->input->post("reset_password");
	        $cpass=$this->input->post("confirm_password");
	        if($pass==$cpass)
	        {
	            if($this->user_model->update_password(array("id"=>$id),$pass))
	            {
	                $this->session->set_flashdata('success',lang('lbl_log_in_success') ); 
	                $this->user_model->unset_token($id);    
	                redirect('user/reset_pass/'.$token.'/'.$id);            
	            }
	            else {
	                $this->session->set_flashdata('fail',lang('lbl_log_in_error'));
	                redirect('login'); 
	            }
	        }else{
	            $this->session->set_flashdata('fail','Please enter same password');
	        }
	   	}
    }	
	//After the completing every reset password function this function message is displayed
    public function common_msg()
    {
        $value['header']=$this->admin_model->get_setting();
		$this->load->view('header',$value);
		$this->load->view("user/forgot_password/common_msg");
		$this->load->view('footer');  
    }
	
	public function display_cms($alias_name)
	{
		$data['cms']=$this->Cms_model->getRecord(array('slug'=>$alias_name));
		if(!empty($data['cms']))
		{
			 $value['header']=$this->admin_model->get_setting();				
			$this->load->view('header',$value);
			$this->load->view('user/dashboard/cms_display',$data);
			$this->load->view('footer');
		}
	}
	
	public function getblog()
	{
		$page =  $_GET['page'];
		$blog_id=$_GET['blog_id'];
        $blogs = $this->user_model->getblog($page,$blog_id);
		$cnt=1;
		$flag=true;
		foreach($blogs as $blog)
		{
			if($flag==true)
			{
				echo '<div class="row post-article mt-5  xs-mt-3">';
				$flag=false;
			}
			if($cnt>5)
			{
		   echo '<div class="col-lg-4 col-md-4 col-sm-6 mb-5 xs-mb-3">';
			echo '<div class="post post-artical">';
			 echo '<div class="post-image clearfix">';
			 if(!empty($blog->image))
			 {
			  $str=base_url("Newassets/images/blog/".$blog->image);
              $str1=base_url('images/blog/01.jpg');
			  if(file_exists(DIRECTORY_PATH."Newassets/images/blog/".$blog->image))
			  {      
				echo '<img class="img-fluid" src="'.$str.'" alt="">';
			  }
			  else
			  { 
			    echo '<img class="img-fluid" src="'.$str1.'" alt="">';
			  }       
			 }
			 else
			 {
			  echo '<img class="img-fluid" src="'.$str1.'" alt="">';
			 }
			 echo '<div class="post-details">';
				echo '<div class="post-title mt-2">';
				   echo '<h5 class="title text-uppercase mt-2">';
					echo '<a href="';
						echo base_url('user/blog_details/'.$blog->id);
					echo '">'.$blog->title.'</a></h5></div>';
			 echo '<p>';echo date_format(date_create($blog->created_date),"F , Y"); echo ' by <a href="#">';echo $blog->author;
			  echo '</a></p>';              
			   echo '<div class="post-icon">';
			    echo '<div class="post-content"><p>';echo substr($blog->description, 0, 100); echo '</p></div>';              
				 echo '<a class="button" href="';
					 echo base_url('user/blog_details/'.$blog->id); 
				echo '">Read More</a>';      
			  echo '</div>';
			  echo '</div>';                
			 echo '</div>';  
		   echo '</div>';
		  echo '</div>';  
			}   
		    $cnt++;
		}
		echo '</div>';
    }

	public function get_all_session_data(){
		$query=$this->user_model->get_all_session_data();
		$user = array();
		$j=0;$m=0;$f=0;
		foreach ($query as $row)
		{
		    //$udata = unserialize($row->data);
		    if( strpos( $row['data'], "logged_in" ) !== false ) {
		    	$strArray = explode(';',$row['data']);
			    $j+=1;
			    foreach ($strArray as $value) {
			    	 if( strpos( $value, "email" ) !== false ) {
			    	 	$email=explode(':',$value);
			    	 	$all_email[]=str_replace('"', "", $email[2]);
			    	 }
			    }
			}
		}
		$all_email=array_unique($all_email);
		foreach ($all_email as $email)
		{
			$checkgen=$this->user_model->get_user_by_con(array("email"=>$email));
    	 	if($checkgen->gender=="male"){
				$m+=1;
    	 	}
    	 	else{
    	 		$f+=1;
    	 	}
		}
		$total_users=$this->user_model->get_total_users();
		echo json_encode(array("total_users"=>$total_users,"total_user_online"=>sizeof($all_email),"men_onlie"=>$m,"female_online"=>$f));
	}
	public function fetch_notification()
	{
		$logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
			redirect('login');
        }
        // create the data object
        $user_id = $this->session->userdata['user_id'];
        $userdetail = $this->user_model->get_user($user_id);
		$data = $this->user_model->get_notification($user_id,$userdetail->location_lat,$userdetail->location_long);
        return $data;
	}
	public function send_mail(){
		$email=$this->input->post("email");
    	$user=$this->user_model->get_user_id_from_email($email);
    	if(empty($user)){
			$verification_code=rand(1000,9999);
			$this->user_model->delete_user_verification(array("email"=>$email));
			$i=$this->user_model->user_verification(array("email"=>$email,"verification_code"=>$verification_code));
			//echo "<pre>";
			//echo $this->db->last_query();die;
			if($i){
				$this->load->library('email');
				$this->email->from('nirav.tandel@potenzaglobalsolutions.com', 'Cupidlove');
				$this->email->to($email);
				$this->email->subject('Verify Account');
				$this->email->message("Your verification code is : ".$verification_code);
				$this->email->send();
				$response['response'] = true;
				$response['email'] = $email;
				$response['message'] = 'Verification code has been sent to your registered Email address..';
			}
			else{
				$response['response'] = false;
				$response['message'] = 'Verification could not be sent.';
			}
    	}
    	else{
    		$response['response'] = true;
			$response['message'] = 'User has already registered with this email address.';
    	}
		echo(json_encode($response));
	}
	public function email_verification(){
		$email=$this->input->post("email");
		$verification_code=$this->input->post("verification_code");
    	$user=$this->user_model->get_users_verification(array("email"=>$email,"verification_code"=>$verification_code));
		$response = array();
		if($user){
			$response['response'] = true;
			$this->user_model->update_user_verification(array("status"=>1),array("email"=>$email));
		}
		else{
			$response['response']= false;
		}
		echo(json_encode($response));
    }
    public function location(){
		$logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
			redirect('login');
        }
        // create the data object
        $user_id = $this->session->userdata['user_id'];
		$userdetail = $this->user_model->get_user($user_id);
		$data['user']=$userdetail;
        $value['header']=$this->admin_model->get_setting();
		$this->load->view('header',$value);
		$this->load->view("user/dashboard/location",$data);
		//add js file 
		$footer['Additional_JS']=array(
			base_url('Newassets/js/location.js'),
			'https://maps.googleapis.com/maps/api/js?key='.GOOGLE_API_KEY.'&libraries=places&callback=initialize',
		);
		$this->load->view('footer',$footer);  
	}
	public function updateLocation(){
		$logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
			redirect('login');
        }
        // create the data object
		$user_id = $this->session->userdata['user_id'];
		$lat=$this->input->post('lat');
		$lon=$this->input->post('lon');
		$update_array=array(
			'location_lat'=>$lat,
			'location_long'=>$lon,
		);
		$user=$this->user_model->update_profile(array('id'=>$user_id),$update_array);
	}
 }