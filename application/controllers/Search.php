<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'/PHPThumb/ThumbLib.inc.php';
/**
 * Search class.
 * 
 * @extends CI_Controller
 */
class Search extends CI_Controller {
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() 
	{
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
        $this->load->library('form_validation');
        $this->load->helper('form');
		$this->load->model('user_model');
		$this->load->model('SiteErrorLog');
		$this->load->model('blocks_model');
		$this->load->model('admin_model');
		$this->load->model('Pushnotification_model');
		$this->load->model('story_model');
		$this->load->model('blog_model');
		$this->load->model('blocks_model');
        $this->load->model('members_model');
		$this->load->model('Cms_model');
		$this->load->library('pagination');
        $this->load->library('table');
		$this->load->model('Testimonial_model');
		$this->load->model('Slider_model');
        $this->load->model('Error404_model');
        $this->load->model('comingsoon_model');
		$this->load->model('background_model');
        date_default_timezone_set('Asia/Kolkata'); 
		$data['header']=$this->admin_model->get_setting();
		$is_comingsoon=$data['header'][18]['status'];
		$method=$this->router->fetch_method();
		if($is_comingsoon==1 && $method!='comingsoon'){
			redirect('user/comingsoon');
		}
	}
	public function index()
	{
	    $logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }
        // create the data object
        $user_id = $this->session->userdata['user_id'];
        $data['content'] = $this->user_model->get_search_prefences($user_id);
		$data['notification']=$this->fetch_notification();
		$data['gallery']=$this->fetch_gallery($user_id);
        $data["crt"]="search";
		$value['header']=$this->admin_model->get_setting();	
		$this->load->view('header',$value);
	    $this->load->view('user/dashboard/search',$data);
		$this->load->view('footer');
	}
	public function search_by_condition()
	{
        $logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }
        // create the data object
        $user_id = $this->session->userdata['user_id'];
        $data['content'] = $this->user_model->get_search_prefences_by_condition($user_id,$this->input->post("find_to"),$this->input->post("age_from"),$this->input->post("age_to"));
		$data['notification']=$this->fetch_notification();
		$data['gallery']=$this->fetch_gallery($user_id);
        $data["crt"]="search";
		$value['header']=$this->admin_model->get_setting();	
		$this->load->view('header',$value);
	    $this->load->view('user/dashboard/search',$data);
		$this->load->view('footer');
    }
	public function fetch_notification()
	{
		$logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
			redirect('login');
        }
        // create the data object
        $user_id = $this->session->userdata['user_id'];
        $userdetail = $this->user_model->get_user($user_id);
		$data = $this->user_model->get_notification($user_id,$userdetail->location_lat,$userdetail->location_long);
        return $data;
	}
	/* Function Name fetch_gallery
	 * Returns Gallery detail 
	 * @param Number $user_id
	 * @return Array of Gallery Images detail of User
	 */
	public function fetch_gallery($user_id)
	{
		$logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }		
		return  $this->user_model->get_gallery_images(array("user_id "=>$user_id));
	}
	
 }