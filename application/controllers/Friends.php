<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'/PHPThumb/ThumbLib.inc.php';
/**
 * Friends class.
 * 
 * @extends CI_Controller
 */
class Friends extends CI_Controller {
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() 
	{
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
        $this->load->library('form_validation');
        $this->load->helper('form');
		$this->load->model('Common_model');
		$this->load->model('user_model');
		$this->load->model('SiteErrorLog');
		$this->load->model('blocks_model');
		$this->load->model('admin_model');
		$this->load->model('Pushnotification_model');
		$this->load->model('story_model');
		$this->load->model('blog_model');
		$this->load->model('blocks_model');
        $this->load->model('members_model');
		$this->load->model('Cms_model');
		$this->load->library('pagination');
        $this->load->library('table');
		$this->load->model('Testimonial_model');
		$this->load->model('Slider_model');
        $this->load->model('Error404_model');
		$this->load->model('comingsoon_model');
		$this->load->model('comingsoon_model');
		$this->load->model('Ejabberd_model');
		$this->load->model('background_model');
        date_default_timezone_set('Asia/Kolkata'); 
		$this->XMPP_ENABLE = $this->Common_model->get_key_configuration(array('key'=>'XMPP_ENABLE'));
		$this->XMPP_HOST = $this->Common_model->get_key_configuration(array('key'=>'XMPP_HOST'));
		$this->XMPP_SERVER = $this->Common_model->get_key_configuration(array('key'=>'XMPP_SERVER'));
		$this->XMPP_HTTP_BIND = 'http://'.$this->XMPP_SERVER.':5280/http-bind';
		$this->XMPP_DEFAULT_PASSWORD = $this->Common_model->get_key_configuration(array('key'=>'XMPP_DEFAULT_PASSWORD'));
		$this->FACEBOOK_KEY = $this->Common_model->get_key_configuration(array('key'=>'FACEBOOK_KEY'));
		$data['header']=$this->admin_model->get_setting();
		$is_comingsoon=$data['header'][18]['status'];
		$method=$this->router->fetch_method();
		if($is_comingsoon==1 && $method!='comingsoon'){
			redirect('user/comingsoon');
		}
	}
	/**
	 Function Name :friend.
	 * Function is used to fetch login users friend
	 * @access public
	 * @return users information
	 */
	public function index()
	{
        $logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
			redirect('login');
        }
        // create the data object
		$user_id = $this->session->userdata['user_id'];
		$userdetail=$this->user_model->get_user_where_row(array("id "=>$user_id));
		/*
		* XMPP Register user
		*/
		if($this->XMPP_ENABLE == "true" && empty($userdetail->ejuser)){
			//register user
			$ejuser=$this->Ejabberd_model->register($user_id);
		}
		else{
			$ejuser=$userdetail->ejuser;
		}
		//get the json from the cache 
		$datepref=array('cafe','bar','restaurant','beach');
		foreach($datepref as $dp){
			$file=FCPATH.'uploads/places/'.$dp.'-'.$user_id.'.json';
			if(!file_exists($file)){
				//create the file and store data
				$data[$dp]=$this->getPlacesfromGoogle($user_id,$dp,$userdetail->location_lat,$userdetail->location_long);
			}
			else{
				//echo "Current Time:".time();
				//now get the data from cache value 
				$difference=time()-filemtime($file);
				if($difference > 86400){
					//update the data
					$data[$dp]=$this->getPlacesfromGoogle($user_id,$dp,$userdetail->location_lat,$userdetail->location_long);
				}
				else{
					//get the data from cache
					$data[$dp]=$this->getPlacesfromCache($user_id,$dp,$userdetail->location_lat,$userdetail->location_long);
				}
			}
		}
		/*
		* XMPP register ends
		*/
		
        $data['content'] = $this->user_model->get_friends_list($user_id);
		$data['notification']=$this->fetch_notification();
		$data['gallery']=$this->fetch_gallery($user_id);
		$data["crt"]="friends";   
		$data["ejuser"]=$ejuser;
        // load views
		$value['header']=$this->admin_model->get_setting();	
		$this->load->view('header',$value);
	    $this->load->view('user/dashboard/friends',$data);
		$this->load->view('footer');
    }
	/**
	 Function Name :friend_decline.
	 * Function is used reject user friend request who send me
	 */
    public function friend_decline($id)
    {
		$id=$this->uri->segment(3);
        $logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }
		$rid=$this->session->userdata('user_id');
        $this->user_model->friend_decline($rid,$id);
        $this->session->set_flashdata('friend_decline', 'You have declined friend request');                
       	echo 'You have declined friend request';
       	//redirect('user/dashboard');
    }
	/**
	 Function Name :friend_profile.
	 * Function is used fetch selected friend profile information
	 * @access public
	 * @return single user information
	 */
    public function friend_profile($fid)
	{
        $logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }
        // create the data object
        $user_id = $this->session->userdata['user_id'];
        $friend_id = $this->uri->segment(3);
        $data['content'] = $this->user_model->get_friend_profile($user_id,$friend_id);
		$data['gallery']=$this->fetch_gallery($friend_id);
		$dat['notification'] = $this->fetch_notification();
        if($data['content']=='')
		{
			redirect('user/friend');
        }
        // load views
        // user logout ok
		$this->load->view('header');        
	    $this->load->view('user/dashboard/friend_profile', $data);
		$this->load->view('footer');
    }
	public function friend_like($fid)
	{
		$logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
			redirect('login');
        }
        $user_id = $this->session->userdata['user_id'];
        $friend_id = $this->uri->segment(3);
		$chk=$this->user_model->get_friend($user_id,$friend_id);
        if(empty($chk))
		{
			$friend=$this->user_model->get_user($friend_id);
			$already=$this->user_model->is_already_friend($user_id,$friend_id);
			if(empty($already)){
				$data['content'] = $this->user_model->friend_like($user_id,$friend_id);
				//$this->session->set_flashdata('add_friend', 'Request sent to '.$friend->fname.' '.$friend->lname);
				/* add user in the roster and subscribe user */
				$user=$this->user_model->get_user($user_id);
				if($this->XMPP_ENABLE=='true')
				{
					//$this->Ejabberd_model->add_rosteritem($user_id,$friend_id);
				}
					//send push notification
				try
				{
					$ludetail=$this->user_model->get_user_array($user_id);
					$udetail=$this->user_model->get_user_array($friend_id);
					$device_token=$this->user_model->get_device_token($friend_id);
					$fdetail["friend_profile"]=$ludetail["profile_image"];
					$fdetail["friend_Fname"]=$ludetail["fname"];
					$fdetail["friend_Lname"]=$ludetail["lname"];
					$fdetail["friendid"]=$ludetail["id"];
					foreach($device_token as $device)
					{
						if($device['device']=="ios")
						{
							try
							{
							$status=$this->Pushnotification_model->send_push($device['device_token'], "Cupid Love",($udetail["notificationcounter"]+1) , $ludetail["fname"]." has Liked you",$type=1,$fdetail);
							}
							catch(Exception $e)
							{
								//$this->session->set_flashdata('error', lang('lbl_notification_error_xmpp').$e->getMessage());
							}
						}
						elseif($device['device']=="android")
							$this->Pushnotification_model->send_push_android($device['device_token'], "Cupid Love",($udetail["notificationcounter"]+1) , $ludetail["fname"]." has Liked you",$type=1,$fdetail);
					}
				}
				catch(Exception $e)
				{
					echo lang('lbl_notification_error_notification');
					//$this->session->set_flashdata('error', lang('lbl_notification_error_notification').$e->getMessage());
				}
			}			
		}
		echo 'Request sent to '.$friend->fname.' '.$friend->lname;
		//redirect('Search/index');
    }
	/**
	 Function Name :friend_dislike.
	 * Function is user to dis-like particular selected user
	 * @access public
	 * @return void
	 */
    public function friend_dislike($fid)
	{
        $logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
			redirect('login');
            //redirect('login');
        }
        // create the data object
        $user_id = $this->session->userdata['user_id'];		
        $friend_id = $this->uri->segment(3);
		$friend=$this->user_model->get_user($friend_id);
		$already=$this->user_model->is_already_friend($user_id,$friend_id);
		if(empty($already)){
			$this->user_model->delete_friend($user_id,$friend_id);
		}
        $data['content'] = $this->user_model->friend_dislike($user_id,$friend_id);
		//$this->session->set_flashdata('dislike_friend', 'You have ignored '.$friend->fname.' '.$friend->lname);
		echo 'You have ignored '.$friend->fname.' '.$friend->lname;
        //redirect('user/search');
    }
	public function friend_approved($fid)
	{
        $logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }
        // create the data object
        $user_id = $this->session->userdata['user_id'];
        $friend_id = $this->uri->segment(3);
		$friend=$this->user_model->get_user($friend_id);
		/*$already=$this->user_model->is_already_friend($user_id,$friend_id);
		if(!empty($already)){
			$this->user_model->delete_friend($user_id,$friend_id);
		}*/
        $data['content'] = $this->user_model->friend_approved($user_id,$friend_id);
        $this->session->set_flashdata('friend_approved', $friend->fname.' '.$friend->lname.' added as your friend');
		$user=$this->user_model->get_user($user_id);
		echo $friend->fname.' '.$friend->lname.' added as your friend';
		if($this->XMPP_ENABLE == 'true')
		{
			$this->Ejabberd_model->add_rosteritem($user_id,$friend_id);
			$this->Ejabberd_model->add_rosteritem($friend_id,$user_id);
		}
		//send push notification
		try
		{
			$ludetail=$this->user_model->get_user_array($user_id);
			$device_token=$this->user_model->get_device_token($friend_id);
			$udetail=$this->user_model->get_user_array($friend_id);
			if($ludetail["profile_image"]=="")
				$fdetail["friend_profile"]="default.png";
			else
				$fdetail["friend_profile"]=$ludetail["profile_image"];
			$fdetail["friend_Fname"]=$ludetail["fname"];
			$fdetail["friend_Lname"]=$ludetail["lname"];
			$fdetail["friendid"]=$ludetail["id"];
			foreach($device_token as $device)
			{
				if($device['device']=="ios")
					$this->Pushnotification_model->send_push($device["device_token"], "Cupid Love",($udetail["notificationcounter"]+1) , "You are matched With ".$ludetail["fname"],$type=2,$fdetail);
				elseif($device['device']=="android")
					$this->Pushnotification_model->send_push_android($device["device_token"], "Cupid Love",($udetail["notificationcounter"]+1) , "You are matched With ".$ludetail["fname"],$type=2,$fdetail);
			}	
		}
		catch(Exception $e)
		{
			$this->session->set_flashdata('error',  lang('lbl_notification_error_notification').$e->getMessage());
		}
		//redirect('user/notification');
    }
	public function fetch_notification()
	{
		$logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
			redirect('login');
        }
        // create the data object
        $user_id = $this->session->userdata['user_id'];
		$userdetail = $this->user_model->get_user($user_id);
		$data = $this->user_model->get_notification($user_id,$userdetail->location_lat,$userdetail->location_long);
        return $data;
	}
	/* Function Name fetch_gallery
	 * Returns Gallery detail 
	 * @param Number $user_id
	 * @return Array of Gallery Images detail of User
	 */
	public function fetch_gallery($user_id)
	{
		$logged_in_user = $this->session->userdata('logged_in');
        if (!$logged_in_user)
        {
            redirect('login');
        }		
		return  $this->user_model->get_gallery_images(array("user_id "=>$user_id));
	}
	public function sendDateRequestResponse(){
		$logged_in_user = $this->session->userdata('user_id');
		$user=$this->user_model->get_user_array($logged_in_user);
        if (!$user)
			die;
		$mode=$this->input->post('mode');
		$from=$user['ejuser'];
		$msg=$this->input->post('msg');
		$to=$this->input->post('to');
		if(empty($mode) || empty($from) || empty($msg) || empty($to))
			die('no data');
		if($mode=='accept')
			$msg.='1';
		else
			$msg.='0';
		$request=array(
			"type"=>"chat",
			"from"=> $from.'@'.$this->XMPP_SERVER,
			"to"=> $to,
			"body"=> $msg
		);
		$result=$this->Ejabberd_model->sendMsg($request);
		//echo $result;
	}
	public function sendDateRequest(){
		$logged_in_user = $this->session->userdata('user_id');
		$user=$this->user_model->get_user_array($logged_in_user);
        if (!$user)
			die;
		$from=$user['ejuser'];
		$to=$this->input->post('receiver');
		$imgPlace=$this->input->post('imgPlace');
		if(!empty($imgPlace)){
			$imgPlace="https://maps.googleapis.com/maps/api/place/photo?maxwidth=100&photoreference=$imgPlace&key=AIzaSyB-0xiIlNO91Dl3mpBqOuW31BsphxnTYis";
		}
		else{
			$imgPlace="";
		}
		$data=array(
			'StarRating'=>$this->input->post('ratings'),
			'imgPlace'=>$imgPlace,
			'TimeOfDate'=>$this->input->post('TimeOfDate'),
			'Title'=>$this->input->post('Title'),
			'Distance'=>$this->input->post('Distance'),
		);
		$placeid=$this->input->post('placeid');

		$message="$***$".$placeid."$***$".json_encode($data)."#";
		$request=array(
			"type"=>"chat",
			"from"=> $from.'@'.$this->XMPP_SERVER,
			"to"=> $to,
			"body"=> $message
		);
		echo $result=$this->Ejabberd_model->sendMsg($request);
		die;
	}
	private function getPlacesfromGoogle($userid,$type,$lat,$long){
		$url="https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=".$lat.",".$long."&radius=5000&type=$type&keyword=$type&key=AIzaSyB-0xiIlNO91Dl3mpBqOuW31BsphxnTYis";
		$data=file_get_contents($url);
		//update the file
		$file = fopen(FCPATH."uploads/places/".$type."-".$userid.".json","w");
		fwrite($file,$data);
		fclose($file);
		$dataarray=json_decode($data);
		$data_item=array();
		$i=0;
		foreach($dataarray->results as $item){
			$data_item[$i]=array(
				'photo_reference'=>(!empty($item->photos)) ? $item->photos[0]->photo_reference : 'NA',
				'id'=>$item->id,
				'rating'=>$item->rating,
				'name'=>$item->name,
				'distance'=>$this->distance($item->geometry->location->lat,$item->geometry->location->lng,$lat,$long,'M')
			);
			$i++;
		}
		return json_encode($data_item);
	}
	private function getPlacesfromCache($userid,$type,$lat,$long){
		$url=base_url()."uploads/places/".$type."-".$userid.".json";
		$data=file_get_contents($url);
		//update the file
		$dataarray=json_decode($data);
		$data_item=array();
		$i=0;
		foreach($dataarray->results as $item){
			$data_item[$i]=array(
				'photo_reference'=>(!empty($item->photos)) ? $item->photos[0]->photo_reference : 'NA',
				'id'=>$item->id,
				'rating'=>$item->rating,
				'name'=>$item->name,
				'distance'=>$this->distance($item->geometry->location->lat,$item->geometry->location->lng,$lat,$long,'M')
			);
			$i++;
		}
		return json_encode($data_item);
	}
	private function distance($lat1, $lon1, $lat2, $lon2, $unit) {
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);
	  
		if ($unit == "K") {
		  return ($miles * 1.609344);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		  } else {
			  return $miles;
			}
	}	  
 }