<?php
class LangSwitch extends CI_Controller
{
    public function __construct() {
        parent::__construct();
		$this->load->model('Admin_model');
		$this->load->library('session');
    }
    public function switchLanguage($language) {
		$language = ($language != "") ? $language : "english";
		$rtl=$this->Common_model->rtl_lang(array("name"=>$language,"rtl"=>1));
        $this->session->set_userdata('rtl_lang', $rtl);
        $this->session->set_userdata('site_lang', $language);
        redirect($_SERVER['HTTP_REFERER']);
    }
}
?>