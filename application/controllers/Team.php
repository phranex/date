<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'/PHPThumb/ThumbLib.inc.php';
/**
 * Team class.
 * 
 * @extends CI_Controller
 */
class Team extends CI_Controller {
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() 
	{
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
        $this->load->library('form_validation');
        $this->load->helper('form');
		$this->load->model('user_model');
		$this->load->model('SiteErrorLog');
		$this->load->model('blocks_model');
		$this->load->model('admin_model');
		$this->load->model('Pushnotification_model');
		$this->load->model('story_model');
		$this->load->model('blog_model');
		$this->load->model('blocks_model');
        $this->load->model('members_model');
		$this->load->model('Cms_model');
		$this->load->library('pagination');
        $this->load->library('table');
		$this->load->model('Testimonial_model');
		$this->load->model('Slider_model');
        $this->load->model('Error404_model');
        $this->load->model('comingsoon_model');
		$this->load->model('background_model');
        date_default_timezone_set('Asia/Kolkata'); 
		$data['header']=$this->admin_model->get_setting();
		$is_comingsoon=$data['header'][18]['status'];
		$method=$this->router->fetch_method();
		if($is_comingsoon==1 && $method!='comingsoon'){
			redirect('user/comingsoon');
		}
	}
	public function index()
	{
	    $value['header']=$this->admin_model->get_setting();
		$data['footer_contact_us'] = $this->blocks_model->get_fetch_block(array('block.id'=>17));
		$data['our_team_member'] = $this->blocks_model->get_fetch_block(array('block.id'=>18));
        $data['members']=$this->members_model->getMembers();
	    $this->load->view('header',$value);			
		$this->load->view('user/dashboard/team',$data);
		$this->load->view('footer',$value);
	}
	public function team_contact_insert()
    {
        $team_id = $this->input->post('team_id');
        $ispost=$this->input->method(TRUE);
        if($ispost=='POST')
        {
            //allow only post method here
            //do the form validation here 
                $this->load->library('form_validation');
                $this->form_validation->set_rules('name', 'Name', 'required');
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
                $this->form_validation->set_rules('subject', 'Subject', 'required');
                $this->form_validation->set_rules('message', 'Comment', 'required');
				$data['contact']=$this->user_model->get_site_setting();
				$data['blocks']=$this->blocks_model->fetch_block();
				if ($this->form_validation->run() === false) 
				{
					$value['header']=$this->admin_model->get_setting();
					$data['footer_contact_us'] = $this->blocks_model->get_fetch_block(array('block.id'=>17));
					$data['our_activity_section'] = $this->blocks_model->get_fetch_block(array('block.id'=>19));
                    $data['member']=$this->members_model->getMember($team_id);
                    if (empty($data['member'])) {
                        redirect('user/team');
                    }else{
            	    $this->load->view('header',$value);			
            		$this->load->view('user/dashboard/singleteam',$data);
            		$this->load->view('footer',$value);
                    }
				} 
                else
                {
                        $dbdata=array(
                            'name'=>$this->input->post('name'),
                            'email'=>$this->input->post('email'),
                            'subject'=>$this->input->post('subject'),
                            'message'=>$this->input->post('message'),
                        );
                        $id=$this->user_model->insert_contact($dbdata);
                        if($id)
                        {
                            //echo success;   //set flash data for success and redirect to index
                            $this->session->set_flashdata('success',lang("lbl_contact_success"));
							redirect('user/singleteam/'.$team_id);
                        }
                        else{
                            //echo fail; //set flash data and redirect back to add
                            $this->session->set_flashdata('fail',lang("lbl_error"));
                            redirect('user/singleteam/'.$team_id);
                        }
                  }
          }
          else
          {
            //set the error message here and redirect to the index page
            $this->session->set_flashdata('error','Something Went Wrong!');
            redirect('user/singleteam/'.$team_id);
          }
	}
	public function singleteam($id)
	{
	    $value['header']=$this->admin_model->get_setting();
		$data['footer_contact_us'] = $this->blocks_model->get_fetch_block(array('block.id'=>17));
		$data['our_activity_section'] = $this->blocks_model->get_fetch_block(array('block.id'=>19));
        $data['member']=$this->members_model->getMember($id);
        if (empty($data['member'])) {
            redirect('user/team');
        }else{
	    $this->load->view('header',$value);			
		$this->load->view('user/dashboard/singleteam',$data);
		$this->load->view('footer',$value);
        }
	}
	
 }