<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'/PHPThumb/ThumbLib.inc.php';
/**
 * Blog class.
 * 
 * @extends CI_Controller
 */
class Blog extends CI_Controller {
    public function __construct()
	{		
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->model('blog_model');
		$this->load->model('admin_model');
		date_default_timezone_set('Asia/Kolkata');
		$logged_in_user = $this->session->userdata('logged_in');
		if ($logged_in_user)
		{
			$is_admin = $this->session->userdata('is_admin');
			if(!$is_admin)
			{
				redirect('admin/login');
			}
		}
	}
    public function index()
	{
		$data['blog']=$this->blog_model->fetch_blogs();
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');
		$this->load->view('admin/blog/blog_list', $data);
		$this->load->view('admin/footer');
	}
	public function new_blog()
	{
	
		$logged_in_user = $this->session->userdata('logged_in');   
		if (!$logged_in_user)
		{
			redirect('login');
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('blogtitle', 'Blog Name', 'required');
		$this->form_validation->set_rules('blogslug', 'Blog Slug', 'required');
		$this->form_validation->set_rules('authorname', 'Author Name', 'required');
		$this->form_validation->set_rules('blogdescrip', 'Author Description', 'required');
		if ($this->form_validation->run() === true)
        {
			if(!empty($_FILES["blogimage"]['name']))
			{
				$data=array(
					'title'=>$this->input->post('blogtitle'),
					'slug'=>$this->input->post('blogslug'),
					'author'=>$this->input->post('authorname'),
					'description'=>$this->input->post('blogdescrip'),
					'image'=>(!empty($_FILES["blogimage"]['name']))?$_FILES["blogimage"]['name']:"",
					'modified_date'=>date("Y-m-d H:i:s")
				);
			}
			else
			{
				$data=array(
					'title'=>$this->input->post('blogtitle'),
					'slug'=>$this->input->post('blogslug'),
					'author'=>$this->input->post('authorname'),
					'description'=>$this->input->post('blogdescrip'),
					'modified_date'=>date("Y-m-d H:i:s")				
				);
			}
			$user= $this->blog_model->setblogitem($data);
			if(!empty($_FILES["blogimage"]['name']))
			{
				$name = $_FILES["blogimage"]["name"];
				$ext = end((explode(".", $name))); 
				// Code for File Upload
				$config['upload_path']= './Newassets/images/';        
				$config['allowed_types']= 'gif|jpg|png|jpeg';
				$config['overwrite'] = TRUE;
				$config['file_name'] = $_FILES["blogimage"]['name'];
				$this->load->library('upload');
				$this->upload->initialize($config);
				if ( ! $this->upload->do_upload('blogimage'))
				{
					// On File Upload Fail
					$this->session->set_flashdata('fail',$this->upload->display_errors());
					redirect('/admin/blog');
				}
				else
				{
					$file_data = $this->upload->data();				
					$source_path = DIRECTORY_PATH.'Newassets/images/'.$file_data["file_name"];
					$target_path = DIRECTORY_PATH.'Newassets/images/thumbnail/'.$file_data["file_name"];
					$thumb = PhpThumbFactory::create($source_path);
					$thumb->adaptiveResize(280, 250);
					$thumb->save($target_path, $ext);
					if($user)
					{					
						$this->session->set_flashdata('success',"Blog Successfully Added");
						redirect('/admin/blog');
					}
					else
					{
						$this->session->set_flashdata('fail',"Error Occured in Blog added");
						redirect('/admin/blog');
					}					
				}				
			}
			if($user)
			{					
				$this->session->set_flashdata('success',"Blog Successfully Added");
				redirect('/admin/blog');
			}
			else
			{
				$this->session->set_flashdata('fail',"Error Occured in Blog added");
				redirect('/admin/blog');
			}
		}
		$this->load->view('admin/header');
        $this->load->view('admin/sidebar');
	    $this->load->view('admin/blog/blog_add');
		$this->load->view('admin/footer');
	}
	public function blog_edit($id)
	{
		//die("hello");
		$data['blog']=$this->blog_model->getblogitem(array('blog.id'=>$id));
		$this->load->view('admin/header');
        $this->load->view('admin/sidebar');
	    $this->load->view('admin/blog/blog_edit', $data);
		$this->load->view('admin/footer');
	}
	public function blog_update()
	{
		$logged_in_user = $this->session->userdata('logged_in');   
		if (!$logged_in_user)
		{
			redirect('login');
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('blogtitle', 'Blog Name', 'required');
		$this->form_validation->set_rules('blogslug', 'Blog Slug', 'required');
		$this->form_validation->set_rules('authorname', 'Author Name', 'required');
		$this->form_validation->set_rules('blogdescrip', 'Author Description', 'required');
		if ($this->form_validation->run() === true)
        {
			if(!empty($_FILES["blogimage"]['name']))
			{
				$data=array(
					'title'=>$this->input->post('blogtitle'),
					'slug'=>$this->input->post('blogslug'),
					'author'=>$this->input->post('authorname'),
					'description'=>$this->input->post('blogdescrip'),
					'image'=>(!empty($_FILES["blogimage"]['name']))?$_FILES["blogimage"]['name']:"",
					'modified_date'=>date("Y-m-d H:i:s")
				);
			}
			else
			{
				$data=array(
					'title'=>$this->input->post('blogtitle'),
					'slug'=>$this->input->post('blogslug'),
					'author'=>$this->input->post('authorname'),
					'description'=>$this->input->post('blogdescrip'),
					'modified_date'=>date("Y-m-d H:i:s")				
				);
			}
			$user= $this->blog_model->updateblogitem(array('id'=>$this->input->post('id')),$data);
			if(!empty($_FILES["blogimage"]['name']))
			{
				$condition=array("id"=>$this->input->post('id'));					
				$image_detail= $this->blog_model->fetch_blogs($condition);
				$unlinkimg=$image_detail["image"];
				unlink(base_url("Newassets/images/".$unlinkimg));
				unlink(base_url("Newassets/images/thumbnail/".$unlinkimg));
				$name = $_FILES["blogimage"]["name"];
				$ext = end((explode(".", $name))); 
				// Code for File Upload
				$config['upload_path']= './Newassets/images/';        
				$config['allowed_types']= 'gif|jpg|png|jpeg';
				$config['overwrite'] = TRUE;
				$config['file_name'] = $_FILES["blogimage"]['name'];
				$this->load->library('upload');
				$this->upload->initialize($config);
				if ( ! $this->upload->do_upload('blogimage'))
				{
					// On File Upload Fail
					$this->session->set_flashdata('fail',$this->upload->display_errors());
					redirect('/admin/blog');
				}
				else
				{
					$file_data = $this->upload->data();				
					$source_path = DIRECTORY_PATH.'Newassets/images/'.$file_data["file_name"];
					$target_path = DIRECTORY_PATH.'Newassets/images/thumbnail/'.$file_data["file_name"];
					$thumb = PhpThumbFactory::create($source_path);
					$thumb->adaptiveResize(280, 250);
					$thumb->save($target_path, $ext);
					if($user)
					{					
						$this->session->set_flashdata('success',"Blog Successfully Updated");
						redirect('/admin/blog');
					}
					else
					{
						$this->session->set_flashdata('fail',"Error Occured in Blog Update");
						redirect('/admin/blog');
					}					
				}				
			}
			if($user)
			{					
				$this->session->set_flashdata('success',"Blog Successfully Updated");
				redirect('/admin/blog');
			}
			else
			{
				$this->session->set_flashdata('fail',"Error Occured in Blog Update");
				redirect('/admin/blog');
			}
		}
	}
	public function blog_delete($id)
	{
		if($this->blog_model->removeblogitem(array('id'=>$id)))
		{
			$this->session->set_flashdata('success',"Blog Successfully Deleted");
			redirect('/admin/blog');
		}
		else
		{
			$this->session->set_flashdata('fail',"Error Occured in Delete Blog");
			redirect('/admin/blog');
		}
	}
}