<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'/PHPThumb/ThumbLib.inc.php';
/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Slider extends CI_Controller {
    public function __construct()
	{		
            parent::__construct();
            $this->load->library(array('session'));
            $this->load->helper(array('url'));
            $this->load->library('form_validation');
            $this->load->helper('form');
            $this->load->model('Slider_model');
            $this->load->model('admin_model');
            date_default_timezone_set('Asia/Kolkata');
            $logged_in_user = $this->session->userdata('logged_in');   
            if ($logged_in_user)
            {
                $is_admin = $this->session->userdata('is_admin');
                if(!$is_admin)
                {
                    redirect('admin/login');
                }
            }
	}
	public function index()
	{
            $data['sliders']=$this->Slider_model->fetch_slider();
            $this->load->view('admin/header');
            $this->load->view('admin/sidebar');
            $this->load->view('admin/slider/slider_list', $data);
            $this->load->view('admin/footer');
	}
	public function new_slider()
	{
		$logged_in_user = $this->session->userdata('logged_in');   
		if (!$logged_in_user)
		{
			redirect('login');
		}
		$this->load->helper('form');
		$this->load->library('form_validation');		
		$this->form_validation->set_rules('slidertitle', 'Slider Title', 'required');
		if ($this->form_validation->run() === true)
        {
			if(!empty($_FILES["sliderimage"]['name']))
			{
				$data=array(
					'title'=>$this->input->post('slidertitle'),
					'image'=>$_FILES["sliderimage"]['name'],
				);
			}
			else
			{
				$data=array(
					'title'=>$this->input->post('slidertitle'),
				);
			}
			$user= $this->Slider_model->setslideritem($data);
			if(!empty($_FILES["sliderimage"]['name']))
			{
				$name = $_FILES["sliderimage"]["name"];
				$ext = end((explode(".", $name))); 
				// Code for File Upload
				$config['upload_path']= './Newassets/images/';        
				$config['allowed_types']= 'gif|jpg|png|jpeg';
				$config['overwrite'] = TRUE;
				$config['file_name'] = $_FILES["sliderimage"]['name'];
				$this->load->library('upload');
				$this->upload->initialize($config);
				if ( ! $this->upload->do_upload('sliderimage'))
				{
					// On File Upload Fail
					$this->session->set_flashdata('fail',$this->upload->display_errors());
					redirect('/admin/slider');
				}
				else
				{
					$file_data = $this->upload->data();				
					$source_path = DIRECTORY_PATH.'Newassets/images/'.$file_data["file_name"];
					$target_path = DIRECTORY_PATH.'Newassets/images/thumbnail/'.$file_data["file_name"];
					$thumb = PhpThumbFactory::create($source_path);
					$thumb->adaptiveResize(280, 250);
					$thumb->save($target_path, $ext);
					if($user)
					{					
						$this->session->set_flashdata('success',"Slider Data Successfully Added");
						redirect('/admin/slider');
					}
					else
					{
						$this->session->set_flashdata('fail',"Error Occured in Slider add");
						redirect('/admin/slider');
					}					
				}				
			}
			if($user)
			{					
				$this->session->set_flashdata('success',"Slider Data Successfully Added");
				redirect('/admin/slider');
			}
			else
			{
				$this->session->set_flashdata('fail',"Error Occured in Slider add");
				redirect('/admin/slider');
			}			
		}
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');
		$this->load->view('admin/slider/slider_add');
		$this->load->view('admin/footer');		
	}
	public function slider_edit($id)
	{
		$data['sliders']=$this->Slider_model->getslideritem(array('id'=>$id));
		$this->load->view('admin/header');
        $this->load->view('admin/sidebar');
	    $this->load->view('admin/slider/slider_edit', $data);
		$this->load->view('admin/footer');
	}
	public function slider_update()
	{
		$logged_in_user = $this->session->userdata('logged_in');   
		if (!$logged_in_user)
		{
			redirect('login');
		}
		$this->load->helper('form');
		$this->load->library('form_validation');		
		$this->form_validation->set_rules('slidertitle', 'Slider Title', 'required');
		if ($this->form_validation->run() === true)
        {
			if(!empty($_FILES["sliderimage"]['name']))
			{
				$data=array(
				'title'=>$this->input->post('slidertitle'),
				'image'=>$_FILES["sliderimage"]['name'],
				);
			}
			else
			{
				$data=array(
				'title'=>$this->input->post('slidertitle'),
				);
			}
			$user= $this->Slider_model->updateslideritem(array('id'=>$this->input->post('id')),$data);
			if(!empty($_FILES["sliderimage"]['name']))
			{
				$condition=array("id"=>$this->input->post('id'));					
				$image_detail= $this->Slider_model->fetch_slider($condition);
				$unlinkimg=$image_detail["image"];
				unlink(base_url("Newassets/images/".$unlinkimg));
				unlink(base_url("Newassets/images/thumbnail/".$unlinkimg));
				$name = $_FILES["sliderimage"]["name"];
				$ext = end((explode(".", $name))); 
				// Code for File Upload
				$config['upload_path']= './Newassets/images/';        
				$config['allowed_types']= 'gif|jpg|png|jpeg';
				$config['overwrite'] = TRUE;
				$config['file_name'] = $_FILES["sliderimage"]['name'];
				$this->load->library('upload');
				$this->upload->initialize($config);
				if ( ! $this->upload->do_upload('sliderimage'))
				{
					// On File Upload Fail
					$this->session->set_flashdata('fail',$this->upload->display_errors());
					redirect('/admin/slider');
				}
				else
				{
					$file_data = $this->upload->data();				
					$source_path = DIRECTORY_PATH.'Newassets/images/'.$file_data["file_name"];
					$target_path = DIRECTORY_PATH.'Newassets/images/thumbnail/'.$file_data["file_name"];
					$thumb = PhpThumbFactory::create($source_path);
					$thumb->adaptiveResize(280, 250);
					$thumb->save($target_path, $ext);
					if($user)
					{					
						$this->session->set_flashdata('success',"Slider Data Successfully Updated");
						redirect('/admin/slider');
					}
					else
					{
						$this->session->set_flashdata('fail',"Error Occured in Slider Update");
						redirect('/admin/slider');
					}					
				}				
			}
			if($user)
			{					
				$this->session->set_flashdata('success',"Slider Data Successfully Updated");
				redirect('/admin/slider');
			}
			else
			{
				$this->session->set_flashdata('fail',"Error Occured in Slider Update");
				redirect('/admin/slider');
			}			
		}
	}
	public function slider_delete($id)
	{
		if($this->Slider_model->removeslideritem(array('id'=>$id)))
		{
			$this->session->set_flashdata('success',"Slider Data Successfully Deleted");
			redirect('/admin/slider');
		}
		else
		{
			$this->session->set_flashdata('fail',"Error Occured in Delete Slider Data");
			redirect('/admin/slider');
		}
	}
}