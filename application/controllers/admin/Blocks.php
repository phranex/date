<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'/PHPThumb/ThumbLib.inc.php';
/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Blocks extends CI_Controller {
    public function __construct()
	{		
            parent::__construct();
            $this->load->library(array('session'));
            $this->load->helper(array('url'));
            $this->load->library('form_validation');
            $this->load->helper('form');
            $this->load->model('blocks_model');
            $this->load->model('admin_model');
            date_default_timezone_set('Asia/Kolkata');
            $logged_in_user = $this->session->userdata('logged_in');   
            if ($logged_in_user)
            {
                $is_admin = $this->session->userdata('is_admin');
                if(!$is_admin)
                {
                    redirect('admin/login');
                }
            }
	}
    public function index()
	{
		$default_language=$this->admin_model->get_default_language();
		$de_lan=$default_language['status'];
		//get deault language id
		$de_lan_id=$this->admin_model->get_language_id($de_lan);
		$languages=$this->admin_model->fetch_language();
		$data['blocks']=$this->blocks_model->fetch_block();
		$data['languages']=$languages;
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');
		$this->load->view('admin/blocks/blocks_list',$data);
		$this->load->view('admin/footer');
	}
	public function blocks_edit($id)
	{
		$data['blocks']=$this->blocks_model->fetch_block(array('block.id'=>$id));
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');
		$this->load->view('admin/blocks/blocks_edit',$data);
		$this->load->view('admin/footer');
	}
	public function blocks_update()
	{
		$logged_in_user = $this->session->userdata('logged_in');   
		if (!$logged_in_user)
		{
			redirect('login');
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Block title', 'required');
		$this->form_validation->set_rules('contant', 'Block Contain', 'required');
		if ($this->form_validation->run() === true)
        {			
			$data=array(
				'title'=>$this->input->post('title'),
				'contain'=>$this->input->post('contant'),
			);
			$parent_id=$this->input->post('id');
			$id=$this->blocks_model->update_block(array('id'=>$parent_id),$data);
			if($id)
			{
				$this->session->set_flashdata('success',"Block Successfully Updated");
				redirect('/admin/blocks');
			}
			else
			{
				$this->session->set_flashdata('fail',"Error Occured in Block Block");
				redirect('/admin/blocks');
			}
		}
	}
}