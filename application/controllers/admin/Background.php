<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'/PHPThumb/ThumbLib.inc.php';
/**
 * Background class.
 * 
 * @extends CI_Controller
 */
class Background extends CI_Controller {
    public function __construct()
	{		
            parent::__construct();
            $this->load->library(array('session'));
            $this->load->helper(array('url'));
            $this->load->library('form_validation');
            $this->load->helper('form');
            $this->load->model('background_model');
            $this->load->model('admin_model');
            date_default_timezone_set('Asia/Kolkata');
            $logged_in_user = $this->session->userdata('logged_in');   
            if ($logged_in_user)
            {
                $is_admin = $this->session->userdata('is_admin');
                if(!$is_admin)
                {
                    redirect('admin/login');
                }
            }
	}
    public function index()
	{
		$data['background']=$this->background_model->getbackgrounditem();
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');
		$this->load->view('admin/background/background',$data);
		$this->load->view('admin/footer');
	}
	public function update_background()
	{
		$logged_in_user = $this->session->userdata('logged_in');   
		if (!$logged_in_user)
		{
			redirect('login');
		}

		if(!empty($_FILES["image_1"]['name']))
		{
			$condition1=array("id"=>$this->input->post('bg_1'));
			$image_detail= $this->background_model->getbackgrounditem($condition1);
			$unlinkimg=$image_detail["background_url"];
			$name = $_FILES["image_1"]["name"];				
			$ext = end((explode(".", $name))); 
			// Code for File Upload
			$config['upload_path']= './images/bg/';        
			$config['allowed_types']= 'jpg|png|jpeg';
			$config['overwrite'] = TRUE;
			$config['file_name'] = $_FILES["image_1"]['name'];
			$this->load->library('upload');
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image_1'))
			{
				// On File Upload Fail
				$this->session->set_flashdata('fail',$this->upload->display_errors());
				redirect('/admin/background');
			}
			else
			{
				$name1=base_url().'images/bg/'.$_FILES["image_1"]['name'];
				
				$data=array('background_url'=>$name1);
				$user = $this->background_model->updatebackground($condition1,$data);			
			}
		}
		if(!empty($_FILES["image_2"]['name']))
		{
			$condition2=array("id"=>$this->input->post('bg_2'));					
			$image_detail= $this->background_model->getbackgrounditem($condition2);
			$unlinkimg=$image_detail["background_url"];
			$name = $_FILES["image_2"]["name"];				
			$ext = end((explode(".", $name))); 
			// Code for File Upload
			$config['upload_path']= './images/bg/';        
			$config['allowed_types']= 'jpg|png|jpeg';
			$config['overwrite'] = TRUE;
			$config['file_name'] = $_FILES["image_2"]['name'];
			$this->load->library('upload');
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image_2'))
			{
				// On File Upload Fail
				$this->session->set_flashdata('fail',$this->upload->display_errors());
				redirect('/admin/background');
			}
			else
			{
				$name2=base_url().'images/bg/'.$_FILES["image_2"]['name'];
				
				$data=array('background_url'=>$name2);
				$user = $this->background_model->updatebackground($condition2,$data);			
			}
		}
		if(!empty($_FILES["image_3"]['name']))
		{
			$condition3=array("id"=>$this->input->post('bg_3'));					
			$image_detail= $this->background_model->getbackgrounditem($condition3);
			$unlinkimg=$image_detail["background_url"];
			$name = $_FILES["image_3"]["name"];				
			$ext = end((explode(".", $name))); 
			// Code for File Upload
			$config['upload_path']= './images/bg/';        
			$config['allowed_types']= 'jpg|png|jpeg';
			$config['overwrite'] = TRUE;
			$config['file_name'] = $_FILES["image_3"]['name'];
			$this->load->library('upload');
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image_3'))
			{
				// On File Upload Fail
				$this->session->set_flashdata('fail',$this->upload->display_errors());
				redirect('/admin/background');
			}
			else
			{
				$name3=base_url().'images/bg/'.$_FILES["image_3"]['name'];
				
				$data=array('background_url'=>$name3);
				$user = $this->background_model->updatebackground($condition3,$data);			
			}
		}
		if(!empty($_FILES["image_4"]['name']))
		{
			$condition4=array("id"=>$this->input->post('bg_4'));					
			$image_detail= $this->background_model->getbackgrounditem($condition4);
			$unlinkimg=$image_detail["background_url"];
			$name = $_FILES["image_4"]["name"];				
			$ext = end((explode(".", $name))); 
			// Code for File Upload
			$config['upload_path']= './images/bg/';        
			$config['allowed_types']= 'jpg|png|jpeg';
			$config['overwrite'] = TRUE;
			$config['file_name'] = $_FILES["image_4"]['name'];
			$this->load->library('upload');
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image_4'))
			{
				// On File Upload Fail
				$this->session->set_flashdata('fail',$this->upload->display_errors());
				redirect('/admin/background');
			}
			else
			{
				$name4=base_url().'images/bg/'.$_FILES["image_4"]['name'];
				
				$data=array('background_url'=>$name4);
				$user = $this->background_model->updatebackground($condition4,$data);			
			}
		}
		if(!empty($_FILES["image_5"]['name']))
		{
			$condition5=array("id"=>$this->input->post('bg_5'));					
			$image_detail= $this->background_model->getbackgrounditem($condition5);
			$unlinkimg=$image_detail["background_url"];
			$name = $_FILES["image_5"]["name"];				
			$ext = end((explode(".", $name))); 
			// Code for File Upload
			$config['upload_path']= './images/bg/';        
			$config['allowed_types']= 'jpg|png|jpeg';
			$config['overwrite'] = TRUE;
			$config['file_name'] = $_FILES["image_5"]['name'];
			$this->load->library('upload');
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image_5'))
			{
				// On File Upload Fail
				$this->session->set_flashdata('fail',$this->upload->display_errors());
				redirect('/admin/background');
			}
			else
			{
				$name5=base_url().'images/bg/'.$_FILES["image_5"]['name'];
				
				$data=array('background_url'=>$name5);
				$user = $this->background_model->updatebackground($condition5,$data);			
			}
		}
		if(!empty($_FILES["image_6"]['name']))
		{
			$condition6=array("id"=>$this->input->post('bg_6'));					
			$image_detail= $this->background_model->getbackgrounditem($condition6);
			$unlinkimg=$image_detail["background_url"];
			$name = $_FILES["image_6"]["name"];				
			$ext = end((explode(".", $name))); 
			// Code for File Upload
			$config['upload_path']= './images/bg/';        
			$config['allowed_types']= 'jpg|png|jpeg';
			$config['overwrite'] = TRUE;
			$config['file_name'] = $_FILES["image_6"]['name'];
			$this->load->library('upload');
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image_6'))
			{
				// On File Upload Fail
				$this->session->set_flashdata('fail',$this->upload->display_errors());
				redirect('/admin/background');
			}
			else
			{
				$name6=base_url().'images/bg/'.$_FILES["image_6"]['name'];
				
				$data=array('background_url'=>$name6);
				$user = $this->background_model->updatebackground($condition6,$data);			
			}
		}
		if(!empty($_FILES["image_7"]['name']))
		{
			$condition7=array("id"=>$this->input->post('bg_7'));					
			$image_detail= $this->background_model->getbackgrounditem($condition7);
			$unlinkimg=$image_detail["background_url"];
			$name = $_FILES["image_7"]["name"];				
			$ext = end((explode(".", $name))); 
			// Code for File Upload
			$config['upload_path']= './images/bg/';        
			$config['allowed_types']= 'jpg|png|jpeg';
			$config['overwrite'] = TRUE;
			$config['file_name'] = $_FILES["image_7"]['name'];
			$this->load->library('upload');
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image_7'))
			{
				// On File Upload Fail
				$this->session->set_flashdata('fail',$this->upload->display_errors());
				redirect('/admin/background');
			}
			else
			{
				$name7=base_url().'images/bg/'.$_FILES["image_7"]['name'];
				
				$data=array('background_url'=>$name7);
				$user = $this->background_model->updatebackground($condition7,$data);			
			}
		}
		if(!empty($_FILES["image_8"]['name']))
		{
			$condition8=array("id"=>$this->input->post('bg_8'));					
			$image_detail= $this->background_model->getbackgrounditem($condition8);
			$unlinkimg=$image_detail["background_url"];
			$name = $_FILES["image_8"]["name"];				
			$ext = end((explode(".", $name))); 
			// Code for File Upload
			$config['upload_path']= './images/bg/';        
			$config['allowed_types']= 'jpg|png|jpeg';
			$config['overwrite'] = TRUE;
			$config['file_name'] = $_FILES["image_8"]['name'];
			$this->load->library('upload');
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image_8'))
			{
				// On File Upload Fail
				$this->session->set_flashdata('fail',$this->upload->display_errors());
				redirect('/admin/background');
			}
			else
			{
				$name8=base_url().'images/bg/'.$_FILES["image_8"]['name'];
				
				$data=array('background_url'=>$name8);
				$user = $this->background_model->updatebackground($condition8,$data);			
			}
		}
		$this->session->set_flashdata('success',"Background Images Successfully Updated");
		redirect('/admin/background');
	}
}