<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'/PHPThumb/ThumbLib.inc.php';
/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Comingsoon extends CI_Controller {
    public function __construct()
	{		
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->model('comingsoon_model');
		$this->load->model('admin_model');
		date_default_timezone_set('Asia/Kolkata');
		$logged_in_user = $this->session->userdata('logged_in');   
		if ($logged_in_user)
		{
			$is_admin = $this->session->userdata('is_admin');
			if(!$is_admin)
			{
				redirect('admin/login');
			}
		}
	}	
	public function index()
	{
		$data['comingsoon']=$this->comingsoon_model->getcomingsoon();
		$this->load->view('admin/header');
        $this->load->view('admin/sidebar');
	    $this->load->view('admin/comingsoon/comingsoon', $data);
		$this->load->view('admin/footer');
	}
    public function comingsoon_update()
	{
		$logged_in_user = $this->session->userdata('logged_in');   
		if (!$logged_in_user)
		{
			redirect('login');
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('comingsoon_title', 'Coming Soon Main Title', 'required');
        $this->form_validation->set_rules('comingsoon_date', 'Enter Coming Soon Date', 'required');
		$files = $_FILES;
		if ($this->form_validation->run() === true)
        {
			if(!empty($files["comingsoon_image"]['name']))
			{
				$data=array(
					'title'=>$this->input->post('comingsoon_title'),
					'date'=>$this->input->post('comingsoon_date'),
					'comingsoon_image'=>$files["comingsoon_image"],
				);
			}
			else
			{
				$data=array(
					'title'=>$this->input->post('comingsoon_title'),
					'date'=>$this->input->post('comingsoon_date'),
				);
			}
			$parent_id=$this->input->post('id');
			$id=$this->comingsoon_model->updatecomingsoon(array('id'=>$parent_id),$data);
			if(!empty($files["comingsoon_image"]['name']))
			{
				$name=$files["comingsoon_image"]['name'];
				$ext = end((explode(".", $name))); 
				// Code for File Upload
				$config['upload_path']= './Newassets/images/';        
				$config['allowed_types']= 'gif|jpg|png|jpeg';
				$config['overwrite'] = TRUE;
				$config['file_name'] = $_FILES["comingsoon_image"]['name'];
				$this->load->library('upload');
				$this->upload->initialize($config);
				if ( ! $this->upload->do_upload('comingsoon_image'))
				{
					// On File Upload Fail
					$this->session->set_flashdata('fail',$this->upload->display_errors());
					redirect('/admin/comingsoon');
				}
				else
				{
					$file_data = $this->upload->data();				
					$source_path = DIRECTORY_PATH.'Newassets/images/'.$file_data["file_name"];
					$target_path = DIRECTORY_PATH.'Newassets/images/thumbnail/'.$file_data["file_name"];
					$thumb = PhpThumbFactory::create($source_path);
					$thumb->adaptiveResize(280, 250);
					if($thumb->save($target_path, $ext))
					{					
					}
					else
					{
					}					
				}
			}
			if($id)
			{	
				$this->session->set_flashdata('success',"Coming Soon Successfully Updated");
				redirect('/admin/comingsoon');
			}
			else
			{
				$this->session->set_flashdata('fail',"Error Occured in Coming Soon Update");
				redirect('/admin/comingsoon');
			}
		}
	}
}