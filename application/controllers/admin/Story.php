<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'/PHPThumb/ThumbLib.inc.php';
/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Story extends CI_Controller {
    public function __construct()
	{		
        parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->library('form_validation');
        $this->load->helper('form');
		$this->load->model('SiteErrorLog');
		$this->load->model('admin_model');
		$this->load->model('testimonial_model');
		$this->load->model('blog_model');
		$this->load->model('story_model');
		$this->load->model('Slider_model');
		$this->load->model('error404_model');
		$this->load->model('comingsoon_model');
		$this->load->model('members_model');
		date_default_timezone_set('Asia/Kolkata');
		$logged_in_user = $this->session->userdata('logged_in');   
		if ($logged_in_user)
		{
			$is_admin = $this->session->userdata('is_admin');
			if(!$is_admin)
			{
				redirect('login');
			}
		}
	}
	public function index()
	{
		$data['stories']=$this->story_model->fetch_stories();
		$this->load->view('admin/header');
        $this->load->view('admin/sidebar');
	    $this->load->view('admin/stories/stories_list', $data);
		$this->load->view('admin/footer');
	}
	public function new_stories()
	{
		$logged_in_user = $this->session->userdata('logged_in');   
		if (!$logged_in_user)
		{
			redirect('login');
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('storiestitle', 'Story Name', 'required');
		$this->form_validation->set_rules('storiesdescrip', 'Story Description', 'required');
		if ($this->form_validation->run() === true)
        {	
			if(!empty($_FILES["storiesimage"]['name']))
			{
				$data=array(
					'title'=>$this->input->post('storiestitle'),
					'description'=>$this->input->post('storiesdescrip'),
					'image'=>$_FILES["storiesimage"]['name']
				);
			}
			else
			{
				$data=array(
					'title'=>$this->input->post('storiestitle'),
					'description'=>$this->input->post('storiesdescrip'),				
				);
			}
			$user= $this->story_model->setstoriesitem($data);
			if(!empty($_FILES["storiesimage"]['name']))
			{
				$name = $_FILES["storiesimage"]["name"];
				$ext = end((explode(".", $name))); 
				// Code for File Upload
				$config['upload_path']= './Newassets/images/';        
				$config['allowed_types']= 'gif|jpg|png|jpeg';
				$config['overwrite'] = TRUE;
				$config['file_name'] = $_FILES["storiesimage"]['name'];
				$this->load->library('upload');
				$this->upload->initialize($config);
				if ( ! $this->upload->do_upload('storiesimage'))
				{
					// On File Upload Fail
					$this->session->set_flashdata('fail',$this->upload->display_errors());
					redirect('/admin/admin/blog');
				}
				else
				{
					$file_data = $this->upload->data();				
					$source_path = DIRECTORY_PATH.'Newassets/images/'.$file_data["file_name"];
					$target_path = DIRECTORY_PATH.'Newassets/images/thumbnail/'.$file_data["file_name"];
					$thumb = PhpThumbFactory::create($source_path);
					$thumb->adaptiveResize(280, 250);
					$thumb->save($target_path, $ext);
					if($user)
					{					
						$this->session->set_flashdata('success',"Stories Successfully Added");
						redirect('/admin/story/');
					}
					else
					{
						$this->session->set_flashdata('fail',"Error Occured in Stories add");
						redirect('/admin/story/');
					}					
				}				
			}
			if($user)
			{					
				$this->session->set_flashdata('success',"Stories Successfully Added");
				redirect('/admin/story/');
			}
			else
			{
				$this->session->set_flashdata('fail',"Error Occured in Stories add");
				redirect('/admin/story/');
			}
		}
		$this->load->view('admin/header');
        $this->load->view('admin/sidebar');
	    $this->load->view('admin/stories/stories_add');
		$this->load->view('admin/footer');
	}
	public function stories_edit($id)
	{
		$data['stories']=$this->story_model->getstoriesitem(array('stories.id'=>$id));
		$this->load->view('admin/header');
        $this->load->view('admin/sidebar');
	    $this->load->view('admin/stories/stories_edit', $data);
		$this->load->view('admin/footer');
	}
	public function stories_update()
	{
		$logged_in_user = $this->session->userdata('logged_in');   
		if (!$logged_in_user)
		{
			redirect('login');
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('storiestitle', 'Story Name', 'required');
		$this->form_validation->set_rules('storiesdescrip', 'Story Description', 'required');
		if ($this->form_validation->run() === true)
        {	
			if(!empty($_FILES["storiesimage"]['name']))
			{
				$data=array(
				'title'=>$this->input->post('storiestitle'),
				'description'=>$this->input->post('storiesdescrip'),
				'image'=>$_FILES["storiesimage"]['name']
				);
			}
			else
			{
				$data=array(
				'title'=>$this->input->post('storiestitle'),
				'description'=>$this->input->post('storiesdescrip'),				
				);
			}
			$user= $this->story_model->updatestoriesitem(array('id'=>$this->input->post('id')),$data);
			if(!empty($_FILES["storiesimage"]['name']))
			{
				$condition=array("id"=>$this->input->post('id'));					
				$image_detail= $this->story_model->fetch_stories($condition);
				$unlinkimg=$image_detail["image"];
				unlink(base_url("Newassets/images/".$unlinkimg));
				unlink(base_url("Newassets/images/thumbnail/".$unlinkimg));
				$name = $_FILES["storiesimage"]["name"];
				$ext = end((explode(".", $name))); 
				// Code for File Upload
				$config['upload_path']= './Newassets/images/';        
				$config['allowed_types']= 'gif|jpg|png|jpeg';
				$config['overwrite'] = TRUE;
				$config['file_name'] = $_FILES["storiesimage"]['name'];
				$this->load->library('upload');
				$this->upload->initialize($config);
				if ( ! $this->upload->do_upload('storiesimage'))
				{
					// On File Upload Fail
					$this->session->set_flashdata('fail',$this->upload->display_errors());
					redirect('/admin/admin/blog');
				}
				else
				{
					$file_data = $this->upload->data();				
					$source_path = DIRECTORY_PATH.'Newassets/images/'.$file_data["file_name"];
					$target_path = DIRECTORY_PATH.'Newassets/images/thumbnail/'.$file_data["file_name"];
					$thumb = PhpThumbFactory::create($source_path);
					$thumb->adaptiveResize(280, 250);
					$thumb->save($target_path, $ext);
					if($user)
					{					
						$this->session->set_flashdata('success',"Stories Successfully Updated");
						redirect('/admin/story/');
					}
					else
					{
						$this->session->set_flashdata('fail',"Error Occured in Stories Update");
						redirect('/admin/story/');
					}					
				}				
			}
			if($user)
			{					
				$this->session->set_flashdata('success',"Stories Successfully Updated");
				redirect('/admin/story/');
			}
			else
			{
				$this->session->set_flashdata('fail',"Error Occured in Stories Update");
				redirect('/admin/story/');
			}
		}
	}
	public function stories_delete($id)
	{
		if($this->story_model->removestoriesitem(array('id'=>$id)))
		{
			$this->session->set_flashdata('success',"Stories Successfully Deleted");
			redirect('/admin/story/');
		}
		else
		{
			$this->session->set_flashdata('fail',"Error Occured in Delete Stories");
			redirect('/admin/story/');
		}
	}
}