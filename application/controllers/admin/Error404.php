<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'/PHPThumb/ThumbLib.inc.php';
/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Error404 extends CI_Controller {
    public function __construct()
	{		
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->model('error404_model');
		$this->load->model('admin_model');
		date_default_timezone_set('Asia/Kolkata');
		$logged_in_user = $this->session->userdata('logged_in');   
		if ($logged_in_user)
		{
			$is_admin = $this->session->userdata('is_admin');
			if(!$is_admin)
			{
				redirect('admin/login');
			}
		}
	}
    public function index()
	{
		$data['error404']=$this->error404_model->geterror404();
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');
		$this->load->view('admin/error404/error404', $data);
		$this->load->view('admin/footer');
	}
    public function error404_update()
	{
		$logged_in_user = $this->session->userdata('logged_in'); 
		if (!$logged_in_user)
		{
			redirect('login');
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('error_title', 'Error Main Title', 'required');
        $this->form_validation->set_rules('error_sub_title', 'Error Sub Title', 'required');
		$this->form_validation->set_rules('error_description', 'Error Description', 'required');
		if ($this->form_validation->run() === true)
        {
			if(!empty($_FILES["error_image"]['name']))
			{
				$data=array(
					'title'=>$this->input->post('error_title'),
					'sub_title'=>$this->input->post('error_sub_title'),
					'description'=>$this->input->post('error_description'),
					'error_image'=>$_FILES["error_image"]['name'],
				);
			}
			else
			{
				$data=array(
					'title'=>$this->input->post('error_title'),
					'sub_title'=>$this->input->post('error_sub_title'),
					'description'=>$this->input->post('error_description'),
				);
			}
            $parent_id=$this->input->post('id');
			$id=$this->error404_model->updateerror404(array('id'=>$parent_id),$data);
			if(!empty($_FILES["error_image"]['name']))
			{
				$name = $_FILES['error_image']['name'];
				$ext = end((explode(".", $name))); 
				// Code for File Upload
				$config['upload_path']= './Newassets/images/';        
				$config['allowed_types']= 'gif|jpg|png|jpeg';
				$config['overwrite'] = TRUE;
				$config['file_name'] = $_FILES["error_image"]['name'];
				$this->load->library('upload');
				$this->upload->initialize($config);
				if ( ! $this->upload->do_upload('error_image'))
				{
					// On File Upload Fail
					$this->session->set_flashdata('fail',$this->upload->display_errors());
					redirect('/admin/admin/error404');
				}
				else
				{
					$file_data = $this->upload->data();				
					$source_path = DIRECTORY_PATH.'Newassets/images/'.$file_data["file_name"];
					$target_path = DIRECTORY_PATH.'Newassets/images/thumbnail/'.$file_data["file_name"];
					$thumb = PhpThumbFactory::create($source_path);
					$thumb->adaptiveResize(280, 250);
					if($thumb->save($target_path, $ext))
					{					
					}
					else
					{
					}					
				}
			}
			if($id)
			{
				$this->session->set_flashdata('success',"Error404 Successfully Updated");
				redirect('/admin/error404');
			}
			else
			{
				$this->session->set_flashdata('fail',"Error Occured in Error404 Update");
				redirect('/admin/error404');
			}
		}
	}
}