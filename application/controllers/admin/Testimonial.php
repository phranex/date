<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'/PHPThumb/ThumbLib.inc.php';
/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Testimonial extends CI_Controller {
    public function __construct()
	{		
            parent::__construct();
            $this->load->library(array('session'));
            $this->load->helper(array('url'));
            $this->load->library('form_validation');
            $this->load->helper('form');
            $this->load->model('testimonial_model');
            $this->load->model('admin_model');
            date_default_timezone_set('Asia/Kolkata');
            $logged_in_user = $this->session->userdata('logged_in');   
            if ($logged_in_user)
            {
                $is_admin = $this->session->userdata('is_admin');
                if(!$is_admin)
                {
                    redirect('admin/login');
                }
            }
	}
    public function index()
	{
		$data['testimonial']=$this->testimonial_model->fetch_testimonial();
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');
		$this->load->view('admin/testimonial/testimonial_list', $data);
		$this->load->view('admin/footer');
	}
	public function new_testimonial()
	{
		$logged_in_user = $this->session->userdata('logged_in');   
		if (!$logged_in_user)
		{
			redirect('login');
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('authorname', 'Author Name', 'required');
		$this->form_validation->set_rules('authordescrip', 'Author Description', 'required');
		if ($this->form_validation->run() === true)
        {
			if(!empty($_FILES["authorimage"]['name']))
			{
				$data=array(
				'author'=>$this->input->post('authorname'),
				'description'=>$this->input->post('authordescrip'),
				'author_image'=>$_FILES["authorimage"]['name']
				);
			}
			else
			{
				$data=array(
				'author'=>$this->input->post('authorname'),
				'description'=>$this->input->post('authordescrip'),				
				);
			}
			$user= $this->testimonial_model->settestomonial($data);
			if(!empty($_FILES["authorimage"]['name']))
			{
				$name = $_FILES["authorimage"]["name"];				
				$ext = end((explode(".", $name))); 
				// Code for File Upload
				$config['upload_path']= './Newassets/images/';        
				$config['allowed_types']= 'gif|jpg|png|jpeg';
				$config['overwrite'] = TRUE;
				$config['file_name'] = $_FILES["authorimage"]['name'];
				$this->load->library('upload');
				$this->upload->initialize($config);
				if ( ! $this->upload->do_upload('authorimage'))
				{
					// On File Upload Fail
					$this->session->set_flashdata('fail',$this->upload->display_errors());
					redirect('/admin/testimonial');
				}
				else
				{
					$file_data = $this->upload->data();				
					$source_path = DIRECTORY_PATH.'Newassets/images/'.$file_data["file_name"];
					$target_path = DIRECTORY_PATH.'Newassets/images/thumbnail/'.$file_data["file_name"];
					$thumb = PhpThumbFactory::create($source_path);
					$thumb->adaptiveResize(280, 250);
					if($thumb->save($target_path, $ext))
					{					
						$this->session->set_flashdata('success',"Testimonial Successfully Added");
						redirect('/admin/testimonial');
					}
					else
					{
						$this->session->set_flashdata('fail',"Error Occured in Testimonial Add");
						redirect('/admin/testimonial');
					}					
				}
			}
			if($user)
			{					
				$this->session->set_flashdata('success',"Testimonial Successfully Added");
				redirect('/admin/testimonial');
			}
			else
			{
				$this->session->set_flashdata('fail',"Error Occured in Testimonial Add");
				redirect('/admin/testimonial');
			}
		}
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');
		$this->load->view('admin/testimonial/testimonial_add');
		$this->load->view('admin/footer');
	}
	public function testimonial_edit($id)
	{
		$data['testimonial']=$this->testimonial_model->gettestimonial(array('id'=>$id));
		$this->load->view('admin/header');
        $this->load->view('admin/sidebar');
	    $this->load->view('admin/testimonial/testimonial_edit', $data);
		$this->load->view('admin/footer');
	}
	public function testimonial_update()
	{
		$logged_in_user = $this->session->userdata('logged_in');   
		if (!$logged_in_user)
		{
			redirect('login');
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('authorname', 'Author Name', 'required');
		$this->form_validation->set_rules('authordescrip', 'Author Description', 'required');
		if ($this->form_validation->run() === true)
        {
			if(!empty($_FILES["authorimage"]['name']))
			{
				$data=array(
				'author'=>$this->input->post('authorname'),
				'description'=>$this->input->post('authordescrip'),
				'author_image'=>$_FILES["authorimage"]['name']
				);
			}
			else
			{
				$data=array(
				'author'=>$this->input->post('authorname'),
				'description'=>$this->input->post('authordescrip'),				
				);
			}
			$user= $this->testimonial_model->updatetestimonial(array('id'=>$this->input->post('id')),$data);
			if(!empty($_FILES["authorimage"]['name']))
			{
				$condition=array("id"=>$this->input->post('id'));					
				$image_detail= $this->testimonial_model->gettestimonial($condition);
				$unlinkimg=$image_detail["author_image"];
				$name = $_FILES["authorimage"]["name"];				
				$ext = end((explode(".", $name))); 
				// Code for File Upload
				$config['upload_path']= './Newassets/images/';        
				$config['allowed_types']= 'gif|jpg|png|jpeg';
				$config['overwrite'] = TRUE;
				$config['file_name'] = $_FILES["authorimage"]['name'];
				$this->load->library('upload');
				$this->upload->initialize($config);
				if ( ! $this->upload->do_upload('authorimage'))
				{
					// On File Upload Fail
					$this->session->set_flashdata('fail',$this->upload->display_errors());
					redirect('/admin/testimonial');
				}
				else
				{
					$file_data = $this->upload->data();				
					$source_path = DIRECTORY_PATH.'Newassets/images/'.$file_data["file_name"];
					$target_path = DIRECTORY_PATH.'Newassets/images/thumbnail/'.$file_data["file_name"];
					$thumb = PhpThumbFactory::create($source_path);
					$thumb->adaptiveResize(280, 250);
					if($thumb->save($target_path, $ext))
					{					
						$this->session->set_flashdata('success',"Testimonial Successfully Updated");
						redirect('/admin/testimonial');
					}
					else
					{
						$this->session->set_flashdata('fail',"Error Occured in Testimonial Update");
						redirect('/admin/testimonial');
					}					
				}
			}
			if($user)
			{					
				$this->session->set_flashdata('success',"Testimonial Successfully Updated");
				redirect('/admin/testimonial');
			}
			else
			{
				$this->session->set_flashdata('fail',"Error Occured in Testimonial Update");
				redirect('/admin/testimonial');
			}
		}
	}
	public function testimonial_delete($id)
	{		
		if($this->testimonial_model->removetestimonial(array('id'=>$id)))
		{
			$this->session->set_flashdata('success',"Testimonial Successfully Deleted");
			redirect('/admin/testimonial');
		}
		else
		{
			$this->session->set_flashdata('fail',"Error Occured in Delete Testimonial");
			redirect('/admin/testimonial');
		}
	}
}