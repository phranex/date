<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'/PHPThumb/ThumbLib.inc.php';
/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Ourmembers extends CI_Controller {
    public function __construct()
	{		
            parent::__construct();
            $this->load->library(array('session'));
            $this->load->helper(array('url'));
            $this->load->library('form_validation');
            $this->load->helper('form');
            $this->load->model('members_model');
            $this->load->model('admin_model');
            date_default_timezone_set('Asia/Kolkata');
            $logged_in_user = $this->session->userdata('logged_in');   
            if ($logged_in_user)
            {
                $is_admin = $this->session->userdata('is_admin');
                if(!$is_admin)
                {
                    redirect('admin/login');
                }
            }
	}
    public function index(){
        $data['members']=$this->members_model->getMembers();
        $this->load->view('admin/header');
        $this->load->view('admin/sidebar');
        $this->load->view('admin/members/list', $data);
        $this->load->view('admin/footer');
    }
    public function new_member()
	{
		$logged_in_user = $this->session->userdata('logged_in');   
		if (!$logged_in_user)
		{
			redirect('login');
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('member_name', 'Member Name', 'required');
        $this->form_validation->set_rules('designation', 'Designation', 'required');
        $this->form_validation->set_rules('member_detail', 'Member Detail', 'required');
		if ($this->form_validation->run() === true)
        {
			if(!empty($_FILES["member_image"]['name']))
			{
				$data=array(
					'member_name'=>$this->input->post('member_name'),
					'designation'=>$this->input->post('designation'),
					'member_detail'=>$this->input->post('member_detail'),
					'member_image'=>$_FILES["member_image"]['name'],
					'facebook_url'=>$this->input->post('facebook_url'),
					'twitter_url'=>$this->input->post('twitter_url'),
					'google_url'=>$this->input->post('google_url'),
					'dribble_url'=>$this->input->post('dribble_url'),
				);
			}
			else
			{
				$data=array(
					'member_name'=>$this->input->post('member_name'),
					'designation'=>$this->input->post('designation'),
					'member_detail'=>$this->input->post('member_detail'),
					'facebook_url'=>$this->input->post('facebook_url'),
					'twitter_url'=>$this->input->post('twitter_url'),
					'google_url'=>$this->input->post('google_url'),
					'dribble_url'=>$this->input->post('dribble_url'),
				);
			}
			$user= $this->members_model->addMember($data);
			if(!empty($_FILES["member_image"]['name']))
			{
				$name = $_FILES["member_image"]["name"];
				$ext = end((explode(".", $name))); 
				// Code for File Upload
				$config['upload_path']= './Newassets/images/';        
				$config['allowed_types']= 'gif|jpg|png|jpeg';
				$config['overwrite'] = TRUE;
				$config['file_name'] = $_FILES["member_image"]['name'];
				$this->load->library('upload');
				$this->upload->initialize($config);
				if ( ! $this->upload->do_upload('member_image'))
				{
					// On File Upload Fail
					$this->session->set_flashdata('fail',$this->upload->display_errors());
					redirect('/admin/ourmembers');
				}
				else
				{
					$file_data = $this->upload->data();				
					$source_path = DIRECTORY_PATH.'Newassets/images/'.$file_data["file_name"];
					$target_path = DIRECTORY_PATH.'Newassets/images/thumbnail/'.$file_data["file_name"];
					$thumb = PhpThumbFactory::create($source_path);
					$thumb->adaptiveResize(280, 250);
					if($thumb->save($target_path, $ext))
					{					
						$this->session->set_flashdata('success',"Member Successfully Added");
						redirect('/admin/ourmembers');
					}
					else
					{
						$this->session->set_flashdata('fail',"Error Occured in Member add");
						redirect('/admin/ourmembers');
					}					
				}
			}
			if($user)
			{					
				$this->session->set_flashdata('success',"Member Successfully Added");
				redirect('/admin/ourmembers');
			}
			else
			{
				$this->session->set_flashdata('fail',"Error Occured in Member add");
				redirect('/admin/ourmembers');
			}
		}
		$this->load->view('admin/header');
        $this->load->view('admin/sidebar');
	    $this->load->view('admin/members/add');
		$this->load->view('admin/footer');
	}
    public function member_edit($id)
	{
		$data['member']=$this->members_model->getMember($id);
		$this->load->view('admin/header');
        $this->load->view('admin/sidebar');
	    $this->load->view('admin/members/edit', $data);
		$this->load->view('admin/footer');
	}
    public function member_update()
	{
		$logged_in_user = $this->session->userdata('logged_in');   
		if (!$logged_in_user)
		{
			redirect('login');
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('member_name', 'Member Name', 'required');
        $this->form_validation->set_rules('designation', 'Designation', 'required');
        $this->form_validation->set_rules('member_detail', 'Member Detail', 'required');
		if ($this->form_validation->run() === true)
        {
			if(!empty($_FILES["member_image"]['name']))
			{
				$data=array(
				'member_name'=>$this->input->post('member_name'),
                'designation'=>$this->input->post('designation'),
                'member_detail'=>$this->input->post('member_detail'),
				'member_image'=>$_FILES["member_image"]['name'],
                'facebook_url'=>$this->input->post('facebook_url'),
                'twitter_url'=>$this->input->post('twitter_url'),
                'google_url'=>$this->input->post('google_url'),
                'dribble_url'=>$this->input->post('dribble_url'),
				);
			}
			else
			{
				$data=array(
				'member_name'=>$this->input->post('member_name'),
                'designation'=>$this->input->post('designation'),
                'member_detail'=>$this->input->post('member_detail'),
                'facebook_url'=>$this->input->post('facebook_url'),
                'twitter_url'=>$this->input->post('twitter_url'),
                'google_url'=>$this->input->post('google_url'),
                'dribble_url'=>$this->input->post('dribble_url'),
				);
			}
			$user= $this->members_model->updateMember(array('id'=>$this->input->post('id')),$data);
			if(!empty($_FILES["member_image"]['name']))
			{
				$unlinkimg=$_FILES["member_image"]['name'];
				unlink(base_url("Newassets/images/".$unlinkimg));
				unlink(base_url("Newassets/images/thumbnail/".$unlinkimg));
				$name = $_FILES["member_image"]["name"];
				$ext = end((explode(".", $name))); 
				// Code for File Upload
				$config['upload_path']= './Newassets/images/';        
				$config['allowed_types']= 'gif|jpg|png|jpeg';
				$config['overwrite'] = TRUE;
				$config['file_name'] = $_FILES["member_image"]['name'];
				$this->load->library('upload');
				$this->upload->initialize($config);
				if ( ! $this->upload->do_upload('member_image'))
				{
					// On File Upload Fail
					$this->session->set_flashdata('fail',$this->upload->display_errors());
					redirect('/admin/ourmembers');
				}
				else
				{
					$file_data = $this->upload->data();				
					$source_path = DIRECTORY_PATH.'Newassets/images/'.$file_data["file_name"];
					$target_path = DIRECTORY_PATH.'Newassets/images/thumbnail/'.$file_data["file_name"];
					$thumb = PhpThumbFactory::create($source_path);
					$thumb->adaptiveResize(280, 250);
					if($thumb->save($target_path, $ext))
					{					
						$this->session->set_flashdata('success',"Member Successfully Updated");
						redirect('/admin/ourmembers');
					}
					else
					{
						$this->session->set_flashdata('fail',"Error Occured in Member Update");
						redirect('/admin/ourmembers');
					}					
				}
			}
			if($user)
			{					
				$this->session->set_flashdata('success',"Member Successfully Updated");
				redirect('/admin/ourmembers');
			}
			else
			{
				$this->session->set_flashdata('fail',"Error Occured in Member Update");
				redirect('/admin/ourmembers');
			}
		}
        else
		{
			$this->session->set_flashdata('fail',"Error Occured in Member Update");
			redirect('/admin/ourmembers');
		}
	}
    public function member_delete($id)
	{		
		if($this->members_model->deleteMember($id))
		{
			$this->session->set_flashdata('success',"Member Successfully Deleted");
			redirect('/admin/ourmembers');
		}
		else
		{
			$this->session->set_flashdata('fail',"Error Occured in Delete Member");
			redirect('/admin/ourmembers');
		}
	}
}