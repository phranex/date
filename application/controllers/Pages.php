<?php
class Pages extends CI_Controller {
    // Captures all calls to this controller
    public function _remap() 
    {
        // Get out URL segments
        $segments = $this->uri->uri_string();
        $segments = explode("/", $segments);
        // Remove blank segments from array
            foreach($segments as $key => $value) {
               if($value == "" || $value == "NULL") {
                   unset($segments[$key]);
               }
            }
            // Store our newly filtered array segments
            $segments = array_values($segments); 
            // Works out what segments we have
            switch (count($segments))
            {
                // We have a category/subcategory/page-name
                case 3:
                    list($cat, $subcat, $page_name) = $segments;
                break;
                // We have a category/page-name
                case 2:
                    list($cat, $page_name) = $segments;
                    $subcat = NULL;
                break;
                // We just have a page name, no categories. So /page-name
                default:
                    list($page_name) = $segments;
                    $cat = $subcat = NULL;
                break;
            }
        if ($cat == '' && $subcat == '') {
            $page  = $this->mpages->fetch_page('', '', $page_name);
        } else if ($cat != '' && $subcat == '') {
            $page  = $this->mpages->fetch_page($cat, '', $page_name);
        } else if ($category != "" && $sub_category != "") {
            $page = $this->mpages->fetch_page($cat, $subcat, $page_name);
        }
                // $page contains your page data, do with it what you wish.
}
}
?>