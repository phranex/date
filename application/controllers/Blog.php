<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'/PHPThumb/ThumbLib.inc.php';
/**
 * Blog class.
 * 
 * @extends CI_Controller
 */
class Blog extends CI_Controller {
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() 
	{
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
        $this->load->library('form_validation');
        $this->load->helper('form');
		$this->load->model('user_model');
		$this->load->model('SiteErrorLog');
		$this->load->model('blocks_model');
		$this->load->model('admin_model');
		$this->load->model('Pushnotification_model');
		$this->load->model('story_model');
		$this->load->model('blog_model');
		$this->load->model('blocks_model');
        $this->load->model('members_model');
		$this->load->model('Cms_model');
		$this->load->library('pagination');
        $this->load->library('table');
		$this->load->model('Testimonial_model');
		$this->load->model('Slider_model');
        $this->load->model('Error404_model');
        $this->load->model('comingsoon_model');
		 $this->load->model('background_model');
        date_default_timezone_set('Asia/Kolkata'); 
		$data['header']=$this->admin_model->get_setting();
		$is_comingsoon=$data['header'][18]['status'];
		$method=$this->router->fetch_method();
		if($is_comingsoon==1 && $method!='comingsoon'){
			redirect('user/comingsoon');
		}
	}
	public function index($author=null)
    {   
        $logged_in_user = $this->session->userdata('logged_in');
		if(!empty($author) && !is_numeric($author)){
			$base_url=site_url('blog/index'.str_replace(" ","_",$author));
		}
		else{
			$base_url=site_url('blog/index');
		}
		$total_rows = $this->blog_model->count_total();
        $config['base_url'] = $base_url;    // url of the page
        $config['total_rows'] = $total_rows;//get total number of records 
        $config['per_page'] = 2;  // define how many records on page
        $config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = lang('lbl_next').' <span aria-hidden="true">&raquo;</span>';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '<span aria-hidden="true">&laquo;</span> '.lang('lbl_prev');
        $config['prev_tag_open'] = '<li class="previous">';
        $config['prev_tag_close'] = '</li>';
        $config['page_query_string'] = FALSE;
        $this->pagination->initialize($config);
        if(!empty($author) && !is_numeric($author)) 
        	$page = ($this->uri->segment(4))? $this->uri->segment(4) : 0;
        else
			$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
		//echo $config['per_page'];
		$db_blog=$this->blog_model->get_my_data('',$config['per_page'],$page);
		//echo "<pre>";print_r($db_blog);die;
        $data=array(
			'db_blog'=>$db_blog,
            'total_rows'=>$total_rows,
        );
        $value['header']=$this->admin_model->get_setting();				
		$this->load->view('header',$value);	
		$this->load->view("user/dashboard/blog",$data);
		$this->load->view('footer');
    }
	public function blog_details($id)
    { 	
		$logged_in_user = $this->session->userdata('logged_in');
		$data['blog_details']=$this->blog_model->getblogitem(array('blog.id'=>$id));	
        $value['header']=$this->admin_model->get_setting();				
		$this->load->view('header',$value);
		$this->load->view("user/dashboard/blog_details",$data);
		$this->load->view('footer');
    }
	
 }