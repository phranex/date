<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['register'] = 'user/register';
$route['login'] = 'user/login';
$route['logout'] = 'user/logout';
$route['user'] = 'user/dashboard';
$route['admin'] = 'admin/admin/login';

$route['default_controller'] = 'user/index';
/*$route['(:any)'] = 'admin/cms/display_cms/$1';*/
$route['user/friend_like/(:num)']                = 'user/friend_like/$fid';
$route['user/friend_dislike/(:num)']                = 'user/friend_dislike/$fid';
$route['user/friend_approved/(:num)']                = 'user/friend_approved/$fid';
$route['user/friend_decline/(:num)']                = 'user/friend_decline/$fid';
$route['user/friend_profile/(:num)']                = 'user/friend_profile/$fid';
$route['404_override'] = 'user/error404';
$route['translate_uri_dashes'] = FALSE;
