<?php
session_start();
$_SESSION['steps']=3;
$_SESSION['language']=$_POST['language'];
$_SESSION['fbapi']=$_POST['fbapi'];
$_SESSION['gskey']=$_POST['gskey'];
$language=$_POST['language'];
$fbapi=$_POST['fbapi'];
$gskey=$_POST['gskey'];

if( $language=="" || $gskey=="" ){
	echo "Please enter all fields";die;
}
$conn = new mysqli($_SESSION["host"], $_SESSION["username"], $_SESSION["upassword"], $_SESSION["dbname"]);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

// output data of each row
$sql = "UPDATE configuration SET value='$gskey' where configuration.key='GOOGLE_PLACE_API_KEY';";
$sql .= "UPDATE configuration SET value='$fbapi' where configuration.key='FACEBOOK_KEY';";
$sql .= "UPDATE site_setting SET status='$language' where site_setting.mode='default_language';";
if ($conn->multi_query($sql) === TRUE) {
    echo "yes";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}
$conn->close();
?>