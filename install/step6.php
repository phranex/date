<?php 
session_start();
$_SESSION['steps']=6;
$_SESSION['instagram_callback_base']	=$_POST['instagram_callback_base'];
$_SESSION['instagram_client_secret']	=$_POST['instagram_client_secret'];
$_SESSION['instagram_client_id']		=$_POST['instagram_client_id'];
$_SESSION['admobkey']					=$_POST['admobkey'];
$_SESSION['admobvideokey']				=$_POST['admobvideokey'];
$_SESSION['removeaddinapppurchase']		=$_POST['removeaddinapppurchase'];
$_SESSION['removeaddinappbilling']		=$_POST['removeaddinappbilling'];

$removeaddinappbilling		=$_POST['removeaddinappbilling'];
$instagram_callback_base	=$_POST['instagram_callback_base'];
$instagram_client_secret	=$_POST['instagram_client_secret'];
$instagram_client_id		=$_POST['instagram_client_id'];
$admobkey					=$_POST['admobkey'];
$admobvideokey				=$_POST['admobvideokey'];
$removeaddinapppurchase		=$_POST['removeaddinapppurchase'];
$conn = new mysqli($_SESSION["host"], $_SESSION["username"], $_SESSION["upassword"], $_SESSION["dbname"]);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

if(!empty($instagram_callback_base) && !empty($instagram_client_secret) && !empty($instagram_client_id) && !empty($admobkey) && !empty($admobvideokey) && !empty($removeaddinapppurchase) && !empty($removeaddinappbilling)){
	$protocol;
	if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    }
    else{
        $protocol = 'http';
    }
    $url= $protocol . "://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
    // output data of each row xmppserver='$xmppserver',
	$sql = "UPDATE configuration SET value='$instagram_callback_base' where configuration.key='INSTAGRAM_CALLBACK_BASE';";
	$sql .= "UPDATE configuration SET value='$instagram_client_secret' where configuration.key='INSTAGRAM_CLIENT_SECRET';";
	$sql .= "UPDATE configuration SET value='$instagram_client_id' where configuration.key='INSTAGRAM_CLIENT_ID';";
	$sql .= "UPDATE configuration SET value='$admobkey' where configuration.key='adMobKey';";
	$sql .= "UPDATE configuration SET value='$admobvideokey' where configuration.key='adMobVideoKey';";
	$sql .= "UPDATE configuration SET value='$removeaddinapppurchase' where configuration.key='RemoveAddInAppPurchase';";
	$sql .= "UPDATE configuration SET value='$removeaddinappbilling' where configuration.key='RemoveAddInAppBilling';";
	$sql .= "UPDATE configuration SET value='$url/user/privacy' where configuration.key='TermsAndConditionsUrl';";
	if ($conn->multi_query($sql) === TRUE) {
	    echo "complete";
	} else {
	    echo "Error: " . $sql . "<br>" . $conn->error;
	}
	//now here we need to update the config file database file and htaccess file
	$template_path 	= 'files/database.php';
	$output_path 	= '../application/config/database.php';

	// Open the file
	$database_file = file_get_contents($template_path);

	$new  = str_replace("%HOSTNAME%",$_SESSION['host'],$database_file);
	$new  = str_replace("%USERNAME%",$_SESSION['username'],$new);
	$new  = str_replace("%PASSWORD%",$_SESSION['upassword'],$new);
	$new  = str_replace("%DATABASE%",$_SESSION['dbname'],$new);

	// Write the new database.php file
	$handle = fopen($output_path,'w+');

	// Chmod the file, in case the user forgot
	@chmod($output_path,0777);

	// Verify file permissions
	if(is_writable($output_path)) {

		// Write the file
		if(fwrite($handle,$new)) {
			//return true;
		} else {
			///return false;
		}

	} else {
		//return false;
	}

	// Config path
	$template_path 	= 'files/config.php';
	$output_path 	= '../application/config/config.php';
	// Open the file
	$database_file = file_get_contents($template_path);
	$redir = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
	$redir .= "://".$_SERVER['HTTP_HOST'];
	$redir .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
	$redir = str_replace('install/','',$redir); 

	$new  = str_replace("%BASE_URL%",$redir,$database_file);
	// Write the new database.php file
	$handle = fopen($output_path,'w+');

	// Chmod the file, in case the user forgot
	@chmod($output_path,0777);

	// Verify file permissions
	if(is_writable($output_path)) {

		// Write the file
		if(fwrite($handle,$new)) {
			//return true;
		} else {
			//return false;
		}

	} else {
		//return false;
	}
	$script=str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
	$script=str_replace('install/','',$script);

	if($script=='')
			$rewritebase="";
	else
		$rewritebase="RewriteBase ".$script;
	// Config path
	$template_path 	= 'files/.htaccess';
	$output_path 	= '../.htaccess';

	// Open the file
	$database_file = file_get_contents($template_path);
	$new  = str_replace("%REWRITEBASE%",$rewritebase,$database_file);

	// Write the new database.php file
	$handle = fopen($output_path,'w+');

	// Chmod the file, in case the user forgot
	@chmod($output_path,0777);

	// Verify file permissions
	if(is_writable($output_path)) {

		// Write the file
		if(fwrite($handle,$new)) {
			//return true;
		} else {
			//return false;
		}

	} else {
		//return false;
	}
	
}
else{
	echo "Please enter values";
}
?>