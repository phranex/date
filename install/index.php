<?php
error_reporting(0);
session_start();
if(empty($_SESSION['steps']))
	$_SESSION['steps']=0;
/*echo "<pre>";
print_r($_SESSION);
echo "</pre>";*/
?>
<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
<head>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.toggle.css">
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/bootstrap-toggle.min.js"></script>
	<!-- <script type="text/javascript" src="js/jquery.session.js"></script> -->
	<style type="text/css">
		#completescript{
			background-color: #ffffff;
		    margin: 100px auto;
		    font-family: Raleway;
		    padding: 40px;
		    width: 70%;
		    min-width: 300px;
		}
	</style>
</head>
<body>
<div class="container">
	<!-- <div id="myDiv">
        <img id="loading-image" src="image/loader.gif" style="display:none;"/>
    </div> -->
	<div id="regForm">
		<h1>Cupid Love Installation</h1>
		<a href="restart.php" class=""><button>Restart</button></a>
		<center>
			<div id="ajaxloader" style="display: none;">
				<img src="image/loader.gif" style="width: 50px;">
			</div>
		</center>
		<div class="row">
			<div class="spancls">
				<?php
				for($i=1;$i<=5;$i++){
					if($i<=$_SESSION['steps']){
						?>
						<span class="step finish"></span>
						<?php
					}
					else{
						?>
						<span class="step"></span>
						<?php
					}
				}
				?>
			</div>
		</div>
		<!-- One "tab" for each step in the form: -->
		<div class="tab" id="step0">
			<div class="row" id="requirements">
				<h4>Requirements</h4>
				<?php 
					$phpversion=phpversion();
					$gd_info=extension_loaded('gd') && function_exists('gd_info');
					$mod_rewrite=in_array('mod_rewrite', apache_get_modules());
					if($phpversion>=5.5){
						?>
						<p class="text-success"><i class="fa fa-check" aria-hidden="true"></i>&nbsp PHP Version PHP 7.</p>
						<?php
					}
					else{
						?>
						<p class="text-danger"><i class="fa fa-times" aria-hidden="true"></i>&nbsp PHP Version PHP 7.</p>
						<?php
						$_SESSION['steps']=0;
					}
					if($mod_rewrite){
						?>
						<p class="text-success"><i class="fa fa-check" aria-hidden="true"></i>&nbsp Mod Rewrite Enable.</p>
						<?php
					}
					else{
						?>
						<p class="text-danger"><i class="fa fa-times" aria-hidden="true"></i>&nbsp Mod Rewrite Enable.</p>
						<?php
						$_SESSION['steps']=0;
					}
					if ($gd_info) {
					    ?>
					    <p class="text-success"><i class="fa fa-check" aria-hidden="true"></i>&nbsp GD Library Enable.</p>
					    <?php
					}		
					else{
						?>
						<p class="text-danger"><i class="fa fa-times" aria-hidden="true"></i>&nbsp GD Library Enable.</p>
						<?php
						$_SESSION['steps']=0;
					}		
					if (function_exists('mysqli_connect')) {
					    ?>
					    <p class="text-success"><i class="fa fa-check" aria-hidden="true"></i>&nbsp MySQLi.</p>
					    <?php
					}		
					else{
						?>
						<p class="text-danger"><i class="fa fa-times" aria-hidden="true"></i>&nbsp MySQLi.</p>
						<?php
						$_SESSION['steps']=0;
					}
					$connection =fsockopen("127.0.0.1",80, $errno, $errstr, 0.1);
					if (is_resource($connection))
					{
						?>
					    <p class="text-success"><i class="fa fa-check" aria-hidden="true"></i>&nbsp Port 5280.</p>
					    <?php
					    //print "I can see port 5280";
					}
					else
					{
						?>
						<p class="text-danger"><i class="fa fa-times" aria-hidden="true"></i>&nbsp Port 5280.</p>
						<?php
						fclose($fp);
						//print "I cannot see port 5280";
					}					
				?>
			</div>			
			<div class="btnDiv row">
				<div style="float:right;">
					<button type="button" id="prevBtn0" onclick="nextPrev(-1)">Previous</button>
					<button type="button" id="nextBtn0" class="nextBtn" onclick="nextPrev(1)"  data-value="step0" >Next</button>
				</div>
			</div>
		</div>
		<div class="tab" id="step1">
			<div class="row">
				<h4>Step 1 : Database Details </h4>
				<input type="text" placeholder="Host" class="fixcls" name="host" id="host" required="" <?php if(isset($_SESSION['host'])) echo "value='".$_SESSION['host']."'";?>>
				<input type="text" placeholder="Username" class="fixcls" name="username" id="username" required="" <?php if(isset($_SESSION['username'])) echo "value='".$_SESSION['username']."'";?>>
				<input type="password" placeholder="Password" class="fixcls" name="upassword" id="upassword" required="" <?php if(isset($_SESSION['upassword'])) echo "value='".$_SESSION['upassword']."'";?>>
				<input type="text" placeholder="Database name" class="fixcls" name="dbname" id="dbname" required="" <?php if(isset($_SESSION['dbname'])) echo "value='".$_SESSION['dbname']."'";?>>
				<!-- <p><input placeholder="Last name..." oninput="this.className = ''" name="lname"></p> -->
			</div>
			<div class="btnDiv row">
				<div style="float:right;">
					<button type="button" id="prevBtn1" onclick="nextPrev(-1)">Previous</button>
					<button type="button" id="nextBtn1" class="nextBtn"  data-value="step1" >Next</button>
					<!-- onclick="nextPrev(1)" -->
				</div>
			</div>
		</div>
		<div class="tab" id="step2">
			<div class="row">
				<h4>Step 2 : Create Admin Account</h4>
				<input type="text" placeholder="E-mail..." class="fixcls" name="adminemail" id="adminemail" <?php if(isset($_SESSION['adminemail'])) echo "value='".$_SESSION['adminemail']."'";?>>
				<input type="password" placeholder="Password..." class="fixcls" name="adminpassword" id="adminpassword"  <?php if(isset($_SESSION['adminpassword'])) echo "value='".$_SESSION['adminpassword']."'";?>>
			</div>
			<div class="btnDiv row">
				<div style="float:right;">
					<button type="button" id="prevBtn2" onclick="nextPrev(-1)">Previous</button>
					<button type="button" id="nextBtn2" class="nextBtn" data-value="step2" >Next</button>
					<!-- onclick="nextPrev(1)" -->
				</div>
			</div>
		</div>
		<div class="tab" id="step3">
			<div class="row">
				<h4>Step 3 : General Configuration </h4>
				<!-- <input placeholder="dd" class="fixcls" name="dd"> -->
				<select name="language" class="fixcls" id="language">
					<option value="">Select Language</option>
					<option <?php if(isset($_SESSION['language'])){if($_SESSION['language']=="english") echo "selected";}?> value="english">English</option>
					<option <?php if(isset($_SESSION['language'])){if($_SESSION['language']=="french") echo "selected";}?> value="french">French</option>
					<option <?php if(isset($_SESSION['language'])){if($_SESSION['language']=="arabic") echo "selected";}?> value="arabic">Arabic</option>
				</select>
				<input type="text" placeholder="Facebook API" class="fixcls" name="fbapi" id="fbapi" required="" <?php if(isset($_SESSION['fbapi'])) echo "value='".$_SESSION['fbapi']."'";?>>
				<input type="text" placeholder="Google Search Key" class="fixcls" name="gskey" id="gskey" required="" <?php if(isset($_SESSION['gskey'])) echo "value='".$_SESSION['gskey']."'";?>>
			</div>
			<div class="btnDiv row">
				<div style="float:right;">
					<button type="button" id="prevBtn3" onclick="nextPrev(-1)">Previous</button>
					<button type="button" id="nextBtn3" class="nextBtn" data-value="step3" >Next</button>
					<!-- onclick="nextPrev(1)" -->
				</div>
			</div>
		</div>
		<div class="tab" id="step4">
			<div class="row">
				<h4>Step 4 : XMPP Server Details</h4>
				XMPP Enable : <input type="checkbox" data-toggle="toggle" data-on="Enable" data-off="Disable" name="xmpptoggle" id="xmpptoggle">
				<input type="hidden" name="xmppenable" id="xmppenable">
				<div class="xmppdiv" style="display: block">
					<!-- <input type="text" placeholder="XMPP Host" class="fixcls" name="xmpphost" id="xmpphost" value="<?php if(isset($_SESSION['xmpphost'])) echo $_SESSION['xmpphost'];?>"> -->
					<input type="text" placeholder="XMPP Server" class="fixcls" name="xmppserver" id="xmppserver" value="<?php if(isset($_SESSION['xmppserver'])) echo $_SESSION['xmppserver'];?>">
					<input type="text" placeholder="XMPP Default Password" class="fixcls" name="xmppdefaultpass" id="xmppdefaultpass" value="<?php if(isset($_SESSION['xmppdefaultpass'])) echo $_SESSION['xmppdefaultpass'];?>">
				</div>
				
			</div>
			<div class="btnDiv row">
				<div style="float:right;">
					<button type="button" id="prevBtn4" onclick="nextPrev(-1)">Previous</button>
					<button type="button" id="nextBtn4" class="nextBtn" data-value="step4" >Next</button>
				</div>
			</div>
		</div>
		<!--div class="btnDiv row">
				<div style="float:right;">
					<button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
					<button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
				</div>
		</div-->
		<!-- Circles which indicates the steps of the form: -->
	</div>
	<div id="completescript" style="display: none;">
		<div class="row text-center">
			<p class="text-success">
				Your Script is successfully installed.<br/>
				<?php 
				$redir = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
				$redir .= "://".$_SERVER['HTTP_HOST'];
				$redir .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
				$redir = str_replace('install/','',$redir); 
				?>
				<a href="<?php echo $redir; ?>">Access Website</a>
			</p>
		</div>
	</div>
</div>

<script>
var currentTab = <?php echo ($_SESSION['steps']?$_SESSION['steps']:0)?>; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the crurrent tab

function showTab(n) {
	// This function will display the specified tab of the form...
	var x = document.getElementsByClassName("tab");
	x[n].style.display = "block";
	//... and fix the Previous/Next buttons:
	if (n == 0) {
		document.getElementById("prevBtn"+currentTab).style.display = "none";
	} else {
		document.getElementById("prevBtn"+currentTab).style.display = "inline";
	}
	if (n == (x.length - 1)) {
		document.getElementById("nextBtn"+currentTab).innerHTML = "Submit";
	} else {
		document.getElementById("nextBtn"+currentTab).innerHTML = "Next";
	}
	//... and run a function that will display the correct step indicator:
	fixStepIndicator(n)
}

function nextPrev(n) {
	// This function will figure out which tab to display
	var x = document.getElementsByClassName("tab");
	// Exit the function if any field in the current tab is invalid:
	if (n == 1 && !validateForm()) return false;
	// Hide the current tab:
	x[currentTab].style.display = "none";
	// Increase or decrease the current tab by 1:
	currentTab = currentTab + n;
	// if you have reached the end of the form...
	if (currentTab >= x.length) {
		// ... the form gets submitted:
		//document.getElementById("regForm").submit();
		return false;
	}
	// Otherwise, display the correct tab:
	showTab(currentTab);
}

function validateForm() {
	// This function deals with validation of the form fields
	var x, y, i, valid = true;
	x = document.getElementsByClassName("tab");
	y = x[currentTab].getElementsByTagName("input");
	//z for select or you can use other tag if you have
	z = x[currentTab].getElementsByTagName("select");
	// A loop that checks every input field in the current tab:
	for (i = 0; i < y.length; i++) {
		// If a field is empty...
		var yid=y[i].id;
		if (y[i].value == "") {
			if(yid!="upassword" ){
			  // add an "invalid" class to the field:
			  y[i].className += " invalid";
			  //document.getElementById("nextBtn"+currentTab).disabled = true;
			  // and set the current valid status to false
			  valid = false;	
				if( yid==="push_sandbox_gateway_url" || yid==="push_gateway_url" ||  yid==="androidpemkey" || yid==="pem_file" || yid==="fbapi" || yid==="instagram_callback_base" || yid==="instagram_client_secret" || yid==="instagram_client_id" || yid==="admobkey" || yid==="admobvideokey" || yid==="removeaddinappbilling"){
					y[i].className = "fixcls"; 
				  	valid = true;	
				}
			  var chkbxbtn=document.getElementById("xmpptoggle").checked;
			  if(chkbxbtn==true){
			  	if( yid==="xmppserver" || yid==="xmppdefaultpass" ){
					y[i].className = "fixcls"; 
				  	valid = true;	
				}
			  }
			  console.log(y[i].id);
			}
		}
	}
	for (i = 0; i < z.length; i++) {
		// If a field is empty...
		if (z[i].value == "") {
			if(z[i].id!="upassword"){
			  // add an "invalid" class to the field:
			  z[i].className += " invalid";
			  // and set the current valid status to false
			  valid = false;	
			}
		}
	}
	// If the valid status is true, mark the step as finished and valid:
	if (valid) {
		document.getElementsByClassName("step")[currentTab].className += " finish";
		//document.getElementById("nextBtn"+currentTab).disabled = false;
	}
	//console.log(valid+" nextBtn"+currentTab);
  	return valid; // return the valid status
}

function fixStepIndicator(n) {
	// This function removes the "active" class of all steps...
	var i, x = document.getElementsByClassName("step");
	for (i = 0; i < x.length; i++) {
		x[i].className = x[i].className.replace(" active", "");
	}
	//... and adds the "active" class on the current step:
	x[n].className += " active";
}
$(document).ready(function(){
	var tog="Live";
	$("#notification_mode").val(tog);
	var f=$("p").hasClass("text-danger");
	if(f){
	 	$("#nextBtn"+currentTab).attr("disabled", "disabled");
	 	$("#nextBtn"+currentTab).addClass("disabled");
	}else{		
	  	$("#nextBtn"+currentTab).removeAttr("disabled");
	  	$("#nextBtn"+currentTab).removeClass("disabled");
	}
	var xmpptog="Enable";
	$("#xmppenable").val(xmpptog);
	$("#xmpptoggle").change(function(){
		if(xmpptog=="Enable"){
			xmpptog="Disable";
			$(".xmppdiv").css("display","none");
		}
		else{
			xmpptog="Enable";
			$(".xmppdiv").css("display","block");	
		}
		$("#xmppenable").val(xmpptog);
	});
	$("#toggle").change(function(){
		if(tog=="Live"){
			tog="Sandbox";
		}
		else{
			tog="Live";
		}
		$("#notification_mode").val(tog);
	});
	$(".nextBtn").click(function(){
		validateForm();
		console.log($(this).attr("data-value"));		
		$.ajax({
			url:$(this).attr("data-value")+".php",
			type:"POST",
			data:{
				host:$("#host").val(),
				username:$("#username").val(),
				upassword:$("#upassword").val(),
				dbname:$("#dbname").val(),
				language:$("#language").val(),
				adminemail:$("#adminemail").val(),
				adminpassword:$("#adminpassword").val(),
				fbapi:$("#fbapi").val(),
				gskey:$("#gskey").val(),
				notification_mode:$("#notification_mode").val(),
				push_sandbox_gateway_url:$("#push_sandbox_gateway_url").val(),
				push_gateway_url:$("#push_gateway_url").val(),
				androidpemkey:$("#androidpemkey").val(),
				toggle:$("#toggle").val(),
				xmppenable:$("#xmppenable").val(),
				//xmpphost:$("#xmpphost").val(),
				xmppserver:$("#xmppserver").val(),
				xmppdefaultpass:$("#xmppdefaultpass").val(),
				instagram_callback_base:$("#instagram_callback_base").val(),
				instagram_client_secret:$("#instagram_client_secret").val(),
				instagram_client_id:$("#instagram_client_id").val(),
				admobkey:$("#admobkey").val(),
				admobvideokey:$("#admobvideokey").val(),
				removeaddinapppurchase:$("#removeaddinapppurchase").val(),
				removeaddinappbilling:$("#removeaddinappbilling").val(),

			},
			beforeSend: function() {
				$(".nextBtn").attr("disabled", true);
              $("#ajaxloader").css("display","block");
           	},
			success:function(data){
				$(".nextBtn").attr("disabled", false);
				$("#loading-image").css("display","none");
				if(data!="yes"){
					if(data=="complete" ){
						$("#ajaxloader").css("display","none");
						$("#regForm").css("display","none");
						$("#completescript").css("display","block");
						nextPrev(1);
					}
					else{
						$("#ajaxloader").css("display","none");
						nextPrev(0);
						if(data != "" || data != null)
							alert(data);						
					}
				}
				else
				{					
					$("#ajaxloader").css("display","none");
					nextPrev(1);
				}
			},
			error:function(data){
				$(".nextBtn").attr("disabled", false);
				$("#ajaxloader").css("display","none");
				nextPrev(0);
			}
		});	
	});
});
</script>
</body>
</html>