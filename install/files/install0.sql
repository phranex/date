-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `background`;
CREATE TABLE `background` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `background_url` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `background` (`id`, `background_url`) VALUES
(2,	'http://themes.potenzaglobalsolutions.com/cupidlovedesign/webapp/images/bg/inner-bg-1.jpg'),
(3,	'http://themes.potenzaglobalsolutions.com/cupidlovedesign/webapp/images/bg/bg-4.jpg'),
(4,	'http://themes.potenzaglobalsolutions.com/cupidlovedesign/webapp/images/bg/01.png'),
(5,	'http://themes.potenzaglobalsolutions.com/cupidlovedesign/webapp/images/bg/02.png'),
(6,	'http://themes.potenzaglobalsolutions.com/cupidlovedesign/webapp/images/bg/03.png'),
(7,	'http://themes.potenzaglobalsolutions.com/cupidlovedesign/webapp/images/bg/04.png'),
(8,	'http://themes.potenzaglobalsolutions.com/cupidlovedesign/webapp/images/bg/bg-2.jpg');

DROP TABLE IF EXISTS `block`;
CREATE TABLE `block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `contain` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `block` (`id`, `title`, `contain`) VALUES
(1,	'Step To Find Your Soul Mate',	'<p><span style=\"color: #3d3d3d; font-family: Montserrat, sans-serif; font-size: 16px; text-align: center;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed&nbsp;</span><br style=\"box-sizing: border-box; color: #3d3d3d; font-family: Montserrat, sans-serif; font-size: 16px; text-align: center;\" /><span style=\"color: #3d3d3d; font-family: Montserrat, sans-serif; font-size: 16px; text-align: center;\">do eiusmod tempor incididunt ut labore et dolore magna</span></p>'),
(2,	'create profile',	'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. enim ad minim veniam, quis'),
(3,	'find match',	'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. enim ad minim veniam, quis'),
(4,	'start dating',	'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  enim ad minim veniam, quis'),
(5,	'Nothing Say Better, Then A Video',	'Eum cu tantas legere complectitur, hinc utamur ea eam. Eum patrioque mnesarchum eu, diam erant convenire et vis. Et essent evertitur sea, vis cu ubique referrentur, sed eu dicant expetendis.'),
(6,	'Our Recent Blogs',	'Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Quisque velit nisi, pretium ut lacinia in, elementum id enim.'),
(7,	'About US',	'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea'),
(8,	'who we are',	'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea'),
(9,	'who we do this',	'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea'),
(10,	'we are the one',	'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the when an unknown printer took.'),
(11,	'Privacy Policy',	' <p>Consectetur adipisicing elit. Quidem error quae illo excepturi nostrum blanditiis laboriosam magnam explicabo.</p>\n      <p> eum nihil expedita dolorum odio dolorem, explicabo rem illum magni perferendis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem error quae illo excepturi nostrum blanditiis laboriosam magnam explicabo. Molestias, eum nihil expedita dolorum odio dolorem, explicabo rem illum magni perferendis.</p>\n      <p>Expedita dolorum odio doloremt, consectetur adipisicing elit. Quidem error quae illo excepturi nostrum blanditiis laboriosam magnam explicabo. Molestias, eum nihil expedita dolorum odio dolorem, explicabo rem illum magni perferendis.</p>\n\n      <h4 class=\"title text-yellow\">Personal Information</h4>\n      <ul class=\"list list-unstyled\">\n        <li> <i class=\"fa fa-angle-right\"></i> Lorem ipsum dolor sit amet, consectetur </li>\n        <li> <i class=\"fa fa-angle-right\"></i> Quidem error quae illo excepturi nostrum blanditiis laboriosam </li>\n        <li> <i class=\"fa fa-angle-right\"></i> Molestias, eum nihil expedita dolorum odio dolorem</li>\n        <li> <i class=\"fa fa-angle-right\"></i> Eum nihil expedita dolorum odio dolorem</li>\n        <li> <i class=\"fa fa-angle-right\"></i> Explicabo rem illum magni perferendis</li>\n      </ul>\n      <p>Debitis nemo animi quia, consectetur adipisicing elit. Possimus, ex, quisquam. Nulla excepturi sint iusto incidunt sed omnis expedita, commodi dolores. Debitis nemo animi quia deleniti commodi nesciunt illo. Deserunt.</p>\n\n      <h4 class=\"title text-yellow\">Use of User Information.</h4>\n      <p>Nulla excepturi sint iusto incidunt sed omnis expedita, consectetur adipisicing elit. Possimus, ex, quisquam. Nulla excepturi sint iusto incidunt sed omnis expedita, commodi dolores. Debitis nemo animi quia deleniti commodi nesciunt illo. Deserunt.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus, ex, quisquam. Nulla excepturi sint iusto incidunt sed omnis expedita, commodi dolores. Debitis nemo animi quia deleniti commodi nesciunt illo. Deserunt.Lorem ipsum dolor sit amet, Possimus, ex, quisquam. Nulla excepturi sint iusto incidunt sed omnis expedita, commodi dolores. Debitis nemo animi quia deleniti commodi nesciunt illo. Deserunt.</p>\n\n      <h4 class=\"title text-yellow\">Disclosure of User Information.</h4>\n      <p>Dipisicing elit, consectetur adipisicing elit. Autem ullam nostrum dolor alias aspernatur nobis suscipit eaque cumque distinctio eos, beatae deserunt, nihil nam maiores vero, eius harum. Reprehenderit, aspernatur.<a href=\"#\">support@website.com</a> </p>\n      \n      <ul class=\"list list-unstyled\">\n        <li><i class=\"fa fa-angle-right\"></i>Nulla excepturi sint iusto incidunt sed omnis expedita </li>\n        <li><i class=\"fa fa-angle-right\"></i>Quidem error quae illo excepturi nostrum blanditiis laboriosam </li>\n        <li><i class=\"fa fa-angle-right\"></i>Deserunt.Lorem ipsum dolor sit amet</li>\n        <li><i class=\"fa fa-angle-right\"></i>Possimus, ex, quisquam. Nulla excepturi</li>\n      </ul>\n      <p>Consectetur adipisicing elit. Possimus, ex, quisquam. Nulla excepturi sint iusto incidunt sed omnis expedita, commodi dolores. Debitis nemo animi quia deleniti commodi nesciunt illo. Deserunt.<a href=\"#\">support@website.com</a></p>'),
(12,	'Our History',	'The overall planning, coordination, and control of a project from beginning to completion. CPM is aimed at meeting a client\'s Construction Project Management.'),
(13,	'Our History Inner',	'<div class=\"row\"><div class=\"col-md-6 xs-mb-3\">\r\n            <img class=\"img-fluid\" src=\"http://themes.potenzaglobalsolutions.com/cupidlovedesign/webapp/images/about/01.jpg\" alt=\"\"/>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n            <h3 class=\"title\">Founded in 1990</h3>\r\n            <h5 class=\"clearfix text-orange mb-20\">A World Of Infinite Opportunities</h5>\r\n            <p>I want to talk about to things that are quite important to me. There are love and one my personal inadequacies. The thing is that I’m quite fond of love, I think that it’s a pretty all right deal. However, I’m going to have to admit that my emotional baggage has built up walls that not even a shock and awe campaign could bring down. But I do love. And in fact I even love unconditionally. <br><br>\r\n            I have a truck. It’s kind of a small truck, but I’m comfortable with myself so that’s okay. I think that I love it. I had a friend about a year ago ask me if I could have any car in the world what would I have. And aside from pointing out that my friend and I have clearly ran out of things to discuss and should probably go our separate ways, my answer told me that I love my truck (obviously I said I would keep my truck).</p>\r\n        </div>\r\n  </div>'),
(14,	'Terms And Conditions',	'<h4 class=\"title text-yellow\">1. Description of Service</h4>\r\n            <p>Consectetur adipisicing elit. Sapiente, distinctio iste praesentium totam quasi tempore, lorem ipsum dolor sit amet, magnam ipsum cum animi at fuga alias harum quo quibusdam odit eum reprehenderit consectetur suscipit!</p>\r\n\r\n            <h4 class=\"title text-yellow\">2. Your Registration Obligations</h4>\r\n            <p>Sapiente, distinctio iste praesentium totam quasi tempore, consectetur adipisicing elit. Distinctio nesciunt officia culpa nostrum maxime vero architecto, corporis placeat repudiandae minima facere animi, pariatur fugit dignissimos qui error est nulla. Doloribus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio nesciunt officia culpa nostrum maxime vero architecto, corporis placeat repudiandae minima facere animi, pariatur fugit dignissimos qui error est nulla. Doloribus.</p>\r\n\r\n            <h4 class=\"title text-yellow\"> 3. User Account, Password, and Security</h4>\r\n            <p>Vitae facere expedita! Voluptatem iure dolorem dignissimos nisi magni a dolore, et inventore optio, voluptas, obcaecati.</p>\r\n\r\n            <h4 class=\"title text-yellow\">4. User Conduct</h4>\r\n            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>\r\n\r\n            <ul class=\"list list-unstyled\">\r\n                <li> <i class=\"fa fa-angle-right\"></i> Eaque ipsa quae ab illo inventore veritatis</li>\r\n                <li> <i class=\"fa fa-angle-right\"></i> Quidem error quae illo excepturi nostrum blanditiis laboriosam </li>\r\n                <li> <i class=\"fa fa-angle-right\"></i> Molestias, eum nihil expedita dolorum odio dolorem</li>\r\n                <li> <i class=\"fa fa-angle-right\"></i> Eum nihil expedita dolorum odio dolorem</li>\r\n                <li> <i class=\"fa fa-angle-right\"></i> Explicabo rem illum magni perferendis</li>\r\n              </ul>\r\n\r\n            <h4 class=\"title text-yellow\">5. International Use</h4>\r\n            <p>Sapiente, distinctio iste praesentium totam quasi tempore, magnam ipsum cum animi. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium vel recusandae ad impedit ipsum, vitae facere expedita! Voluptatem iure dolorem dignissimos nisi magni a dolore, Lorem ipsum dolor sit amet, consectetur adipisicing elit et inventore optio, voluptas, obcaecati. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate incidunt aliquam sint, magnam excepturi quas a, id doloremque quasi iusto quo consequuntur dolorum neque optio ipsum, rerum nesciunt illo iure. </p>\r\n            '),
(15,	'Why Choose Us',	'Eum cu tantas legere complectitur, hinc utamur ea eam. Eum patrioque mnesarchum eu, diam erant convenire et vis. Et essent evertitur sea, vis cu ubique referrentur, sed eu dicant expetendis. Eum cu'),
(16,	'About Page Subscribe Section',	'<section class=\"ptb-50 action-box-img bg text-center text-white bg-overlay-black-80\" style=\"background-image:url(http://themes.potenzaglobalsolutions.com/cupidlovedesign/webapp/images/bg/bg-4.jpg)\">\n    <div class=\"container\">\n        <div class=\"row\">\n          <div class=\"col-sm-12\">\n                <h5 class=\"pb-20\">Want to hear more story, subscribe for our newsletter</h5>\n                <a class=\"button  btn-lg btn-theme full-rounded animated right-icn\"><span>Subscribe<i class=\"glyph-icon flaticon-hearts\" aria-hidden=\"true\"></i></span></a>\n          </div>\n        </div>\n    \n    </div>\n</section>'),
(17,	'Footer Contact Us Section',	'<section class=\"page-section-ptb text-white text-center c_us\" style=\"background: url(http://themes.potenzaglobalsolutions.com/cupidlovedesign/webapp/images/pattern/04.png) no-repeat 0 0; background-size: cover;\">\r\n    <div class=\"container\">\r\n        <div class=\"row justify-content-center\">\r\n            <div class=\"col-md-10\">\r\n                <div class=\"row mb-5 xs-mb-3\">\r\n                    <div class=\"col-sm-12\">\r\n                        <h2 class=\"title divider mb-3\">Contact Us</h2>\r\n                        <p class=\"lead\">Eum cu tantas legere complectitur, hinc utamur ea eam. Eum patrioque mnesarchum eu, diam erant convenire et vis. Et essent evertitur sea, vis cu ubique referrentur, sed eu dicant expetendis. Eum cu</p>\r\n                    </div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-4\">\r\n                        <div class=\"address-block\">\r\n                            <i class=\"fa fa-desktop\" aria-hidden=\"true\"></i>\r\n                            <a href=\"mailto:somemail@mail.com\">somemail@mail.com</a>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-sm-4\">\r\n                        <div class=\"address-block\">\r\n                            <i class=\"fa fa-home\" aria-hidden=\"true\"></i>\r\n                            <address>\r\n                            T317 Timber Oak Drive<br>\r\n                            Sundown, TX 79372\r\n                            </address>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-sm-4\">\r\n                        <div class=\"address-block\">\r\n                            <i class=\"fa fa-phone\" aria-hidden=\"true\"></i>\r\n                            <a href=\"tel:+000123456789\">+000 - 123 - 456 - 789</a>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>'),
(18,	'Our Team Member',	'Eum cu tantas legere complectitur, hinc utamur ea eam. Eum patrioque mnesarchum eu, diam erant convenire et vis. Et essent evertitur sea, vis cu ubique referrentur, sed eu dicant expetendis. Eum cu'),
(19,	'Our Activity Section',	'<section class=\"page-section-ptb dark-bg bg fixed text-white bg-overlay-black-60 pb-50\" style=\"background-image:url(http://themes.potenzaglobalsolutions.com/cupidlovedesign/webapp/Newassets/images/bg/bg-2.jpg);\">\n<div class=\"container\">\n   <div class=\"row mb-70 xs-mb-30 text-center\">\n       <div class=\"col-lg-12 col-md-12\">\n          <h2 class=\"title divider\">Our Activities</h2>\n      </div>\n     </div>\n\n     <div class=\"row\">\n <div class=\"col-md-6 col-sm-12 xs-mb-30\">\n    <h4 class=\"mb-30 text-uppercase\">Why Us</h4>\n        <div class=\"accordion boxed\">\n            <div class=\"acd-group acd-active\"><a href=\"#\" class=\"acd-heading acd-active\">Nam libero tempore, cum soluta nobis est</a>\n                <div class=\"acd-des\" style=\"display: block;\">Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.</div>\n            </div>\n            <div class=\"acd-group\"><a href=\"#\" class=\"acd-heading\">Proin gravida nibh vel velit auctor aliquet</a>\n                <div class=\"acd-des\" style=\"display: none;\">Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example.</div>\n            </div>\n            <div class=\"acd-group\"><a href=\"#\" class=\"acd-heading\">Nisi elit consequat ipsum, nec sagittis?</a>\n                <div class=\"acd-des\" style=\"display: none;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.</div>\n            </div>\n            <div class=\"acd-group\"><a href=\"#\" class=\"acd-heading\">Morbi accumsan ipsum velit. Nam nec tellus a odio?</a>\n                <div class=\"acd-des\" style=\"display: none;\">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>\n            </div>\n      </div>\n    </div>\n    \n    \n     <div class=\"col-md-6 col-sm-12\">\n      <h4 class=\"mb-30 text-uppercase\">Skill</h4>\n       <div class=\"skill-bar skill-box\">\n            <div class=\"progress-bar progress-animated\" data-percent=\"96\" data-delay=\"0\" data-type=\"%\" style=\"width: 96%;\">\n                <div class=\"progress-title\">comprehensive range</div>\n            </div>\n        </div>\n\n        <div class=\"skill-bar skill-box\">\n            <div class=\"progress-bar progress-animated\" data-percent=\"75\" data-delay=\"100\" data-type=\"%\" style=\"width: 75%;\">\n                <div class=\"progress-title\">tempor incididun</div>\n            </div>\n        </div>\n\n        <div class=\"skill-bar skill-box\">\n            <div class=\"progress-bar progress-animated\" data-percent=\"88\" data-delay=\"200\" data-type=\"%\" style=\"width: 88%;\">\n                <div class=\"progress-title\">magna aliqu</div>\n            </div>\n        </div>\n\n        <div class=\"skill-bar skill-box\">\n            <div class=\"progress-bar progress-animated\" data-percent=\"60\" data-delay=\"300\" data-type=\"%\" style=\"width: 60%;\">\n                <div class=\"progress-title\">eiusmod tempor</div>\n            </div>\n        </div>\n        \n         <div class=\"skill-bar skill-box\">\n            <div class=\"progress-bar progress-animated\" data-percent=\"75\" data-delay=\"100\" data-type=\"%\" style=\"width: 75%;\">\n                <div class=\"progress-title\">patrioque mnesarchum</div>\n            </div>\n        </div>\n        \n  </div>\n    \n    \n   </div>\n  </div>\n</section>'),
(20,	'Frequently Asked Questions',	'Have a question? Please check our knowledgebase first.'),
(21,	'FAQ Questions',	'<div class=\"acd-group acd-active\"><a href=\"#\" class=\"acd-heading acd-active\">Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit</a>\n			<div class=\"acd-des\" style=\"display: block;\">\n            	<h5 class=\"title mb-20\">Sed ut perspiciatis unde omnis iste natus?</h5>\n                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt lorem ipsum dolor sit amet of Lorem Ipsum. auctor a ornare odio. Sed non mauris vitae erat</p>\n                <ol class=\"list-unstyled\">\n                   <li>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum</li>\n                   <li>Auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. </li>\n                   <li>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</li>\n                   <li>Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit Class.</li>\n                 </ol>\n            </div>\n        </div>\n        <div class=\"acd-group\"><a href=\"#\" class=\"acd-heading\">Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor?</a>\n			<div class=\"acd-des\" style=\"display: none;\">\n            	<h5 class=\"title mb-20\">There are many variations of passages alteration?</h5>\n                <p> Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise , except to obtain some advantage from it? But who has any right .</p>\n                <ol class=\"list-unstyled\">\n                   <li>Et harum quidem rerum facilis est et expedita distinctio nam libero tempore.</li>\n                   <li>Except to obtain some advantage ipsum, nec sagittis sem nibh id elit. </li>\n                   <li>Sed non mauris vitae amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</li>\n                   <li>Sed non  mauris vitae erat consequat auctor eu in elit Class. Nam nec tellus a odio tincidunt auctor a ornare odio. .</li>\n                 </ol>\n            </div>\n        </div>\n        <div class=\"acd-group\"><a href=\"#\" class=\"acd-heading\">Nisi elit consequat ipsum, nec sagittis?</a>\n			<div class=\"acd-des\" style=\"display: none;\">\n            	<h5 class=\"title mb-20\">It has survived not only five centuries?</h5>\n                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text.</p>\n                <ol class=\"list-unstyled\">\n                   <li>The generated Lorem Ipsum is therefore always free from repetition, injected humour.</li>\n                   <li>You need to be sure there isn\'t anything embarrassing in the middle of text.</li>\n                   <li>Except to obtain some advantage from it? But who has any right .</li>\n                   <li>Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit Class.</li>\n                 </ol>\n            </div>\n        </div>\n        <div class=\"acd-group\"><a href=\"#\" class=\"acd-heading\">Sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris?</a>\n			<div class=\"acd-des\" style=\"display: none;\">\n            	<h5 class=\"title mb-20\">The standard Lorem Ipsum passage?</h5>\n                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt Lorem ipsum dolor sit amet of Lorem Ipsum auctor a ornare odio. Sed non mauris vitae erat</p>\n                <ol class=\"list-unstyled\">\n                   <li> It uses a dictionary of over 200 Latin words, combined with a handful of model sentence</li>\n                   <li>Morbi accumsan ipsum velit consequat ipsum, nec sagittis sem nibh id elit. </li>\n                   <li>Sed non  mauris vitae erat consequat vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</li>\n                   <li>The generated Lorem Ipsum is therefore always free from repetition, injected humour.</li>\n                 </ol>\n            </div>\n        </div>\n        <div class=\"acd-group\"><a href=\"#\" class=\"acd-heading\">Morbi accumsan ipsum velit. Nam nec tellus a odio?</a>\n			<div class=\"acd-des\" style=\"display: none;\">\n            	<h5 class=\"title mb-20\">Rremaining essentially unchanged.?</h5>\n                <p> Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n                <ol class=\"list-unstyled\">\n                   <li>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque.</li>\n                   <li>But I must explain to you how all this mistaken idea of denouncing pleasure.</li>\n                   <li>Voluptatem accusantium nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</li>\n                   <li>Perspiciatis tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit Class.</li>\n                 </ol>\n            </div>\n        </div>\n        <div class=\"acd-group\"><a href=\"#\" class=\"acd-heading\">Tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit?</a>\n			<div class=\"acd-des\" style=\"display: none;\">\n            	<h5 class=\"title mb-20\">Sed ut perspiciatis unde omnis iste natus error?</h5>\n                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptate nesciunt.</p>\n                <ol class=\"list-unstyled\">\n                   <li>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus</li>\n                   <li>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus. </li>\n                   <li>DMorbi accumsan ipsum velit. uis sed odio sit amet nibh vulputate cursus a sit amet mauris. </li>\n                   <li>Consequat auctor eu in elit Class nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat .</li>\n                 </ol>\n            </div>\n        </div>\n        <div class=\"acd-group\"><a href=\"#\" class=\"acd-heading\">Class aptent taciti sociosqu?</a>\n			<div class=\"acd-des\" style=\"display: none;\">\n            	<h5 class=\"title mb-20\">Lorem ipsum dolor sit amet of Lorem Ipsum?</h5>\n                <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain are bound to ensue; to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These </p>\n                <ol class=\"list-unstyled\">\n                   <li>The wise man therefore always holds in these matters to this principle of selection</li>\n                   <li>Every pleasure is to be welcomed and every pain avoided. </li>\n                   <li>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</li>\n                   <li>Nor again is there anyone who loves or pursues or desires to obtain pain of itself, but because occasionally circumstances .</li>\n                 </ol>\n            </div>\n        </div>'),
(22,	'Home Page 3 Subscribe Section',	'<section class=\"page-section-ptb bg fixed text-white bg-overlay-black-50\" style=\"background-image:url(http://themes.potenzaglobalsolutions.com/cupidlovedesign/webapp/images/bg/bg-5.jpg)\"><div class=\"container\"><div class=\"row\">\n  <div class=\"col-sm-12 text-center\">\n      <h2 class=\"title title2 divider mb-30\">Get The Best <label>Dating</label> Team Now</h2>\n        <h5 class=\"pb-20\">Want to hear more story, subscribe for our newsletter</h5>\n        <a class=\"button  btn-lg btn-theme full-rounded animated right-icn\"><span>Subscribe<i class=\"glyph-icon flaticon-hearts\" aria-hidden=\"true\"></i></span></a>\n  </div>\n</div></div></section>');

DROP TABLE IF EXISTS `blockuser`;
CREATE TABLE `blockuser` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `blockid` int(11) DEFAULT NULL,
  `blockstatus` blob,
  `created_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text CHARACTER SET utf8 COLLATE utf8_bin,
  `slug` text CHARACTER SET utf8 COLLATE utf8_bin,
  `author` text CHARACTER SET utf8 COLLATE utf8_bin,
  `image` text,
  `created_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_bin,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `blog` (`id`, `title`, `slug`, `author`, `image`, `created_date`, `modified_date`, `description`) VALUES
(1,	'Intentions That Energize You',	'Intentions That Energize You',	'John Doe',	'01.jpg',	'2018-01-04 00:00:00',	'2018-01-04 00:00:00',	'Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Pellentesque in ipsum id orci porta dapibus. Curabitur aliquet quam.Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Pellentesque in ipsum id orci.'),
(2,	'A Brief History Of Creation',	'A Brief History Of Creation',	'John Doe',	'02.jpg\r\n',	'2018-01-04 00:00:00',	'2018-01-04 00:00:00',	'Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Pellentesque in ipsum id orci porta dapibus. Curabitur aliquet quam.Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Pellentesque in ipsum id orci.'),
(3,	'MAJOR MOTIVES OF OUR LIVES',	'MAJOR MOTIVES OF OUR LIVES',	'John Doe',	'03.jpg',	'2018-01-04 00:00:00',	'2018-01-04 00:00:00',	'Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Pellentesque in ipsum id orci porta dapibus. Curabitur aliquet quam.Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Pellentesque in ipsum id orci.'),
(4,	'A TIME TRAVEL POSTCARD',	'A TIME TRAVEL POSTCARD',	'John Doe',	'04.jpg',	'2018-01-04 00:00:00',	'2018-01-04 00:00:00',	'Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Pellentesque in ipsum id orci porta dapibus. Curabitur aliquet quam.Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Pellentesque in ipsum id orci.'),
(5,	'ALL FAITH NEEDS FEET',	'ALL FAITH NEEDS FEET',	'John Doe',	'05.jpg\r\n',	'2018-01-04 00:00:00',	'2018-01-04 00:00:00',	'Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Pellentesque in ipsum id orci porta dapibus. Curabitur aliquet quam.Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Pellentesque in ipsum id orci.');

DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `cms`;
CREATE TABLE `cms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `slug` text NOT NULL,
  `content` text NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `cms` (`id`, `title`, `slug`, `content`, `created_date`, `updated_date`, `language_id`, `parent_id`) VALUES
(1,	'cms title',	'cms sulg',	'cms content',	'2018-04-24 12:44:07',	'2018-04-24 12:44:07',	1,	0),
(2,	'cms',	'frdt',	'sertdrtg',	'2017-12-28 07:50:53',	'2017-12-28 07:50:53',	5,	1);

DROP TABLE IF EXISTS `comingsoon`;
CREATE TABLE `comingsoon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `date` varchar(255) NOT NULL,
  `comingsoon_image` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `comingsoon` (`id`, `title`, `date`, `comingsoon_image`) VALUES
(1,	'comingsoon',	'12/27/2017',	'bg-4.jpg');

DROP TABLE IF EXISTS `configuration`;
CREATE TABLE `configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` text NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `configuration` (`id`, `key`, `value`) VALUES
(1,	'GOOGLE_PLACE_API_KEY',	''),
(2,	'FACEBOOK_KEY',	''),
(3,	'XMPP_ENABLE',	''),
(4,	'XMPP_HOST',	''),
(5,	'XMPP_DEFAULT_PASSWORD',	''),
(6,	'XMPP_SERVER',	''),
(8,	'PUSH_ENABLE_SANDBOX',	''),
(9,	'PUSH_SANDBOX_GATEWAY_URL',	''),
(10,	'PUSH_GATEWAY_URL',	''),
(11,	'ANDROID_FCM_KEY',	''),
(12,	'INSTAGRAM_CALLBACK_BASE',	'/'),
(13,	'INSTAGRAM_CLIENT_SECRET',	''),
(14,	'INSTAGRAM_CLIENT_ID',	''),
(15,	'adMobKey',	''),
(16,	'adMobVideoKey',	''),
(17,	'RemoveAddInAppPurchase',	''),
(18,	'TermsAndConditionsUrl',	''),
(19,	'APP_XMPP_HOST',	''),
(20,	'APP_XMPP_SERVER',	'');

DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `subject` varchar(150) NOT NULL,
  `message` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `contact` (`id`, `name`, `email`, `subject`, `message`) VALUES
(1,	'Keyur Patel',	'keyur.patel@potenzaglobalsolutions.com',	'Hello',	'afafassf'),
(2,	'#@#@$@354353651243shdfjhsfhfkjhdjfhjsdhfjk sdhfjkd',	'fdjhfjsdhfkjhsdkjfhjdshfjhdkjshfkhsdkfh$#%$#%$#%$#%@gmail.com',	'4%$%^$65564@#$$5765345274fgsjdfgjsdghdjfhjgfsdhfsdf',	'fsdjfghdshdsjhg$%@$%@%$#$$%#%@$#!#$@$#%$#^%$^%$&^$&^$^&$^&$gfjgdsgf hghd fghds fhgdsh fdfjgdjs hfjgsdhh h fsdjgfs hds ds ds ds ds ds ds ds ds ds ds ds ds ds ds ds ds ds ds ds ds ds ds ds ds ds ds ds d'),
(3,	'sfsd',	'shruti.thakor@gmail.com',	'dfd',	'dsf'),
(4,	'dfsd',	'shruti.thakor@gmail.com',	'dfs',	'sfdsd'),
(5,	'Estimate',	'test@test.com',	'test',	'test'),
(6,	'Kiran Suthar',	'test@test.com',	'test',	'test'),
(7,	'ni',	'ni@yopmail.com',	'sada',	'sdfasdasd'),
(8,	'ni',	'ni@yopmail.com',	'sada',	'asdsadas');

DROP TABLE IF EXISTS `datepreference`;
CREATE TABLE `datepreference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0- Active,1-Deactive',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `datepreference` (`id`, `name`, `status`, `created_date`, `modified_date`) VALUES
(1,	'Coffee',	'1',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00'),
(2,	'Drink',	'1',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00'),
(3,	'Food',	'1',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00'),
(4,	'Fun',	'1',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00');

DROP TABLE IF EXISTS `error_404`;
CREATE TABLE `error_404` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `sub_title` text NOT NULL,
  `description` text NOT NULL,
  `error_image` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `error_404` (`id`, `title`, `sub_title`, `description`, `error_image`) VALUES
(1,	'404',	'This is Awkward',	'Think back over your life. Think about the people that had a positive influence on you. If your past was like mine, many of them didn&rsquo;t realize the impact they made. The influence was usually due to them caring about you and doing some little thing. What little things have been done for you that changed your life? What little things have you done for someone else that might have changed theirs?',	'error-404.png');

DROP TABLE IF EXISTS `ethnicity`;
CREATE TABLE `ethnicity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `ethnicity` (`id`, `status`, `created_date`, `modified_date`, `name`) VALUES
(1,	1,	'2017-03-03 00:00:00',	'2017-03-03 00:00:00',	' South Korea'),
(2,	1,	'2017-03-03 00:00:00',	'2017-03-03 00:00:00',	'Black / African Decad'),
(4,	1,	'2017-06-19 00:00:00',	'2017-06-14 00:00:00',	'Hispanic / Latino'),
(5,	1,	'2017-06-20 00:00:00',	'2017-06-14 00:00:00',	'Middle East'),
(6,	1,	'2017-06-20 00:00:00',	'2017-06-20 00:00:00',	'Pacific island'),
(7,	1,	'2017-06-21 00:00:00',	'2017-06-14 00:00:00',	'South Asian'),
(8,	1,	'2017-06-10 00:00:00',	'2017-06-03 00:00:00',	'White / Caucasian'),
(10,	1,	'2017-05-29 13:31:40',	'2017-05-29 13:31:40',	'Other'),
(12,	1,	'2017-05-29 18:08:26',	'2017-05-29 18:08:26',	'South Indian'),
(13,	1,	'2018-03-12 16:53:06',	'2018-03-12 16:53:06',	'arabian');

DROP TABLE IF EXISTS `ethnicity_lang`;
CREATE TABLE `ethnicity_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ethnicity_id` int(11) NOT NULL,
  `language` varchar(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `ethnicity_lang` (`id`, `ethnicity_id`, `language`, `value`) VALUES
(1,	12,	'french',	'Indien du Sud'),
(2,	12,	'russian',	'Южно-индийский');

DROP TABLE IF EXISTS `friends`;
CREATE TABLE `friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `send_user_id` int(11) NOT NULL,
  `receive_user_id` int(11) NOT NULL,
  `approved` enum('0','1','2') NOT NULL DEFAULT '2',
  `status` enum('0','1') NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_frd` (`send_user_id`,`receive_user_id`),
  KEY `send_user_id` (`send_user_id`),
  KEY `receive_user_id` (`receive_user_id`),
  CONSTRAINT `friends_ibfk_1` FOREIGN KEY (`send_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `friends_ibfk_5` FOREIGN KEY (`receive_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `instaimages`;
CREATE TABLE `instaimages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `userid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `language`;
CREATE TABLE `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `rtl` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `language` (`id`, `name`, `status`, `created_date`, `modified_date`, `rtl`) VALUES
(1,	'english',	1,	'2017-06-11 03:10:17',	'2017-06-11 02:06:07',	0),
(2,	'french',	0,	'2017-12-26 12:21:10',	'2017-12-26 12:21:10',	0),
(3,	'arabic',	0,	'2018-01-02 00:00:00',	'2018-01-02 00:00:00',	1);

DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_name` text NOT NULL,
  `designation` text NOT NULL,
  `member_detail` text NOT NULL,
  `member_image` text,
  `facebook_url` text NOT NULL,
  `twitter_url` text NOT NULL,
  `google_url` text NOT NULL,
  `dribble_url` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `members` (`id`, `member_name`, `designation`, `member_detail`, `member_image`, `facebook_url`, `twitter_url`, `google_url`, `dribble_url`) VALUES
(1,	'Bill Nelson',	'Founder',	'<p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. consectetur adipisicing eli. Vero quod conseqt quibusdam enim expedita sed quia nesciunt Lorem ipsum dolor sit amet, incidunt accusamus necessitatibus modi adipisci officia libero accusantium esse hic obcaecati.</p>',	'team1.png',	'http://www.facebook.com',	'http://www.twitter.com',	'http://www.google.com',	'http://www.dribble.com'),
(2,	'Francisco Pierce',	'Photographer',	'<p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. consectetur adipisicing eli. Vero quod conseqt quibusdam enim expedita sed quia nesciunt Lorem ipsum dolor sit amet, incidunt accusamus necessitatibus modi adipisci officia libero accusantium esse hic obcaecati.</p>',	'team2.png',	'http://www.facebook.com',	'http://www.twitter.com',	'http://www.google.com',	'http://www.dribble.com'),
(3,	'Nelle Townsend',	'Interpreter',	'<p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. consectetur adipisicing eli. Vero quod conseqt quibusdam enim expedita sed quia nesciunt Lorem ipsum dolor sit amet, incidunt accusamus necessitatibus modi adipisci officia libero accusantium esse hic obcaecati.</p>',	'team3.png',	'http://www.facebook.com',	'http://www.twitter.com',	'http://www.google.com',	'http://www.dribble.com'),
(4,	'Glen Bell',	'Administrator',	'<p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. consectetur adipisicing eli. Vero quod conseqt quibusdam enim expedita sed quia nesciunt Lorem ipsum dolor sit amet, incidunt accusamus necessitatibus modi adipisci officia libero accusantium esse hic obcaecati.</p>',	'team4.png',	'http://www.facebook.com',	'http://www.twitter.com',	'http://www.google.com',	'http://www.dribble.com');

DROP TABLE IF EXISTS `notification`;
CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `message` varchar(200) NOT NULL,
  `users` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `questions` (`id`, `created_date`, `modified_date`, `name`) VALUES
(1,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'What are you looking for in a relationship'),
(2,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'Tell us an embarrassing story'),
(3,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'Tell us about your pet'),
(4,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'How was it growing in your hometown?'),
(5,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'The biggest lesson I ever learned was'),
(6,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'Favorite movie'),
(7,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'What song makes you want to sing aloud'),
(8,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'The worst date ever'),
(9,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'What is your secret talent'),
(10,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'Tell me a joke'),
(11,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'I make a difference in the world by'),
(12,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'Two truths and a lie'),
(13,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'Lifeline'),
(15,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'The biggest fear'),
(16,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'What did you want to be when you\'re tall?'),
(17,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'What are your dreams'),
(18,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'My friends say that I am'),
(19,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'What did you love for'),
(20,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'What is your guilty pleasure'),
(21,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'What do you like about your job?'),
(22,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'What would you like'),
(23,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'What ritual you must do every day'),
(24,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'Sometimes I can be'),
(26,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'What always makes you smile'),
(27,	'2017-06-05 00:00:00',	'2017-06-05 00:00:00',	'What is your name?');

DROP TABLE IF EXISTS `questions_lang`;
CREATE TABLE `questions_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `language` varchar(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `questions_lang` (`id`, `question_id`, `language`, `value`) VALUES
(1,	27,	'french',	'Quel est ton nom?'),
(2,	27,	'russian',	'Каково ваше имя?');

DROP TABLE IF EXISTS `religions`;
CREATE TABLE `religions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` enum('0','1') NOT NULL COMMENT '0-Active,1-Deactive',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `religions` (`id`, `status`, `created_date`, `modified_date`, `name`) VALUES
(1,	'1',	'2017-03-03 00:00:00',	'2017-03-03 00:00:00',	'Hindu'),
(2,	'1',	'2017-03-03 00:00:00',	'2017-03-03 00:00:00',	'Buddhist'),
(3,	'1',	'2017-03-03 00:00:00',	'2017-03-03 00:00:00',	'Catholic'),
(4,	'1',	'2017-06-13 00:00:00',	'2017-06-20 00:00:00',	'Christian'),
(5,	'1',	'2017-06-12 00:00:00',	'2017-06-13 00:00:00',	'Jewish'),
(6,	'1',	'2017-06-12 00:00:00',	'2017-06-12 00:00:00',	'Muslim'),
(7,	'1',	'2017-06-13 00:00:00',	'2017-06-19 00:00:00',	'Spiritual'),
(8,	'1',	'2018-04-24 19:02:19',	'2018-04-24 19:02:19',	'Agnostic'),
(12,	'1',	'2017-05-29 13:28:55',	'2017-05-29 13:28:55',	'Other'),
(21,	'1',	'2017-05-29 17:53:11',	'2017-05-29 17:53:11',	'Siya Muslim');

DROP TABLE IF EXISTS `religions_lang`;
CREATE TABLE `religions_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `religion_id` int(11) NOT NULL,
  `language` varchar(100) NOT NULL,
  `value` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `religions_lang` (`id`, `religion_id`, `language`, `value`) VALUES
(1,	21,	'french',	'Siya Muslim'),
(2,	21,	'russian',	'Сия Муслим');

DROP TABLE IF EXISTS `reporteuser`;
CREATE TABLE `reporteuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_from_id` int(11) NOT NULL,
  `report_to_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `input_type` enum('input','textarea','radio','dropdown','timezones') CHARACTER SET latin1 NOT NULL,
  `options` text COMMENT 'Use for radio and dropdown: key|value on each line',
  `is_numeric` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'forces numeric keypad on mobile devices',
  `show_editor` enum('0','1') NOT NULL DEFAULT '0',
  `input_size` enum('large','medium','small') DEFAULT NULL,
  `translate` enum('0','1') NOT NULL DEFAULT '0',
  `help_text` varchar(256) DEFAULT NULL,
  `validation` varchar(128) NOT NULL,
  `sort_order` tinyint(3) unsigned NOT NULL,
  `label` varchar(128) NOT NULL,
  `value` text COMMENT 'If translate is 1, just start with your default language',
  `last_update` datetime DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `updated_by` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `settings` (`id`, `name`, `input_type`, `options`, `is_numeric`, `show_editor`, `input_size`, `translate`, `help_text`, `validation`, `sort_order`, `label`, `value`, `last_update`, `updated_by`) VALUES
(1,	'site_name',	'input',	NULL,	'0',	'0',	'large',	'0',	NULL,	'required|trim|min_length[3]|max_length[128]',	10,	'Site Name',	'Audit',	'2017-02-07 13:15:54',	1),
(2,	'per_page_limit',	'dropdown',	'10|10\r\n25|25\r\n50|50\r\n75|75\r\n100|100',	'1',	'0',	'small',	'0',	NULL,	'required|trim|numeric',	50,	'Items Per Page',	'10',	'2017-02-07 13:15:54',	1),
(3,	'meta_keywords',	'input',	NULL,	'0',	'0',	'large',	'0',	'Comma-seperated list of site keywords',	'trim',	20,	'Meta Keywords',	'these, are, keywords',	'2017-02-07 13:15:54',	1),
(4,	'meta_description',	'textarea',	NULL,	'0',	'0',	'large',	'0',	'Short description describing your site.',	'trim',	30,	'Meta Description',	'This is the site description.',	'2017-02-07 13:15:54',	1),
(5,	'site_email',	'input',	NULL,	'0',	'0',	'medium',	'0',	'Email address all emails will be sent from.',	'required|trim|valid_email',	40,	'Site Email',	'nitinpatoliya19@gmail.com',	'2017-02-07 13:15:54',	1),
(6,	'timezones',	'timezones',	NULL,	'0',	'0',	'medium',	'0',	NULL,	'required|trim',	60,	'Timezone',	'UTC',	'2017-02-07 13:15:54',	1),
(7,	'welcome_message',	'textarea',	NULL,	'0',	'1',	'large',	'1',	'Message to display on home page.',	'trim',	70,	'Welcome Message',	'a:7:{s:5:\"dutch\";s:4489:\"<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD8AAABjCAYAAAAsE9hTAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABZgAAAWYAGbyQRvAAAACXZwQWcAAAA/AAAAYwCuLjAVAAALCUlEQVR42tXdaZBcVRUH8N90iIQlEAwJEJZSYgUUEYiBoAbZZAcBxVIQlwqogDuuuIC4b4iKWoIiUIpooYBioSggilCgRJEQFCGJoCIYWcOShcz44bwmd3r6LdP9umf8f0nS7/ad+7/n3HPP9iYDeoDTrloMz8Bm+DeeOv1lM3vxo7pCo4dzr8JqbIOBbEPGFXpCPpHy/ZiUbYDxtgE9k3y2AUO4C9thi7Em2zfyyQaswmIchfXHk/R7Sj7BYmyMYxk/6t9z8sn5vxZvw4vGywb0S/Lw1+zPT2LaWBPvN/lHsAT74F2YMNbS7yf51ViW/f0kHMTYqn8/yROWH6bgI9h6zJiPAflJyd/nCvVfZ6yk30/yEzG15bP52J+xUf9+kt8AW7Z8NgUfMEbWv5/kt9T+jM/DcfRf+v0kvxM2zVnDidi5r8z7QT6R5osxIWfYNuL6m9hP6fdL8tMy8kU4Ci+lf+rfU/IJibl4bsnwTfB2YRj//8lnGMChht/xeXiZcH/7Iv1+kN82I1UFGwjLv34f1tU78onkjsBospf76tPZ77Xkp+HVo/zOhngt1unx2npDPpHYATq7v/cReb//P/IZpojzO7GD784QRrKnql87+WSxByu/24twCDbqGfNekM8wBceLqk2n2FG5bzB+yCdSPxwv6XK6Kdi1Zd7xSz7D5sJP70bqTewinKTxTT6RztGYU9O022K9cU8+w0y8ucZ5p+qht1eLI5FJvYETsH2N65uYrvHg794y7OEVr9u5q8m7llBLvP76GonDk9ZmfJvYUD32pDb13ADvxvSayf8dy1s+2wjPY6Qm9JV8y9V2cM3E4QZR7EixDM9XQ9KzDsnPEPn3KvH6aPAQfsuwYqdkMw6lO+l3TD6R+nz1XW0pbsai9IPEwP1TlLu7OmYdkU+I74Q3qd8RGcSP8HjO84fEuT+IzqXfjdqvi3fK+m1qxl/wC0aofBNrhMU/Whc5v1GTT6R+kMi49gLfwz0FzyeJNPjuQvv6Qz7DZngvJveA+CL8gFypE8WPSaLVZT86U/1RkW8xci/qAfE1+Ka430cgIThTHDsiOdpR3F/ZvU2I7yLc2F5EhNfhIgqlPoDZyb+3x7Px53TQ0GktXzp95ESjJTAJJ+uNkXsYn8cDJeOmy+L8DJvIT3o8Q2YQWzejMvlE6ofhyB4QhwvwKwqlDi8wPBU+Ac+i7bl/Ktustg7YaCQ/A+/Rm3LSApypoEE5IbafkWFua9NDU80HhUe4PSOlX0o+k/qAcGZ26wHxR0R72t0Vxm4tc2tbUORa/0cckxHtr4XkE3Wfozee3BDOwc/IV/dE6gdgVpshRTyaIfHrGS79Kmq/nghXt6wwdrS4Fl9SrR9/akagXY1/VbsvJBZ+Md4gK6A0NyCXfIuRO7wHxO/FqbivaFAi9SPl+xbLFGOZcMzeba1/UCr5LcTVVncebZW41n5HqXVvruMt2vsla0Rbe1FaawVW4pWy7q+h03LIJ1I/Tm+M3CU4t4x4IvXj8MKcYQ/g9go/c8jajNMzaUM+IT5bZGLrNnK34+N4rGhQQnyOkHreOm7D0pKfub616j5PlnXKU/tJ2Q7V3R76GD4hQtYq6r6+8C22Khjza/lxfxNpCnyiLBQeRj6R+iF648mdK1S+qrq/RjQ35OG/sri/JI29mcTQiXt/50bOwJPV78ndgC9gVUXiO4juzCIH5nqh9m2R3OkzDdfyqdjj6Q9awtXdayb+AE7HvyqO3wAf0t6haWKViPtXlMw1QVR8UzQwt1XyO6i33MRaL+5qKqv7seJaKsIfcRWlKr+J9l0esxoMKzcdL4uQasSNOAtrKhKfgw8afkZbsQbniTNfhu1EvN+KaamEZwsDUyeWC2fm3xXHb4KPKRfATbiUfKkn532e9pmeyY3krB8jaut14oe4gkrqPiDq+geWzLkCX1Pu0hJxyV45zyY2JT8TL6+Z+N34qurWfS+8Q35zchOX46dUqtLuaHjWJ8VAk/z+ohGgLgyJO31hxfHT8VHlFZh78DklTk2i8odpk+jIsLIh7tED1OvGLsT5VFL3hnjZcK+SOVcK+7GASlKfoVibH28Ia1hnrW1QWOJ/FA1K1H1f8bJB2eZflM1bSDyR+hHi6s7D/Y2MeJ119YW4mEq++3TxitmmJeMWiJjgiYrdGNNF8qLIfixtCG+uky7JPFyoxJNLrPubxVVUhPtwingbsxCJ1I+RHwI3cUtDsWqMFktwGZXycXOFuhd5kyvwKVlKu6K67yhsSJHUl+P6huJwcbS4XJZVKcFkEbTMKBn3HXy7jHiCjUVqrOy8LcXChvCq6sCjIlwdqiD1o5W3sVwtUtoryohnUp8g/IQjKqz1WvyrodiHHg3+hFsqjNtehMxFHVVLhH9f6hYn6n6kSMCU1R8flbnGdUZvV2QTt0Um9YnZAot66Z8Qlv1mKp/zFwvnp4oWX4PfN8k/VuELZXhIpJPaGrpE3efhVSVznS+r1FYkPgtfVs1DXS7C6yea5MuqolXwN9xZMmaCiNOLpHMTPouVFQ3cFhnxXasMFjbpGqKg0aiw6CpYJGpuRdgKexY8f0Rca/8omacp9Y2zjTqo4hoXi+rQyuYHDWGousUdIpgpwizFJa/vq5CMzIiviw+LF5GqYIWwCbeytozVEN0Qj1acpB0GRV9cmTu7jfxk5FJ8HasrEG82OJc5MikuEE1Owzo0mpK/owvyTylXeUJNixa3qOjLLQHLqar34V8tiiRPtramNMTvr/p5F+SHjOyPbYe8MUuUWPeE+E74tKzcVAG3Cp/i3nYPm/f8parn2VoxoFoL+P0i8diKy8RtUYZN8RnV37dbLI7GsHPejvzCbBGdYKJilW5iiZG25RH8hFKpD4gg6ICKa1oq8oHX5RGHRmak1uBsxV2PeRiQBUclb0EtNlLCCxW4xC0e3EmqeaRLhEH8ZRFxLZP9WTQADnawAdspz8Q81FxQghuV3zTNYmWVzPIiUXEqJf40+eSKOhtXdkB+RwWeW6LSl1hrfIZkjYPtVD6R+l6yhoIS/AFvxG+qEH+afIIHRRa1SkyeYpbs1Y8S3Cpy+YSnVWZkJ4i3sMuKpteItNXNVYkPI59If4FIGz04CvIbyVzXvHOfSXcQ3xDqOaS8yLgN9ih4PiRuqvmymn9V4sPIt2zAJUIDRhPxFeXIU9wlKrZPyMnkJCq/u/wGiUHhtZ0g6+EbDfER5JMNGMS3skWWdT00sTP2plT6xOaeId63XaegXXye/Cak88S7Pf8ZOH30xNuSTzZgNb4iUstV3Nd1hfoV9uBnG7BGhKK/ke+tTRZ9tu2InyP6/R/shHQh+ZYNOEu8TlLFA9xbtLQU3vnZBjwpHKu8guPmRpaWB0VS8xQ83A1xKpSokt7bffFF5a913IRX4N5Of8txdub3FKmxtAfwQpGk7EriTZR6TMnvr71KpKAulNPumWE3mTfW5XvvWxkeAl+J99dFvBL55gZkm3Cn6Il7n/yqzICwwAfS1Yv/05L1LRJn/N5OJ+uYfLoJwvqfJVLFP5akhRJMFRnY54x2A5Jrrmk4l4tujdvozKrXQj7ZgCFr3ckThefWGhPMFrF3p0WR5touljUj1EmcLmvyiUS3Fu3g8w1PITevtI9gRVUDmEn/PeJ4HYIFdRPvmnzLJgwIH/9Y4Y83G/9WCG/xTCUdWS3k9xYp6TOwZtySb7MJzxI24Sjh+a3CW8VNUVq3T2pvDazuBfHaybdsApF62kdowubCRtw5Xv6Xg/8BZXrc12H898kAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTMtMDgtMTNUMDA6MTM6NDktMDc6MDCUK2T1AAAAJXRFWHRkYXRlOm1vZGlmeQAyMDEzLTA4LTEzVDAwOjEzOjQ5LTA3OjAw5XbcSQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAUdEVYdFRpdGxlAEdhcyBGbGFtZSBMb2dvWz7WuwAAAABJRU5ErkJggg==\" data-filename=\"ci3-fire-starter.png\" style=\"line-height: 1.42857; width: 63px; float: left;\"></p><p>Deze inhoud wordt <em style=\"color: rgb(41, 82, 24); background-color: rgb(255, 239, 198);\">dynamisch</em> gegenereerd. <strong>Deze tekst kan worden bewerkt in de admin -instellingen.</strong></p><p></p>\";s:7:\"english\";s:4483:\"<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD8AAABjCAYAAAAsE9hTAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABZgAAAWYAGbyQRvAAAACXZwQWcAAAA/AAAAYwCuLjAVAAALCUlEQVR42tXdaZBcVRUH8N90iIQlEAwJEJZSYgUUEYiBoAbZZAcBxVIQlwqogDuuuIC4b4iKWoIiUIpooYBioSggilCgRJEQFCGJoCIYWcOShcz44bwmd3r6LdP9umf8f0nS7/ad+7/n3HPP9iYDeoDTrloMz8Bm+DeeOv1lM3vxo7pCo4dzr8JqbIOBbEPGFXpCPpHy/ZiUbYDxtgE9k3y2AUO4C9thi7Em2zfyyQaswmIchfXHk/R7Sj7BYmyMYxk/6t9z8sn5vxZvw4vGywb0S/Lw1+zPT2LaWBPvN/lHsAT74F2YMNbS7yf51ViW/f0kHMTYqn8/yROWH6bgI9h6zJiPAflJyd/nCvVfZ6yk30/yEzG15bP52J+xUf9+kt8AW7Z8NgUfMEbWv5/kt9T+jM/DcfRf+v0kvxM2zVnDidi5r8z7QT6R5osxIWfYNuL6m9hP6fdL8tMy8kU4Ci+lf+rfU/IJibl4bsnwTfB2YRj//8lnGMChht/xeXiZcH/7Iv1+kN82I1UFGwjLv34f1tU78onkjsBospf76tPZ77Xkp+HVo/zOhngt1unx2npDPpHYATq7v/cReb//P/IZpojzO7GD784QRrKnql87+WSxByu/24twCDbqGfNekM8wBceLqk2n2FG5bzB+yCdSPxwv6XK6Kdi1Zd7xSz7D5sJP70bqTewinKTxTT6RztGYU9O022K9cU8+w0y8ucZ5p+qht1eLI5FJvYETsH2N65uYrvHg794y7OEVr9u5q8m7llBLvP76GonDk9ZmfJvYUD32pDb13ADvxvSayf8dy1s+2wjPY6Qm9JV8y9V2cM3E4QZR7EixDM9XQ9KzDsnPEPn3KvH6aPAQfsuwYqdkMw6lO+l3TD6R+nz1XW0pbsai9IPEwP1TlLu7OmYdkU+I74Q3qd8RGcSP8HjO84fEuT+IzqXfjdqvi3fK+m1qxl/wC0aofBNrhMU/Whc5v1GTT6R+kMi49gLfwz0FzyeJNPjuQvv6Qz7DZngvJveA+CL8gFypE8WPSaLVZT86U/1RkW8xci/qAfE1+Ka430cgIThTHDsiOdpR3F/ZvU2I7yLc2F5EhNfhIgqlPoDZyb+3x7Px53TQ0GktXzp95ESjJTAJJ+uNkXsYn8cDJeOmy+L8DJvIT3o8Q2YQWzejMvlE6ofhyB4QhwvwKwqlDi8wPBU+Ac+i7bl/Ktustg7YaCQ/A+/Rm3LSApypoEE5IbafkWFua9NDU80HhUe4PSOlX0o+k/qAcGZ26wHxR0R72t0Vxm4tc2tbUORa/0cckxHtr4XkE3Wfozee3BDOwc/IV/dE6gdgVpshRTyaIfHrGS79Kmq/nghXt6wwdrS4Fl9SrR9/akagXY1/VbsvJBZ+Md4gK6A0NyCXfIuRO7wHxO/FqbivaFAi9SPl+xbLFGOZcMzeba1/UCr5LcTVVncebZW41n5HqXVvruMt2vsla0Rbe1FaawVW4pWy7q+h03LIJ1I/Tm+M3CU4t4x4IvXj8MKcYQ/g9go/c8jajNMzaUM+IT5bZGLrNnK34+N4rGhQQnyOkHreOm7D0pKfub616j5PlnXKU/tJ2Q7V3R76GD4hQtYq6r6+8C22Khjza/lxfxNpCnyiLBQeRj6R+iF648mdK1S+qrq/RjQ35OG/sri/JI29mcTQiXt/50bOwJPV78ndgC9gVUXiO4juzCIH5nqh9m2R3OkzDdfyqdjj6Q9awtXdayb+AE7HvyqO3wAf0t6haWKViPtXlMw1QVR8UzQwt1XyO6i33MRaL+5qKqv7seJaKsIfcRWlKr+J9l0esxoMKzcdL4uQasSNOAtrKhKfgw8afkZbsQbniTNfhu1EvN+KaamEZwsDUyeWC2fm3xXHb4KPKRfATbiUfKkn532e9pmeyY3krB8jaut14oe4gkrqPiDq+geWzLkCX1Pu0hJxyV45zyY2JT8TL6+Z+N34qurWfS+8Q35zchOX46dUqtLuaHjWJ8VAk/z+ohGgLgyJO31hxfHT8VHlFZh78DklTk2i8odpk+jIsLIh7tED1OvGLsT5VFL3hnjZcK+SOVcK+7GASlKfoVibH28Ia1hnrW1QWOJ/FA1K1H1f8bJB2eZflM1bSDyR+hHi6s7D/Y2MeJ119YW4mEq++3TxitmmJeMWiJjgiYrdGNNF8qLIfixtCG+uky7JPFyoxJNLrPubxVVUhPtwingbsxCJ1I+RHwI3cUtDsWqMFktwGZXycXOFuhd5kyvwKVlKu6K67yhsSJHUl+P6huJwcbS4XJZVKcFkEbTMKBn3HXy7jHiCjUVqrOy8LcXChvCq6sCjIlwdqiD1o5W3sVwtUtoryohnUp8g/IQjKqz1WvyrodiHHg3+hFsqjNtehMxFHVVLhH9f6hYn6n6kSMCU1R8flbnGdUZvV2QTt0Um9YnZAot66Z8Qlv1mKp/zFwvnp4oWX4PfN8k/VuELZXhIpJPaGrpE3efhVSVznS+r1FYkPgtfVs1DXS7C6yea5MuqolXwN9xZMmaCiNOLpHMTPouVFQ3cFhnxXasMFjbpGqKg0aiw6CpYJGpuRdgKexY8f0Rca/8omacp9Y2zjTqo4hoXi+rQyuYHDWGousUdIpgpwizFJa/vq5CMzIiviw+LF5GqYIWwCbeytozVEN0Qj1acpB0GRV9cmTu7jfxk5FJ8HasrEG82OJc5MikuEE1Owzo0mpK/owvyTylXeUJNixa3qOjLLQHLqar34V8tiiRPtramNMTvr/p5F+SHjOyPbYe8MUuUWPeE+E74tKzcVAG3Cp/i3nYPm/f8parn2VoxoFoL+P0i8diKy8RtUYZN8RnV37dbLI7GsHPejvzCbBGdYKJilW5iiZG25RH8hFKpD4gg6ICKa1oq8oHX5RGHRmak1uBsxV2PeRiQBUclb0EtNlLCCxW4xC0e3EmqeaRLhEH8ZRFxLZP9WTQADnawAdspz8Q81FxQghuV3zTNYmWVzPIiUXEqJf40+eSKOhtXdkB+RwWeW6LSl1hrfIZkjYPtVD6R+l6yhoIS/AFvxG+qEH+afIIHRRa1SkyeYpbs1Y8S3Cpy+YSnVWZkJ4i3sMuKpteItNXNVYkPI59If4FIGz04CvIbyVzXvHOfSXcQ3xDqOaS8yLgN9ih4PiRuqvmymn9V4sPIt2zAJUIDRhPxFeXIU9wlKrZPyMnkJCq/u/wGiUHhtZ0g6+EbDfER5JMNGMS3skWWdT00sTP2plT6xOaeId63XaegXXye/Cak88S7Pf8ZOH30xNuSTzZgNb4iUstV3Nd1hfoV9uBnG7BGhKK/ke+tTRZ9tu2InyP6/R/shHQh+ZYNOEu8TlLFA9xbtLQU3vnZBjwpHKu8guPmRpaWB0VS8xQ83A1xKpSokt7bffFF5a913IRX4N5Of8txdub3FKmxtAfwQpGk7EriTZR6TMnvr71KpKAulNPumWE3mTfW5XvvWxkeAl+J99dFvBL55gZkm3Cn6Il7n/yqzICwwAfS1Yv/05L1LRJn/N5OJ+uYfLoJwvqfJVLFP5akhRJMFRnY54x2A5Jrrmk4l4tujdvozKrXQj7ZgCFr3ckThefWGhPMFrF3p0WR5touljUj1EmcLmvyiUS3Fu3g8w1PITevtI9gRVUDmEn/PeJ4HYIFdRPvmnzLJgwIH/9Y4Y83G/9WCG/xTCUdWS3k9xYp6TOwZtySb7MJzxI24Sjh+a3CW8VNUVq3T2pvDazuBfHaybdsApF62kdowubCRtw5Xv6Xg/8BZXrc12H898kAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTMtMDgtMTNUMDA6MTM6NDktMDc6MDCUK2T1AAAAJXRFWHRkYXRlOm1vZGlmeQAyMDEzLTA4LTEzVDAwOjEzOjQ5LTA3OjAw5XbcSQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAUdEVYdFRpdGxlAEdhcyBGbGFtZSBMb2dvWz7WuwAAAABJRU5ErkJggg==\" data-filename=\"ci3-fire-starter.png\" style=\"line-height: 1.42857; width: 63px; float: left;\"></p><p>This content is being generated <em style=\"color: rgb(41, 82, 24); background-color: rgb(255, 239, 198);\">dynamically</em>. <strong>This text is editable in the admin settings.</strong></p>\r\n<p></p>\";s:10:\"indonesian\";s:4476:\"<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD8AAABjCAYAAAAsE9hTAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABZgAAAWYAGbyQRvAAAACXZwQWcAAAA/AAAAYwCuLjAVAAALCUlEQVR42tXdaZBcVRUH8N90iIQlEAwJEJZSYgUUEYiBoAbZZAcBxVIQlwqogDuuuIC4b4iKWoIiUIpooYBioSggilCgRJEQFCGJoCIYWcOShcz44bwmd3r6LdP9umf8f0nS7/ad+7/n3HPP9iYDeoDTrloMz8Bm+DeeOv1lM3vxo7pCo4dzr8JqbIOBbEPGFXpCPpHy/ZiUbYDxtgE9k3y2AUO4C9thi7Em2zfyyQaswmIchfXHk/R7Sj7BYmyMYxk/6t9z8sn5vxZvw4vGywb0S/Lw1+zPT2LaWBPvN/lHsAT74F2YMNbS7yf51ViW/f0kHMTYqn8/yROWH6bgI9h6zJiPAflJyd/nCvVfZ6yk30/yEzG15bP52J+xUf9+kt8AW7Z8NgUfMEbWv5/kt9T+jM/DcfRf+v0kvxM2zVnDidi5r8z7QT6R5osxIWfYNuL6m9hP6fdL8tMy8kU4Ci+lf+rfU/IJibl4bsnwTfB2YRj//8lnGMChht/xeXiZcH/7Iv1+kN82I1UFGwjLv34f1tU78onkjsBospf76tPZ77Xkp+HVo/zOhngt1unx2npDPpHYATq7v/cReb//P/IZpojzO7GD784QRrKnql87+WSxByu/24twCDbqGfNekM8wBceLqk2n2FG5bzB+yCdSPxwv6XK6Kdi1Zd7xSz7D5sJP70bqTewinKTxTT6RztGYU9O022K9cU8+w0y8ucZ5p+qht1eLI5FJvYETsH2N65uYrvHg794y7OEVr9u5q8m7llBLvP76GonDk9ZmfJvYUD32pDb13ADvxvSayf8dy1s+2wjPY6Qm9JV8y9V2cM3E4QZR7EixDM9XQ9KzDsnPEPn3KvH6aPAQfsuwYqdkMw6lO+l3TD6R+nz1XW0pbsai9IPEwP1TlLu7OmYdkU+I74Q3qd8RGcSP8HjO84fEuT+IzqXfjdqvi3fK+m1qxl/wC0aofBNrhMU/Whc5v1GTT6R+kMi49gLfwz0FzyeJNPjuQvv6Qz7DZngvJveA+CL8gFypE8WPSaLVZT86U/1RkW8xci/qAfE1+Ka430cgIThTHDsiOdpR3F/ZvU2I7yLc2F5EhNfhIgqlPoDZyb+3x7Px53TQ0GktXzp95ESjJTAJJ+uNkXsYn8cDJeOmy+L8DJvIT3o8Q2YQWzejMvlE6ofhyB4QhwvwKwqlDi8wPBU+Ac+i7bl/Ktustg7YaCQ/A+/Rm3LSApypoEE5IbafkWFua9NDU80HhUe4PSOlX0o+k/qAcGZ26wHxR0R72t0Vxm4tc2tbUORa/0cckxHtr4XkE3Wfozee3BDOwc/IV/dE6gdgVpshRTyaIfHrGS79Kmq/nghXt6wwdrS4Fl9SrR9/akagXY1/VbsvJBZ+Md4gK6A0NyCXfIuRO7wHxO/FqbivaFAi9SPl+xbLFGOZcMzeba1/UCr5LcTVVncebZW41n5HqXVvruMt2vsla0Rbe1FaawVW4pWy7q+h03LIJ1I/Tm+M3CU4t4x4IvXj8MKcYQ/g9go/c8jajNMzaUM+IT5bZGLrNnK34+N4rGhQQnyOkHreOm7D0pKfub616j5PlnXKU/tJ2Q7V3R76GD4hQtYq6r6+8C22Khjza/lxfxNpCnyiLBQeRj6R+iF648mdK1S+qrq/RjQ35OG/sri/JI29mcTQiXt/50bOwJPV78ndgC9gVUXiO4juzCIH5nqh9m2R3OkzDdfyqdjj6Q9awtXdayb+AE7HvyqO3wAf0t6haWKViPtXlMw1QVR8UzQwt1XyO6i33MRaL+5qKqv7seJaKsIfcRWlKr+J9l0esxoMKzcdL4uQasSNOAtrKhKfgw8afkZbsQbniTNfhu1EvN+KaamEZwsDUyeWC2fm3xXHb4KPKRfATbiUfKkn532e9pmeyY3krB8jaut14oe4gkrqPiDq+geWzLkCX1Pu0hJxyV45zyY2JT8TL6+Z+N34qurWfS+8Q35zchOX46dUqtLuaHjWJ8VAk/z+ohGgLgyJO31hxfHT8VHlFZh78DklTk2i8odpk+jIsLIh7tED1OvGLsT5VFL3hnjZcK+SOVcK+7GASlKfoVibH28Ia1hnrW1QWOJ/FA1K1H1f8bJB2eZflM1bSDyR+hHi6s7D/Y2MeJ119YW4mEq++3TxitmmJeMWiJjgiYrdGNNF8qLIfixtCG+uky7JPFyoxJNLrPubxVVUhPtwingbsxCJ1I+RHwI3cUtDsWqMFktwGZXycXOFuhd5kyvwKVlKu6K67yhsSJHUl+P6huJwcbS4XJZVKcFkEbTMKBn3HXy7jHiCjUVqrOy8LcXChvCq6sCjIlwdqiD1o5W3sVwtUtoryohnUp8g/IQjKqz1WvyrodiHHg3+hFsqjNtehMxFHVVLhH9f6hYn6n6kSMCU1R8flbnGdUZvV2QTt0Um9YnZAot66Z8Qlv1mKp/zFwvnp4oWX4PfN8k/VuELZXhIpJPaGrpE3efhVSVznS+r1FYkPgtfVs1DXS7C6yea5MuqolXwN9xZMmaCiNOLpHMTPouVFQ3cFhnxXasMFjbpGqKg0aiw6CpYJGpuRdgKexY8f0Rca/8omacp9Y2zjTqo4hoXi+rQyuYHDWGousUdIpgpwizFJa/vq5CMzIiviw+LF5GqYIWwCbeytozVEN0Qj1acpB0GRV9cmTu7jfxk5FJ8HasrEG82OJc5MikuEE1Owzo0mpK/owvyTylXeUJNixa3qOjLLQHLqar34V8tiiRPtramNMTvr/p5F+SHjOyPbYe8MUuUWPeE+E74tKzcVAG3Cp/i3nYPm/f8parn2VoxoFoL+P0i8diKy8RtUYZN8RnV37dbLI7GsHPejvzCbBGdYKJilW5iiZG25RH8hFKpD4gg6ICKa1oq8oHX5RGHRmak1uBsxV2PeRiQBUclb0EtNlLCCxW4xC0e3EmqeaRLhEH8ZRFxLZP9WTQADnawAdspz8Q81FxQghuV3zTNYmWVzPIiUXEqJf40+eSKOhtXdkB+RwWeW6LSl1hrfIZkjYPtVD6R+l6yhoIS/AFvxG+qEH+afIIHRRa1SkyeYpbs1Y8S3Cpy+YSnVWZkJ4i3sMuKpteItNXNVYkPI59If4FIGz04CvIbyVzXvHOfSXcQ3xDqOaS8yLgN9ih4PiRuqvmymn9V4sPIt2zAJUIDRhPxFeXIU9wlKrZPyMnkJCq/u/wGiUHhtZ0g6+EbDfER5JMNGMS3skWWdT00sTP2plT6xOaeId63XaegXXye/Cak88S7Pf8ZOH30xNuSTzZgNb4iUstV3Nd1hfoV9uBnG7BGhKK/ke+tTRZ9tu2InyP6/R/shHQh+ZYNOEu8TlLFA9xbtLQU3vnZBjwpHKu8guPmRpaWB0VS8xQ83A1xKpSokt7bffFF5a913IRX4N5Of8txdub3FKmxtAfwQpGk7EriTZR6TMnvr71KpKAulNPumWE3mTfW5XvvWxkeAl+J99dFvBL55gZkm3Cn6Il7n/yqzICwwAfS1Yv/05L1LRJn/N5OJ+uYfLoJwvqfJVLFP5akhRJMFRnY54x2A5Jrrmk4l4tujdvozKrXQj7ZgCFr3ckThefWGhPMFrF3p0WR5touljUj1EmcLmvyiUS3Fu3g8w1PITevtI9gRVUDmEn/PeJ4HYIFdRPvmnzLJgwIH/9Y4Y83G/9WCG/xTCUdWS3k9xYp6TOwZtySb7MJzxI24Sjh+a3CW8VNUVq3T2pvDazuBfHaybdsApF62kdowubCRtw5Xv6Xg/8BZXrc12H898kAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTMtMDgtMTNUMDA6MTM6NDktMDc6MDCUK2T1AAAAJXRFWHRkYXRlOm1vZGlmeQAyMDEzLTA4LTEzVDAwOjEzOjQ5LTA3OjAw5XbcSQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAUdEVYdFRpdGxlAEdhcyBGbGFtZSBMb2dvWz7WuwAAAABJRU5ErkJggg==\" data-filename=\"ci3-fire-starter.png\" style=\"line-height: 1.42857; width: 63px; float: left;\"></p><p>Konten ini sedang dihasilkan secara <em style=\"color: rgb(41, 82, 24); background-color: rgb(255, 239, 198);\">dinamis</em>. <strong>Teks ini diedit dalam pengaturan admin.</strong></p><p></p>\";s:7:\"russian\";s:4570:\"<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD8AAABjCAYAAAAsE9hTAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABZgAAAWYAGbyQRvAAAACXZwQWcAAAA/AAAAYwCuLjAVAAALCUlEQVR42tXdaZBcVRUH8N90iIQlEAwJEJZSYgUUEYiBoAbZZAcBxVIQlwqogDuuuIC4b4iKWoIiUIpooYBioSggilCgRJEQFCGJoCIYWcOShcz44bwmd3r6LdP9umf8f0nS7/ad+7/n3HPP9iYDeoDTrloMz8Bm+DeeOv1lM3vxo7pCo4dzr8JqbIOBbEPGFXpCPpHy/ZiUbYDxtgE9k3y2AUO4C9thi7Em2zfyyQaswmIchfXHk/R7Sj7BYmyMYxk/6t9z8sn5vxZvw4vGywb0S/Lw1+zPT2LaWBPvN/lHsAT74F2YMNbS7yf51ViW/f0kHMTYqn8/yROWH6bgI9h6zJiPAflJyd/nCvVfZ6yk30/yEzG15bP52J+xUf9+kt8AW7Z8NgUfMEbWv5/kt9T+jM/DcfRf+v0kvxM2zVnDidi5r8z7QT6R5osxIWfYNuL6m9hP6fdL8tMy8kU4Ci+lf+rfU/IJibl4bsnwTfB2YRj//8lnGMChht/xeXiZcH/7Iv1+kN82I1UFGwjLv34f1tU78onkjsBospf76tPZ77Xkp+HVo/zOhngt1unx2npDPpHYATq7v/cReb//P/IZpojzO7GD784QRrKnql87+WSxByu/24twCDbqGfNekM8wBceLqk2n2FG5bzB+yCdSPxwv6XK6Kdi1Zd7xSz7D5sJP70bqTewinKTxTT6RztGYU9O022K9cU8+w0y8ucZ5p+qht1eLI5FJvYETsH2N65uYrvHg794y7OEVr9u5q8m7llBLvP76GonDk9ZmfJvYUD32pDb13ADvxvSayf8dy1s+2wjPY6Qm9JV8y9V2cM3E4QZR7EixDM9XQ9KzDsnPEPn3KvH6aPAQfsuwYqdkMw6lO+l3TD6R+nz1XW0pbsai9IPEwP1TlLu7OmYdkU+I74Q3qd8RGcSP8HjO84fEuT+IzqXfjdqvi3fK+m1qxl/wC0aofBNrhMU/Whc5v1GTT6R+kMi49gLfwz0FzyeJNPjuQvv6Qz7DZngvJveA+CL8gFypE8WPSaLVZT86U/1RkW8xci/qAfE1+Ka430cgIThTHDsiOdpR3F/ZvU2I7yLc2F5EhNfhIgqlPoDZyb+3x7Px53TQ0GktXzp95ESjJTAJJ+uNkXsYn8cDJeOmy+L8DJvIT3o8Q2YQWzejMvlE6ofhyB4QhwvwKwqlDi8wPBU+Ac+i7bl/Ktustg7YaCQ/A+/Rm3LSApypoEE5IbafkWFua9NDU80HhUe4PSOlX0o+k/qAcGZ26wHxR0R72t0Vxm4tc2tbUORa/0cckxHtr4XkE3Wfozee3BDOwc/IV/dE6gdgVpshRTyaIfHrGS79Kmq/nghXt6wwdrS4Fl9SrR9/akagXY1/VbsvJBZ+Md4gK6A0NyCXfIuRO7wHxO/FqbivaFAi9SPl+xbLFGOZcMzeba1/UCr5LcTVVncebZW41n5HqXVvruMt2vsla0Rbe1FaawVW4pWy7q+h03LIJ1I/Tm+M3CU4t4x4IvXj8MKcYQ/g9go/c8jajNMzaUM+IT5bZGLrNnK34+N4rGhQQnyOkHreOm7D0pKfub616j5PlnXKU/tJ2Q7V3R76GD4hQtYq6r6+8C22Khjza/lxfxNpCnyiLBQeRj6R+iF648mdK1S+qrq/RjQ35OG/sri/JI29mcTQiXt/50bOwJPV78ndgC9gVUXiO4juzCIH5nqh9m2R3OkzDdfyqdjj6Q9awtXdayb+AE7HvyqO3wAf0t6haWKViPtXlMw1QVR8UzQwt1XyO6i33MRaL+5qKqv7seJaKsIfcRWlKr+J9l0esxoMKzcdL4uQasSNOAtrKhKfgw8afkZbsQbniTNfhu1EvN+KaamEZwsDUyeWC2fm3xXHb4KPKRfATbiUfKkn532e9pmeyY3krB8jaut14oe4gkrqPiDq+geWzLkCX1Pu0hJxyV45zyY2JT8TL6+Z+N34qurWfS+8Q35zchOX46dUqtLuaHjWJ8VAk/z+ohGgLgyJO31hxfHT8VHlFZh78DklTk2i8odpk+jIsLIh7tED1OvGLsT5VFL3hnjZcK+SOVcK+7GASlKfoVibH28Ia1hnrW1QWOJ/FA1K1H1f8bJB2eZflM1bSDyR+hHi6s7D/Y2MeJ119YW4mEq++3TxitmmJeMWiJjgiYrdGNNF8qLIfixtCG+uky7JPFyoxJNLrPubxVVUhPtwingbsxCJ1I+RHwI3cUtDsWqMFktwGZXycXOFuhd5kyvwKVlKu6K67yhsSJHUl+P6huJwcbS4XJZVKcFkEbTMKBn3HXy7jHiCjUVqrOy8LcXChvCq6sCjIlwdqiD1o5W3sVwtUtoryohnUp8g/IQjKqz1WvyrodiHHg3+hFsqjNtehMxFHVVLhH9f6hYn6n6kSMCU1R8flbnGdUZvV2QTt0Um9YnZAot66Z8Qlv1mKp/zFwvnp4oWX4PfN8k/VuELZXhIpJPaGrpE3efhVSVznS+r1FYkPgtfVs1DXS7C6yea5MuqolXwN9xZMmaCiNOLpHMTPouVFQ3cFhnxXasMFjbpGqKg0aiw6CpYJGpuRdgKexY8f0Rca/8omacp9Y2zjTqo4hoXi+rQyuYHDWGousUdIpgpwizFJa/vq5CMzIiviw+LF5GqYIWwCbeytozVEN0Qj1acpB0GRV9cmTu7jfxk5FJ8HasrEG82OJc5MikuEE1Owzo0mpK/owvyTylXeUJNixa3qOjLLQHLqar34V8tiiRPtramNMTvr/p5F+SHjOyPbYe8MUuUWPeE+E74tKzcVAG3Cp/i3nYPm/f8parn2VoxoFoL+P0i8diKy8RtUYZN8RnV37dbLI7GsHPejvzCbBGdYKJilW5iiZG25RH8hFKpD4gg6ICKa1oq8oHX5RGHRmak1uBsxV2PeRiQBUclb0EtNlLCCxW4xC0e3EmqeaRLhEH8ZRFxLZP9WTQADnawAdspz8Q81FxQghuV3zTNYmWVzPIiUXEqJf40+eSKOhtXdkB+RwWeW6LSl1hrfIZkjYPtVD6R+l6yhoIS/AFvxG+qEH+afIIHRRa1SkyeYpbs1Y8S3Cpy+YSnVWZkJ4i3sMuKpteItNXNVYkPI59If4FIGz04CvIbyVzXvHOfSXcQ3xDqOaS8yLgN9ih4PiRuqvmymn9V4sPIt2zAJUIDRhPxFeXIU9wlKrZPyMnkJCq/u/wGiUHhtZ0g6+EbDfER5JMNGMS3skWWdT00sTP2plT6xOaeId63XaegXXye/Cak88S7Pf8ZOH30xNuSTzZgNb4iUstV3Nd1hfoV9uBnG7BGhKK/ke+tTRZ9tu2InyP6/R/shHQh+ZYNOEu8TlLFA9xbtLQU3vnZBjwpHKu8guPmRpaWB0VS8xQ83A1xKpSokt7bffFF5a913IRX4N5Of8txdub3FKmxtAfwQpGk7EriTZR6TMnvr71KpKAulNPumWE3mTfW5XvvWxkeAl+J99dFvBL55gZkm3Cn6Il7n/yqzICwwAfS1Yv/05L1LRJn/N5OJ+uYfLoJwvqfJVLFP5akhRJMFRnY54x2A5Jrrmk4l4tujdvozKrXQj7ZgCFr3ckThefWGhPMFrF3p0WR5touljUj1EmcLmvyiUS3Fu3g8w1PITevtI9gRVUDmEn/PeJ4HYIFdRPvmnzLJgwIH/9Y4Y83G/9WCG/xTCUdWS3k9xYp6TOwZtySb7MJzxI24Sjh+a3CW8VNUVq3T2pvDazuBfHaybdsApF62kdowubCRtw5Xv6Xg/8BZXrc12H898kAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTMtMDgtMTNUMDA6MTM6NDktMDc6MDCUK2T1AAAAJXRFWHRkYXRlOm1vZGlmeQAyMDEzLTA4LTEzVDAwOjEzOjQ5LTA3OjAw5XbcSQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAUdEVYdFRpdGxlAEdhcyBGbGFtZSBMb2dvWz7WuwAAAABJRU5ErkJggg==\" data-filename=\"ci3-fire-starter.png\" style=\"line-height: 1.42857; width: 63px; float: left;\"></p><p>Это содержание генерируется <em style=\"color: rgb(41, 82, 24); background-color: rgb(255, 239, 198);\">динамически</em>. <strong>Этот текст можно изменить в настройках администратора.</strong></p><p></p>\";s:18:\"simplified-chinese\";s:4376:\"<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD8AAABjCAYAAAAsE9hTAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABZgAAAWYAGbyQRvAAAACXZwQWcAAAA/AAAAYwCuLjAVAAALCUlEQVR42tXdaZBcVRUH8N90iIQlEAwJEJZSYgUUEYiBoAbZZAcBxVIQlwqogDuuuIC4b4iKWoIiUIpooYBioSggilCgRJEQFCGJoCIYWcOShcz44bwmd3r6LdP9umf8f0nS7/ad+7/n3HPP9iYDeoDTrloMz8Bm+DeeOv1lM3vxo7pCo4dzr8JqbIOBbEPGFXpCPpHy/ZiUbYDxtgE9k3y2AUO4C9thi7Em2zfyyQaswmIchfXHk/R7Sj7BYmyMYxk/6t9z8sn5vxZvw4vGywb0S/Lw1+zPT2LaWBPvN/lHsAT74F2YMNbS7yf51ViW/f0kHMTYqn8/yROWH6bgI9h6zJiPAflJyd/nCvVfZ6yk30/yEzG15bP52J+xUf9+kt8AW7Z8NgUfMEbWv5/kt9T+jM/DcfRf+v0kvxM2zVnDidi5r8z7QT6R5osxIWfYNuL6m9hP6fdL8tMy8kU4Ci+lf+rfU/IJibl4bsnwTfB2YRj//8lnGMChht/xeXiZcH/7Iv1+kN82I1UFGwjLv34f1tU78onkjsBospf76tPZ77Xkp+HVo/zOhngt1unx2npDPpHYATq7v/cReb//P/IZpojzO7GD784QRrKnql87+WSxByu/24twCDbqGfNekM8wBceLqk2n2FG5bzB+yCdSPxwv6XK6Kdi1Zd7xSz7D5sJP70bqTewinKTxTT6RztGYU9O022K9cU8+w0y8ucZ5p+qht1eLI5FJvYETsH2N65uYrvHg794y7OEVr9u5q8m7llBLvP76GonDk9ZmfJvYUD32pDb13ADvxvSayf8dy1s+2wjPY6Qm9JV8y9V2cM3E4QZR7EixDM9XQ9KzDsnPEPn3KvH6aPAQfsuwYqdkMw6lO+l3TD6R+nz1XW0pbsai9IPEwP1TlLu7OmYdkU+I74Q3qd8RGcSP8HjO84fEuT+IzqXfjdqvi3fK+m1qxl/wC0aofBNrhMU/Whc5v1GTT6R+kMi49gLfwz0FzyeJNPjuQvv6Qz7DZngvJveA+CL8gFypE8WPSaLVZT86U/1RkW8xci/qAfE1+Ka430cgIThTHDsiOdpR3F/ZvU2I7yLc2F5EhNfhIgqlPoDZyb+3x7Px53TQ0GktXzp95ESjJTAJJ+uNkXsYn8cDJeOmy+L8DJvIT3o8Q2YQWzejMvlE6ofhyB4QhwvwKwqlDi8wPBU+Ac+i7bl/Ktustg7YaCQ/A+/Rm3LSApypoEE5IbafkWFua9NDU80HhUe4PSOlX0o+k/qAcGZ26wHxR0R72t0Vxm4tc2tbUORa/0cckxHtr4XkE3Wfozee3BDOwc/IV/dE6gdgVpshRTyaIfHrGS79Kmq/nghXt6wwdrS4Fl9SrR9/akagXY1/VbsvJBZ+Md4gK6A0NyCXfIuRO7wHxO/FqbivaFAi9SPl+xbLFGOZcMzeba1/UCr5LcTVVncebZW41n5HqXVvruMt2vsla0Rbe1FaawVW4pWy7q+h03LIJ1I/Tm+M3CU4t4x4IvXj8MKcYQ/g9go/c8jajNMzaUM+IT5bZGLrNnK34+N4rGhQQnyOkHreOm7D0pKfub616j5PlnXKU/tJ2Q7V3R76GD4hQtYq6r6+8C22Khjza/lxfxNpCnyiLBQeRj6R+iF648mdK1S+qrq/RjQ35OG/sri/JI29mcTQiXt/50bOwJPV78ndgC9gVUXiO4juzCIH5nqh9m2R3OkzDdfyqdjj6Q9awtXdayb+AE7HvyqO3wAf0t6haWKViPtXlMw1QVR8UzQwt1XyO6i33MRaL+5qKqv7seJaKsIfcRWlKr+J9l0esxoMKzcdL4uQasSNOAtrKhKfgw8afkZbsQbniTNfhu1EvN+KaamEZwsDUyeWC2fm3xXHb4KPKRfATbiUfKkn532e9pmeyY3krB8jaut14oe4gkrqPiDq+geWzLkCX1Pu0hJxyV45zyY2JT8TL6+Z+N34qurWfS+8Q35zchOX46dUqtLuaHjWJ8VAk/z+ohGgLgyJO31hxfHT8VHlFZh78DklTk2i8odpk+jIsLIh7tED1OvGLsT5VFL3hnjZcK+SOVcK+7GASlKfoVibH28Ia1hnrW1QWOJ/FA1K1H1f8bJB2eZflM1bSDyR+hHi6s7D/Y2MeJ119YW4mEq++3TxitmmJeMWiJjgiYrdGNNF8qLIfixtCG+uky7JPFyoxJNLrPubxVVUhPtwingbsxCJ1I+RHwI3cUtDsWqMFktwGZXycXOFuhd5kyvwKVlKu6K67yhsSJHUl+P6huJwcbS4XJZVKcFkEbTMKBn3HXy7jHiCjUVqrOy8LcXChvCq6sCjIlwdqiD1o5W3sVwtUtoryohnUp8g/IQjKqz1WvyrodiHHg3+hFsqjNtehMxFHVVLhH9f6hYn6n6kSMCU1R8flbnGdUZvV2QTt0Um9YnZAot66Z8Qlv1mKp/zFwvnp4oWX4PfN8k/VuELZXhIpJPaGrpE3efhVSVznS+r1FYkPgtfVs1DXS7C6yea5MuqolXwN9xZMmaCiNOLpHMTPouVFQ3cFhnxXasMFjbpGqKg0aiw6CpYJGpuRdgKexY8f0Rca/8omacp9Y2zjTqo4hoXi+rQyuYHDWGousUdIpgpwizFJa/vq5CMzIiviw+LF5GqYIWwCbeytozVEN0Qj1acpB0GRV9cmTu7jfxk5FJ8HasrEG82OJc5MikuEE1Owzo0mpK/owvyTylXeUJNixa3qOjLLQHLqar34V8tiiRPtramNMTvr/p5F+SHjOyPbYe8MUuUWPeE+E74tKzcVAG3Cp/i3nYPm/f8parn2VoxoFoL+P0i8diKy8RtUYZN8RnV37dbLI7GsHPejvzCbBGdYKJilW5iiZG25RH8hFKpD4gg6ICKa1oq8oHX5RGHRmak1uBsxV2PeRiQBUclb0EtNlLCCxW4xC0e3EmqeaRLhEH8ZRFxLZP9WTQADnawAdspz8Q81FxQghuV3zTNYmWVzPIiUXEqJf40+eSKOhtXdkB+RwWeW6LSl1hrfIZkjYPtVD6R+l6yhoIS/AFvxG+qEH+afIIHRRa1SkyeYpbs1Y8S3Cpy+YSnVWZkJ4i3sMuKpteItNXNVYkPI59If4FIGz04CvIbyVzXvHOfSXcQ3xDqOaS8yLgN9ih4PiRuqvmymn9V4sPIt2zAJUIDRhPxFeXIU9wlKrZPyMnkJCq/u/wGiUHhtZ0g6+EbDfER5JMNGMS3skWWdT00sTP2plT6xOaeId63XaegXXye/Cak88S7Pf8ZOH30xNuSTzZgNb4iUstV3Nd1hfoV9uBnG7BGhKK/ke+tTRZ9tu2InyP6/R/shHQh+ZYNOEu8TlLFA9xbtLQU3vnZBjwpHKu8guPmRpaWB0VS8xQ83A1xKpSokt7bffFF5a913IRX4N5Of8txdub3FKmxtAfwQpGk7EriTZR6TMnvr71KpKAulNPumWE3mTfW5XvvWxkeAl+J99dFvBL55gZkm3Cn6Il7n/yqzICwwAfS1Yv/05L1LRJn/N5OJ+uYfLoJwvqfJVLFP5akhRJMFRnY54x2A5Jrrmk4l4tujdvozKrXQj7ZgCFr3ckThefWGhPMFrF3p0WR5touljUj1EmcLmvyiUS3Fu3g8w1PITevtI9gRVUDmEn/PeJ4HYIFdRPvmnzLJgwIH/9Y4Y83G/9WCG/xTCUdWS3k9xYp6TOwZtySb7MJzxI24Sjh+a3CW8VNUVq3T2pvDazuBfHaybdsApF62kdowubCRtw5Xv6Xg/8BZXrc12H898kAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTMtMDgtMTNUMDA6MTM6NDktMDc6MDCUK2T1AAAAJXRFWHRkYXRlOm1vZGlmeQAyMDEzLTA4LTEzVDAwOjEzOjQ5LTA3OjAw5XbcSQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAUdEVYdFRpdGxlAEdhcyBGbGFtZSBMb2dvWz7WuwAAAABJRU5ErkJggg==\" data-filename=\"ci3-fire-starter.png\" style=\"line-height: 1.42857; width: 63px; float: left;\"></p><p>正在动态生成此内容. <strong>该文本可编辑在管理设置.</strong></p><p></p>\";s:7:\"spanish\";s:4494:\"<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD8AAABjCAYAAAAsE9hTAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABZgAAAWYAGbyQRvAAAACXZwQWcAAAA/AAAAYwCuLjAVAAALCUlEQVR42tXdaZBcVRUH8N90iIQlEAwJEJZSYgUUEYiBoAbZZAcBxVIQlwqogDuuuIC4b4iKWoIiUIpooYBioSggilCgRJEQFCGJoCIYWcOShcz44bwmd3r6LdP9umf8f0nS7/ad+7/n3HPP9iYDeoDTrloMz8Bm+DeeOv1lM3vxo7pCo4dzr8JqbIOBbEPGFXpCPpHy/ZiUbYDxtgE9k3y2AUO4C9thi7Em2zfyyQaswmIchfXHk/R7Sj7BYmyMYxk/6t9z8sn5vxZvw4vGywb0S/Lw1+zPT2LaWBPvN/lHsAT74F2YMNbS7yf51ViW/f0kHMTYqn8/yROWH6bgI9h6zJiPAflJyd/nCvVfZ6yk30/yEzG15bP52J+xUf9+kt8AW7Z8NgUfMEbWv5/kt9T+jM/DcfRf+v0kvxM2zVnDidi5r8z7QT6R5osxIWfYNuL6m9hP6fdL8tMy8kU4Ci+lf+rfU/IJibl4bsnwTfB2YRj//8lnGMChht/xeXiZcH/7Iv1+kN82I1UFGwjLv34f1tU78onkjsBospf76tPZ77Xkp+HVo/zOhngt1unx2npDPpHYATq7v/cReb//P/IZpojzO7GD784QRrKnql87+WSxByu/24twCDbqGfNekM8wBceLqk2n2FG5bzB+yCdSPxwv6XK6Kdi1Zd7xSz7D5sJP70bqTewinKTxTT6RztGYU9O022K9cU8+w0y8ucZ5p+qht1eLI5FJvYETsH2N65uYrvHg794y7OEVr9u5q8m7llBLvP76GonDk9ZmfJvYUD32pDb13ADvxvSayf8dy1s+2wjPY6Qm9JV8y9V2cM3E4QZR7EixDM9XQ9KzDsnPEPn3KvH6aPAQfsuwYqdkMw6lO+l3TD6R+nz1XW0pbsai9IPEwP1TlLu7OmYdkU+I74Q3qd8RGcSP8HjO84fEuT+IzqXfjdqvi3fK+m1qxl/wC0aofBNrhMU/Whc5v1GTT6R+kMi49gLfwz0FzyeJNPjuQvv6Qz7DZngvJveA+CL8gFypE8WPSaLVZT86U/1RkW8xci/qAfE1+Ka430cgIThTHDsiOdpR3F/ZvU2I7yLc2F5EhNfhIgqlPoDZyb+3x7Px53TQ0GktXzp95ESjJTAJJ+uNkXsYn8cDJeOmy+L8DJvIT3o8Q2YQWzejMvlE6ofhyB4QhwvwKwqlDi8wPBU+Ac+i7bl/Ktustg7YaCQ/A+/Rm3LSApypoEE5IbafkWFua9NDU80HhUe4PSOlX0o+k/qAcGZ26wHxR0R72t0Vxm4tc2tbUORa/0cckxHtr4XkE3Wfozee3BDOwc/IV/dE6gdgVpshRTyaIfHrGS79Kmq/nghXt6wwdrS4Fl9SrR9/akagXY1/VbsvJBZ+Md4gK6A0NyCXfIuRO7wHxO/FqbivaFAi9SPl+xbLFGOZcMzeba1/UCr5LcTVVncebZW41n5HqXVvruMt2vsla0Rbe1FaawVW4pWy7q+h03LIJ1I/Tm+M3CU4t4x4IvXj8MKcYQ/g9go/c8jajNMzaUM+IT5bZGLrNnK34+N4rGhQQnyOkHreOm7D0pKfub616j5PlnXKU/tJ2Q7V3R76GD4hQtYq6r6+8C22Khjza/lxfxNpCnyiLBQeRj6R+iF648mdK1S+qrq/RjQ35OG/sri/JI29mcTQiXt/50bOwJPV78ndgC9gVUXiO4juzCIH5nqh9m2R3OkzDdfyqdjj6Q9awtXdayb+AE7HvyqO3wAf0t6haWKViPtXlMw1QVR8UzQwt1XyO6i33MRaL+5qKqv7seJaKsIfcRWlKr+J9l0esxoMKzcdL4uQasSNOAtrKhKfgw8afkZbsQbniTNfhu1EvN+KaamEZwsDUyeWC2fm3xXHb4KPKRfATbiUfKkn532e9pmeyY3krB8jaut14oe4gkrqPiDq+geWzLkCX1Pu0hJxyV45zyY2JT8TL6+Z+N34qurWfS+8Q35zchOX46dUqtLuaHjWJ8VAk/z+ohGgLgyJO31hxfHT8VHlFZh78DklTk2i8odpk+jIsLIh7tED1OvGLsT5VFL3hnjZcK+SOVcK+7GASlKfoVibH28Ia1hnrW1QWOJ/FA1K1H1f8bJB2eZflM1bSDyR+hHi6s7D/Y2MeJ119YW4mEq++3TxitmmJeMWiJjgiYrdGNNF8qLIfixtCG+uky7JPFyoxJNLrPubxVVUhPtwingbsxCJ1I+RHwI3cUtDsWqMFktwGZXycXOFuhd5kyvwKVlKu6K67yhsSJHUl+P6huJwcbS4XJZVKcFkEbTMKBn3HXy7jHiCjUVqrOy8LcXChvCq6sCjIlwdqiD1o5W3sVwtUtoryohnUp8g/IQjKqz1WvyrodiHHg3+hFsqjNtehMxFHVVLhH9f6hYn6n6kSMCU1R8flbnGdUZvV2QTt0Um9YnZAot66Z8Qlv1mKp/zFwvnp4oWX4PfN8k/VuELZXhIpJPaGrpE3efhVSVznS+r1FYkPgtfVs1DXS7C6yea5MuqolXwN9xZMmaCiNOLpHMTPouVFQ3cFhnxXasMFjbpGqKg0aiw6CpYJGpuRdgKexY8f0Rca/8omacp9Y2zjTqo4hoXi+rQyuYHDWGousUdIpgpwizFJa/vq5CMzIiviw+LF5GqYIWwCbeytozVEN0Qj1acpB0GRV9cmTu7jfxk5FJ8HasrEG82OJc5MikuEE1Owzo0mpK/owvyTylXeUJNixa3qOjLLQHLqar34V8tiiRPtramNMTvr/p5F+SHjOyPbYe8MUuUWPeE+E74tKzcVAG3Cp/i3nYPm/f8parn2VoxoFoL+P0i8diKy8RtUYZN8RnV37dbLI7GsHPejvzCbBGdYKJilW5iiZG25RH8hFKpD4gg6ICKa1oq8oHX5RGHRmak1uBsxV2PeRiQBUclb0EtNlLCCxW4xC0e3EmqeaRLhEH8ZRFxLZP9WTQADnawAdspz8Q81FxQghuV3zTNYmWVzPIiUXEqJf40+eSKOhtXdkB+RwWeW6LSl1hrfIZkjYPtVD6R+l6yhoIS/AFvxG+qEH+afIIHRRa1SkyeYpbs1Y8S3Cpy+YSnVWZkJ4i3sMuKpteItNXNVYkPI59If4FIGz04CvIbyVzXvHOfSXcQ3xDqOaS8yLgN9ih4PiRuqvmymn9V4sPIt2zAJUIDRhPxFeXIU9wlKrZPyMnkJCq/u/wGiUHhtZ0g6+EbDfER5JMNGMS3skWWdT00sTP2plT6xOaeId63XaegXXye/Cak88S7Pf8ZOH30xNuSTzZgNb4iUstV3Nd1hfoV9uBnG7BGhKK/ke+tTRZ9tu2InyP6/R/shHQh+ZYNOEu8TlLFA9xbtLQU3vnZBjwpHKu8guPmRpaWB0VS8xQ83A1xKpSokt7bffFF5a913IRX4N5Of8txdub3FKmxtAfwQpGk7EriTZR6TMnvr71KpKAulNPumWE3mTfW5XvvWxkeAl+J99dFvBL55gZkm3Cn6Il7n/yqzICwwAfS1Yv/05L1LRJn/N5OJ+uYfLoJwvqfJVLFP5akhRJMFRnY54x2A5Jrrmk4l4tujdvozKrXQj7ZgCFr3ckThefWGhPMFrF3p0WR5touljUj1EmcLmvyiUS3Fu3g8w1PITevtI9gRVUDmEn/PeJ4HYIFdRPvmnzLJgwIH/9Y4Y83G/9WCG/xTCUdWS3k9xYp6TOwZtySb7MJzxI24Sjh+a3CW8VNUVq3T2pvDazuBfHaybdsApF62kdowubCRtw5Xv6Xg/8BZXrc12H898kAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTMtMDgtMTNUMDA6MTM6NDktMDc6MDCUK2T1AAAAJXRFWHRkYXRlOm1vZGlmeQAyMDEzLTA4LTEzVDAwOjEzOjQ5LTA3OjAw5XbcSQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAUdEVYdFRpdGxlAEdhcyBGbGFtZSBMb2dvWz7WuwAAAABJRU5ErkJggg==\" data-filename=\"ci3-fire-starter.png\" style=\"line-height: 1.42857; width: 63px; float: left;\"></p><p>Este contenido se genera <em style=\"color: rgb(41, 82, 24); background-color: rgb(255, 239, 198);\">dinámicamente</em>. <strong>Este texto es editable en la configuración de administrador.</strong></p><p></p>\";s:7:\"turkish\";s:4489:\"<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD8AAABjCAYAAAAsE9hTAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABZgAAAWYAGbyQRvAAAACXZwQWcAAAA/AAAAYwCuLjAVAAALCUlEQVR42tXdaZBcVRUH8N90iIQlEAwJEJZSYgUUEYiBoAbZZAcBxVIQlwqogDuuuIC4b4iKWoIiUIpooYBioSggilCgRJEQFCGJoCIYWcOShcz44bwmd3r6LdP9umf8f0nS7/ad+7/n3HPP9iYDeoDTrloMz8Bm+DeeOv1lM3vxo7pCo4dzr8JqbIOBbEPGFXpCPpHy/ZiUbYDxtgE9k3y2AUO4C9thi7Em2zfyyQaswmIchfXHk/R7Sj7BYmyMYxk/6t9z8sn5vxZvw4vGywb0S/Lw1+zPT2LaWBPvN/lHsAT74F2YMNbS7yf51ViW/f0kHMTYqn8/yROWH6bgI9h6zJiPAflJyd/nCvVfZ6yk30/yEzG15bP52J+xUf9+kt8AW7Z8NgUfMEbWv5/kt9T+jM/DcfRf+v0kvxM2zVnDidi5r8z7QT6R5osxIWfYNuL6m9hP6fdL8tMy8kU4Ci+lf+rfU/IJibl4bsnwTfB2YRj//8lnGMChht/xeXiZcH/7Iv1+kN82I1UFGwjLv34f1tU78onkjsBospf76tPZ77Xkp+HVo/zOhngt1unx2npDPpHYATq7v/cReb//P/IZpojzO7GD784QRrKnql87+WSxByu/24twCDbqGfNekM8wBceLqk2n2FG5bzB+yCdSPxwv6XK6Kdi1Zd7xSz7D5sJP70bqTewinKTxTT6RztGYU9O022K9cU8+w0y8ucZ5p+qht1eLI5FJvYETsH2N65uYrvHg794y7OEVr9u5q8m7llBLvP76GonDk9ZmfJvYUD32pDb13ADvxvSayf8dy1s+2wjPY6Qm9JV8y9V2cM3E4QZR7EixDM9XQ9KzDsnPEPn3KvH6aPAQfsuwYqdkMw6lO+l3TD6R+nz1XW0pbsai9IPEwP1TlLu7OmYdkU+I74Q3qd8RGcSP8HjO84fEuT+IzqXfjdqvi3fK+m1qxl/wC0aofBNrhMU/Whc5v1GTT6R+kMi49gLfwz0FzyeJNPjuQvv6Qz7DZngvJveA+CL8gFypE8WPSaLVZT86U/1RkW8xci/qAfE1+Ka430cgIThTHDsiOdpR3F/ZvU2I7yLc2F5EhNfhIgqlPoDZyb+3x7Px53TQ0GktXzp95ESjJTAJJ+uNkXsYn8cDJeOmy+L8DJvIT3o8Q2YQWzejMvlE6ofhyB4QhwvwKwqlDi8wPBU+Ac+i7bl/Ktustg7YaCQ/A+/Rm3LSApypoEE5IbafkWFua9NDU80HhUe4PSOlX0o+k/qAcGZ26wHxR0R72t0Vxm4tc2tbUORa/0cckxHtr4XkE3Wfozee3BDOwc/IV/dE6gdgVpshRTyaIfHrGS79Kmq/nghXt6wwdrS4Fl9SrR9/akagXY1/VbsvJBZ+Md4gK6A0NyCXfIuRO7wHxO/FqbivaFAi9SPl+xbLFGOZcMzeba1/UCr5LcTVVncebZW41n5HqXVvruMt2vsla0Rbe1FaawVW4pWy7q+h03LIJ1I/Tm+M3CU4t4x4IvXj8MKcYQ/g9go/c8jajNMzaUM+IT5bZGLrNnK34+N4rGhQQnyOkHreOm7D0pKfub616j5PlnXKU/tJ2Q7V3R76GD4hQtYq6r6+8C22Khjza/lxfxNpCnyiLBQeRj6R+iF648mdK1S+qrq/RjQ35OG/sri/JI29mcTQiXt/50bOwJPV78ndgC9gVUXiO4juzCIH5nqh9m2R3OkzDdfyqdjj6Q9awtXdayb+AE7HvyqO3wAf0t6haWKViPtXlMw1QVR8UzQwt1XyO6i33MRaL+5qKqv7seJaKsIfcRWlKr+J9l0esxoMKzcdL4uQasSNOAtrKhKfgw8afkZbsQbniTNfhu1EvN+KaamEZwsDUyeWC2fm3xXHb4KPKRfATbiUfKkn532e9pmeyY3krB8jaut14oe4gkrqPiDq+geWzLkCX1Pu0hJxyV45zyY2JT8TL6+Z+N34qurWfS+8Q35zchOX46dUqtLuaHjWJ8VAk/z+ohGgLgyJO31hxfHT8VHlFZh78DklTk2i8odpk+jIsLIh7tED1OvGLsT5VFL3hnjZcK+SOVcK+7GASlKfoVibH28Ia1hnrW1QWOJ/FA1K1H1f8bJB2eZflM1bSDyR+hHi6s7D/Y2MeJ119YW4mEq++3TxitmmJeMWiJjgiYrdGNNF8qLIfixtCG+uky7JPFyoxJNLrPubxVVUhPtwingbsxCJ1I+RHwI3cUtDsWqMFktwGZXycXOFuhd5kyvwKVlKu6K67yhsSJHUl+P6huJwcbS4XJZVKcFkEbTMKBn3HXy7jHiCjUVqrOy8LcXChvCq6sCjIlwdqiD1o5W3sVwtUtoryohnUp8g/IQjKqz1WvyrodiHHg3+hFsqjNtehMxFHVVLhH9f6hYn6n6kSMCU1R8flbnGdUZvV2QTt0Um9YnZAot66Z8Qlv1mKp/zFwvnp4oWX4PfN8k/VuELZXhIpJPaGrpE3efhVSVznS+r1FYkPgtfVs1DXS7C6yea5MuqolXwN9xZMmaCiNOLpHMTPouVFQ3cFhnxXasMFjbpGqKg0aiw6CpYJGpuRdgKexY8f0Rca/8omacp9Y2zjTqo4hoXi+rQyuYHDWGousUdIpgpwizFJa/vq5CMzIiviw+LF5GqYIWwCbeytozVEN0Qj1acpB0GRV9cmTu7jfxk5FJ8HasrEG82OJc5MikuEE1Owzo0mpK/owvyTylXeUJNixa3qOjLLQHLqar34V8tiiRPtramNMTvr/p5F+SHjOyPbYe8MUuUWPeE+E74tKzcVAG3Cp/i3nYPm/f8parn2VoxoFoL+P0i8diKy8RtUYZN8RnV37dbLI7GsHPejvzCbBGdYKJilW5iiZG25RH8hFKpD4gg6ICKa1oq8oHX5RGHRmak1uBsxV2PeRiQBUclb0EtNlLCCxW4xC0e3EmqeaRLhEH8ZRFxLZP9WTQADnawAdspz8Q81FxQghuV3zTNYmWVzPIiUXEqJf40+eSKOhtXdkB+RwWeW6LSl1hrfIZkjYPtVD6R+l6yhoIS/AFvxG+qEH+afIIHRRa1SkyeYpbs1Y8S3Cpy+YSnVWZkJ4i3sMuKpteItNXNVYkPI59If4FIGz04CvIbyVzXvHOfSXcQ3xDqOaS8yLgN9ih4PiRuqvmymn9V4sPIt2zAJUIDRhPxFeXIU9wlKrZPyMnkJCq/u/wGiUHhtZ0g6+EbDfER5JMNGMS3skWWdT00sTP2plT6xOaeId63XaegXXye/Cak88S7Pf8ZOH30xNuSTzZgNb4iUstV3Nd1hfoV9uBnG7BGhKK/ke+tTRZ9tu2InyP6/R/shHQh+ZYNOEu8TlLFA9xbtLQU3vnZBjwpHKu8guPmRpaWB0VS8xQ83A1xKpSokt7bffFF5a913IRX4N5Of8txdub3FKmxtAfwQpGk7EriTZR6TMnvr71KpKAulNPumWE3mTfW5XvvWxkeAl+J99dFvBL55gZkm3Cn6Il7n/yqzICwwAfS1Yv/05L1LRJn/N5OJ+uYfLoJwvqfJVLFP5akhRJMFRnY54x2A5Jrrmk4l4tujdvozKrXQj7ZgCFr3ckThefWGhPMFrF3p0WR5touljUj1EmcLmvyiUS3Fu3g8w1PITevtI9gRVUDmEn/PeJ4HYIFdRPvmnzLJgwIH/9Y4Y83G/9WCG/xTCUdWS3k9xYp6TOwZtySb7MJzxI24Sjh+a3CW8VNUVq3T2pvDazuBfHaybdsApF62kdowubCRtw5Xv6Xg/8BZXrc12H898kAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTMtMDgtMTNUMDA6MTM6NDktMDc6MDCUK2T1AAAAJXRFWHRkYXRlOm1vZGlmeQAyMDEzLTA4LTEzVDAwOjEzOjQ5LTA3OjAw5XbcSQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAUdEVYdFRpdGxlAEdhcyBGbGFtZSBMb2dvWz7WuwAAAABJRU5ErkJggg==\" data-filename=\"ci3-fire-starter.png\" style=\"line-height: 1.42857; width: 63px; float: left;\"></p><p>Bu içerik <em style=\"color: rgb(41, 82, 24); background-color: rgb(255, 239, 198);\">dinamik</em> olarak oluşturulan ediliyor. <strong>Bu metin yönetici ayarlarında düzenlenebilir.</strong></p><p></p>\";}',	'2017-02-07 13:15:55',	1);

DROP TABLE IF EXISTS `site_error_log`;
CREATE TABLE `site_error_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `erroemsg` text NOT NULL,
  `userid` int(11) NOT NULL,
  `client_ip` varchar(35) NOT NULL,
  `errortime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `site_setting`;
CREATE TABLE `site_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mode` varchar(20) NOT NULL,
  `status` varchar(250) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `site_setting` (`id`, `mode`, `status`, `created_date`, `modified_date`) VALUES
(1,	'chk_all',	'1',	'2017-03-15 07:33:14',	'2017-06-17 13:12:04'),
(2,	'chk_new',	'1',	'2017-03-15 07:33:14',	'2017-06-17 13:12:04'),
(3,	'site_logo',	'logo.png',	'2017-03-25 09:55:33',	'2017-03-25 09:55:33'),
(4,	'default_language',	'english',	'2017-05-16 07:16:16',	'2017-06-02 12:27:24'),
(15,	'site_email',	'admin@cupidlove.com',	'2017-07-14 17:50:56',	'2017-07-14 17:50:56'),
(16,	'site_phone',	'022-2154745',	'2017-07-14 17:50:56',	'2017-07-14 17:50:56'),
(17,	'facebook',	'http://www.facebook.com',	'2017-07-14 17:50:56',	'2017-07-14 17:50:56'),
(18,	'twitter',	'http://www.twitter.com',	'2017-07-14 17:50:56',	'2017-07-14 17:50:56'),
(19,	'instagram',	'https://www.instagram.com/',	'2017-07-14 17:50:56',	'2017-07-14 17:50:56'),
(20,	'copyright',	'https://www.youtube.com/embed/8xg3vE8Ie_E',	'2017-07-14 17:50:56',	'2017-07-14 17:50:56'),
(21,	'contact_text',	'<p>hello</p>',	'2017-07-14 17:50:56',	'2017-07-14 17:50:56'),
(23,	'loaderimage',	'loading.gif',	'2017-07-14 18:31:18',	'2017-07-14 18:31:18'),
(26,	'video_url',	'https://www.youtube.com/embed/8xg3vE8Ie_E',	'2017-07-28 14:57:14',	'2017-07-28 14:57:14'),
(27,	'contact_address',	'T317 Timber Oak Drive\r\nSundown, TX 79372\r\nSundown ',	'2017-07-28 15:20:22',	'2017-07-28 15:20:22'),
(28,	'contact_email',	'info@belgradetraining.com\r\nsupport@cupidlove.com\r\n',	'2017-07-28 15:20:44',	'2017-07-28 15:20:44'),
(25,	'google_contact_map',	'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.8351288872545!2d144.9556518!3d-37.8173306!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4c2b349649%3A0xb6899234e561db11!2sEnvato!5e0!3m2!1sen!2sin!4v1443621171568',	'2017-07-28 14:53:14',	'2017-07-28 14:53:14'),
(30,	'contact_landline',	'+91-123-456-7890',	'2017-07-28 15:41:30',	'2017-07-28 15:41:30'),
(31,	'contact_mobile',	'+91-234-567-8900',	'2017-07-28 15:41:44',	'2017-07-28 15:41:44'),
(32,	'coming_soon',	'0',	'2017-11-17 21:18:34',	'2017-11-17 21:18:34'),
(34,	'site_style',	'skin-default',	'2017-11-17 21:19:34',	'2017-11-17 21:19:34'),
(35,	'default_homepage',	'1',	'2017-11-29 03:33:32',	'2017-11-29 03:33:32'),
(47,	'sample_data',	'1',	'2018-05-17 23:21:00',	'2018-05-17 23:21:00');

DROP TABLE IF EXISTS `slider`;
CREATE TABLE `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `image` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `slider` (`id`, `title`, `image`) VALUES
(1,	'Are You <span class=\"sliderspan\">Waiting</span> For <span class=\"sliderspan\"> Dating ?</span>',	'bg-1.jpg'),
(2,	'Meet big <span class=\"sliderspan\"> and </span> beautiful love <span class=\"sliderspan\"> here!</span>',	'bg-2.jpg'),
(3,	'The Right <span class=\"sliderspan\">Place</span> to Meet Your <span class=\"sliderspan\"> Soul Mate!</span>',	'bg-3.jpg');

DROP TABLE IF EXISTS `stories`;
CREATE TABLE `stories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `excerpt` text CHARACTER SET utf8 COLLATE utf8_bin,
  `description` text CHARACTER SET utf8 COLLATE utf8_bin,
  `image` text,
  `story_author` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `stories` (`id`, `title`, `excerpt`, `description`, `image`, `story_author`, `created_date`) VALUES
(2,	'Adam & Eve',	'Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in',	'<p class=\"mt-20\">ARE YOU FAMOUS OR FOCUSED? Are you famous or are you focused? Let&rsquo;s start with &ldquo;famous&rdquo;. When people look at you, what do they say that you are famous for? Are you famous for procrastination&hellip;negative words&hellip;poor time management&hellip;What are you famous for? Let me share with you what I am famous for. I am famous for consistent, structured, focused daily strategies and techniques to live a victorious life. I know&hellip;that is a pretty strong statement! You see, I would rather be &ldquo;focused&rdquo; than famous. I am focused on success. I am focused on assisting you in achieving you dream and discover the greatness that is inside all of you! Forget about being famous, let&rsquo;s be focused. Be focused on your dreams&hellip;be focused on providing for your family&hellip;be focused on building a huge team&hellip;be focused on Finding Your Why! Let&rsquo;s commit today to become focused and start changing people&rsquo;s lives<br /><br /> Millions of people world wide watch shows like &ldquo;Who wants to be a Millionaire&rdquo; or &ldquo;Lifestyles of the Rich and Famous&rdquo;. The interesting thing is that everyone sitting on the couch with a bag of popcorn watching these shows are broke. Why? Because they are famous for sitting back and watching shows on other people being famous. How about we flick the switch in life from famous to focused. Stop dreaming of other people being famous and start being focused on your own destiny! You must laser-focus in on your success and demolish procrastination and create action. Demolish resolutions and create results! Let&rsquo;s demolish fear and create faith! How is this all possible by simply being focused. How do people become gold medalist, super bowl champions, etc? They take charge of their lives, hire a coach and become focused<br /><br /> What are you? Who are you? Where are you going? How are you going to get there? More importantly, why are you not focused? Those that know their &ldquo;Why&rdquo; are very focused. I am focused on my Why of changing lives of Champions worldwide through my coaching, mentoring, seminars and success library and creating massive success in those Champions lives! Do not allow famous to get in your way of being focused. Make a commitment today to forget fame and become laser-focused! Let&rsquo;s get focused and know your Why! Let&rsquo;s go out and impact the world one heart at a time. Focus creates success and Fame creates problems. It&rsquo;s your decision&hellip;become focused and live your dream!!! Find Your Why &amp; Fly</p>',	'02.jpg',	'JOHNET LEO',	'2018-01-04 00:22:50'),
(4,	'Bella & Edward',	'Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in',	'<p class=\"mt-20\">ARE YOU FAMOUS OR FOCUSED? Are you famous or are you focused? Let&rsquo;s start with &ldquo;famous&rdquo;. When people look at you, what do they say that you are famous for? Are you famous for procrastination&hellip;negative words&hellip;poor time management&hellip;What are you famous for? Let me share with you what I am famous for. I am famous for consistent, structured, focused daily strategies and techniques to live a victorious life. I know&hellip;that is a pretty strong statement! You see, I would rather be &ldquo;focused&rdquo; than famous. I am focused on success. I am focused on assisting you in achieving you dream and discover the greatness that is inside all of you! Forget about being famous, let&rsquo;s be focused. Be focused on your dreams&hellip;be focused on providing for your family&hellip;be focused on building a huge team&hellip;be focused on Finding Your Why! Let&rsquo;s commit today to become focused and start changing people&rsquo;s lives<br /><br /> Millions of people world wide watch shows like &ldquo;Who wants to be a Millionaire&rdquo; or &ldquo;Lifestyles of the Rich and Famous&rdquo;. The interesting thing is that everyone sitting on the couch with a bag of popcorn watching these shows are broke. Why? Because they are famous for sitting back and watching shows on other people being famous. How about we flick the switch in life from famous to focused. Stop dreaming of other people being famous and start being focused on your own destiny! You must laser-focus in on your success and demolish procrastination and create action. Demolish resolutions and create results! Let&rsquo;s demolish fear and create faith! How is this all possible by simply being focused. How do people become gold medalist, super bowl champions, etc? They take charge of their lives, hire a coach and become focused<br /><br /> What are you? Who are you? Where are you going? How are you going to get there? More importantly, why are you not focused? Those that know their &ldquo;Why&rdquo; are very focused. I am focused on my Why of changing lives of Champions worldwide through my coaching, mentoring, seminars and success library and creating massive success in those Champions lives! Do not allow famous to get in your way of being focused. Make a commitment today to forget fame and become laser-focused! Let&rsquo;s get focused and know your Why! Let&rsquo;s go out and impact the world one heart at a time. Focus creates success and Fame creates problems. It&rsquo;s your decision&hellip;become focused and live your dream!!! Find Your Why &amp; Fly</p>',	'03.jpg',	'JOHNET LEO',	'2018-01-04 00:22:50'),
(5,	'DEMI & HEAVEN',	'Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in',	'<p class=\"mt-20\">ARE YOU FAMOUS OR FOCUSED? Are you famous or are you focused? Let&rsquo;s start with &ldquo;famous&rdquo;. When people look at you, what do they say that you are famous for? Are you famous for procrastination&hellip;negative words&hellip;poor time management&hellip;What are you famous for? Let me share with you what I am famous for. I am famous for consistent, structured, focused daily strategies and techniques to live a victorious life. I know&hellip;that is a pretty strong statement! You see, I would rather be &ldquo;focused&rdquo; than famous. I am focused on success. I am focused on assisting you in achieving you dream and discover the greatness that is inside all of you! Forget about being famous, let&rsquo;s be focused. Be focused on your dreams&hellip;be focused on providing for your family&hellip;be focused on building a huge team&hellip;be focused on Finding Your Why! Let&rsquo;s commit today to become focused and start changing people&rsquo;s lives<br /><br /> Millions of people world wide watch shows like &ldquo;Who wants to be a Millionaire&rdquo; or &ldquo;Lifestyles of the Rich and Famous&rdquo;. The interesting thing is that everyone sitting on the couch with a bag of popcorn watching these shows are broke. Why? Because they are famous for sitting back and watching shows on other people being famous. How about we flick the switch in life from famous to focused. Stop dreaming of other people being famous and start being focused on your own destiny! You must laser-focus in on your success and demolish procrastination and create action. Demolish resolutions and create results! Let&rsquo;s demolish fear and create faith! How is this all possible by simply being focused. How do people become gold medalist, super bowl champions, etc? They take charge of their lives, hire a coach and become focused<br /><br /> What are you? Who are you? Where are you going? How are you going to get there? More importantly, why are you not focused? Those that know their &ldquo;Why&rdquo; are very focused. I am focused on my Why of changing lives of Champions worldwide through my coaching, mentoring, seminars and success library and creating massive success in those Champions lives! Do not allow famous to get in your way of being focused. Make a commitment today to forget fame and become laser-focused! Let&rsquo;s get focused and know your Why! Let&rsquo;s go out and impact the world one heart at a time. Focus creates success and Fame creates problems. It&rsquo;s your decision&hellip;become focused and live your dream!!! Find Your Why &amp; Fly</p>',	'05.jpg',	'JOHNET LEO',	'2018-01-04 00:22:50'),
(6,	'David & Bathsheba',	'Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in',	'<p class=\"mt-20\">ARE YOU FAMOUS OR FOCUSED? Are you famous or are you focused? Let&rsquo;s start with &ldquo;famous&rdquo;. When people look at you, what do they say that you are famous for? Are you famous for procrastination&hellip;negative words&hellip;poor time management&hellip;What are you famous for? Let me share with you what I am famous for. I am famous for consistent, structured, focused daily strategies and techniques to live a victorious life. I know&hellip;that is a pretty strong statement! You see, I would rather be &ldquo;focused&rdquo; than famous. I am focused on success. I am focused on assisting you in achieving you dream and discover the greatness that is inside all of you! Forget about being famous, let&rsquo;s be focused. Be focused on your dreams&hellip;be focused on providing for your family&hellip;be focused on building a huge team&hellip;be focused on Finding Your Why! Let&rsquo;s commit today to become focused and start changing people&rsquo;s lives<br /><br /> Millions of people world wide watch shows like &ldquo;Who wants to be a Millionaire&rdquo; or &ldquo;Lifestyles of the Rich and Famous&rdquo;. The interesting thing is that everyone sitting on the couch with a bag of popcorn watching these shows are broke. Why? Because they are famous for sitting back and watching shows on other people being famous. How about we flick the switch in life from famous to focused. Stop dreaming of other people being famous and start being focused on your own destiny! You must laser-focus in on your success and demolish procrastination and create action. Demolish resolutions and create results! Let&rsquo;s demolish fear and create faith! How is this all possible by simply being focused. How do people become gold medalist, super bowl champions, etc? They take charge of their lives, hire a coach and become focused<br /><br /> What are you? Who are you? Where are you going? How are you going to get there? More importantly, why are you not focused? Those that know their &ldquo;Why&rdquo; are very focused. I am focused on my Why of changing lives of Champions worldwide through my coaching, mentoring, seminars and success library and creating massive success in those Champions lives! Do not allow famous to get in your way of being focused. Make a commitment today to forget fame and become laser-focused! Let&rsquo;s get focused and know your Why! Let&rsquo;s go out and impact the world one heart at a time. Focus creates success and Fame creates problems. It&rsquo;s your decision&hellip;become focused and live your dream!!! Find Your Why &amp; Fly</p>',	'06.jpg',	'JOHNET LEO',	'2018-01-04 00:22:50'),
(7,	'Eros & Psychi',	'Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in',	'<p class=\"mt-20\">ARE YOU FAMOUS OR FOCUSED? Are you famous or are you focused? Let&rsquo;s start with &ldquo;famous&rdquo;. When people look at you, what do they say that you are famous for? Are you famous for procrastination&hellip;negative words&hellip;poor time management&hellip;What are you famous for? Let me share with you what I am famous for. I am famous for consistent, structured, focused daily strategies and techniques to live a victorious life. I know&hellip;that is a pretty strong statement! You see, I would rather be &ldquo;focused&rdquo; than famous. I am focused on success. I am focused on assisting you in achieving you dream and discover the greatness that is inside all of you! Forget about being famous, let&rsquo;s be focused. Be focused on your dreams&hellip;be focused on providing for your family&hellip;be focused on building a huge team&hellip;be focused on Finding Your Why! Let&rsquo;s commit today to become focused and start changing people&rsquo;s lives<br /><br /> Millions of people world wide watch shows like &ldquo;Who wants to be a Millionaire&rdquo; or &ldquo;Lifestyles of the Rich and Famous&rdquo;. The interesting thing is that everyone sitting on the couch with a bag of popcorn watching these shows are broke. Why? Because they are famous for sitting back and watching shows on other people being famous. How about we flick the switch in life from famous to focused. Stop dreaming of other people being famous and start being focused on your own destiny! You must laser-focus in on your success and demolish procrastination and create action. Demolish resolutions and create results! Let&rsquo;s demolish fear and create faith! How is this all possible by simply being focused. How do people become gold medalist, super bowl champions, etc? They take charge of their lives, hire a coach and become focused<br /><br /> What are you? Who are you? Where are you going? How are you going to get there? More importantly, why are you not focused? Those that know their &ldquo;Why&rdquo; are very focused. I am focused on my Why of changing lives of Champions worldwide through my coaching, mentoring, seminars and success library and creating massive success in those Champions lives! Do not allow famous to get in your way of being focused. Make a commitment today to forget fame and become laser-focused! Let&rsquo;s get focused and know your Why! Let&rsquo;s go out and impact the world one heart at a time. Focus creates success and Fame creates problems. It&rsquo;s your decision&hellip;become focused and live your dream!!! Find Your Why &amp; Fly</p>',	'07.jpg',	'JOHNET LEO',	'2018-01-04 00:22:50'),
(8,	'Hector & Andromache',	'Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in',	'<p class=\"mt-20\">ARE YOU FAMOUS OR FOCUSED? Are you famous or are you focused? Let&rsquo;s start with &ldquo;famous&rdquo;. When people look at you, what do they say that you are famous for? Are you famous for procrastination&hellip;negative words&hellip;poor time management&hellip;What are you famous for? Let me share with you what I am famous for. I am famous for consistent, structured, focused daily strategies and techniques to live a victorious life. I know&hellip;that is a pretty strong statement! You see, I would rather be &ldquo;focused&rdquo; than famous. I am focused on success. I am focused on assisting you in achieving you dream and discover the greatness that is inside all of you! Forget about being famous, let&rsquo;s be focused. Be focused on your dreams&hellip;be focused on providing for your family&hellip;be focused on building a huge team&hellip;be focused on Finding Your Why! Let&rsquo;s commit today to become focused and start changing people&rsquo;s lives<br /><br /> Millions of people world wide watch shows like &ldquo;Who wants to be a Millionaire&rdquo; or &ldquo;Lifestyles of the Rich and Famous&rdquo;. The interesting thing is that everyone sitting on the couch with a bag of popcorn watching these shows are broke. Why? Because they are famous for sitting back and watching shows on other people being famous. How about we flick the switch in life from famous to focused. Stop dreaming of other people being famous and start being focused on your own destiny! You must laser-focus in on your success and demolish procrastination and create action. Demolish resolutions and create results! Let&rsquo;s demolish fear and create faith! How is this all possible by simply being focused. How do people become gold medalist, super bowl champions, etc? They take charge of their lives, hire a coach and become focused<br /><br /> What are you? Who are you? Where are you going? How are you going to get there? More importantly, why are you not focused? Those that know their &ldquo;Why&rdquo; are very focused. I am focused on my Why of changing lives of Champions worldwide through my coaching, mentoring, seminars and success library and creating massive success in those Champions lives! Do not allow famous to get in your way of being focused. Make a commitment today to forget fame and become laser-focused! Let&rsquo;s get focused and know your Why! Let&rsquo;s go out and impact the world one heart at a time. Focus creates success and Fame creates problems. It&rsquo;s your decision&hellip;become focused and live your dream!!! Find Your Why &amp; Fly</p>',	'08.jpg',	'JOHNET LEO',	'2018-01-04 00:22:50'),
(9,	'Bonnie & Clyde',	'Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in',	'<p class=\"mt-20\">ARE YOU FAMOUS OR FOCUSED? Are you famous or are you focused? Let&rsquo;s start with &ldquo;famous&rdquo;. When people look at you, what do they say that you are famous for? Are you famous for procrastination&hellip;negative words&hellip;poor time management&hellip;What are you famous for? Let me share with you what I am famous for. I am famous for consistent, structured, focused daily strategies and techniques to live a victorious life. I know&hellip;that is a pretty strong statement! You see, I would rather be &ldquo;focused&rdquo; than famous. I am focused on success. I am focused on assisting you in achieving you dream and discover the greatness that is inside all of you! Forget about being famous, let&rsquo;s be focused. Be focused on your dreams&hellip;be focused on providing for your family&hellip;be focused on building a huge team&hellip;be focused on Finding Your Why! Let&rsquo;s commit today to become focused and start changing people&rsquo;s lives<br /><br /> Millions of people world wide watch shows like &ldquo;Who wants to be a Millionaire&rdquo; or &ldquo;Lifestyles of the Rich and Famous&rdquo;. The interesting thing is that everyone sitting on the couch with a bag of popcorn watching these shows are broke. Why? Because they are famous for sitting back and watching shows on other people being famous. How about we flick the switch in life from famous to focused. Stop dreaming of other people being famous and start being focused on your own destiny! You must laser-focus in on your success and demolish procrastination and create action. Demolish resolutions and create results! Let&rsquo;s demolish fear and create faith! How is this all possible by simply being focused. How do people become gold medalist, super bowl champions, etc? They take charge of their lives, hire a coach and become focused<br /><br /> What are you? Who are you? Where are you going? How are you going to get there? More importantly, why are you not focused? Those that know their &ldquo;Why&rdquo; are very focused. I am focused on my Why of changing lives of Champions worldwide through my coaching, mentoring, seminars and success library and creating massive success in those Champions lives! Do not allow famous to get in your way of being focused. Make a commitment today to forget fame and become laser-focused! Let&rsquo;s get focused and know your Why! Let&rsquo;s go out and impact the world one heart at a time. Focus creates success and Fame creates problems. It&rsquo;s your decision&hellip;become focused and live your dream!!! Find Your Why &amp; Fly</p>',	'09.jpg',	'JOHNET LEO',	'2018-01-04 00:22:50'),
(10,	'Casanova & Francesca',	'Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in',	'<p class=\"mt-20\">ARE YOU FAMOUS OR FOCUSED? Are you famous or are you focused? Let&rsquo;s start with &ldquo;famous&rdquo;. When people look at you, what do they say that you are famous for? Are you famous for procrastination&hellip;negative words&hellip;poor time management&hellip;What are you famous for? Let me share with you what I am famous for. I am famous for consistent, structured, focused daily strategies and techniques to live a victorious life. I know&hellip;that is a pretty strong statement! You see, I would rather be &ldquo;focused&rdquo; than famous. I am focused on success. I am focused on assisting you in achieving you dream and discover the greatness that is inside all of you! Forget about being famous, let&rsquo;s be focused. Be focused on your dreams&hellip;be focused on providing for your family&hellip;be focused on building a huge team&hellip;be focused on Finding Your Why! Let&rsquo;s commit today to become focused and start changing people&rsquo;s lives<br /><br /> Millions of people world wide watch shows like &ldquo;Who wants to be a Millionaire&rdquo; or &ldquo;Lifestyles of the Rich and Famous&rdquo;. The interesting thing is that everyone sitting on the couch with a bag of popcorn watching these shows are broke. Why? Because they are famous for sitting back and watching shows on other people being famous. How about we flick the switch in life from famous to focused. Stop dreaming of other people being famous and start being focused on your own destiny! You must laser-focus in on your success and demolish procrastination and create action. Demolish resolutions and create results! Let&rsquo;s demolish fear and create faith! How is this all possible by simply being focused. How do people become gold medalist, super bowl champions, etc? They take charge of their lives, hire a coach and become focused<br /><br /> What are you? Who are you? Where are you going? How are you going to get there? More importantly, why are you not focused? Those that know their &ldquo;Why&rdquo; are very focused. I am focused on my Why of changing lives of Champions worldwide through my coaching, mentoring, seminars and success library and creating massive success in those Champions lives! Do not allow famous to get in your way of being focused. Make a commitment today to forget fame and become laser-focused! Let&rsquo;s get focused and know your Why! Let&rsquo;s go out and impact the world one heart at a time. Focus creates success and Fame creates problems. It&rsquo;s your decision&hellip;become focused and live your dream!!! Find Your Why &amp; Fly</p>',	'10.jpg',	'JOHNET LEO',	'2018-01-04 00:22:50'),
(11,	'Jack & Sally',	'Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in',	'<p class=\"mt-20\">ARE YOU FAMOUS OR FOCUSED? Are you famous or are you focused? Let&rsquo;s start with &ldquo;famous&rdquo;. When people look at you, what do they say that you are famous for? Are you famous for procrastination&hellip;negative words&hellip;poor time management&hellip;What are you famous for? Let me share with you what I am famous for. I am famous for consistent, structured, focused daily strategies and techniques to live a victorious life. I know&hellip;that is a pretty strong statement! You see, I would rather be &ldquo;focused&rdquo; than famous. I am focused on success. I am focused on assisting you in achieving you dream and discover the greatness that is inside all of you! Forget about being famous, let&rsquo;s be focused. Be focused on your dreams&hellip;be focused on providing for your family&hellip;be focused on building a huge team&hellip;be focused on Finding Your Why! Let&rsquo;s commit today to become focused and start changing people&rsquo;s lives<br /><br /> Millions of people world wide watch shows like &ldquo;Who wants to be a Millionaire&rdquo; or &ldquo;Lifestyles of the Rich and Famous&rdquo;. The interesting thing is that everyone sitting on the couch with a bag of popcorn watching these shows are broke. Why? Because they are famous for sitting back and watching shows on other people being famous. How about we flick the switch in life from famous to focused. Stop dreaming of other people being famous and start being focused on your own destiny! You must laser-focus in on your success and demolish procrastination and create action. Demolish resolutions and create results! Let&rsquo;s demolish fear and create faith! How is this all possible by simply being focused. How do people become gold medalist, super bowl champions, etc? They take charge of their lives, hire a coach and become focused<br /><br /> What are you? Who are you? Where are you going? How are you going to get there? More importantly, why are you not focused? Those that know their &ldquo;Why&rdquo; are very focused. I am focused on my Why of changing lives of Champions worldwide through my coaching, mentoring, seminars and success library and creating massive success in those Champions lives! Do not allow famous to get in your way of being focused. Make a commitment today to forget fame and become laser-focused! Let&rsquo;s get focused and know your Why! Let&rsquo;s go out and impact the world one heart at a time. Focus creates success and Fame creates problems. It&rsquo;s your decision&hellip;become focused and live your dream!!! Find Your Why &amp; Fly</p>',	'11.jpg',	'JOHNET LEO',	'2018-01-04 00:22:50'),
(12,	'James & Lilly',	'Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in',	'<p class=\"mt-20\">ARE YOU FAMOUS OR FOCUSED? Are you famous or are you focused? Let&rsquo;s start with &ldquo;famous&rdquo;. When people look at you, what do they say that you are famous for? Are you famous for procrastination&hellip;negative words&hellip;poor time management&hellip;What are you famous for? Let me share with you what I am famous for. I am famous for consistent, structured, focused daily strategies and techniques to live a victorious life. I know&hellip;that is a pretty strong statement! You see, I would rather be &ldquo;focused&rdquo; than famous. I am focused on success. I am focused on assisting you in achieving you dream and discover the greatness that is inside all of you! Forget about being famous, let&rsquo;s be focused. Be focused on your dreams&hellip;be focused on providing for your family&hellip;be focused on building a huge team&hellip;be focused on Finding Your Why! Let&rsquo;s commit today to become focused and start changing people&rsquo;s lives<br /><br /> Millions of people world wide watch shows like &ldquo;Who wants to be a Millionaire&rdquo; or &ldquo;Lifestyles of the Rich and Famous&rdquo;. The interesting thing is that everyone sitting on the couch with a bag of popcorn watching these shows are broke. Why? Because they are famous for sitting back and watching shows on other people being famous. How about we flick the switch in life from famous to focused. Stop dreaming of other people being famous and start being focused on your own destiny! You must laser-focus in on your success and demolish procrastination and create action. Demolish resolutions and create results! Let&rsquo;s demolish fear and create faith! How is this all possible by simply being focused. How do people become gold medalist, super bowl champions, etc? They take charge of their lives, hire a coach and become focused<br /><br /> What are you? Who are you? Where are you going? How are you going to get there? More importantly, why are you not focused? Those that know their &ldquo;Why&rdquo; are very focused. I am focused on my Why of changing lives of Champions worldwide through my coaching, mentoring, seminars and success library and creating massive success in those Champions lives! Do not allow famous to get in your way of being focused. Make a commitment today to forget fame and become laser-focused! Let&rsquo;s get focused and know your Why! Let&rsquo;s go out and impact the world one heart at a time. Focus creates success and Fame creates problems. It&rsquo;s your decision&hellip;become focused and live your dream!!! Find Your Why &amp; Fly</p>',	'12.jpg',	'JOHNET LEO',	'2018-01-04 00:22:50');

DROP TABLE IF EXISTS `testimonials`;
CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` text COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin,
  `author_image` text COLLATE utf8_bin,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `testimonials` (`id`, `author`, `description`, `author_image`) VALUES
(1,	'Jack Thompson - <span>Usa</span>',	'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.',	'thum-3.jpg'),
(2,	'Miss Jorina Akter - <span>Iraq</span>',	'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.',	'thum-2.jpg\r\n'),
(3,	'Adam Cooper - <span> New york</span>',	'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.',	'thum-1.jpg');

DROP TABLE IF EXISTS `usergallery`;
CREATE TABLE `usergallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `img_key` varchar(20) NOT NULL,
  `imgposition` int(11) NOT NULL DEFAULT '0',
  `img_url` varchar(250) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `usergallery` (`id`, `user_id`, `img_key`, `imgposition`, `img_url`, `created_date`, `modified_date`) VALUES
(1,	751,	'img2',	2,	'20180509174448.png',	'2018-05-09 17:44:48',	'2018-05-09 17:44:48'),
(2,	752,	'img2',	2,	'20180509174852.png',	'2018-05-09 17:48:52',	'2018-05-09 17:48:52'),
(3,	752,	'img3',	3,	'',	'2018-05-09 17:49:02',	'2018-05-11 12:33:32'),
(4,	753,	'img2',	2,	'20180509175202.png',	'2018-05-09 17:52:02',	'2018-05-09 17:52:02'),
(5,	754,	'img2',	2,	'20180509175405.png',	'2018-05-09 17:54:05',	'2018-05-09 17:54:05'),
(6,	755,	'img2',	2,	'20180509175927.png',	'2018-05-09 17:59:27',	'2018-05-09 17:59:27'),
(7,	755,	'img3',	3,	'',	'2018-05-09 17:59:35',	'2018-05-11 12:33:32'),
(8,	756,	'img2',	2,	'20180509180205.png',	'2018-05-09 18:02:05',	'2018-05-09 18:02:05'),
(9,	747,	'img2',	2,	'20180509182052.png',	'2018-05-09 18:20:52',	'2018-05-09 18:20:52'),
(10,	747,	'img3',	3,	'',	'2018-05-09 18:21:01',	'2018-05-11 12:33:32'),
(11,	747,	'img4',	4,	'20180509182112.png',	'2018-05-09 18:21:12',	'2018-05-09 18:21:12'),
(12,	748,	'img2',	2,	'20180509182202.png',	'2018-05-09 18:22:02',	'2018-05-09 18:22:02'),
(13,	749,	'img2',	2,	'20180509182256.png',	'2018-05-09 18:22:56',	'2018-05-09 18:22:56'),
(14,	749,	'img3',	3,	'',	'2018-05-09 18:23:08',	'2018-05-11 12:33:32'),
(15,	750,	'img2',	2,	'20180509182352.png',	'2018-05-09 18:23:52',	'2018-05-09 18:23:52'),
(16,	757,	'img2',	0,	'1525935849376.jpg',	'2018-05-10 12:33:32',	'2018-05-10 12:33:32'),
(18,	758,	'img3',	3,	'',	'2018-05-10 15:01:37',	'2018-05-11 12:33:32'),
(19,	760,	'img2',	2,	'20180511104723.png',	'2018-05-11 10:47:24',	'2018-05-11 10:47:24'),
(20,	760,	'img3',	3,	'',	'2018-05-11 10:47:43',	'2018-05-11 12:33:32'),
(22,	762,	'img2',	2,	'20180511111806.png',	'2018-05-11 11:18:06',	'2018-05-11 11:18:06'),
(23,	762,	'img3',	3,	'20180511123346.png',	'2018-05-11 11:18:22',	'2018-05-11 12:33:46'),
(24,	762,	'img4',	4,	'20180511123359.png',	'2018-05-11 12:33:59',	'2018-05-11 12:33:59'),
(25,	763,	'img2',	0,	'1526100695675.jpg',	'2018-05-12 10:21:37',	'2018-05-12 10:21:37'),
(26,	763,	'img3',	0,	'1526100705939.jpg',	'2018-05-12 10:21:47',	'2018-05-12 10:21:47'),
(27,	764,	'img4',	0,	'IMG-20150628-WA0004.jpeg',	'2018-05-15 10:24:34',	'2018-05-15 10:24:34'),
(28,	764,	'img3',	0,	'IMG-20160629-WA0003~2.jpg',	'2018-05-15 10:24:34',	'2018-05-15 10:24:34'),
(29,	764,	'img6',	0,	'20180507_162436.jpg',	'2018-05-15 10:24:34',	'2018-05-15 10:24:34'),
(30,	764,	'img5',	0,	'IMG-20150827-WA0000.jpg',	'2018-05-15 10:24:34',	'2018-05-15 10:24:34'),
(31,	764,	'img2',	0,	'20180307_223036.jpg',	'2018-05-15 10:24:34',	'2018-05-15 10:24:34'),
(32,	763,	'img4',	0,	'Xxw5hrYrHYimg4.jpg',	'2018-05-29 12:07:24',	'2018-05-29 12:07:24');

DROP TABLE IF EXISTS `userlogin`;
CREATE TABLE `userlogin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `device` tinytext NOT NULL,
  `AuthToken` text NOT NULL,
  `device_token` text,
  `login_time` time NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `userlogin_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(250) NOT NULL,
  `profile_image` varchar(250) DEFAULT NULL,
  `fb_id` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `dob` date NOT NULL DEFAULT '1970-01-01',
  `about` text,
  `gender` enum('male','female') NOT NULL DEFAULT 'male',
  `education` varchar(100) NOT NULL DEFAULT '-',
  `profession` varchar(250) NOT NULL DEFAULT '-',
  `religion` varchar(250) NOT NULL DEFAULT '0',
  `height` varchar(20) NOT NULL DEFAULT '5''0 (152 cm)',
  `kids` enum('1','2','3','4','5','6','7','None','One Day','I Don''t Want Kids') NOT NULL DEFAULT 'None',
  `address` text,
  `location_lat` varchar(25) DEFAULT '0',
  `location_long` varchar(25) DEFAULT '0',
  `date_pref` varchar(100) NOT NULL DEFAULT '1,2,3,4',
  `gender_pref` enum('male','female') NOT NULL DEFAULT 'female',
  `max_age_pref` int(3) NOT NULL DEFAULT '60',
  `min_age_pref` int(3) NOT NULL DEFAULT '15',
  `max_dist_pref` varchar(10) NOT NULL DEFAULT '200',
  `min_dist_pref` varchar(10) NOT NULL DEFAULT '0',
  `ethnicity` varchar(100) DEFAULT '0',
  `que_id` int(11) NOT NULL DEFAULT '1',
  `que_ans` text,
  `access_location` enum('0','1') NOT NULL DEFAULT '1',
  `status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0- Active,1-Deactive',
  `is_admin` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `is_deleted` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `is_confirmed` enum('0','1') CHARACTER SET latin1 NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `notificationcounter` int(11) NOT NULL DEFAULT '0',
  `enableAdd` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '1',
  `ejuser` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `pass_token` text CHARACTER SET latin1,
  `pass_code` int(4) DEFAULT NULL,
  `ethnicity_pref` varchar(200) DEFAULT NULL,
  `religion_pref` varchar(200) DEFAULT NULL,
  `sampledata` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `fname`, `lname`, `username`, `email`, `password`, `profile_image`, `fb_id`, `dob`, `about`, `gender`, `education`, `profession`, `religion`, `height`, `kids`, `address`, `location_lat`, `location_long`, `date_pref`, `gender_pref`, `max_age_pref`, `min_age_pref`, `max_dist_pref`, `min_dist_pref`, `ethnicity`, `que_id`, `que_ans`, `access_location`, `status`, `is_admin`, `is_deleted`, `is_confirmed`, `created_date`, `modified_date`, `notificationcounter`, `enableAdd`, `ejuser`, `pass_token`, `pass_code`, `ethnicity_pref`, `religion_pref`, `sampledata`) VALUES
(1,	'Deborah',	'Williams',	'admin',	'admin@yopmail.com',	'$2y$10$jMHSLekxJekQSFL30Z7uWubRqp6DIVteCxlcxn2Oe92iS1IZ7FIUS',	'1_5a704503194c1.png',	NULL,	'1994-12-07',	'she is engineershe is engineer',	'male',	'B.Tech',	'Professor',	'4',	'3\'0 (92 cm)',	'',	'Hatfield Day Nursery',	'21.1925707',	'72.7997356',	'1,2,3,4',	'male',	50,	14,	'120',	'0',	'8',	1,	'ghygycfgvcxb',	'1',	'1',	'1',	'0',	'0',	'2017-03-25 13:16:52',	'2018-01-30 15:42:19',	0,	'1',	'DW1',	'',	NULL,	NULL,	NULL,	'0'),
(747,	'Joseph',	'Jenkins',	NULL,	'joseph.jenkins@yopmail.com',	'$2y$10$jMHSLekxJekQSFL30Z7uWubRqp6DIVteCxlcxn2Oe92iS1IZ7FIUS',	'20180509182044.png',	'',	'1989-04-01',	'Nothing',	'male',	'IMIT',	'Architech',	'4',	'5\'6 (168 cm)',	'None',	'Toronto, ON, Canada',	'21.1702401',	'72.83106070000008',	'2,4,1,3',	'female',	60,	18,	'200',	'1',	'6',	1,	'Nothing',	'1',	'0',	'0',	'0',	'0',	'2018-05-09 17:25:39',	'2018-05-09 18:20:44',	0,	'1',	'JJ747',	NULL,	NULL,	'1,2,5,7,8',	'7,12,21,1,3,5,4',	'1'),
(748,	'Eleanor',	'Wright',	NULL,	'eleanor.wright@yopmail.com',	'$2y$10$TJIplv2b/7bhOvEQF/UjsumVPNGwLuxiVSReVVNaPsP2toeRvp3pi',	'20180509182152.png',	'',	'1991-01-01',	'Nothing',	'male',	'Master of Computer Application',	'Web Developer',	'4',	'5\'4 (163 cm)',	'None',	'Toronto, ON, Canada',	'43.653226',	'-79.3831843',	'1,2,3,4',	'female',	40,	18,	'55',	'1',	'6',	6,	'Twilight saga',	'1',	'0',	'0',	'0',	'0',	'2018-05-09 17:35:00',	'2018-05-09 18:21:52',	0,	'1',	'EW748',	NULL,	NULL,	'1,2,5,7',	'7,12,21',	'1'),
(749,	'Jack',	'Marshall',	NULL,	'jack.marshall@yopmail.com',	'$2y$10$dzXI6jjRB9MByzkPXWqsxugECIS7LOhGUdOYnig.1nqP7QnfCRKfu',	'20180509182243.png',	'',	'1992-01-01',	'Nothing',	'male',	'Architecture Engineering',	'Direactor',	'4',	'5\'10 (178 cm)',	'None',	'Adajan Patiya, Surat, Gujarat, India',	'21.20503816758083',	'72.77079820632935',	'1,2,3,4',	'female',	50,	14,	'120',	'0',	'8',	1,	'Nothing',	'1',	'0',	'0',	'0',	'0',	'2018-05-09 17:38:01',	'2018-05-09 18:22:43',	0,	'1',	'JM749',	NULL,	NULL,	'1,7,8,12',	'1,3,4,5,12',	'1'),
(750,	'Sara',	'Hall',	NULL,	'sara.hall@yopmail.com',	'$2y$10$kdJIXY5e9wlpTbG1twxGGOUuzR5qNbLDMExtj7bZfLcWW5xSdhbAK',	'20180509182344.png',	'',	'1987-01-01',	'Nothing',	'female',	'Master of Computer Application',	'Direactor',	'4',	'5\'5 (165 cm)',	'None',	'Covent Garden, London, UK',	'21.205188',	'72.775795',	'1,2,3,4',	'male',	50,	14,	'120',	'0',	'8',	1,	'Nothing',	'1',	'0',	'0',	'0',	'0',	'2018-05-09 17:40:43',	'2018-05-09 18:23:44',	0,	'0',	'SH750',	NULL,	NULL,	'1,7,8,12',	'1,3,4,5,12',	'1'),
(751,	'Freddie',	'Bell',	NULL,	'freddie.bell@yopmail.com',	'$2y$10$QKCZfGqD/xbg1hJ9QUmJseC2b4t8QpycHV1jWqxCI45D.60hhadKC',	'20180509174438.png',	'',	'1988-09-04',	'Nothing',	'male',	'MIT',	'Actor',	'4',	'6\'0 (183 cm)',	'None',	'Adajan, Surat, Gujarat, India',	'21.1925707',	'72.7997356',	'1,2,3,4',	'female',	50,	14,	'120',	'0',	'8',	1,	'Nothing',	'1',	'0',	'0',	'0',	'0',	'2018-05-09 17:44:21',	'2018-05-09 17:44:38',	0,	'1',	'FB751',	NULL,	NULL,	'1,7,8,12',	'1,3,4,5,12',	'1'),
(752,	'Heather',	'Clarke',	NULL,	'heather.clarke@yopmail.com',	'$2y$10$gibMoN3GXStRvBVq2Iv3Eu/DL9fPsT4L6NopCMjTquCRjevsuwAZO',	'20180509182800.png',	'',	'1994-06-09',	'Nothing',	'female',	'VNSGU',	'Designer',	'4',	'5\'9 (175 cm)',	'None',	'Covent Garden, London, UK',	'0.000',	'0.000',	'1,2,3,4',	'male',	50,	14,	'120',	'0',	'8',	1,	'Nothing',	'1',	'0',	'0',	'0',	'0',	'2018-05-09 17:48:23',	'2018-05-09 18:28:00',	0,	'1',	'HC752',	NULL,	NULL,	'1,7,8,12',	'1,3,4,5,12',	'1'),
(753,	'Tom',	'Ross',	NULL,	'tom.ross@yopmail.com',	'$2y$10$X0Fqmrp70zHi4W5sUGu2hO236vPpq4gGLCZLKxPK5j/w2PnqqwXqm',	'20180509175151.png',	'',	'1984-10-08',	'Nothing',	'male',	'Punjab University',	'Accountant',	'4',	'5\'0 (152 cm)',	'None',	'Adajan, Surat, Gujarat, India',	'21.1925707',	'72.7997356',	'1,2,3,4',	'female',	50,	14,	'120',	'0',	'8',	1,	'Nothing',	'1',	'0',	'0',	'0',	'0',	'2018-05-09 17:51:37',	'2018-05-09 17:51:51',	0,	'1',	'TR753',	NULL,	NULL,	'1,7,8,12',	'1,3,4,5,12',	'1'),
(754,	'Monica',	'Cooper',	NULL,	'monica.cooper@yopmail.com',	'$2y$10$UjGq/BhFdgGJiGg1xvbFqOztAulTN1WJ5aVI6vEiZUWdNyBev7AUG',	'20180509175355.png',	'',	'1983-09-27',	'Nothing',	'female',	'Master of Computer Application',	'Web Developer',	'4',	'5\'0 (152 cm)',	'None',	'Covent Garden, London, UK',	'21.1925707',	'72.7997356',	'1,2,3,4',	'male',	50,	14,	'120',	'0',	'8',	1,	'Nothing',	'1',	'0',	'0',	'0',	'0',	'2018-05-09 17:53:42',	'2018-05-09 17:53:56',	0,	'1',	'MC754',	NULL,	NULL,	'1,7,8,12',	'1,3,4,5,12',	'1'),
(755,	'Anthony',	'Wells',	NULL,	'anthony.wells@yopmail.com',	'$2y$10$n3AOQ8Sea/rZoE38/j.IQOkuo6jBNXQktb/i0/OsY3eHkrmVhOCbe',	'20180509175919.png',	'',	'1984-07-15',	'Nothing',	'male',	'Master of Computer Application',	'Direactor',	'4',	'5\'0 (152 cm)',	'None',	'Adajan, Surat, Gujarat, India',	'21.1925707',	'72.7997356',	'1,2,3,4',	'female',	50,	14,	'120',	'0',	'8',	1,	'Nothing',	'1',	'0',	'0',	'0',	'0',	'2018-05-09 17:58:58',	'2018-05-09 17:59:19',	0,	'1',	'AW755',	NULL,	NULL,	'1,7,8,12',	'1,3,4,5,12',	'1'),
(756,	'Fiona',	'Peterson',	NULL,	'fiona.peterson@yopmail.com',	'$2y$10$UiwATemztbZnda6vQ0xd7..N4YPUXTfMSX8IWeeDpKbLVs9hpC3rG',	'20180509180157.png',	'',	'1989-11-17',	'Nothing',	'female',	'IMIT',	'Machanic',	'4',	'5\'0 (152 cm)',	'None',	'Covent Garden, London, UK',	'21.170240099999997',	'72.83106070000001',	'1,2,3,4',	'female',	50,	14,	'120',	'0',	'8',	1,	'Nothing',	'1',	'0',	'0',	'0',	'0',	'2018-05-09 18:01:45',	'2018-05-09 18:01:57',	2,	'1',	'FP756',	NULL,	NULL,	'1,7,8,12',	'1,3,4,5,12',	'1'),
(757,	'Laura',	'Kivett',	NULL,	'LauraKivett@yopmail.com',	'$2y$10$74HMRJEzUx.lWTJosaQPreIewFQN7gNNHWzF0GYegec0SLAN4P5z2',	'1525935838736.jpg',	'',	'1999-09-30',	'Love Travelling',	'female',	'Santa Barbara City College',	'Optimetrist',	'4',	'5\'2 (158 cm)',	'3',	NULL,	'23.022505',	'72.5713621',	'2,1,4,3',	'male',	50,	18,	'176',	'0',	'8',	6,	'Avenger',	'1',	'0',	'0',	'0',	'0',	'2018-05-10 12:31:13',	'2018-05-10 12:33:21',	0,	'1',	'LK757',	NULL,	NULL,	'8',	'4',	'1'),
(758,	'Georgia',	'Walker',	NULL,	'walkergeorgia@yopmail.com',	'$2y$10$pDn5PgZU5I7H9zpg3CIgXOSN7bJ1T2bPpKX3LvhgJSrODyqvRS37C',	'1526031795710.jpg',	'',	'1999-09-30',	'Love travelling',	'female',	'Santa Barbara City College',	'Rediilogic Nurse',	'4',	'5\'5 (165 cm)',	'3',	NULL,	'21.170240',	'72.831061',	'4,1,3,2',	'male',	60,	18,	'200',	'0',	'8',	13,	'Mother',	'1',	'0',	'0',	'0',	'0',	'2018-05-10 13:07:42',	'2018-05-11 15:13:17',	0,	'0',	'GW758',	NULL,	NULL,	'8,4',	'4',	'1'),
(759,	'Paul',	'Raid',	NULL,	'paulraid@yopmail.com',	'$2y$10$Ml4U78NxUJI6diElo/Sq6u74EDUC2y7X9KXjUFnsDnikYMS4rXhaK',	'',	'',	'1998-04-10',	'N c',	'male',	'Valencia College',	'Copy Writer',	'4',	'5\'11 (180 cm)',	'1',	NULL,	'21.1925707',	'72.7997356',	'1,2,3,4',	'female',	60,	18,	'200',	'0',	'8',	3,	'Cute',	'1',	'0',	'0',	'0',	'0',	'2018-05-10 16:56:28',	'2018-05-10 17:19:28',	0,	'1',	'PR759',	NULL,	NULL,	'0',	'0',	'1'),
(761,	'Justin',	'London',	NULL,	'justinlondon@yopmail.com',	'$2y$10$OBXWQ8W8jKwl62w1YBqs4et79HptDO/Ixsz4PIoLxuk0Z/ux4UIlK',	'OIL6D8Mqjaimg1.jpg',	'',	'2000-05-10',	'Humble',	'male',	'Valencia College',	'Copy Writer',	'4',	'5\'4 (163 cm)',	'1',	NULL,	'21.205231',	'72.775771',	'2,4,3,1',	'female',	60,	18,	'200',	'0',	'8',	3,	'So cute',	'1',	'0',	'0',	'0',	'0',	'2018-05-11 11:03:54',	'2018-05-11 16:32:12',	0,	'1',	'JL761',	NULL,	NULL,	'8,1,4,6,12',	'4,7,1,5,8',	'1'),
(762,	'Dominic',	'Torreto',	NULL,	'domtorreto01@yopmail.com',	'$2y$10$3HXlYcIIL67vYbDKb8pmU.fIVN6iwaISwI6JL6eM.TIdWeXGhpl8q',	'20180511111752.png',	'',	'1994-06-22',	'Nothing',	'male',	'NewYork University',	'Actor',	'4',	'6\'0 (183 cm)',	'None',	'Covent Garden, London, UK',	'21.1925707',	'72.7997356',	'3,1,4,2',	'female',	39,	18,	'155',	'0',	'8',	1,	'Nothing',	'1',	'0',	'0',	'0',	'0',	'2018-05-11 11:17:27',	'2018-05-11 11:17:52',	0,	'1',	'DT762',	NULL,	NULL,	'1,7,8,10,12',	'4,5,8,12',	'1'),
(763,	'Joseph',	'Hogan',	NULL,	'Joseph@yopmail.com',	'$2y$10$YvjiCHyzAHAa2DO0CKzLO.NkIHJDpiVfXsz6rEzLQ2mFV4nXl122e',	'1526100685751.jpg',	'',	'1994-04-22',	'I am very cool and honest person',	'male',	'Oxford university ',	'Videographrt',	'4',	'5\'10 (178 cm)',	'None',	NULL,	'21.205',	'72.776',	'4,1,2,3',	'female',	60,	18,	'200',	'0',	'8',	1,	'honesty',	'1',	'0',	'0',	'0',	'0',	'2018-05-12 10:14:21',	'2018-05-12 10:21:27',	0,	'1',	'JH763',	NULL,	NULL,	'8',	'4',	'1'),
(764,	'rosley',	'pineda',	NULL,	'rosley15@hotmail.com',	'$2y$10$prNh48XRuA9YM3fTNff6GOHmkH730ufKzvEg/pfRDLFJc8aWrAH5m',	'20180507_162328.jpg',	'',	'1995-10-15',	'I think about myself as the most amazing dude ever, cause if I dont believe that nobody will make me believe in anything so that\'s a start point',	'male',	'san sebastian',	'laboratory',	'1',	'5\'9 (175 cm)',	'None',	'surat',	'34.40995',	'-118.45437',	'1,2,3,4',	'female',	29,	18,	'36',	'0',	'2',	1,	'well I\'m not really looking for anything, I wanna get surprise with whoever I\'ll chat with, that\'s my point of view. there\'s no fun if you look for someone who you want her/him to be, just enjoy that personality that\'s all',	'1',	'0',	'0',	'0',	'0',	'2018-05-15 10:08:55',	'2018-05-15 10:24:34',	0,	'1',	'rp764',	NULL,	NULL,	NULL,	NULL,	'1');

DROP TABLE IF EXISTS `user_verification`;
CREATE TABLE `user_verification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `verification_code` varchar(12) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_verification_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2018-06-01 08:01:31
